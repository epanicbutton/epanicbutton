﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Main.Master" AutoEventWireup="true" CodeBehind="Events.aspx.cs" Inherits="epanicbutton.Events" %>

<%@ Register Assembly="Trirand.Web" Namespace="Trirand.Web.UI.WebControls" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="header">
        Event Viewer <span style="float: right">
            <asp:LinkButton ID="downloadCSV" runat="server" CssClass="btn btn-primary header-btn" OnClick="downloadCSV_Click">
              <span class="glyphicon glyphicon-download"  ></span>
            </asp:LinkButton></span>
    </div>
    <div id="content">
        <cc1:JQGrid ID="epbGrid" runat="server" Width="1082" Height="414" PagerSettings-PageSize="18" PagerSettings-PageSizeOptions="false">
            <Columns>
                <cc1:JQGridColumn DataField="Id" Editable="false" PrimaryKey="true" Visible="False" Searchable="false" />
                <cc1:JQGridColumn DataField="Description" HeaderText="Description" Editable="false" Width="450" Searchable="true" />
                <cc1:JQGridColumn DataField="AdminName" HeaderText="User" Editable="false" Width="100" Searchable="true" />
                <cc1:JQGridColumn DataField="CreatedOn" HeaderText="Created On" Editable="false" Width="100" Searchable="false" />
            </Columns>
            <SearchDialogSettings MultipleSearch="true" />
            <ToolBarSettings ToolBarPosition="Bottom" ShowEditButton="false" ShowAddButton="false" ShowDeleteButton="false" ShowSearchButton="true" ShowRefreshButton="true" />
            <AppearanceSettings HighlightRowsOnHover="true" />
            <ClientSideEvents AfterEditDialogShown="hideNav" />
        </cc1:JQGrid>
    </div>
</asp:Content>
