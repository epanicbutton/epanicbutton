﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace epanicbutton.ajax
{
    public partial class ButtonUtilities : System.Web.UI.Page
    {

        [WebMethod]
        public static object HKExist(int Client, int Group, int Button, string Key, string Modifier)
        {
            DataClass dc = new DataClass();
            if (!dc.HKExist(Client, Group, Button, Key, Modifier))
            {
                return new { Success = true };
            }
            else
            {
                return new { Success = false };
            }
        }

        //---------------------------------------------------------------------------------------------------------------

        [WebMethod]
        public static object IconExist(int Client, int Group, int Button, string Icon)
        {
            DataClass dc = new DataClass();
            if (!dc.IconExist(Client, Group, Button, Icon))
            {
                return new { Success = true };
            }
            else
            {
                return new { Success = false };
            }
        }
    }
}