﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace epanicbutton.ajax
{
    public partial class AdminUtilities : System.Web.UI.Page
    {
        [WebMethod]
        public static object AdminExist(string AdminName)
        {
            DataClass dc = new DataClass();
            if (!dc.AdminExist(AdminName))
            {
                return new { Success = true };
            }
            else
            {
                return new { Success = false };
            }
        }

        [WebMethod]
        public static object ClientExist(string ClientName)
        {
            DataClass dc = new DataClass();
            if (!dc.ClientExist(ClientName))
            {
                return new { Success = true };
            }
            else
            {
                return new { Success = false };
            }
        }

       [System.Web.Services.WebMethod(EnableSession = true)]
        public static object DeleteUsers(string[] UserIds)
        {
            int iClient = Convert.ToInt32(HttpContext.Current.Session["clientid"].ToString());   

            DataClass dc = new DataClass();
            if (dc.DeleteSelectedUsers(iClient, UserIds))
            {
                return new { Success = true };
            }
            else
            {
                return new { Success = false };
            }

        }

        [System.Web.Services.WebMethod(EnableSession = true)]
        public static object DeleteEmails(string[] Ids)
        {   
            int  iClient = Convert.ToInt32(HttpContext.Current.Session["clientid"].ToString());               

            DataClass dc = new DataClass();
            if (dc.DeleteSelectedEmails(iClient, Ids))
            {
                return new { Success = true };
            }
            else
            {
                return new { Success = false };
            }

        }

        [System.Web.Services.WebMethod(EnableSession = true)]
        public static object DeleteSMS(string[] Ids)
        {
            int iClient = Convert.ToInt32(HttpContext.Current.Session["clientid"].ToString());   

            DataClass dc = new DataClass();
            if (dc.DeleteSelectedSMS(iClient, Ids))
            {
                return new { Success = true };
            }
            else
            {
                return new { Success = false };
            }

        }
    }
}