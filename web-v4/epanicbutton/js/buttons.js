﻿var ddData = [{
    text: "Green Circle",
    value: 13,
    selected: true,
    description: "Low Level Alert",
    imageSrc: "images/13.png"
},
{
    text: "Blue Circle",
    value: 14,
    selected: false,
    description: "Medium Level Alert",
    imageSrc: "images/14.png"
},
{
    text: "Yellow Circle",
    value: 14,
    selected: false,
    description: "Medium Level Alert",
    imageSrc: "images/12.png"
},
{
    text: "Red Circle",
    value: 15,
    selected: false,
    description: "High Level Alert",
    imageSrc: "images/15.png"
},
{
    text: "Green Arrow",
    value: 13,
    selected: true,
    description: "Low Level Alert",
    imageSrc: "images/18.png"
},
{
    text: "Blue Arrow",
    value: 14,
    selected: false,
    description: "Medium Level Alert",
    imageSrc: "images/19.png"
},
{
    text: "Yellow Arrow",
    value: 14,
    selected: false,
    description: "Medium Level Alert",
    imageSrc: "images/17.png"
},
{
    text: "Red Arrow",
    value: 15,
    selected: false,
    description: "High Level Alert",
    imageSrc: "images/20.png"
}

];
