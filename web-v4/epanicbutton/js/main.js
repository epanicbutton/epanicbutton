﻿$(document).ready(function () {
    // setup the session dialog
    $("#dialog").dialog({
        autoOpen: false,
        modal: true,
        //width: 400,
        //height: 200,
        closeOnEscape: false,
        draggable: false,
        resizable: false,
        buttons: {
            'Yes, keep working': function () {
                $(this).dialog('close');
            },
            'No, logoff': function () {
                // fire whatever the configured onTimeout callback is.
                // using .call(this) keeps the default behavior of "this" being the warning
                // element (the dialog in this case) inside the callback.
                $.idleTimeout.options.onClick.call(this);
            }
        }
    });

    // cache a reference to the countdown element so we don't have to query the DOM for it on each ping.
    var $countdown = $("#dialog-countdown");

    // start the idle timer plugin
    $.idleTimeout('#dialog', 'div.ui-dialog-buttonpane button:first', {
        //idleAfter: 2,  
        //pollingInterval: 2,
        keepAliveURL: 'keepalive.html',
        serverResponseEquals: 'OK',
        onClick: function () {
            window.location = "Logout.aspx";
        },
        onTimeout: function () {
            window.location = "LogoutAuto.aspx";
        },
        onIdle: function () {
            $(this).dialog("open");
        },
        onCountdown: function (counter) {
            $countdown.html(counter); // update the counter
        }
    });
});

function displayPwd() {
    $("#pwdDialog").dialog({
        modal: true,
        width: 400,
        height: 280,
        closeOnEscape: true,
        draggable: false,
        resizable: false,
        buttons: {
            'OK': function () {
                $(this).dialog('close');
            }
        }
    });
}

function createDefault(value, editOptions) {
    if ((value == "") || (value == "No")) {
        var span = $("<span />");
        var label = $("<span />", { html: "Yes&nbsp;" });
        var radio = $("<input>", { type: "radio", value: "1", name: "defaultGroup", id: "Yes", checked: false });
        var label1 = $("<span />", { html: "&nbsp;&nbsp;&nbsp;&nbsp;No&nbsp;" });
        var radio1 = $("<input>", { type: "radio", value: "0", name: "defaultGroup", id: "No", checked: true });

        span.append(label).append(radio).append(label1).append(radio1);
    }
    if (value == "Yes") {

        var span = $("<span />");
        var label = $("<span />", { html: "Yes" });
        var radio = $("<input>", { type: "radio", value: "1", name: "default", id: "Yes", checked: (value == "Yes") });

        span.append(label).append(radio);
    }

    return span;
}

function getDefault(elem, oper, value) {
    if (oper === "set") {
        var radioButton = $(elem).find("input:radio[value='" + value + "']");
        if (radioButton.length > 0) {
            radioButton.prop("checked", true);
        }
    }

    if (oper === "get") {
        return $(elem).find("input:radio:checked").val();
    }
}

function createActive(value, editOptions) {
    var span = $("<span />");
    var label = $("<span />", { html: "Yes&nbsp;" });
    var radio = $("<input>", { type: "radio", value: "1", name: "defaultGroup", id: "Yes", checked: (value != "False") });
    var label1 = $("<span />", { html: "&nbsp;&nbsp;&nbsp;&nbsp;No&nbsp;" });
    var radio1 = $("<input>", { type: "radio", value: "0", name: "defaultGroup", id: "No", checked: value == "False" });

    span.append(label).append(radio).append(label1).append(radio1);

    return span;
}

function getActive(elem, oper, value) {
    if (oper === "set") {
        var radioButton = $(elem).find("input:radio[value='" + value + "']");
        if (radioButton.length > 0) {
            radioButton.prop("checked", true);
        }
    }

    if (oper === "get") {
        return $(elem).find("input:radio:checked").val();
    }
}

function createGrpMsg(value, editOptions) {
    if (value == "") {
        value = "False";
    }
    var span = $("<span />");
    var label = $("<span />", { html: "Yes&nbsp;" });
    var radio = $("<input>", { type: "radio", value: "1", name: "defaultGroup", id: "Yes", checked: (value != "False") });
    var label1 = $("<span />", { html: "&nbsp;&nbsp;&nbsp;&nbsp;No&nbsp;" });
    var radio1 = $("<input>", { type: "radio", value: "0", name: "defaultGroup", id: "No", checked: value == "False" });

    span.append(label).append(radio).append(label1).append(radio1);

    return span;
}

function createSev(value, editOptions) {
    var h = "";
    var m = "";
    var l = "";

    if (value == 'High') {
        h = "selected";
    }
    else if (value == 'Medium') {
        m = "selected";
    }
    else if (value == 'Low') {
        l = "selected";
    }
    var sel = "<select class='dropdowns'><option " + h + " value='High'>High</option><option " + m + "  value='Medium'>Medium</option><option " + l + "  value='Low'>Low</option></select>";

    return sel;
}

function getSev(elem, oper, value) {
    if (oper === "set") {
        //var ddl = $(elem).find("input:select[value='" + value + "']");
    }

    if (oper === "get") {
        return $(elem).val();
    }
}

function createMute(value, editOptions) {
    var y = "";
    var n = "";

    if (value == '') {
        n = "selected";
    }

    if (value == 'True') {
        y = "selected";
    }
    else if (value == 'False') {
        n = "selected";
    }
    var sel = "<select class='dropdowns'><option " + y + " value='1'>Yes</option><option " + n + "  value='0'>No</option></select>";

    return sel;
}

function getMute(elem, oper, value) {
    if (oper === "set") {
        //var ddl = $(elem).find("input:select[value='" + value + "']");
    }

    if (oper === "get") {
        return $(elem).val();
    }
}

function createDelay(value, editOptions) {
    var x1 = "0";
    var x2 = "5";
    var x3 = "10";

    if (value == '0') {
        x1 = "selected";
    }
    else if (value == '5') {
        x2 = "selected";
    }
    else if (value == '10') {
        x3 = "selected";
    }
    var sel = "<select class='dropdowns'><option " + x1 + " value='0'>0</option><option " + x2 + "  value='5'>5</option><option " + x3 + "  value='10'>10</option></select>";

    return sel;
}

function getDelay(elem, oper, value) {
    if (oper === "set") {
        //var ddl = $(elem).find("input:select[value='" + value + "']");
    }

    if (oper === "get") {
        return $(elem).val();
    }
}


function createAud(value, editOptions) {
    var h = "";
    var m = "";
    var l = "";

    if (value == 'High') {
        h = "selected";
    }
    else if (value == 'Medium') {
        m = "selected";
    }
    else if (value == 'Low') {
        l = "selected";
    }
    var sel = "<select class='dropdownsred'><option " + h + " value='High'>High</option><option " + m + "  value='Medium'>Medium</option><option " + l + "  value='Low'>Low</option></select>";

    return sel;
}

function getAud(elem, oper, value) {
    if (oper === "set") {
        //var ddl = $(elem).find("input:select[value='" + value + "']");
    }

    if (oper === "get") {
        return $(elem).val();
    }
}

function createImg(value, editOptions) {
    var img = value.substr(value.lastIndexOf("/") + 1);
    img = img.substr(0, img.length - 2);

    var sel = "<select class='dropdowns'>";
    sel += "<option value='15.png'>Red Circle</option>";
    sel += "<option value='12.png'>Yellow Circle</option>";
    sel += "<option value='14.png'>Blue Circle</option>";
    sel += "<option value='13.png'>Green Circle</option>";
    sel += "<option value='20.png'>Red Arrow</option>";
    sel += "<option value='17.png'>Yellow Arrow</option>";
    sel += "<option value='19.png'>Blue Arrow</option>";
    sel += "<option value='18.png'>Green Arrow</option>";
    sel += "<option value='1.png'>Red Exclaimation</option>";
    sel += "<option value='2.png'>Yellow Exclaimation</option>";
    sel += "<option value='3.png'>Green Exclaimation</option>";
    sel += "<option value='8.png'>Red Check</option>";
    sel += "<option value='9.png'>Blue Check</option>";
    sel += "<option value='7.png'>Green Check</option>";
    sel += "<option value='21.png'>Medic Bag</option>";
    sel += "</select>";

    //sel = "<ol id='selectable'>";
    //sel += "<li class='ui-state-default'><img src='/images/15.png'/></li>";
    //sel += "<li class='ui-state-default'><img src='/images/12.png'/></li>";
    //sel += "<li class='ui-state-default'><img src='/images/14.png'/></li>";    
    //sel += "<li class='ui-state-default'><img src='/images/13.png'/></li>";
    //sel += "<li class='ui-state-default'><img src='/images/20.png'/></li>";
    //sel += "<li class='ui-state-default'><img src='/images/17.png'/></li>";
    //sel += "<li class='ui-state-default'><img src='/images/19.png'/></li>";
    //sel += "<li class='ui-state-default'><img src='/images/18.png'/></li>";
    //sel += "</ol>";

    sel = sel.replace(" value='" + img + "'", " selected value='" + img + "'");
    return sel;
}

function getImg(elem, oper, value) {
    if (oper === "set") {
        //var ddl = $(elem).find("input:select[value='" + value + "']");
    }

    if (oper === "get") {
        return $(elem).val();
    }
}

function createResetPwd(value, editOptions) {

    if (value == 'No') {
        value = '0'
    }
    if (value == 'Yes') {
        value = '1'
    }

    var sel = "<select id='key' class='dropdowns'><option value='1'>Yes</option>";
    sel += "<option value='0'>No</option></select>";

    sel = sel.replace(" value='" + value + "'", " selected value='" + value + "'");

    return sel;
}

function getResetPwd(elem, oper, value) {
    if (oper === "set") {
        var radioButton = $(elem).find("input:radio[value='" + value + "']");
        if (radioButton.length > 0) {
            radioButton.prop("checked", true);
        }
    }

    if (oper === "get") {
        return $(elem).val();
    }
}



function createKey(value, editOptions) {
    var sel = "<select id='key'class='dropdowns'><option value='Ctrl+Alt+Shift'>Ctrl+Alt+Shift</option>";
    sel += "<option value='Ctrl+Shift'>Ctrl+Shift</optio>";
    sel += "<option value='Ctrl+Alt'>Ctrl+Alt</option>";
    sel += "<option value='Alt+Shift'>Alt+Shift</option>";
    sel += "<option value='Alt'>Alt</option>";
    sel += "<option value='Shift'>Shift</option>";
    sel += "<option value='Ctrl'>Ctrl</option></select>";

    sel = sel.replace(" value='" + value + "'", " selected value='" + value + "'");

    return sel;
}

function getKey(elem, oper, value) {
    if (oper === "set") {
        var radioButton = $(elem).find("input:radio[value='" + value + "']");
        if (radioButton.length > 0) {
            radioButton.prop("checked", true);
        }
    }

    if (oper === "get") {
        return $(elem).val();
    }
}

function createMod(value, editOptions) {
    var span = "<select id='mod' class='dropdowns'><option value='A'>A</option><option value='B'>B</option><option value='C'>C</option><option value='D'>D</option><option value='E'>E</option><option value='F'>F</option><option value='G'>G</option><option value='H'>H</option><option value='I'>I</option><option value='J'>J</option><option value='K'>K</option><option value='L'>L</option><option value='M'>M</option><option value='N'>N</option><option value='O'>O</option><option value='P'>P</option><option value='Q'>Q</option><option value='R'>R</option><option value='S'>S</option><option value='T'>T</option><option value='U'>U</option><option value='V'>V</option><option value='W'>W</option><option value='X'>X</option><option value='Y'>Y</option><option value='Z'>Z</option><option value='0'>0</option><option value='1'>1</option><option value='2'>2</option><option value='3'>3</option><option value='4'>4</option><option value='5'>5</option><option value='6'>6</option><option value='7'>7</option><option value='8'>8</option><option value='9'>9</option><option value='F2'>F2</option><option value='F3'>F3</option><option value='F4'>F4</option><option value='F5'>F5</option><option value='F6'>F6</option><option value='F7'>F7</option><option value='F8'>F8</option><option value='F9'>F9</option><option value='F10'>F10</option><option value='F11'>F11</option></select>";
    span = span.replace(" value='" + value + "'", " selected value='" + value + "'");
    return span;
}

function getMod(elem, oper, value) {
    if (oper === "set") {
        var radioButton = $(elem).find("input:radio[value='" + value + "']");
        if (radioButton.length > 0) {
            radioButton.prop("checked", true);
        }
    }

    if (oper === "get") {
        return $(elem).val();
    }
}

function displayMsg() {
    $("#msg").css('visibility', 'visible');
    $("#msg").attr('class', 'ui-state-error');
    return false;
}

function displayGroupMsg() {
    $("#msg").css('visibility', 'visible');
    $("#msg").toggleClass('class', 'ui-state-highlight');
    return false;
}

function displayPasswordMsg() {
    $("#pwdMsg").css('visibility', 'visible');
    $("#pwdMsg").toggleClass('class', 'ui-state-highlight');
    return false;
}


function AdminhideNav() {
    $("#pData").css('visibility', 'hidden');
    $("#nData").css('visibility', 'hidden');
    $("#AdminName").prop("disabled", true);
    $("#AdminName").addClass("ui-state-disabled");
}

function accountAdd() {
    $("#InactiveDate").datepicker();
    $("#pData").css('visibility', 'hidden');
    $("#nData").css('visibility', 'hidden');

    var today = new Date();
    var addDays = new Date();
    var days = 60; //parseInt($(this).attr("id").split("_")[1], 10);
    addDays.setDate(today.getDate() + days);
    var newDate = (addDays.getMonth() + 1) + '/' + addDays.getDate() + '/' + addDays.getFullYear();
    $("#InactiveDate").val(newDate);
    $("#TotalConnections").val('100');

}

function accountEdit() {
    $("#InactiveDate").datepicker();
    $("#pData").css('visibility', 'hidden');
    $("#nData").css('visibility', 'hidden');
    $("#AdminName").addClass("ui-state-disabled");
    $("#ClientName").addClass("ui-state-disabled");
    $("#Emails").addClass("readOnly");
    $("#Emails").prop('readonly', true);
    $("#Machines").addClass("readOnly");
    $("#Machines").prop('readonly', true);
    $("#SMS").addClass("readOnly");
    $("#SMS").prop('readonly', true);
}

function hideNavAndAdditional() {
    
    $("#pData").css('visibility', 'hidden');
    $("#nData").css('visibility', 'hidden');
    $("[id='tr_Copy buttons']").show();
}

function hideNav() {
    $("#pData").css('visibility', 'hidden');
    $("#nData").css('visibility', 'hidden');
    $("[id='tr_Copy buttons']").hide();
}

function isEmailValid(value) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (value == '' || !re.test(value)) {
        return [false, 'Please enter a valid email address!'];
    }
    else {
        return [true, ''];
    }
}

function isUserUnique(value) {
    if (value.length > 40)
        return [false, column + ": maximum character length is 40!"];
    else {
        var result, msg;

        msg = "Admin Name must be unique";
        result = false

        msg = "";
        result = true;

        data: JSON.stringify({
            Client: '1',
            type: "POST",
            url: '/ajax/AdminUtilities.aspx/AdminExist',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify({ AdminName: value }),
            async: true,
            success: function (data) {
                if (data.d.Success) {
                    msg = "";
                    result = true;
                }
                else {
                    msg = "Admin Name must be unique";
                    result = false;
                }
            },
            error: function () {
                msg = "An error occurred attempting to validate your request";
                result = false
            }
        });

        return [result, msg];
    }
}

function deleteSelectedUsers(values) {
    var result;
    if (values.length > 0) {
        $.ajax({
            type: "POST",
            url: '/ajax/AdminUtilities.aspx/DeleteUsers',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify({ UserIds: values }),
            async: true,
            success: function (data) {
                if (data.d.Success) {
                    result = true;
                }
                else {
                    result = false;
                }
            },
            error: function () {
                result = false
            }
        });
    }
}

function deleteSelectedEmails(values) {
    var result;
    if (values.length > 0) {
        $.ajax({
            type: "POST",
            url: '/ajax/AdminUtilities.aspx/DeleteEmails',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify({ Ids: values }),
            async: true,
            success: function (data) {
                if (data.d.Success) {
                    result = true;
                }
                else {
                    result = false;
                }
            },
            error: function () {
                result = false
            }
        });
    }
}

function deleteSelectedSMS(values) {
    var result;
    if (values.length > 0) {
        $.ajax({
            type: "POST",
            url: '/ajax/AdminUtilities.aspx/DeleteSMS',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify({ Ids: values }),
            async: true,
            success: function (data) {
                if (data.d.Success) {
                    result = true;
                }
                else {
                    result = false;
                }
            },
            error: function () {
                result = false
            }
        });
    }
}