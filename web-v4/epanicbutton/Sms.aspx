﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Main.Master" AutoEventWireup="true" CodeBehind="Sms.aspx.cs" Inherits="epanicbutton.Sms" %>

<%@ Register Assembly="Trirand.Web" Namespace="Trirand.Web.UI.WebControls" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div id="header">
        <asp:Label ID="lblReceiverHeader" runat="server" Text="Label"></asp:Label>
    </div>

    <div id="msg" class="ui-state-highlight">
        <asp:Label ID="lblMsg" runat="server" Text="Label"></asp:Label>
    </div>

    <div id="lhdr">
        <asp:Label ID="lblRecipients" runat="server" Text="Current Recipients"></asp:Label>
    </div>
    <div id="lcontent">
        <cc1:JQGrid ID="epbGrid" runat="server" Width="491" Height="385" PagerSettings-PageSize="18"
            MultiSelect="true" OnRowAdding="epbGrid_RowAdding" OnRowDeleting="epbGrid_RowDeleting" OnRowEditing="epbGrid_RowEditing">
            <Columns>
                <cc1:JQGridColumn DataField="id" Editable="false" PrimaryKey="true" Visible="False" Searchable="false" />
                <cc1:JQGridColumn DataField="Number" HeaderText="Recipient Mobile Number" Editable="true" Visible="true" Searchable="true" />
                <cc1:JQGridColumn DataField="ContactName" HeaderText="Recipient Name" Editable="true" Visible="true" Searchable="true" />
            </Columns>
            <ToolBarSettings ToolBarPosition="Bottom" ShowSearchButton="true" ShowRefreshButton="true" ShowDeleteButton="false" ShowAddButton="true" ShowEditButton="true" >
                <CustomButtons>
                    <cc1:JQGridToolBarButton
                        Text=""
                        ToolTip="Delete Text Entries"
                        ButtonIcon="ui-icon-trash"
                        Position="Last"
                        OnClick="delTxts" />
                </CustomButtons>
                </ToolBarSettings>
            <PagerSettings PageSize="18" PageSizeOptions="[18,20,50,100]" />
            <AppearanceSettings HighlightRowsOnHover="true" />
<%--            <ClientSideEvents RowSelect="crSelected" LoadComplete="crRestore" />--%>

            <ClientSideEvents AfterEditDialogShown="hideNav"  AfterAddDialogShown="hideNav" />

            <EditDialogSettings CloseAfterEditing="true" Caption="Edit Number" Modal="true" Resizable="false" Width="400"/>
            <AddDialogSettings CloseAfterAdding="true" Caption="Add Number" Modal="true" Resizable="false" Width="400" />

            <SearchDialogSettings MultipleSearch="true" />
        </cc1:JQGrid>
    </div>
    

    <div id="mhdr"></div>
    <div id="mcontent">
          <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <asp:Button ID="btnAddSMS" runat="server" Text="<< Add " Width="85" CssClass="ui-button recipients-btn"  OnClick="btnAddSMS_Click"  /><br />
        <br />  
    </div>

    <div id="rhdr">Batch Upload</div>
    <div id="rcontent">
        <div id="uploadInstruct"><p id="uploadInsText"><p>Enter list of the numbers you wish to add.</p><p>You may use a comma, semi-colon to separate the numbers.  <br />Be sure to include the country code if outside the USA.</p><strong>Example:<br />13361234567,13361234578,13361234598</strong></p></div>
        <div id="uploadPanel">
            <asp:TextBox ID="tbNumbers" runat="server" CssClass="form-control form-control-multiline" Columns="50" Rows="15" TextMode="MultiLine"></asp:TextBox>
        </div>
    </div>
        <script type="text/javascript">
            var selectedRows = [];
            var grid = $("#<%= epbGrid.ClientID %>");
            function delTxts() {
                var selectedRowIDs = $(grid).jqGrid('getGridParam', 'selarrrow');
                deleteSelectedSMS(selectedRowIDs);
                $(grid).trigger('reloadGrid');
            }
    </script>

</asp:Content>
