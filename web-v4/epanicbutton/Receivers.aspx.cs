﻿using System;
using System.Data;
using System.Web.UI;


namespace epanicbutton
{
    public partial class Receivers : System.Web.UI.Page
    {
        DataClass dc = new DataClass();
        ErrorHandler error = new ErrorHandler();
        Session session = new Session();
        int iClient, iUserId, iButton, iGroup;
        string sButton, sGroupName;
        bool group, result;


        protected void Page_Load(object sender, EventArgs e)
        {

            if ((Session["clientid"] != null) && ((Session["userid"] != null)))
            {
                iClient = Convert.ToInt32(Session["clientid"].ToString());
                iUserId = Convert.ToInt32(Session["userid"].ToString());
            }
            else
            {
                Response.Redirect("login.aspx");
            }
            if (Request.QueryString["b"] != null)
            {
                try
                {
                    string s = Request.QueryString["b"].ToString();
                    result = int.TryParse(s, out iButton);                                      

                    if (result)
                        iButton = Convert.ToInt32(s);

                    DataTable button = dc.GetSingleButton(iClient, iButton);
                    if (button.Rows.Count < 1)
                    {
                        Response.Redirect("logout.aspx");
                    }
                    sButton = button.Rows[0]["ButtonName"].ToString();
                    sGroupName = button.Rows[0]["GroupName"].ToString();
                    group = Convert.ToBoolean(button.Rows[0]["GroupMessage"].ToString());
                    iGroup = Convert.ToInt32(button.Rows[0]["UserGroupsId"].ToString());


                    if (group)
                    {
                        lblRecipients.Text = "All members of group " + sGroupName + " will receive alerts!";
                        lblReceiverHeader.Text = "Recipient Manager for " + sGroupName + " Alert Button Named: " + sButton;
                        lblMsg.Visible = true;
                        lblMsg.Text = "This buttons is designated as a GROUP button. ALL members will automatically recieve alerts.";
                        //Page.ClientScript.RegisterStartupScript(this.GetType(), "messgebox", "displayGroupMsg();", true);
                    }
                    else
                    {
                        lblMsg.Visible = false;
                        lblRecipients.Text = "Current Recipients";
                        lblReceiverHeader.Text = "Recipient Manager for Alert Button Named: " + sButton;
                        
                    }

                }

                catch (System.Threading.ThreadAbortException)
                {
                    //just catch the redirect thread 
                }
                catch (System.Exception ex)
                {
                    string method = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();
                    error.ErrorCatch(this.ToString(), iClient.ToString(), iUserId.ToString(), method, ex.Message, ex.StackTrace);
                    Response.Redirect("/apperror.aspx", false);
                }

            }
            

            if (!IsPostBack)
            {
                try
                {
                    epbGrid.DataSource = dc.GetReceivers(iClient, iButton);
                    epbGrid.DataBind();

                    epbAvailReceiver.DataSource = dc.GetAvailableMachineReceivers(iClient, iButton);
                    epbAvailReceiver.DataBind();

                }
                catch (System.Exception ex)
                {
                    string method = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();
                    error.ErrorCatch(this.ToString(), iClient.ToString(), iUserId.ToString(), method, ex.Message, ex.StackTrace);
                    Response.Redirect("/apperror.aspx", false);

                }
            }
        }

        protected void btnAddReceiver_Click(object sender, EventArgs e)
        {
            string nr = nrSelectedRowsState.Value.TrimEnd('|');
            try
            {
                if (nr != "")
                {

                    DataTable dt = dc.GetReceivers(iClient, iButton);

                    dt.PrimaryKey = new DataColumn[] { dt.Columns["Id"] };

                    dc.InsertMachineReceivers(iClient, iUserId, iButton, sButton, nr);

                    nrSelectedRowsState.Value = "";

                    epbGrid.DataSource = dc.GetReceivers(iClient, iButton);
                    epbGrid.DataBind();


                }
                else
                {
                    lblMsg.Visible = true;
                    lblMsg.Text = "You must select at least one user to add.";

                    Page.ClientScript.RegisterStartupScript(this.GetType(), "messgebox", "displayMsg();", true);
                }

            }
            catch (System.Exception ex)
            {
                string method = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();
                error.ErrorCatch(this.ToString(), iClient.ToString(), iUserId.ToString(), method, ex.Message, ex.StackTrace);
                Response.Redirect("/apperror.aspx", false);
            }
        }

        protected void btnDelReceiver_Click(object sender, EventArgs e)
        {
            string cr = crSelectedRowsState.Value.TrimEnd('|');

            try
            {
                if (cr != "")
                {
                    DataTable dt = dc.GetReceivers(iClient, iButton);

                    dt.PrimaryKey = new DataColumn[] { dt.Columns["Id"] };

                    dc.DeleteMachineReceiver(iClient, iUserId, iButton, sButton, cr);

                    int iGroup = Convert.ToInt32(dt.Rows[0]["UserGroupsId"].ToString());
                    QueueClass q = new QueueClass();
                    q.CreateGroupQueueMsg(iGroup.ToString(), iGroup.ToString(), "update");

                    crSelectedRowsState.Value = "";

                    epbGrid.DataSource = dc.GetReceivers(iClient, iButton);
                    epbGrid.DataBind();
                }
                else
                {
                    lblMsg.Visible = true;
                    lblMsg.Text = "You must select at least one recipient to remove.";

                    Page.ClientScript.RegisterStartupScript(this.GetType(), "messgebox", "displayMsg();", true);
                }

            }
            catch (System.Exception ex)
            {
                string method = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();
                error.ErrorCatch(this.ToString(), iClient.ToString(), iUserId.ToString(), method, ex.Message, ex.StackTrace);
                Response.Redirect("/apperror.aspx", false);

            }
        }
    }
}
