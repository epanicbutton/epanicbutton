﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace epanicbutton
{
    public partial class Recover : System.Web.UI.Page
    {
        DataClass dc = new DataClass();

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            //Check for valid email address...
            Utility utility = new Utility();
            if (utility.ValidateEmailAddress(txtEmail.Text, 100))
            {
                if (dc.IsValidEmail(txtEmail.Text))
                {
                    string originalpwd = Utility.GeneratePassword();
                    string newpwd = UserAccount.ComputeHash(originalpwd, "MD5", null);
                    dc.ResetPwd(txtEmail.Text, newpwd);
                    string adminName = dc.GetAdminName(txtEmail.Text);
                    System.Text.StringBuilder msg = GenerateResetPasswordMessage(originalpwd, adminName);

                    Utility.SendSimpleHtmlMessage(txtEmail.Text, msg.ToString());

                    DisplaySuccessMessage();
                }
                else
                {
                    DisplayErrorMessage();
                }
            }
            else
            {
                DisplayErrorMessage();
            }
        }

        private void DisplayErrorMessage()
        {
            this.lblPasswordMsg.Visible = true;
            this.lblPasswordMsg.CssClass = "login-warning";
            this.lblPasswordMsg.Text = "Provided email address is not registered with us";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "messgebox", "displayPasswordMsg();", true);
        }
        private void DisplaySuccessMessage()
        {
            this.lblPasswordMsg.Visible = true;
            this.lblPasswordMsg.CssClass = "reset-success";
            this.lblPasswordMsg.Text = "Your password has been reset, you will receive an email with the new password at the provided email address.";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "messgebox", "displayPasswordMsg();", true);
        }

        private System.Text.StringBuilder GenerateResetPasswordMessage(string originalpwd, string adminName)
        {
            System.Text.StringBuilder msg = new System.Text.StringBuilder();
            msg.Append("Your password for the ePanicbutton Admin Portal has been reset to <br/><br/>");
            msg.Append("Username : " + adminName + "<br />");
            msg.Append("Password : " + originalpwd + "<br /><br/>");
            msg.Append("Please login to change your password. <br/>");
            msg.Append("Thank you and please let us know how we can be of assistance. <br />");
            msg.Append("<a href='mailto:support@epanicbutton.com'>support@epanicbutton.com</a><br/>");
            msg.Append("919-701-9707<br/>");
            return msg;
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            Response.Redirect("/Login.aspx", false);
        }
    }
}