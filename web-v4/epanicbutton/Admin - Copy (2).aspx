﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Main.Master" AutoEventWireup="true" CodeBehind="Admin.aspx.cs" Inherits="epanicbutton.Admin" %>

<%@ Register Assembly="Trirand.Web" Namespace="Trirand.Web.UI.WebControls" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="header">Admin Manager</div>
    <div id="msg" class="ui-state-highlight">
    </div>
    <div id="admincontent">
        <div class="row">
            <div class="col-md-6">
                <div id="adminlcontent">
                    <table>
                        <tr>
                            <td colspan="3">
                                <h3>Account Details</h3>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblLoginLabel" runat="server" Text="Admin Login" Width="200"></asp:Label></td>
                            <td></td>
                            <td>
                                <asp:Label ID="lblLogin" runat="server" Text="Label"></asp:Label>

                            </td>
                        </tr>
                        <tr>
                            <td>Admin Contact First Name</td>
                            <td></td>
                            <td>
                                <asp:TextBox ID="txtFirstName" runat="server" Width="300"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>Admin Contact Last Name</td>
                            <td></td>
                            <td>
                                <asp:TextBox ID="txtLastName" runat="server" Width="300"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>Admin Contact Email</td>
                            <td></td>
                            <td>
                                <asp:TextBox ID="txtEmail" runat="server" Width="300"></asp:TextBox></td>
                        </tr>
                        <tr id="trTimezone" runat="server">
                            <td>Timezone</td>
                            <td></td>
                            <td>
                                <asp:DropDownList ID="ddlTimeZone" runat="server" Width="300"></asp:DropDownList></td>
                        </tr>


                        <tr>
                            <td></td>
                            <td></td>
                            <td>
                                <asp:Button ID="btnSave" runat="server" Text="Save Details" Width="150" OnClick="btnSave_Click" />
                                <asp:DropDownList ID="ddlClients" runat="server" Visible="false"></asp:DropDownList>
                            </td>

                        </tr>


                        <tr>
                            <td colspan="3">
                                <asp:Label ID="lblAccountMsg" runat="server" Text="" Visible="false"></asp:Label></td>

                        </tr>

                    </table>
                </div>
            </div>
            <div class="col-md-6">

                <div id="adminrcontent">
                    <table>
                        <tr>
                            <td colspan="3">
                                <h3>Change Password</h3>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblCurrentPassword" runat="server" Text="Current Password" Width="200"></asp:Label></td>
                            <td></td>
                            <td>
                                <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" Width="200"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>New Password</td>
                            <td></td>
                            <td>
                                <asp:TextBox ID="txtNew" runat="server" TextMode="Password" Width="200"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>Confirm New Password</td>
                            <td></td>
                            <td>
                                <asp:TextBox ID="txtConfirm" runat="server" TextMode="Password" Width="200"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: right;"></td>
                            <td></td>
                            <td style="vertical-align: middle;">
                                <asp:Button ID="btnUpdatePwd" runat="server" Text="Update Password" Width="150" OnClick="btnUpdatePwd_Click" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img alt="Password Help" src="images/qm3.png" onclick="displayPwd();" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <div id="pwdMsg" class="ui-state-highlight">
                                    <asp:Label ID="lblPasswordMsg" runat="server" Text="" Visible="false"></asp:Label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblCode" runat="server" Text="Code" Visible="false"></asp:Label></td>
                            <td>
                                <asp:Button ID="btnCode" runat="server" Text="..." Visible="false" OnClick="btnCode_Click" /></td>
                            <td>
                                <asp:TextBox ID="txtCode" runat="server" Width="200" Visible="false"></asp:TextBox></td>
                        </tr>
                    </table>
                </div>

            </div>
        </div>
    </div>
    <div id="admingrid">
        <cc1:JQGrid ID="epbGrid" runat="server" Width="1078" Height="200" PagerSettings-PageSize="10"
            OnRowEditing="epbGrid_RowEditing"
            OnRowAdding="epbGrid_RowAdding"
            OnRowDeleting="epbGrid_RowDeleting">
            <Columns>
                <cc1:JQGridColumn DataField="Id" Editable="false" PrimaryKey="true" Visible="false" Searchable="false" />
                <cc1:JQGridColumn DataField="ClientsId" Editable="false" PrimaryKey="false" Visible="false" Searchable="false" />
                <cc1:JQGridColumn DataField="ClientName" HeaderText="Client Name" Editable="true"
                    EditType="DropDown"
                    EditorControlID="ddlClientList"
                    EditTypeCustomGetValue="getGroupElementValue" />
                <cc1:JQGridColumn DataField="AdminName" HeaderText="Admin Name" Editable="true" PrimaryKey="false" Visible="true" Searchable="true">
                    <EditClientSideValidators>
                        <cc1:RequiredValidator />
                        <cc1:CustomValidator ValidationFunction="isUserUnique" />
                    </EditClientSideValidators>
                </cc1:JQGridColumn>

                <cc1:JQGridColumn DataField="EmailAddress" HeaderText="Email Address" Editable="true" PrimaryKey="false" Visible="true" Searchable="true">
                    <EditClientSideValidators>
                        <cc1:RequiredValidator />
                        <cc1:CustomValidator ValidationFunction="isEmailValid" />
                    </EditClientSideValidators>
                </cc1:JQGridColumn>

                <cc1:JQGridColumn DataField="FirstName" HeaderText="First Name" Editable="true" PrimaryKey="false" Visible="true" Searchable="true" />
                <cc1:JQGridColumn DataField="LastName" HeaderText="Last Name" Editable="true" PrimaryKey="false" Visible="true" Searchable="true" />
                <cc1:JQGridColumn DataField="ChangePassword" HeaderText="Reset Password" Editable="true" PrimaryKey="false" Visible="true" Searchable="true" Width="80" TextAlign="Center"
                    EditType="Custom"
                    EditTypeCustomCreateElement="createResetPwd"
                    EditTypeCustomGetValue="getResetPwd" />
                <%--<cc1:JQGridColumn DataField="MachineGuid" HeaderText="Machine GUi" Editable="true" PrimaryKey="false" Visible="true" Searchable="true" />--%>
            </Columns>
            <ToolBarSettings ToolBarPosition="Bottom" ShowEditButton="true" ShowAddButton="true" ShowDeleteButton="false" ShowSearchButton="true" ShowRefreshButton="true" />
            <EditDialogSettings CloseAfterEditing="true" Caption="Admin User Editor" LoadingMessageText="Loading..." Width="450" />
            <AddDialogSettings CloseAfterAdding="true" Width="450" />
            <AppearanceSettings HighlightRowsOnHover="true" />
            <ClientSideEvents AfterEditDialogShown="AdminhideNav" AfterAddDialogShown="hideNav" />

        </cc1:JQGrid>
    </div>
    <asp:DropDownList runat="server" ID="ddlClientList" CssClass="adminClientDDL" />

    <!-- dialog window markup  class="ui-icon ui-icon-alert" s -->
    <div id="pwdDialog" title="ePanicButton Password Requirements" style="display: none;">
        <p>
            Your new password must satisfy the following requirements:<br />
            <asp:BulletedList ID="pwdList" runat="server" BulletStyle="Circle">
                <asp:ListItem>Length must be greater than or equal to 8</asp:ListItem>
                <asp:ListItem>Contain one or more uppercase characters</asp:ListItem>
                <asp:ListItem>Contain one or more lowercase characters</asp:ListItem>
                <asp:ListItem>Contain one or more numeric values</asp:ListItem>
                <asp:ListItem>Contain one or more special characters, i.e. @ # $ % !</asp:ListItem>
            </asp:BulletedList>
        </p>
    </div>

</asp:Content>
