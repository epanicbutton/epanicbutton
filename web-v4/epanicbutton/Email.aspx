﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Main.Master" AutoEventWireup="true" CodeBehind="Email.aspx.cs" Inherits="epanicbutton.Email" %>

<%@ Register Assembly="Trirand.Web" Namespace="Trirand.Web.UI.WebControls" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div id="header">
        <asp:Label ID="lblReceiverHeader" runat="server" Text="Label"></asp:Label>
    </div>

    <div id="msg" class="ui-state-highlight">
        <asp:Label ID="lblMsg" runat="server" Text="Label"></asp:Label>
    </div>

    <div id="lhdr">
        <asp:Label ID="lblRecipients" runat="server" Text="Current Recipients"></asp:Label>
    </div>
    <div id="lcontent">
        <cc1:JQGrid ID="epbGrid" runat="server" Width="491" Height="385" PagerSettings-PageSize="18"
            MultiSelect="true" OnRowAdding="epbGrid_RowAdding" OnRowDeleting="epbGrid_RowDeleting" OnRowEditing="epbGrid_RowEditing">
            <Columns>
                <cc1:JQGridColumn DataField="id" Editable="false" PrimaryKey="true" Visible="False" Searchable="false" />
                <cc1:JQGridColumn DataField="EmailAddress" HeaderText="Recipient Email Address" Editable="true" Visible="true" Searchable="true" />
            </Columns>
            <ToolBarSettings ToolBarPosition="Bottom" ShowSearchButton="true" ShowRefreshButton="true" ShowDeleteButton="false" ShowAddButton="true" ShowEditButton="true">
                <CustomButtons>
                    <cc1:JQGridToolBarButton
                        Text=""
                        ToolTip="Delete Emails"
                        ButtonIcon="ui-icon-trash"
                        Position="Last"
                        OnClick="delEmails" />
                </CustomButtons>
                </ToolBarSettings>
            <PagerSettings PageSize="18" PageSizeOptions="[18,20,50,100]" />
            <AppearanceSettings HighlightRowsOnHover="true" />
            <%--            <ClientSideEvents RowSelect="crSelected" LoadComplete="crRestore" />--%>

            <ClientSideEvents AfterEditDialogShown="hideNav" AfterAddDialogShown="hideNav" />

            <EditDialogSettings CloseAfterEditing="true" Caption="Edit Email Address" Modal="true" Resizable="false" Width="400" />
            <AddDialogSettings CloseAfterAdding="true" Caption="Add Email Address" Modal="true" Resizable="false" Width="400" />

            <SearchDialogSettings MultipleSearch="true" />
        </cc1:JQGrid>
    </div>


    <div id="mhdr"></div>
    <div id="mcontent">
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <asp:Button ID="btnAddEmail" runat="server" Text="<< Add " Width="85" CssClass="ui-button recipients-btn" OnClick="btnAddEmail_Click" /><br />
        <br />
    </div>

    <div id="rhdr">Batch Upload</div>
    <div id="rcontent">
        <div id="uploadInstruct">
            <p id="uploadInsText">
                Enter list of the emails you wish to add.<br />
                <br />
                You may use a comma, semi-colon to separate the numbers. 
                <br />
                <br />
                <strong>Example:<br />
                    info@salesteam.com,jane.doe@her-site.com,jsmith@domain.com</strong>
        </div>
        <div id="uploadPanel">
            <asp:TextBox ID="tbEmails" runat="server" Columns="50" Rows="15" TextMode="MultiLine" CssClass="form-control form-control-multiline"></asp:TextBox>
        </div>
    </div>
    <script type="text/javascript">
        var selectedRows = [];
        var grid = $("#<%= epbGrid.ClientID %>");
        function delEmails() {
            var selectedRowIDs = $(grid).jqGrid('getGridParam', 'selarrrow');
            deleteSelectedEmails(selectedRowIDs);
            $(grid).trigger('reloadGrid');
        }
    </script>
</asp:Content>
