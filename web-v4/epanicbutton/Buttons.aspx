﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Main.Master" AutoEventWireup="true" CodeBehind="Buttons.aspx.cs" Inherits="epanicbutton.Buttons" %>

<%@ Register Assembly="Trirand.Web" Namespace="Trirand.Web.UI.WebControls" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="header">Button Manager</div>

    <div id="content">
        <cc1:JQGrid ID="epbGrid" runat="server" Width="1082" Height="414" PagerSettings-PageSize="8"
            OnRowDeleting="epbGrid_RowDeleting"
            OnRowAdding="epbGrid_RowAdding"
            OnRowEditing="epbGrid_RowEditing">
            <Columns>
                <cc1:JQGridColumn DataField="ID" Editable="false" PrimaryKey="true" Visible="false" Searchable="false" />
                <cc1:JQGridColumn DataField="GroupID" Editable="false" PrimaryKey="false" Visible="False" Searchable="false" />
                <cc1:JQGridColumn DataField="ClientName" Editable="True" PrimaryKey="false" Visible="false" Searchable="false"
                    EditType="DropDown"
                    EditorControlID="ddlClients" />
                <cc1:JQGridColumn DataField="GroupName" HeaderText="Group Name" Editable="true" PrimaryKey="false" Visible="true" Searchable="true"
                    EditType="DropDown"
                    EditorControlID="ddlGroups" />
                <cc1:JQGridColumn DataField="ButtonName" HeaderText="Button Name" Editable="true" PrimaryKey="false" Visible="true" Searchable="true">
                    <EditClientSideValidators>
                        <cc1:RequiredValidator />
                    </EditClientSideValidators>
                </cc1:JQGridColumn>

                <cc1:JQGridColumn DataField="ButtonTitle" HeaderText="Button Title" Editable="true" PrimaryKey="false" Visible="true" Searchable="true">
                    <EditClientSideValidators>
                        <cc1:RequiredValidator />
                        <cc1:CustomValidator ValidationFunction="titleLength" />
                    </EditClientSideValidators>
                </cc1:JQGridColumn>

                <cc1:JQGridColumn DataField="ButtonMessage" HeaderText="Button Message" Editable="true" PrimaryKey="false" Visible="true" Searchable="true" Width="200" EditType="TextArea">
                    <EditFieldAttributes>
                        <cc1:JQGridEditFieldAttribute Name="cols" Value="35" />
                        <cc1:JQGridEditFieldAttribute Name="rows" Value="5" />
                    </EditFieldAttributes>
                    <EditClientSideValidators>
                        <cc1:RequiredValidator />
                        <cc1:CustomValidator ValidationFunction="msgLength" />
                    </EditClientSideValidators>
                </cc1:JQGridColumn>

                <cc1:JQGridColumn DataField="ButtonIcon" HeaderText="Button Icon" Editable="true" PrimaryKey="false" Visible="true" Searchable="false" TextAlign="Center" EditType="Custom"
                    EditTypeCustomCreateElement="createImg"
                    EditTypeCustomGetValue="getImg">
                    <EditClientSideValidators>
                        <cc1:RequiredValidator />
                        <cc1:CustomValidator ValidationFunction="dupeIconChk" />
                    </EditClientSideValidators>
                </cc1:JQGridColumn>

                <cc1:JQGridColumn DataField="ButtonImage" HeaderText="Button Image" Editable="false" PrimaryKey="false" Visible="false" Searchable="false" />
                <cc1:JQGridColumn DataField="Mute" HeaderText="Mute Sound" Editable="true" PrimaryKey="false" Visible="false" Searchable="false" 
                    EditType="Custom"
                    EditTypeCustomCreateElement="createMute"
                    EditTypeCustomGetValue="getMute" />
                <cc1:JQGridColumn DataField="SendDelay" HeaderText="Send Delay" Editable="true" PrimaryKey="false" Visible="false" Searchable="false" 
                    EditType="Custom"
                    EditTypeCustomCreateElement="createDelay"
                    EditTypeCustomGetValue="getDelay" />

                <cc1:JQGridColumn DataField="GroupMessage" HeaderText="Group Message" Editable="false" PrimaryKey="false" Visible="false" Searchable="true"
                    EditType="Custom"
                    EditTypeCustomCreateElement="createGrpMsg"
                    EditTypeCustomGetValue="getDefault" />
                <cc1:JQGridColumn DataField="Severity" HeaderText="Severity Level" Editable="true" PrimaryKey="false" Visible="true" Searchable="true" EditType="Custom"
                    EditTypeCustomCreateElement="createSev"
                    EditTypeCustomGetValue="getSev" />
                <cc1:JQGridColumn DataField="HotKeyCode" HeaderText="Hot Key Code" Editable="true" PrimaryKey="false" Visible="true" Searchable="true" EditType="Custom"
                    EditTypeCustomCreateElement="createKey"
                    EditTypeCustomGetValue="getKey" />
                <cc1:JQGridColumn DataField="HotKeyModifier" HeaderText="Hot Key" TextAlign="Center" Width="80" Editable="true" PrimaryKey="false" Visible="true" Searchable="true" EditType="Custom"
                    EditTypeCustomCreateElement="createMod"
                    EditTypeCustomGetValue="getMod">
                    <EditClientSideValidators>
                        <cc1:RequiredValidator />
                        <cc1:CustomValidator ValidationFunction="dupeHCChk" />
                    </EditClientSideValidators>
                </cc1:JQGridColumn>
                <cc1:JQGridColumn DataField="AddMachine" HeaderText="Receivers" Width="180" Editable="false" PrimaryKey="false" Visible="true" Searchable="false" TextAlign="Center" />
                
            </Columns>
            <PagerSettings PageSize="8" PageSizeOptions="[8,20,50,100]" />
            <ToolBarSettings ToolBarPosition="Bottom" ShowEditButton="true" ShowAddButton="true" ShowDeleteButton="true" ShowSearchButton="true" ShowRefreshButton="true">
                <CustomButtons>
                    <cc1:JQGridToolBarButton
                        Text=""
                        ToolTip="Copy this button"
                        ButtonIcon="ui-icon-copy"
                        Position="Last"
                        OnClick="copyButton" />
                </CustomButtons>
            </ToolBarSettings>
            <EditDialogSettings CloseAfterEditing="true" Caption="Edit Button" Modal="true" Resizable="false" Width="400" Height="300"/>
            <AddDialogSettings CloseAfterAdding="true" Caption="Add Button" Resizable="false" Width="400" />
            <SearchDialogSettings MultipleSearch="true" />
            <ClientSideEvents AfterEditDialogShown="hideNav" AfterAddDialogShown="hideNav" RowSelect="editRow" BeforeAddDialogShown="addRow"/>
            <AppearanceSettings HighlightRowsOnHover="false" />
        </cc1:JQGrid>
    </div>
    <asp:DropDownList runat="server" ID="ddlGroups" />
    <asp:DropDownList runat="server" ID="ddlClients" />
    <script type="text/javascript">
        var btnId;

        function editRow(id) {
            btnId = id;
        }

        function copyButton() {
            
        }

        function addRow() {
            btnId = 0;
        }
        function titleLength(value, column) {
            if (value.length > 8)
                return [false, column + ": maximum character length is 8!"];
            else
                return [true, ""];
        }

        function msgLength(value, column) {
            if (value.length > 200)
                return [false, column + ": maximum character length is 200!"];
            else
                return [true, ""];
        }

        function dupeHCChk(value, column) {
            var success, msg;
            $.ajax({
                type: "POST",
                url: '/ajax/ButtonUtilities.aspx/HKExist',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({                    
                    Client: $('#ClientName').val(),
                    Group: $('#GroupName').val(),
                    Button: btnId, 
                    Key: $('#HotKeyCode').val(), 
                    Modifier: $('#HotKeyModifier').val() 
                }),
                async: false,
                success: function (data) {                    
                    if (data.d.Success) {
                        msg = "";
                        success = true;
                    }
                    else {                       
                        msg = "A button is already defined with that hot key combo.";
                        success = false;
                    }
                },
                error: function () {
                    msg = "An error occurred attempting to validate your request";
                    success = false
                }
            });
            return [success, msg];
        }

        function dupeIconChk(value, column) {
            var success, msg;
            $.ajax({
                type: "POST",
                url: '/ajax/ButtonUtilities.aspx/IconExist',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({
                    Client: $('#ClientName').val(),
                    Group: $('#GroupName').val(),
                    Button: btnId,
                    Icon: $('#ButtonIcon').val()
                }),
                async: false,
                success: function (data) {
                    if (data.d.Success) {
                        msg = "";
                        success = true;
                    }
                    else {
                        msg = "A button is already defined with that icon for this group.";
                        success = false;
                    }
                },
                error: function () {
                    msg = "An error occurred attempting to validate your request";
                    success = false
                }
            });

            return [success, msg];
        }

        function dupeHC() {
            return [true, ""];
        }

        function setMsg(v) {
            return msg;
        }


        $(function () {
            $("#selectable").selectable();
        });

    </script>

</asp:Content>

