﻿IF EXISTS( SELECT *
           FROM sys.objects
           WHERE object_id = OBJECT_ID(N'[dbo].[UpdateClientTimezone]')
             AND type IN( N'P', N'PC' ))
DROP PROCEDURE [dbo].[UpdateClientTimezone];
GO
CREATE PROCEDURE [dbo].[UpdateClientTimezone](
       @clientsid INT,
       @timezone  VARCHAR(100))
AS
     UPDATE Clients
       SET TimeZone = @timezone
     WHERE ClientsId = @clientsid;
GO
IF EXISTS( SELECT *
           FROM sys.objects
           WHERE object_id = OBJECT_ID(N'[dbo].[UpdateClient]')
             AND type IN( N'P', N'PC' ))
DROP PROCEDURE [dbo].[UpdateClient];
GO
CREATE PROCEDURE [dbo].[UpdateClient]
       @clientid   INT,
       @clientname NVARCHAR(40),
       @conn       VARCHAR(40),
       @active     INT,
       @date       DATETIME,
       @adminsid   INT,
       @email      NVARCHAR(75),
       @timeZone   NVARCHAR(500)
AS

/*

UpdateUser 2, 42, 1, 6

UpdateUser 2, 42, 1,8



select * from clients



*/



     UPDATE Clients
       SET ClientName = @clientname,
           Active = @active,
           InactiveDate = @date,
           TotalConnections = @conn,
           TimeZone = @timeZone
     WHERE ClientsId = @clientid;
     UPDATE Admins
       SET EmailAddress = @email
     WHERE AdminsId = @adminsid;
GO
IF EXISTS( SELECT *
           FROM sys.objects
           WHERE object_id = OBJECT_ID(N'[dbo].[GetClient]')
             AND type IN( N'P', N'PC' ))
DROP PROCEDURE [dbo].[GetClient];
GO
CREATE PROCEDURE [dbo].[GetClient]
       @clientsid INT
AS

/*



exec GetClient 1

exec GetClient 0

*/



     IF @clientsid = 0
         BEGIN
             SELECT DISTINCT c.ClientsId AS Id,
                             a.AdminsId,
                             ClientName,
                             UPPER(ClientGuid) AS RegistrationId,
                             Active,
                             TotalConnections,
                             CONVERT( VARCHAR, InactiveDate, 101) AS InactiveDate,
                             AdminName,
                             a.EmailAddress,
                             u.Emails,
                             u.Machines,
                             u.SMS,
                             c.TimeZone
             FROM Clients c
                  JOIN Admins a ON c.ClientsId = a.ClientsId
                  LEFT JOIN ClientUsage u ON u.ClientsId = c.ClientsId
             GROUP BY c.ClientsId,
                      a.AdminsId,
                      ClientName,
                      ClientGuid,
                      Active,
                      TotalConnections,
                      InactiveDate,
                      AdminName,
                      a.EmailAddress,
                      u.Emails,
                      u.Machines,
                      u.SMS,
                      c.TimeZone
             ORDER BY c.ClientName,
                      a.AdminName;
         END;
             IF @clientsid != 0
                 BEGIN
                     SELECT DISTINCT c.ClientsId AS Id,
                                     a.AdminsId,
                                     ClientName,
                                     UPPER(ClientGuid) AS RegistrationId,
                                     Active,
                                     TotalConnections,
                                     CONVERT( VARCHAR, InactiveDate, 101) AS InactiveDate,
                                     AdminName,
                                     a.EmailAddress,
                                     u.Emails,
                                     u.Machines,
                                     u.SMS,
                                     c.TimeZone
                     FROM Clients c
                          JOIN Admins a ON c.ClientsId = a.ClientsId
                          LEFT JOIN ClientUsage u ON u.ClientsId = c.ClientsId
                     WHERE c.ClientsId = @clientsid
                     GROUP BY c.ClientsId,
                              a.AdminsId,
                              ClientName,
                              ClientGuid,
                              Active,
                              TotalConnections,
                              InactiveDate,
                              AdminName,
                              a.EmailAddress,
                              u.Emails,
                              u.Machines,
                              u.SMS,
                              c.TimeZone
                     ORDER BY c.ClientName,
                              a.AdminName;
                 END;
GO
IF EXISTS( SELECT *
           FROM sys.objects
           WHERE object_id = OBJECT_ID(N'[dbo].[GetUsers]')
             AND type IN( N'P', N'PC' ))
DROP PROCEDURE [dbo].[GetUsers];
GO
CREATE PROCEDURE [dbo].[GetUsers]
       @clientid INT
AS

/*

GetUsers 2

--select * from usergroups



*/



     SELECT UsersId AS Id,
            UserLastName,
            UserFirstName,
            UserDepartment,
            GroupName,
            MachineName,
            UserEmail,
            UserTelephone,
            u.MachineGuid
     FROM Users u
          JOIN UserGroups g ON g.UserGroupsId = u.UserGroupsId
          JOIN Clients c ON c.ClientsId = g.ClientsId
     WHERE c.Clientsid = @clientid
     ORDER BY UserLastName;
GO
IF EXISTS( SELECT *
           FROM sys.objects
           WHERE object_id = OBJECT_ID(N'[dbo].[ApplicationEventLog]')
             AND type IN( N'P', N'PC' ))
DROP PROCEDURE [dbo].[ApplicationEventLog];
GO
CREATE PROCEDURE [dbo].[ApplicationEventLog]
       @clientsid   INT,
       @usersid     INT,
       @description NVARCHAR(200),
       @createdon   DATETIME
AS

/*
select * from Events

exec ApplicationEventLog 2,1,'edited a group'

*/


     INSERT INTO Events
            ( ClientsId,
              CreatedOn,
              Description,
              UsersId
            )
     VALUES( @clientsid, @createdon, @description, @usersid );
GO
IF EXISTS( SELECT *
           FROM sys.objects
           WHERE object_id = OBJECT_ID(N'[dbo].[CalculateClientUsage]')
             AND type IN( N'P', N'PC' ))
DROP PROCEDURE [dbo].[CalculateClientUsage];
GO
CREATE PROCEDURE [dbo].[CalculateClientUsage]
AS

/*

exec CalculateClientUsage
*/

     truncate TABLE [dbo].[ClientUsage];
     INSERT INTO [dbo].[ClientUsage]
            SELECT c.ClientsId,
                   COUNT(DISTINCT e.EmailAddress) AS Emails,
                   COUNT(DISTINCT s.Number) AS SMS,
                   COUNT(DISTINCT u.MachineGuid) AS Machines
            FROM Clients c
                 JOIN Admins a ON c.ClientsId = a.ClientsId
                 LEFT JOIN UserGroups g ON g.ClientsId = c.ClientsId
                 LEFT JOIN Users u ON u.UserGroupsId = g.UserGroupsId
                 LEFT JOIN Buttons b ON b.UserGroupsId = g.UserGroupsId
                 LEFT JOIN Emails e ON e.ButtonsId = b.ButtonsId
                 LEFT JOIN Machines m ON m.ButtonsId = b.ButtonsId
                 LEFT JOIN SMS s ON s.ButtonsId = b.ButtonsId
            GROUP BY c.ClientsId;
GO
IF EXISTS( SELECT *
           FROM sys.objects
           WHERE object_id = OBJECT_ID(N'[dbo].[InsertClient]')
             AND type IN( N'P', N'PC' ))
DROP PROCEDURE [dbo].[InsertClient];
GO
CREATE PROCEDURE [dbo].[InsertClient]
       @clientname    VARCHAR(100),
       @connections   INT,
       @active        INT,
       @date          DATETIME,
       @adminname     NVARCHAR(40),
       @email         NVARCHAR(75),
       @scrubbedname  VARCHAR(100),
       @timzone       NVARCHAR(500),
       @newClientGuid UNIQUEIDENTIFIER OUTPUT
AS

/*



declare @name varchar(20)

set @name = 'new'

exec InsertClient @name,100,1,'04/01/2016',@name,'andrew.wing@wingweb.com',@name,'EF26A6DE-25D2-4617-A2B8-162E207BEE63'



*/



     DECLARE @id INT;
     DECLARE @admintypeid INT;
     DECLARE @clientid INT;
     DECLARE @groupid INT;
     DECLARE @alertid INT;
     DECLARE @buttonid INT;
     SET @newClientGuid = NEWID();
     SELECT @newClientGuid AS newClientGuid;
     INSERT INTO Clients
            ( ClientName,
              ClientGuid,
              Active,
              InactiveDate,
              TotalConnections,
              ClientTypeId,
              VirtualHost,
              TimeZone
            )
     VALUES( @clientname, @newClientGuid, 1, @date, @connections, 1, @scrubbedname, @timzone );
     SET @id = SCOPE_IDENTITY();
     PRINT @id;
     SELECT @admintypeid = AdminTypeId
     FROM AdminType
     WHERE AdminType = 'Client';

     --select * from AdminType where AdminType = 'Client'
     --print @newClientGuid
     --print @admintypeid



     INSERT INTO Admins
            ( AdminName,
              [Password],
              EmailAddress,
              ClientsId,
              AdminTypeId
            )
     VALUES( @adminname, 'PphKRX5GY1bXtzX/PKTywXrtWObpobI=', @email, @id, 3 );
     SELECT @clientid = clientsid
     FROM clients
     WHERE Clientname = @clientname;
     INSERT INTO UserGroups
            ( GroupName,
              ClientsId,
              GroupDescription,
              CreatedBy,
              ModifiedOn,
              ModifiedBy,
              ClientDefaultGroup
            )
     VALUES( 'General Group', @clientid, @clientname + ' General Group', 1, NULL, NULL, 1 );
     SELECT @groupid = usergroupsid
     FROM UserGroups
     WHERE ClientsId = @clientid
       AND ClientDefaultGroup = 1;
     INSERT INTO Buttons  

     /************THIS GOES TO ALL USERS FOR THE CLIENT*******************/

            ( UserGroupsId,
              ButtonName,
              ButtonTitle,
              ButtonMessage,
              ButtonIcon,
              ButtonImage,
              Severity,
              AudibleAlarmLevel,
              HotKeyCode,
              HotKeyModifier,
              CreatedOn,
              CreatedBy,
              ModifiedOn,
              ModifiedBy,
              GroupMessage
            )
     VALUES( @groupid, 'Security', 'Security', 'Security', '15.png', NULL, 'High', 1, 'Ctrl', 'F5', GETDATE(), 1, GETDATE(), 1, 1 );
     SET @buttonid = SCOPE_IDENTITY();
     INSERT INTO Emails
            ( ButtonsId,
              EmailAddress
            )
     VALUES( @buttonid, @email );
     INSERT INTO Buttons  

     /************THIS GOES TO ALL USERS FOR THE CLIENT*******************/

            ( UserGroupsId,
              ButtonName,
              ButtonTitle,
              ButtonMessage,
              ButtonIcon,
              ButtonImage,
              Severity,
              AudibleAlarmLevel,
              HotKeyCode,
              HotKeyModifier,
              CreatedOn,
              CreatedBy,
              ModifiedOn,
              ModifiedBy,
              GroupMessage
            )
     VALUES( @groupid, 'Support', 'Support', 'Support', '12.png', NULL, 'Low', 1, 'Ctrl', 'F4', GETDATE(), 1, GETDATE(), 1, 1 );
     SET @buttonid = SCOPE_IDENTITY();
     INSERT INTO Emails
            ( ButtonsId,
              EmailAddress
            )
     VALUES( @buttonid, @email );
GO
IF EXISTS( SELECT *
           FROM sys.objects
           WHERE object_id = OBJECT_ID(N'[dbo].[ResetPwd]')
             AND type IN( N'P', N'PC' ))
DROP PROCEDURE [dbo].[ResetPwd];
GO
CREATE PROC [dbo].[ResetPwd]
       @username NVARCHAR(75),
       @newpwd   NVARCHAR(40)
AS
     UPDATE Admins
       SET Password = @newpwd,
           ChangePassword = 1
     WHERE EmailAddress = @username;
GO
IF EXISTS( SELECT *
           FROM sys.objects
           WHERE object_id = OBJECT_ID(N'[dbo].[IsValidEmail]')
             AND type IN( N'P', N'PC' ))
DROP PROCEDURE [dbo].[IsValidEmail];
GO
CREATE PROC [dbo].[IsValidEmail]
       @username NVARCHAR(75)
AS
     SELECT COUNT(*)
     FROM Admins
     WHERE EmailAddress = @username;
GO
IF EXISTS( SELECT *
           FROM sys.objects
           WHERE object_id = OBJECT_ID(N'[dbo].[GetGroupButtons]')
             AND type IN( N'P', N'PC' ))
DROP PROCEDURE [dbo].[GetGroupButtons];
GO
CREATE PROCEDURE [dbo].[GetGroupButtons]
       @clientsid INT,
       @groupid   INT
AS
     SELECT b.ButtonsId AS Id,
            b.ButtonName,
            b.ButtonTitle,
            b.ButtonMessage,
            b.ButtonIcon AS ButtonIcon,
            b.ButtonImage,
            b.Severity,
            b.HotKeyCode,
            b.HotKeyModifier,
            b.CreatedOn,
            b.AudibleAlarmLevel,
            b.GroupMessage,
            b.SendDelay,
            b.Mute
     FROM Buttons b
          JOIN UserGroups g ON g.UserGroupsId = b.UserGroupsId
                           AND g.usergroupsid = @groupid
          JOIN Clients c ON c.ClientsId = g.ClientsId
                        AND c.ClientsId = @clientsid
     ORDER BY ButtonName;
GO
IF EXISTS( SELECT *
           FROM sys.objects
           WHERE object_id = OBJECT_ID(N'[dbo].[InsertButton]')
             AND type IN( N'P', N'PC' ))
DROP PROCEDURE [dbo].[InsertButton];
GO
CREATE PROCEDURE [dbo].[InsertButton](
       @clientid    INT,
       @adminid     INT,
       @groupid     INT,
       @buttonname  VARCHAR(40),
       @buttontitle VARCHAR(40),
       @buttonmsg   VARCHAR(200),
       @buttonicon  VARCHAR(40),
       @severity    VARCHAR(40),
       @hotkey      VARCHAR(40),
       @modkey      VARCHAR(40),
       @audio       VARCHAR(40),
       @groupmsg    INT,
       @senddelay   VARCHAR(2),
       @mutesound   VARCHAR(10))
AS
     DECLARE @newbuttonid INT;
     INSERT INTO Buttons
            ( ButtonName,
              ButtonTitle,
              ButtonMessage,
              ButtonIcon,
              HotKeyCode,
              HotKeyModifier,
              UserGroupsId,
              Severity,
              AudibleAlarmLevel,
              CreatedBy,
              ModifiedBy,
              ModifiedOn,
              GroupMessage,
              SendDelay,
              Mute
            )
     VALUES( @buttonname, @buttontitle, @buttonmsg, @buttonicon, @hotkey, @modkey, @groupid, @severity, @audio, @adminid, @adminid, GETDATE(), @groupmsg, @senddelay, @mutesound );
     SET @newbuttonid = SCOPE_IDENTITY();
     INSERT INTO Emails
            SELECT DISTINCT @newbuttonid,
                            EmailAddress,
                            GETDATE()
            FROM Admins
            WHERE ClientsId = @clientid;
     SELECT @newbuttonid;
GO
IF EXISTS( SELECT *
           FROM sys.objects
           WHERE object_id = OBJECT_ID(N'[dbo].[GetMachineReceivers]')
             AND type IN( N'P', N'PC' ))
DROP PROCEDURE [dbo].[GetMachineReceivers];
GO
CREATE PROCEDURE [dbo].[GetMachineReceivers]
       @clientsid INT,
       @buttonid  INT
AS

/*

GetAvailableMachineReceivers 2,1208

*/

     SELECT u.UsersId AS Id,
            g.GroupDescription,
            g.GroupName,
            u.UserFirstName,
            u.UserLastName,
            u.UserLocation,
            u.MachineName,
            u.UserDepartment
     FROM Users u
          JOIN UserGroups g ON g.UserGroupsId = u.UserGroupsId
          JOIN Clients c ON c.ClientsId = g.ClientsId
                        AND c.ClientsId = @clientsid
                        AND EXISTS( 
                                    SELECT 1
                                    FROM Machines m
                                    WHERE m.UsersId = u.UsersId
                                      AND m.ButtonsId = @buttonid )
     ORDER BY u.UserLastName;
GO