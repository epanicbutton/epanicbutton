﻿
IF NOT EXISTS( SELECT column_name
               FROM INFORMATION_SCHEMA.columns
               WHERE TABLE_NAME = 'Clients'
                 AND COLUMN_NAME = 'TimeZone' )
    BEGIN
        ALTER TABLE Clients
        ADD TimeZone NVARCHAR(500);
    END;
GO

  DROP INDEX Buttons.ButtonImageUnique