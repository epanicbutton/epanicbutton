﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace epanicbutton.ViewModel
{
    public class TimeZoneModel
    {
        public string ID { get; set; }
        public string DisplayName { get; set; }
    }
}