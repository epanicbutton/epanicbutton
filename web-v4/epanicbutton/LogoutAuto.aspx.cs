﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace epanicbutton
{
    public partial class autologout : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session.RemoveAll();
            Session["autologout"] = "yes";
            Response.Redirect("login.aspx", true);   
        }
    }
}