﻿using System;
using System.Data;
using System.Web.UI;

namespace epanicbutton
{
    public partial class Email : System.Web.UI.Page
    {
        DataClass dc = new DataClass();
        ErrorHandler error = new ErrorHandler();
        Session session = new Session();
        int iClient, iUserId, iButton;
        string sButton, sGroupName;
        bool group, result;

        protected void Page_Load(object sender, EventArgs e)
        {
        if ((Session["clientid"] != null) && ((Session["userid"] != null)))
            {
                iClient = Convert.ToInt32(Session["clientid"].ToString());
                iUserId = Convert.ToInt32(Session["userid"].ToString());
            }
            else
            {
                Response.Redirect("login.aspx");
            }
            if (Request.QueryString["b"] != null)
            {
                try
                {
                    string s = Request.QueryString["b"].ToString();
                    result = int.TryParse(s, out iButton);

                    if (result)
                        iButton = Convert.ToInt32(s);

                    DataTable button = dc.GetSingleButton(iClient, iButton);
                    if (button.Rows.Count < 1)
                    {
                        Response.Redirect("logout.aspx");
                    }
                    sButton = button.Rows[0]["ButtonName"].ToString();
                    sGroupName = button.Rows[0]["GroupName"].ToString();
                    group = Convert.ToBoolean(button.Rows[0]["GroupMessage"].ToString());

                    lblRecipients.Text = "Current Recipients";
                    lblReceiverHeader.Text = "Recipient Manager for Alert Button Named: " + sButton;
                }

                catch (System.Threading.ThreadAbortException)
                {
                    //just catch the redirect thread 
                }
                catch (System.Exception ex)
                {
                    string method = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();
                    error.ErrorCatch(this.ToString(), iClient.ToString(), iUserId.ToString(), method, ex.Message, ex.StackTrace);
                    Response.Redirect("/apperror.aspx", false);
                }

            }
            lblMsg.Visible = false;


            if (!IsPostBack)
            {
                try
                {
                    epbGrid.DataSource = dc.GetEmailReceivers(iClient, iButton);
                    epbGrid.DataBind();
                }
                catch (System.Exception ex)
                {
                    string method = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();
                    error.ErrorCatch(this.ToString(), iClient.ToString(), iUserId.ToString(), method, ex.Message, ex.StackTrace);
                    Response.Redirect("/apperror.aspx", false);

                }
            }
        }


        protected void epbGrid_RowAdding(object sender, Trirand.Web.UI.WebControls.JQGridRowAddEventArgs e)
        {
            Utility utility = new Utility();
            try
            {
                DataTable dt = dc.GetEmailReceivers(iClient, iButton);

                dt.PrimaryKey = new DataColumn[] { dt.Columns["Id"] };
                DataRow row = dt.NewRow();

                string sEmail = e.RowData["EmailAddress"];

                if (utility.ValidateEmailAddress(sEmail, 100))
                {
                    if (row != null)
                    {
                        dc.InsertEmailAddress(iClient, iUserId, iButton, sEmail);
                    }
                    epbGrid.DataSource = dt;
                    epbGrid.DataBind();
                }
                else
                {
                    //display a message to user
                }

            }


            catch (System.Exception ex)
            {
                string method = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();
                error.ErrorCatch(this.ToString(), iClient.ToString(), iUserId.ToString(), method, ex.Message, ex.StackTrace);

                if (ex.Message.IndexOf("Unique") > 0)
                {
                    Session["nonunique"] = "true";
                }

                Page.ClientScript.RegisterStartupScript(this.GetType(), "messgebox", "displayMsg();", true);

            }
        }

        protected void epbGrid_RowDeleting(object sender, Trirand.Web.UI.WebControls.JQGridRowDeleteEventArgs e)
        {
            try
            {
                DataTable dt = dc.GetEmailReceivers(iClient, iButton);

                dt.PrimaryKey = new DataColumn[] { dt.Columns["Id"] };
                DataRow drow = dt.Rows.Find(e.RowKey);

                if (drow != null)
                {
                    int iRow = Convert.ToInt32(drow.ItemArray[0].ToString());
                    dc.DeleteEmail(iClient, iUserId, iRow);
                }

                epbGrid.DataSource = dc.GetEmailReceivers(iClient, iButton);
                epbGrid.DataBind();

            }
            catch (System.Exception ex)
            {
                string method = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();
                error.ErrorCatch(this.ToString(), iClient.ToString(), iUserId.ToString(), method, ex.Message, ex.StackTrace);
                Response.Redirect("/apperror.aspx", false);
            }
        }

        protected void epbGrid_RowEditing(object sender, Trirand.Web.UI.WebControls.JQGridRowEditEventArgs e)
        {
            try
            {
                DataTable dt = dc.GetEmailReceivers(iClient, iButton);

                dt.PrimaryKey = new DataColumn[] { dt.Columns["Id"] };
                DataRow row = dt.Rows.Find(e.RowKey);

                string sEmail = e.RowData["EmailAddress"];

                if (row != null)
                {
                    int iRow = Convert.ToInt32(row.ItemArray[0].ToString());
                    dc.UpdateEmailAddress(iClient, iUserId, iRow, sEmail);
                }
                epbGrid.DataSource = dt;
                epbGrid.DataBind();
            }


            catch (System.Exception ex)
            {
                string method = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();
                error.ErrorCatch(this.ToString(), iClient.ToString(), iUserId.ToString(), method, ex.Message, ex.StackTrace);

                if (ex.Message.IndexOf("Unique") > 0)
                {
                    Session["nonunique"] = "true";
                }


                Page.ClientScript.RegisterStartupScript(this.GetType(), "messgebox", "displayMsg();", true);

            }
        }

        protected void btnAddEmail_Click(object sender, EventArgs e)
        {
            try
            {
                string addresses = tbEmails.Text; // Regex.Replace(tbEmails.Text, "[^,0-9]", "");
                if (addresses != "")
                {
                    addresses = addresses.Replace(" ", ""); 
                    addresses = addresses.Replace(",", "|");
                    addresses = addresses.Replace(";", "|");
                    addresses = addresses.Replace(":", "|");
                    addresses = addresses.Replace("\t", "|");  //tab delimited

                    string[] nbrarray = addresses.Split('|');
                    dc.InsertMassEmailAddress(iClient, iUserId, iButton, nbrarray);
                    tbEmails.Text = "";
                }
                else
                {
                    lblMsg.Visible = true;
                    lblMsg.Text = "You must have at least one email address to add.";

                    Page.ClientScript.RegisterStartupScript(this.GetType(), "messgebox", "displayMsg();", true);
                }


            }

            catch (System.Exception ex)
            {
                string method = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();
                error.ErrorCatch(this.ToString(), iClient.ToString(), iUserId.ToString(), method, ex.Message, ex.StackTrace);
            }
        }
    }
}