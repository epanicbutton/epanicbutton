﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Login.Master" AutoEventWireup="true" CodeBehind="Recover.aspx.cs" Inherits="epanicbutton.Recover" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="container">
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8 header" align="center">
                <img src="../images/ePanic-Logo-Long-Black.png" style="height: 76px" />
                <i>ePanic Button</i> Recover Password
                    <hr />
            </div>
            <div class="col-md-2"></div>
        </div>
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">

                <!--   <asp:Label ID="lblEmail" runat="server" Text="Enter the email address associated with your account:"></asp:Label>-->


                <asp:TextBox ID="txtEmail" Placeholder="EMAIL" AltText="test" CssClass="form-control" runat="server"></asp:TextBox>



                <br />
            </div>

            <div class="col-md-3>"></div>
        </div>
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-6">
                        <asp:Button ID="btnSubmit" CssClass="primary-btn form-control" runat="server" Text="Reset Password" OnClick="btnSubmit_Click" /><br />
                    </div>
                    <div class="col-md-6">
                        <asp:Button ID="btnLogin" CssClass="secondary-btn form-control" runat="server" Text="Back to Login" OnClick="btnLogin_Click" CausesValidation="false" /><br />
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-12">
                <div class="login-warning">
                    <asp:RequiredFieldValidator runat="server" ID="reqName" ControlToValidate="txtEmail" ErrorMessage="Email address is required!" />
                </div>
                <div class="login-warning">

                    <asp:RegularExpressionValidator
                        ID="regEmail"
                        ControlToValidate="txtEmail"
                        Text="Invalid email"
                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                        runat="server" />
                </div>
                <div id="pwdMsg" class="ui-state-highlight login-warning">
                    <asp:Label ID="lblPasswordMsg" runat="server" Text="" Visible="false"></asp:Label>
                </div>
                </div>
        </div>
    </div>
</asp:Content>
