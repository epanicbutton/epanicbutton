﻿using System;
using System.Data;
using System.Linq;
using System.Web.UI;

namespace epanicbutton
{
    public partial class Buttons : System.Web.UI.Page
    {
        DataClass dc = new DataClass();
        ErrorHandler error = new ErrorHandler();
        Session session = new Session();
        int iClient, iUserId, iGroup;

        protected void Page_Load(object sender, EventArgs e)
        {

            if ((Session["clientid"] != null) && ((Session["userid"] != null)))
            {
                iClient = Convert.ToInt32(Session["clientid"].ToString());
                iUserId = Convert.ToInt32(Session["userid"].ToString());
                session.ClientId = Convert.ToInt32(Session["clientid"].ToString());
            }
            else
            {
                Response.Redirect("login.aspx");
            }

            try
            {
                if (!IsPostBack)
                {                   
                    LoadGroupDropDown();
                    LoadClientDropDown();
                } 

                epbGrid.DataSource = dc.GetClientButtons(iClient);
                epbGrid.DataBind();

            }
            catch (System.Exception ex)
            {
                string method = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();
                error.ErrorCatch(this.ToString(), iClient.ToString(), iUserId.ToString(), method, ex.Message, ex.StackTrace);
                Response.Redirect("/apperror.aspx", true);
            }
        }

        private void LoadGroupDropDown()
        {
            DataTable dtGroups = dc.GetGroups(iClient);

            ddlGroups.DataValueField = "ID";
            ddlGroups.DataTextField = "GroupName";
            ddlGroups.DataSource = dtGroups;
            ddlGroups.DataBind();
        }

        private void LoadClientDropDown()
        {
            DataTable dtClients = dc.GetClient(iClient);


            foreach (DataRow r in dtClients.Rows.Cast<DataRow>().Skip(1))
            {
                r.Delete();
            }


            ddlClients.DataValueField = "ID";
            ddlClients.DataTextField = "ClientName";
            ddlClients.DataSource = dtClients;
            ddlClients.DataBind();
        }
        
        protected void epbGrid_RowDeleting(object sender, Trirand.Web.UI.WebControls.JQGridRowDeleteEventArgs e)
        {            
            try
            {
                DataTable dt = dc.GetClientButtons(iClient);

                dt.PrimaryKey = new DataColumn[] { dt.Columns["Id"] };
                DataRow drow = dt.Rows.Find(e.RowKey);

                if (drow != null)
                {
                    int iRow = Convert.ToInt32(drow.ItemArray[0].ToString());
                    dc.DeleteButton(iClient, iUserId, iRow);

                    string iGroup = drow.ItemArray[3].ToString();

                    QueueClass q = new QueueClass();
                    q.CreateGroupQueueMsg(iGroup, iGroup, "update");
                }

                epbGrid.DataSource = dc.GetClientButtons(iClient);
                epbGrid.DataBind();

            }
            catch (System.Exception ex)
            {
                string method = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();
                error.ErrorCatch(this.ToString(), iClient.ToString(), iUserId.ToString(), method, ex.Message, ex.StackTrace);
                Response.Redirect("/apperror.aspx", false);
            }
        }

        protected void epbGrid_RowAdding(object sender, Trirand.Web.UI.WebControls.JQGridRowAddEventArgs e)
        {
            try
            {
                DataTable dt = dc.GetClientButtons(iClient);

                dt.PrimaryKey = new DataColumn[] { dt.Columns["Id"] };
                DataRow row = dt.NewRow();

                iGroup = Convert.ToInt32(e.RowData[1]);
                string sButtonName = e.RowData["ButtonName"];
                string sButtonTitle = e.RowData["ButtonTitle"];
                string sButtonMsg = e.RowData["ButtonMessage"];
                string sButtonIcon = e.RowData["ButtonIcon"];
                string sSeverity = e.RowData["Severity"].ToString();
                string sHotKey = e.RowData["HotKeyCode"].ToString();
                string sModKey = e.RowData["HotKeyModifier"].ToString();
                string sSendDelay = e.RowData["SendDelay"].ToString();
                int iMuteSound = Convert.ToInt32(e.RowData["Mute"].ToString()); 
               // int iGroupMsg = Convert.ToInt32(e.RowData["GroupMessage"].ToString());
                

                if (row != null)
                {
                    dc.InsertButton(iClient, iUserId, iGroup, sButtonName, sButtonTitle, sButtonMsg, sButtonIcon, sSeverity, sHotKey, sModKey, sSeverity, 0, sSendDelay, iMuteSound);

                    QueueClass q = new QueueClass();
                    q.CreateGroupQueueMsg(iGroup.ToString(), iGroup.ToString(), "update");
                }
                epbGrid.DataSource = dt;
                epbGrid.DataBind();
            }


            catch (System.Exception ex)
            {
                string method = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();
                error.ErrorCatch(this.ToString(), iClient.ToString(), iUserId.ToString(), method, ex.Message, ex.StackTrace);

                if (ex.Message.IndexOf("Unique") > 0)
                {
                    Session["nonunique"] = "true";
                }

                


            }
        }

        protected void epbGrid_RowEditing(object sender, Trirand.Web.UI.WebControls.JQGridRowEditEventArgs e)
        {
            try
            {
                DataTable dt = dc.GetClientButtons(iClient);

                dt.PrimaryKey = new DataColumn[] { dt.Columns["Id"] };
                DataRow drow = dt.Rows.Find(e.RowKey);

                string sButtonName = e.RowData["ButtonName"];
                string sButtonTitle = e.RowData["ButtonTitle"];
                string sButtonMsg = e.RowData["ButtonMessage"];
                string sButtonIcon = e.RowData["ButtonIcon"];
                string sSeverity = e.RowData["Severity"].ToString();
                string sHotKey = e.RowData["HotKeyCode"].ToString();
                string sModKey = e.RowData["HotKeyModifier"].ToString();
                string sSendDelay = e.RowData["SendDelay"].ToString();
                int iMuteSound = Convert.ToInt32(e.RowData["Mute"].ToString()); 
               // int iGroupMsg = Convert.ToInt32(e.RowData["GroupMessage"].ToString());
                int iGroup = Convert.ToInt32(e.RowData["GroupName"].ToString());

                if (drow != null)
                {
                    int iRow = Convert.ToInt32(drow.ItemArray[0].ToString());
                    dc.UpdateButton(iClient, iUserId, iRow, sButtonName, sButtonTitle, sButtonMsg, sButtonIcon, sSeverity, sHotKey, sModKey, sSeverity, 0, iGroup, sSendDelay, iMuteSound);

                    QueueClass q = new QueueClass();
                    q.CreateGroupQueueMsg(iGroup.ToString(), iGroup.ToString(), "update");
                }

                epbGrid.DataSource = dc.GetClientButtons(iClient);
                epbGrid.DataBind();

            }
            catch (System.Exception ex)
            {
                string method = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();
                error.ErrorCatch(this.ToString(), iClient.ToString(), iUserId.ToString(), method, ex.Message, ex.StackTrace);
                if (ex.Message.IndexOf("Unique") > 0)
                {
                    Session["nonunique"] = "true";
                }

            }
        }
    }
}