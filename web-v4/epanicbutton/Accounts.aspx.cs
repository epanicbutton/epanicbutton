﻿using System;
using System.Data;
using EasyNetQ.Management.Client;
using EasyNetQ.Management.Client.Model;
using System.Web.Services;
using epanicbutton.ViewModel;
using System.Collections.Generic;

namespace epanicbutton
{
    public partial class Accounts : System.Web.UI.Page
    {
        DataClass dc = new DataClass();
        Utility ut = new Utility();
        ErrorHandler error = new ErrorHandler();
        int iClient, iUserId;
        string sAdminType;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if ((Session["clientid"] != null) && ((Session["userid"] != null)))
                {
                    iClient = Convert.ToInt32(Session["clientid"].ToString());
                    iUserId = Convert.ToInt32(Session["userid"].ToString());
                    sAdminType = (Session["admintype"].ToString());
                }
                else
                {
                    Response.Redirect("login.aspx");
                }

                try
                {
                    if (sAdminType == "Global")
                    {
                        LoadTimezoneDropDown();

                        epbGrid.ToolBarSettings.ShowAddButton = true;
                        epbGrid.ToolBarSettings.ShowEditButton = true;
                        epbGrid.DataSource = dc.GetClient(0);
                        epbGrid.DataBind();
                    }
                    else
                    {
                        epbGrid.DataSource = dc.GetClient(iClient);
                        epbGrid.DataBind();
                    }
                }
                catch (System.Exception ex)
                {
                    string method = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();
                    error.ErrorCatch(this.ToString(), iClient.ToString(), iUserId.ToString(), method, ex.Message, ex.StackTrace);
                    Response.Redirect("/apperror.aspx", true);
                }
            }
        }

        private void LoadTimezoneDropDown()
        {
            List<TimeZoneModel> model = GetTimeZones();

            ddlTimeZones.DataValueField = "ID";
            ddlTimeZones.DataTextField = "DisplayName";
            ddlTimeZones.DataSource = model;
            ddlTimeZones.DataBind();
        }

        protected DataTable LoadAdminTypes()
        {
            DataTable dtAdminTypes = dc.GetAdminTypes();
            return dtAdminTypes;
        }

        [WebMethod]
        public static List<TimeZoneModel> GetTimeZones()
        {
            try
            {
                return Utility.AllTimeZones();
            }
            catch (Exception)
            {
                throw;
            }
        }

        protected void epbGrid_RowEditing(object sender, Trirand.Web.UI.WebControls.JQGridRowEditEventArgs e)
        {
            try
            {
                if (sAdminType == "Global")
                    iClient = 0;

                DataTable dt = dc.GetClient(iClient);

                dt.PrimaryKey = new DataColumn[] { dt.Columns["AdminsId"] };
                DataRow drow = dt.Rows.Find(e.RowKey);

                string sClientName = e.RowData["ClientName"];
                string sConn = e.RowData["TotalConnections"];
                int iActive = Convert.ToInt32(e.RowData["Active"].ToString());
                string sEmail = e.RowData["EmailAddress"].ToString();
                int iAdminid = Convert.ToInt32(e.RowData["AdminsId"].ToString());
                string sTimeZone = e.RowData["TimeZone"].ToString();

                if (drow != null)
                {
                    int iRow = Convert.ToInt32(drow.ItemArray[0].ToString());
                    dc.UpdateClient(iRow, iUserId, sClientName, sConn, iActive, iAdminid, sEmail, sTimeZone);
                }

                epbGrid.DataSource = dc.GetClient(iClient);
                epbGrid.DataBind();

            }
            catch (System.Exception ex)
            {
                string method = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();
                error.ErrorCatch(this.ToString(), iClient.ToString(), iUserId.ToString(), method, ex.Message, ex.StackTrace);
                Response.Redirect("/apperror.aspx", false);

            }

        }

        protected void epbGrid_RowAdding(object sender, Trirand.Web.UI.WebControls.JQGridRowAddEventArgs e)
        {
            try
            {
                DataTable dt = dc.GetClient(0);
                DataRow row = dt.NewRow();

                string ClientName = e.RowData["ClientName"];
                int iConn = 100;
                string sDate = e.RowData["InactiveDate"];
                string sAdminName = e.RowData["AdminName"];
                int iActive = Convert.ToInt32(e.RowData["Active"].ToString());
                string sEmail = e.RowData["EmailAddress"].ToString();
                string sTimeZone = e.RowData["TimeZone"].ToString();

                if (row != null)
                {
                    Utility utility = new Utility();
                    string scrubbedName = utility.scrubClientName(ClientName);

                    string clientGUID = dc.InsertClient(ClientName, iConn, iActive, sAdminName, sEmail, scrubbedName, sTimeZone);

                    QueueClass queue = new QueueClass();
                    if (!queue.CreateClientQueueObjects(scrubbedName, clientGUID))
                    {
                        string error = string.Empty;
                    }

                    Utility.SendWelcomeMessage(sEmail, sAdminName);
                }

                epbGrid.DataSource = dc.GetClient(0);
                epbGrid.DataBind();

            }
            catch (System.Exception ex)
            {
                string method = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();
                error.ErrorCatch(this.ToString(), iClient.ToString(), iUserId.ToString(), method, ex.Message, ex.StackTrace);
                Response.Redirect("/apperror.aspx", false);

            }
        }

    }
}