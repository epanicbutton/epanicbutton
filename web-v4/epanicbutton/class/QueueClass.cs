﻿using EasyNetQ;
using EasyNetQ.Management.Client;
using EasyNetQ.Management.Client.Model;
using RabbitMQ.Client;
using Microsoft.ServiceBus;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using Microsoft.ServiceBus.Messaging;

namespace epanicbutton
{
    public class QueueClass
    {
        private static string ConnectionString = ConfigurationManager.AppSettings["AzureConnectionString"];
        private static string url = ConfigurationManager.AppSettings["RabbitMQ.Host"];
        private static string admin = ConfigurationManager.AppSettings["RabbitMQ.Admin"];
        private static string pwd = ConfigurationManager.AppSettings["RabbitMQ.Pwd"];
        private static IPAddress[] IPList = Dns.GetHostAddresses(url);
        private static string hostIP = IPList[0].ToString();
        private readonly NamespaceManager namespaceManager = null;

        public bool CreateClientQueueObjects(string clientName, string clientGUID)
        {
            String[] creds = clientGUID.Split('-');

            string clientAdmin = creds[0].ToString().ToUpper();
            string clientPwd = creds[4].ToString().ToUpper();

            ErrorHandler eh = new ErrorHandler();
            eh.ErrorWrite(clientGUID + "|" + clientPwd);

            try
            {
                //Utility utility = new Utility();
                string newvHost = clientName;

                var initial = new ManagementClient(hostIP, admin, pwd);

                // first create a new virtual host
                var vhost = initial.CreateVirtualHost(newvHost);

                // next create a user for that virutal host
                var user = initial.CreateUser(new UserInfo(admin, pwd));
                var clientUser = initial.CreateUser(new UserInfo(clientAdmin, clientPwd));

                // give the new user all permissions on the virtual host
                initial.CreatePermission(new PermissionInfo(user, vhost));
                initial.CreatePermission(new PermissionInfo(clientUser, vhost));

                eh.ErrorWrite("Successfully created: " + newvHost);

                return true;
            }

            catch (Exception ex)
            {
                string error = ex.Message.ToString();
                eh.ErrorWrite(error);
                return false;
            }

        }

        public bool CreateGroupQueueMsg(string groupId, string newGroupId, string type)
        {
            ErrorHandler eh = new ErrorHandler();
            string clientGuid = HttpContext.Current.Session["ClientGuid"].ToString();
            string message = "update";

            try
            {
                //****************
                //this is a message to instruct the user to update its group       
                //we will need one similar that handles a brand new user.         
                if (type == "usermove")
                {
                    message = "group_change," + newGroupId;  //group_change,newgroupid
                }

                var client = TopicClient.CreateFromConnectionString(ConnectionString, clientGuid);
                string route = "update." + clientGuid + "." + groupId;
                var brokeredMessage = new BrokeredMessage(message)
                {
                    ContentType = "application/json",
                    Label = route,
                    Properties = 
                    {
                        { "RoutingKey", route }
                    }
                };
                client.SendAsync(brokeredMessage);
                return true;
            }

            catch (Exception x)
            {
                string error = x.Message.ToString();                
                eh.ErrorWrite("CreateGroupQueueMsg - " + error);
                return false;
            }
        }

        //proceess update from Admin calls this method
        public bool CreateAutoUpdateQueueMsg(int[] groupID)
        {

            //call this when the client admin clicks the process update button
            ErrorHandler eh = new ErrorHandler();
            string clientGuid = HttpContext.Current.Session["ClientGuid"].ToString();
            string message = "update";

            try
            {
                //drop message on the queue for the groupID passed in..
                //loop through the groupID collection here
                var client = TopicClient.CreateFromConnectionString(ConnectionString, clientGuid);

                string route = "update." + clientGuid + "." + groupID;
                var brokeredMessage = new BrokeredMessage(message)
                {
                    ContentType = "application/json",
                    Label = route,
                    Properties = 
                    {
                        { "RoutingKey", route }
                    }
                };
                client.SendAsync(brokeredMessage);
                var body = brokeredMessage.GetBody<string>();

                return true;
            }

            catch (Exception x)
            {
                string error = x.Message.ToString();
                eh.ErrorWrite("CreateGroupQueueMsg - " + error);
                return false;
            }


        }
    }
}
