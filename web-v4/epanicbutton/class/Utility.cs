﻿using epanicbutton.ViewModel;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Text.RegularExpressions;
using System.Web;
using System.Linq;


namespace epanicbutton
{
    public class Utility
    {

        static string mailgunKey = (ConfigurationManager.AppSettings["mailgun.key"]);
        static string mailgunDomain = (ConfigurationManager.AppSettings["mailgun.domain"]);  /*     */

        public string MakeSafeParameter(string p)
        {
            string sSafeParam = p;
            //-- Replace spacial characters
            //sSafeParam = sSafeParam;
            sSafeParam = sSafeParam.Replace("';", string.Empty);
            sSafeParam = sSafeParam.Replace("'%3B", string.Empty);
            // sSafeParam = sSafeParam.Replace("--", "_");
            sSafeParam = sSafeParam.Replace("'", "''");
            sSafeParam = sSafeParam.Replace(";", "_");
            sSafeParam = sSafeParam.Replace("%3B", "");
            sSafeParam = sSafeParam.Replace("exec", string.Empty);
            sSafeParam = sSafeParam.Replace("cast(", string.Empty);
            sSafeParam = sSafeParam.Replace("%20or%20", "");
            sSafeParam = sSafeParam.Replace(" or ", "");
            sSafeParam = sSafeParam.Replace("=", "");
            sSafeParam = sSafeParam.Replace("set", "");
            sSafeParam = sSafeParam.Replace("update", "");
            sSafeParam = sSafeParam.Replace("varchar", "");
            sSafeParam = sSafeParam.Replace("ntext", "");
            sSafeParam = sSafeParam.Replace("%20from%20", "");
            sSafeParam = sSafeParam.Replace("--", string.Empty);
            sSafeParam = sSafeParam.Replace("%27", string.Empty);
            sSafeParam = sSafeParam.Replace("<", string.Empty);
            sSafeParam = sSafeParam.Replace(">", string.Empty);
            sSafeParam = sSafeParam.Replace("replace(", string.Empty);
            sSafeParam = sSafeParam.Replace("char(", string.Empty);
            sSafeParam = sSafeParam.Replace(";)", string.Empty);
            sSafeParam = sSafeParam.Replace(";", string.Empty);
            sSafeParam = sSafeParam.Replace("{", string.Empty);
            sSafeParam = sSafeParam.Replace("}", string.Empty);
            sSafeParam = sSafeParam.Replace("//", string.Empty);
            sSafeParam = sSafeParam.Replace("function", string.Empty);
            sSafeParam = sSafeParam.Replace("(", string.Empty);
            sSafeParam = sSafeParam.Replace(")", string.Empty);
            sSafeParam = sSafeParam.Replace("\"", string.Empty);
            sSafeParam = sSafeParam.Replace("xp_", string.Empty);
            sSafeParam = sSafeParam.Replace("begin", string.Empty);
            sSafeParam = sSafeParam.Replace("cursor", string.Empty);
            sSafeParam = sSafeParam.Replace("declare", string.Empty);
            sSafeParam = sSafeParam.Replace("execute", string.Empty);
            sSafeParam = sSafeParam.Replace("fetch", string.Empty);
            sSafeParam = sSafeParam.Replace("kill", string.Empty);
            sSafeParam = sSafeParam.Replace("open", string.Empty);
            sSafeParam = sSafeParam.Replace("insert", string.Empty);
            sSafeParam = sSafeParam.Replace("select", string.Empty);
            sSafeParam = sSafeParam.Replace("sys ", string.Empty);
            sSafeParam = sSafeParam.Replace("sysobjects", string.Empty);
            sSafeParam = sSafeParam.Replace("syscolumns", string.Empty);
            sSafeParam = sSafeParam.Replace("table", string.Empty);
            sSafeParam = sSafeParam.Replace("drop", string.Empty);
            sSafeParam = sSafeParam.Replace("alter", string.Empty);
            sSafeParam = sSafeParam.Replace("create", string.Empty);
            sSafeParam = sSafeParam.Replace("rename", string.Empty);
            sSafeParam = sSafeParam.Replace("delete", string.Empty);
            sSafeParam = sSafeParam.Replace("iframe ", string.Empty);

            return sSafeParam;
        }

        public bool ValidateEmailAddress(string emailString, int maxLength)
        {

            bool result = false;
            if (String.IsNullOrWhiteSpace(emailString))
                return result;

            bool isEmail = Regex.IsMatch(emailString, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z");


            if (isEmail && emailString.Length <= maxLength)
            {
                result = true;
            }
            else
            {
                result = false;
            }

            return result;
        }

        public bool ValidatePasswordStrength(string pwd, int minLength)
        {

            //The password length must be greater than or equal to 8
            //The password must contain one or more uppercase characters
            //The password must contain one or more lowercase characters
            //The password must contain one or more numeric values
            //The password must contain one or more special characters, i.e. @ # $ % !

            bool result = false;
            if (String.IsNullOrWhiteSpace(pwd))
                return result;

            bool isStrong = Regex.IsMatch(pwd, @"(?=^.{8,}$)(?=.*\d)(?=.*[!@#$%^&*]+)(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$");


            if (isStrong && pwd.Length >= minLength)
            {
                result = true;
            }
            else
            {
                result = false;
            }

            return result;
        }

        public static IRestResponse SendNewAccountMessage(string AdminEmail, string AdminName)
        {
            DataClass dc = new DataClass();
            DataTable dt = new DataTable();
            string message = string.Empty;
            ErrorHandler eh = new ErrorHandler();
            eh.ErrorWrite("SendNewAccountMessage started");


            dt = dc.GetMessage("Welcome");
            message = dt.Rows[0]["MessageContent"].ToString();

            message = message.Replace("[[USERNAME]]", AdminName);

            //DataClass evt = new DataClass();
            //evt.InsertEvent(1, 1, "Welcome Message: " + message.ToString());

            try
            {
                RestClient client = new RestClient();
                client.BaseUrl = "https://api.mailgun.net/v2";
                client.Authenticator =
                        new HttpBasicAuthenticator("api",
                                                   mailgunKey);
                //key-4a-zo18hpxv1zq-8ruhbbnqpbgo1o9h0  --verified
                RestRequest request = new RestRequest();
                request.AddParameter("domain",
                                     mailgunDomain, ParameterType.UrlSegment);
                request.Resource = "{domain}/messages";
                request.AddParameter("from", "ePanic Admin <support@epanicbutton.com>");
                request.AddParameter("to", AdminEmail);
                //request.AddParameter("bcc", "contact@epanicbutton.com");
                //request.AddParameter("bcc", "andrew.wing@wingweb.com");
                //request.AddParameter("bcc", "drew.wing@gmail.com");
                request.AddParameter("subject", "ePanic Button Admin Account Created");
                request.AddParameter("html", message);
                request.Method = Method.POST;
                return null;
                //return client.Execute(request);
            }
            catch (Exception x)
            {
                string e = x.Message.ToString();
                return null;
            }

            eh.ErrorWrite("SendNewAccountMessage ended");

        }

        public static IRestResponse SendWelcomeMessage(string AdminEmail, string AdminName)
        {
            try
            {
                RestClient client = new RestClient();
                client.BaseUrl = "https://api.mailgun.net/v2";
                client.Authenticator =
                        new HttpBasicAuthenticator("api",
                                                   mailgunKey);
                RestRequest request = new RestRequest();
                request.AddParameter("domain",
                                     mailgunDomain, ParameterType.UrlSegment);
                request.Resource = "{domain}/messages";
                request.AddParameter("from", "ePanic Admin <support@epanicbutton.com>");
                request.AddParameter("to", AdminEmail);
                //request.AddParameter("to", "drew.wing@gmail.com");
                request.AddParameter("subject", "ePanicButton Admin Account Created");
                request.AddParameter("text", "Your admin account for ePanic Button has been successfully created.\n\nGo to https://eq.epanicbutton.com to login.\n\nYour account is " + AdminName + ".\nYour temporary password is eP@nic123.\n\nYou will need to change the password once you login for the first time.");
                request.Method = Method.POST;
                //return null;
                return client.Execute(request);

            }
            catch (Exception x)
            {
                string e = x.Message.ToString();
                return null;
            }
        }

        public static IRestResponse SendSimpleMessage(string Email, string Message)
        {
            try
            {
                TimeZone localzone = TimeZone.CurrentTimeZone;

                RestClient client = new RestClient();
                client.BaseUrl = "https://api.mailgun.net/v2";
                client.Authenticator =
                        new HttpBasicAuthenticator("api",
                                                   mailgunKey);
                RestRequest request = new RestRequest();
                request.AddParameter("domain",
                                     mailgunDomain, ParameterType.UrlSegment);
                request.Resource = "{domain}/messages";
                request.AddParameter("from", "ePanic Admin <alert@epanicbutton.com>");
                request.AddParameter("to", Email);
                //request.AddParameter("to", "drew.wing@gmail.com");
                request.AddParameter("subject", "ePanicButton Password Reset");
                request.AddParameter("text", Message);
                request.Method = Method.POST;

                return client.Execute(request);
            }
            catch (Exception x)
            {
                string e = x.Message.ToString();
                return null;
            }
        }

        public static IRestResponse SendSimpleHtmlMessage(string Email, string Message)
        {
            try
            {
                TimeZone localzone = TimeZone.CurrentTimeZone;

                RestClient client = new RestClient();
                client.BaseUrl = "https://api.mailgun.net/v2";
                client.Authenticator =
                        new HttpBasicAuthenticator("api",
                                                   mailgunKey);
                RestRequest request = new RestRequest();
                request.AddParameter("domain",
                                     mailgunDomain, ParameterType.UrlSegment);
                request.Resource = "{domain}/messages";
                request.AddParameter("from", "ePanic Admin <alert@epanicbutton.com>");
                request.AddParameter("to", Email);
                //request.AddParameter("to", "drew.wing@gmail.com");
                request.AddParameter("subject", "ePanicButton Password Reset");
                request.AddParameter("html", Message);
                request.Method = Method.POST;

                return client.Execute(request);
            }
            catch (Exception x)
            {
                string e = x.Message.ToString();
                return null;
            }
        }

        public string scrubClientName(string clientName)
        {
            string newName = clientName;
            newName = newName.Replace(" ", string.Empty);
            newName = newName.Replace("'", string.Empty);
            newName = newName.Replace("&", string.Empty);
            newName = newName.Replace("\"", string.Empty);
//
//  20160311 - JH
//  Replace all non-alphanumeric characters with an empty character ""
//
            newName = Regex.Replace(newName, "[^a-zA-Z0-9_.]+", "", RegexOptions.Compiled);

            return newName;
        }

        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }

        public static List<TimeZoneModel> AllTimeZones()
        {
            List<TimeZoneModel> model = HttpContext.Current.Cache["_TimeZone"] as List<TimeZoneModel>;
            if (model == null)
            {
                model = TimeZoneInfo.GetSystemTimeZones().Select(s => new TimeZoneModel { ID = s.Id, DisplayName = s.DisplayName }).ToList();
            }
            return model;
        }

        public static DateTime ConvertTimeFromUtc(DateTime DateTime, TimeZoneInfo TimeZoneInfo)
        {
            return TimeZoneInfo.ConvertTimeFromUtc(DateTime, TimeZoneInfo);
        }

        public static TimeZoneInfo GetTimeZoneInfo(string TimezoneID)
        {
            return TimeZoneInfo.FindSystemTimeZoneById(TimezoneID);
        }


        public static string GeneratePassword()
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            var random = new Random();
            var result = new string(
                Enumerable.Repeat(chars, 10)
                          .Select(s => s[random.Next(s.Length)])
                          .ToArray());
            return result;
        }
    }
}