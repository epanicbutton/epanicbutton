﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;

namespace epanicbutton
{
    public class DataClass
    {
        Utility utility = new Utility();

        public DataSet dSet(SqlCommand cmd)
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
            {
                //go get the form info and data 
                DataSet ds = new DataSet();
                SqlDataAdapter sda = new SqlDataAdapter();
                cmd.CommandType = CommandType.Text;
                cmd.Connection = conn;

                sda.SelectCommand = cmd;
                sda.Fill(ds);

                return ds;
            }
        }

        public DataTable dTable(SqlCommand cmd)
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
            {
                //go get the form info and data 
                DataTable dt = new DataTable();
                SqlDataAdapter sda = new SqlDataAdapter();
                cmd.CommandType = CommandType.Text;
                cmd.Connection = conn;

                sda.SelectCommand = cmd;
                sda.Fill(dt);

                return dt;
            }
        }

        public DataTable GetActivity()
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
            {
                string cmd = "select * from Activity order by id desc;";
                conn.Open();
                SqlCommand sqlCmd = new SqlCommand(cmd, conn);

                SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);

                sqlDa.Fill(dt);
            }
            return dt;
        }

        public DataTable GetGroups(int iClient)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
            {
                string cmd = "exec GetGroups @clientsid";
                conn.Open();
                SqlCommand sqlCmd = new SqlCommand(cmd, conn);

                sqlCmd.Parameters.AddWithValue("@clientsid", iClient);

                SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);

                sqlDa.Fill(dt);
            }
            return dt;
        }

        public DataTable GetAllMessages()
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
            {
                string cmd = "exec GetAllMessages";
                conn.Open();
                SqlCommand sqlCmd = new SqlCommand(cmd, conn);

                SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);

                sqlDa.Fill(dt);
            }
            return dt;
        }

        public DataTable GetMessageById(int iMessageId)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
            {
                string cmd = "exec GetMessageById @messageid";
                conn.Open();
                SqlCommand sqlCmd = new SqlCommand(cmd, conn);

                sqlCmd.Parameters.AddWithValue("@messageid", iMessageId);

                SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);

                sqlDa.Fill(dt);
            }
            return dt;
        }

        public DataTable GetMessage(string sMessage)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
            {
                string cmd = "exec GetMessage @msg";
                conn.Open();
                SqlCommand sqlCmd = new SqlCommand(cmd, conn);

                sqlCmd.Parameters.AddWithValue("@msg", sMessage);

                SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);

                sqlDa.Fill(dt);
            }
            return dt;
        }

        public DataTable GetClient(int iClient)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
            {
                string cmd = "exec GetClient @clientsid";
                conn.Open();
                SqlCommand sqlCmd = new SqlCommand(cmd, conn);

                sqlCmd.Parameters.AddWithValue("@clientsid", iClient);

                SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);

                sqlDa.Fill(dt);
            }
            return dt;
        }


        public DataTable GetClientButtons(int iClient)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
            {
                string cmd = "exec GetClientButtons @clientsid";
                conn.Open();
                SqlCommand sqlCmd = new SqlCommand(cmd, conn);

                sqlCmd.Parameters.AddWithValue("@clientsid", iClient);

                SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);

                sqlDa.Fill(dt);
            }
            return dt;
        }

        public DataTable GetSingleButton(int iClient, int iButton)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
            {
                string cmd = "exec GetSingleButton @clientsid, @buttonsid";
                conn.Open();
                SqlCommand sqlCmd = new SqlCommand(cmd, conn);

                sqlCmd.Parameters.AddWithValue("@clientsid", iClient);
                sqlCmd.Parameters.AddWithValue("@buttonsid", iButton);

                SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);

                sqlDa.Fill(dt);
            }
            return dt;
        }

        public DataTable GetSingleGroup(int iClient, int iGroup)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
            {
                string cmd = "exec GetSingleGroup @clientsid, @groupid";
                conn.Open();
                SqlCommand sqlCmd = new SqlCommand(cmd, conn);

                sqlCmd.Parameters.AddWithValue("@clientsid", iClient);
                sqlCmd.Parameters.AddWithValue("@groupid", iGroup);

                SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);

                sqlDa.Fill(dt);
            }
            return dt;
        }

        public DataTable GetButton(int iClient, int iGroup, int iButton)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
            {
                string cmd = "exec GetButton @clientsid, @groupid, @buttonid";
                conn.Open();
                SqlCommand sqlCmd = new SqlCommand(cmd, conn);

                sqlCmd.Parameters.AddWithValue("@clientsid", iClient);
                sqlCmd.Parameters.AddWithValue("@groupid", iGroup);
                sqlCmd.Parameters.AddWithValue("@buttonid", iButton);

                SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);

                sqlDa.Fill(dt);
            }
            return dt;
        }

        public DataTable GetUsers(int iClient)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
            {
                string cmd = "exec GetUsers @clientsid";
                conn.Open();
                SqlCommand sqlCmd = new SqlCommand(cmd, conn);

                sqlCmd.Parameters.AddWithValue("@clientsid", iClient);

                SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);

                sqlDa.Fill(dt);
            }
            return dt;
        }

        public DataTable GetReceivers(int iClient, int iButton)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
            {
                string cmd = "exec GetButtonReceivers @clientsid, @buttonid";
                conn.Open();
                SqlCommand sqlCmd = new SqlCommand(cmd, conn);

                sqlCmd.Parameters.AddWithValue("@clientsid", iClient);
                sqlCmd.Parameters.AddWithValue("@buttonid", iButton);

                SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);

                sqlDa.Fill(dt);
            }
            return dt;
        }

        public DataTable GetSMSReceivers(int iClient, int iButton)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
            {
                string cmd = "exec GetSMSReceivers @clientsid, @buttonid";
                conn.Open();
                SqlCommand sqlCmd = new SqlCommand(cmd, conn);

                sqlCmd.Parameters.AddWithValue("@clientsid", iClient);
                sqlCmd.Parameters.AddWithValue("@buttonid", iButton);

                SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);

                sqlDa.Fill(dt);
            }
            return dt;
        }

        public DataTable GetEmailReceivers(int iClient, int iButton)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
            {
                string cmd = "exec GetEmailReceivers @clientsid, @buttonid";
                conn.Open();
                SqlCommand sqlCmd = new SqlCommand(cmd, conn);

                sqlCmd.Parameters.AddWithValue("@clientsid", iClient);
                sqlCmd.Parameters.AddWithValue("@buttonid", iButton);

                SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);

                sqlDa.Fill(dt);
            }
            return dt;
        }

        public DataTable GetMachineReceivers(int iClient, int iButton)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
            {
                string cmd = "exec GetMachineReceivers @clientsid, @buttonid";
                conn.Open();
                SqlCommand sqlCmd = new SqlCommand(cmd, conn);

                sqlCmd.Parameters.AddWithValue("@clientsid", iClient);
                sqlCmd.Parameters.AddWithValue("@buttonid", iButton);

                SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);

                sqlDa.Fill(dt);
            }
            return dt;
        }

        public DataTable GetAvailableMachineReceivers(int iClient, int iButton)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
            {
                string cmd = "exec GetAvailableMachineReceivers @clientsid, @buttonid";
                conn.Open();
                SqlCommand sqlCmd = new SqlCommand(cmd, conn);

                sqlCmd.Parameters.AddWithValue("@clientsid", iClient);
                sqlCmd.Parameters.AddWithValue("@buttonid", iButton);

                SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);

                sqlDa.Fill(dt);
            }
            return dt;
        }

        public DataTable GetGroupMembers(int iClient, int iGroup)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
            {
                string cmd = "exec GetGroupMembers @clientsid, @groupid";
                conn.Open();
                SqlCommand sqlCmd = new SqlCommand(cmd, conn);

                sqlCmd.Parameters.AddWithValue("@clientsid", iClient);
                sqlCmd.Parameters.AddWithValue("@groupid", iGroup);

                SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);

                sqlDa.Fill(dt);
            }
            return dt;
        }

        public DataTable GetAvailableMembers(int iClient, int iGroup)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
            {
                string cmd = "exec GetAvailableMembers @clientsid, @groupid";
                conn.Open();
                SqlCommand sqlCmd = new SqlCommand(cmd, conn);

                sqlCmd.Parameters.AddWithValue("@clientsid", iClient);
                sqlCmd.Parameters.AddWithValue("@groupid", iGroup);

                SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);

                sqlDa.Fill(dt);
            }
            return dt;
        }

        public DataTable GetSingleAdmin(int iClient, int iAdmin)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
            {
                string cmd = "exec GetAdmin @clientsid, @adminid";
                conn.Open();
                SqlCommand sqlCmd = new SqlCommand(cmd, conn);

                sqlCmd.Parameters.AddWithValue("@clientsid", iClient);
                sqlCmd.Parameters.AddWithValue("@adminid", iAdmin);

                SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);

                sqlDa.Fill(dt);
            }
            return dt;
        }

        public DataTable GetAdmins(int iClient)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
            {
                string cmd = "exec GetAdminUsers @clientsid";
                conn.Open();
                SqlCommand sqlCmd = new SqlCommand(cmd, conn);

                sqlCmd.Parameters.AddWithValue("@clientsid", iClient);

                SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);

                sqlDa.Fill(dt);
            }
            return dt;
        }

        public DataTable GetAdminTypes()
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
            {
                string cmd = "exec GetAdminTypes";
                conn.Open();
                SqlCommand sqlCmd = new SqlCommand(cmd, conn);

                SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);

                sqlDa.Fill(dt);
            }
            return dt;
        }

        public DataTable GetClients()
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
            {
                string cmd = "select ClientsId as ID, ClientName as Name from Clients order by ClientName; ";
                conn.Open();
                SqlCommand sqlCmd = new SqlCommand(cmd, conn);

                SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);

                sqlDa.Fill(dt);
            }
            return dt;
        }

        public DataTable GetUserClient(int iClient)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
            {
                string cmd = "select ClientsId as ID, ClientName as Name from Clients where ClientsId = @clientsid order by ClientName; ";
                conn.Open();
                SqlCommand sqlCmd = new SqlCommand(cmd, conn);
                sqlCmd.Parameters.AddWithValue("@clientsid", iClient);

                SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);

                sqlDa.Fill(dt);
            }
            return dt;
        }

        public DataTable GetEvents(int iClient)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
            {
                string cmd = "exec GetEvents @clientsid";
                conn.Open();
                SqlCommand sqlCmd = new SqlCommand(cmd, conn);

                sqlCmd.Parameters.AddWithValue("@clientsid", iClient);

                SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);

                sqlDa.Fill(dt);
            }
            return dt;
        }

        public bool AdminExist(string sAdminName)
        {
            bool exist = false;
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
            {
                string cmd = "exec AdminExist @username";
                conn.Open();
                SqlCommand sqlCmd = new SqlCommand(cmd, conn);

                sqlCmd.Parameters.AddWithValue("@username", sAdminName);

                SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);

                sqlDa.Fill(dt);
            }
            if (dt.Rows.Count > 0)
                exist = true;

            return exist;
        }

        public bool ClientExist(string sClientName)
        {
            bool exist = false;
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
            {
                string cmd = "exec ClientExist @name";
                conn.Open();
                SqlCommand sqlCmd = new SqlCommand(cmd, conn);

                sqlCmd.Parameters.AddWithValue("@name", sClientName);

                SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);

                sqlDa.Fill(dt);
            }
            if (dt.Rows.Count > 0)
                exist = true;

            return exist;
        }

        public bool HKExist(int Client, int Group, int Button, string Key, string Modifier)
        {
            bool exist = false;
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
            {
                string cmd = "exec HotKeyComboExist @clientsid, @groupsid, @buttonsid, @key, @modifier";
                conn.Open();
                SqlCommand sqlCmd = new SqlCommand(cmd, conn);

                sqlCmd.Parameters.AddWithValue("@clientsid", Client);
                sqlCmd.Parameters.AddWithValue("@groupsid", Group);
                sqlCmd.Parameters.AddWithValue("@buttonsid", Button);
                sqlCmd.Parameters.AddWithValue("@key", Key);
                sqlCmd.Parameters.AddWithValue("@modifier", Modifier);

                SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);

                sqlDa.Fill(dt);
            }
            if (dt.Rows.Count > 0)
                exist = true;

            return exist;

        }

        public bool IconExist(int Client, int Group, int Button, string Icon)
        {
            bool exist = false;
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
            {
                string cmd = "exec ButtonImageExist @clientsid, @groupsid, @buttonsid, @buttonicon";
                conn.Open();
                SqlCommand sqlCmd = new SqlCommand(cmd, conn);

                sqlCmd.Parameters.AddWithValue("@clientsid", Client);
                sqlCmd.Parameters.AddWithValue("@groupsid", Group);
                sqlCmd.Parameters.AddWithValue("@buttonsid", Button);
                sqlCmd.Parameters.AddWithValue("@buttonicon", Icon);

                SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);

                sqlDa.Fill(dt);
            }
            if (dt.Rows.Count > 0)
                exist = true;

            return exist;

        }

        public bool DeleteUsers(int iClient, int iAdmin, int iUser)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
                {
                    string cmd = "exec DeleteUser @clientsid, @userid";
                    conn.Open();
                    SqlCommand sqlCmd = new SqlCommand(cmd, conn);

                    sqlCmd.Parameters.AddWithValue("@clientsid", iClient);
                    sqlCmd.Parameters.AddWithValue("@userid", iUser);

                    SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);

                    sqlCmd.ExecuteNonQuery();

                    InsertEvent(iClient, iAdmin, "Deleted userid: " + iUser.ToString());

                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public int DeleteAdmin(int iClient, int iAdmin, int iUser)
        {
            int returnId = 0;
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
                {
                    //string cmd = "exec DeleteAdmin @clientsid, @adminid";
                    //conn.Open();
                    //SqlCommand sqlCmd = new SqlCommand(cmd, conn);

                    //sqlCmd.Parameters.AddWithValue("@clientsid", iClient);
                    //sqlCmd.Parameters.AddWithValue("@adminid", iAdmin);

                    //SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);

                    //sqlCmd.ExecuteNonQuery();

                    SqlCommand sqlCmd = new SqlCommand();
                    sqlCmd.Connection = conn;
                    conn.Open();
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    sqlCmd.CommandText = "DeleteAdmin";
                    sqlCmd.Parameters.Add("@clientid", SqlDbType.BigInt).Value = iClient;
                    sqlCmd.Parameters.Add("@adminid", SqlDbType.BigInt).Value = iAdmin;

                    SqlParameter result = sqlCmd.Parameters.Add("@return", SqlDbType.Int);
                    result.Direction = ParameterDirection.Output;
                    result.Value = 0;

                    SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);
                    sqlCmd.ExecuteNonQuery();
                    returnId = Convert.ToInt32(result.Value.ToString());

                    InsertEvent(iClient, iUser, "Deleted adminid: " + iAdmin.ToString());

                    return returnId;

                }
            }
            catch (Exception ex)
            {
                return -1;

            }
        }

        public bool DeleteSelectedUsers(int iClient, string[] sUserIds)
        {
            try
            {

                string sUser = string.Empty;
                for (int i = 0; i < sUserIds.Length; i++)
                {

                    using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
                    {
                        sUser = sUserIds[i].ToString();
                        string cmd = "exec DeleteUser @clientsid, @usersid";
                        conn.Open();
                        SqlCommand sqlCmd = new SqlCommand(cmd, conn);

                        sqlCmd.Parameters.AddWithValue("@clientsid", iClient);
                        sqlCmd.Parameters.AddWithValue("@usersid", Convert.ToInt32(sUser));

                        SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);

                        sqlCmd.ExecuteNonQuery();
                        InsertEvent(1, 131, "Deleted user: " + sUser.ToString());
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public bool DeleteSelectedEmails(int iClient, string[] sIds)
        {
            try
            {

                string sUser = string.Empty;
                for (int i = 0; i < sIds.Length; i++)
                {

                    using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
                    {
                        sUser = sIds[i].ToString();
                        string cmd = "exec DeleteEmail @clientsid, @usersid";
                        conn.Open();
                        SqlCommand sqlCmd = new SqlCommand(cmd, conn);

                        sqlCmd.Parameters.AddWithValue("@clientsid", iClient);
                        sqlCmd.Parameters.AddWithValue("@usersid", Convert.ToInt32(sUser));

                        SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);

                        sqlCmd.ExecuteNonQuery();
                        InsertEvent(1, 131, "Deleted user: " + sUser.ToString());
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public bool DeleteSelectedSMS(int iClient, string[] sIds)
        {
            try
            {
                string sUser = string.Empty;
                for (int i = 0; i < sIds.Length; i++)
                {

                    using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
                    {
                        sUser = sIds[i].ToString();
                        string cmd = "exec DeleteSMSNumber @clientsid, @usersid";
                        conn.Open();
                        SqlCommand sqlCmd = new SqlCommand(cmd, conn);

                        sqlCmd.Parameters.AddWithValue("@clientsid", iClient);
                        sqlCmd.Parameters.AddWithValue("@usersid", Convert.ToInt32(sUser));

                        SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);

                        sqlCmd.ExecuteNonQuery();
                        InsertEvent(1, 131, "Deleted user: " + sUser.ToString());
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public bool DeleteGroup(int iClient, int iAdmin, string sGroupName, string sGroupId)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
                {
                    string cmd = "exec DeleteGroup @clientsid, @groupid";
                    conn.Open();
                    SqlCommand sqlCmd = new SqlCommand(cmd, conn);

                    sqlCmd.Parameters.AddWithValue("@clientsid", iClient);
                    sqlCmd.Parameters.AddWithValue("@groupid", sGroupId);

                    SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);

                    sqlCmd.ExecuteNonQuery();
                    InsertEvent(iClient, iAdmin, "Deleted group: " + sGroupName.ToString());

                    return true;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler eh = new ErrorHandler();
                eh.ErrorWrite(ex.ToString());
                return false;
            }

        }

        public bool DeleteButton(int iClient, int iAdmin, int iButton)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
                {
                    string cmd = "exec DeleteButton @clientsid, @buttonid";
                    conn.Open();
                    SqlCommand sqlCmd = new SqlCommand(cmd, conn);

                    sqlCmd.Parameters.AddWithValue("@clientsid", iClient);
                    sqlCmd.Parameters.AddWithValue("@buttonid", iButton);

                    SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);

                    sqlCmd.ExecuteNonQuery();
                    InsertEvent(iClient, iAdmin, "Deleted record buttonsid: " + iButton.ToString());

                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public bool DeleteSMSNumber(int iClient, int iAdmin, int iSmsId)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
                {
                    string cmd = "exec DeleteSMSNumber @clientsid, @smsid";
                    conn.Open();
                    SqlCommand sqlCmd = new SqlCommand(cmd, conn);

                    sqlCmd.Parameters.AddWithValue("@clientsid", iClient);
                    sqlCmd.Parameters.AddWithValue("@smsid", iSmsId);

                    SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);

                    sqlCmd.ExecuteNonQuery();
                    InsertEvent(iClient, iAdmin, "Deleted record smsid: " + iSmsId.ToString());

                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool DeleteEmail(int iClient, int iAdmin, int iEmailId)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
                {
                    string cmd = "exec DeleteEmail @clientsid, @emailsid";
                    conn.Open();
                    SqlCommand sqlCmd = new SqlCommand(cmd, conn);

                    sqlCmd.Parameters.AddWithValue("@clientsid", iClient);
                    sqlCmd.Parameters.AddWithValue("@emailsid", iEmailId);

                    SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);

                    sqlCmd.ExecuteNonQuery();
                    InsertEvent(iClient, iAdmin, "Deleted record emailsid: " + iEmailId.ToString());
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool DeleteMachineReceiver(int iClient, int iAdmin, int iButton, string sButton, string sReceivers)
        {
            try
            {
                sReceivers = sReceivers.Replace(",", "|");
                string[] ids = sReceivers.Split('|');
                string sUser = string.Empty;
                int iGroup = 0;
                for (int i = 0; i < ids.Length; i++)
                {

                    using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
                    {

                        sUser = ids[i].ToString();

                        SqlCommand sqlCmd = new SqlCommand();
                        sqlCmd.Connection = conn;
                        conn.Open();
                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        sqlCmd.CommandText = "DeleteMachineReceiver";
                        sqlCmd.Parameters.AddWithValue("@clientsid", iClient);
                        sqlCmd.Parameters.AddWithValue("@buttonsid", iButton);
                        sqlCmd.Parameters.AddWithValue("@usersid", Convert.ToInt32(sUser));

                        SqlParameter groupId = sqlCmd.Parameters.Add("@groupid", SqlDbType.BigInt);
                        groupId.Direction = ParameterDirection.Output;
                        groupId.Value = 0;

                        SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);

                        sqlCmd.ExecuteNonQuery();

                        iGroup = Convert.ToInt32(groupId.Value.ToString());

                        QueueClass q = new QueueClass();
                        q.CreateGroupQueueMsg(iGroup.ToString(), iGroup.ToString(), "update");
                        //sUser = ids[i].ToString();
                        //string cmd = "exec DeleteMachineReceiver @clientsid, @buttonsid, @usersid";
                        //conn.Open();
                        //SqlCommand sqlCmd = new SqlCommand(cmd, conn);

                        //sqlCmd.Parameters.AddWithValue("@clientsid", iClient);
                        //sqlCmd.Parameters.AddWithValue("@buttonsid", iButton);
                        //sqlCmd.Parameters.AddWithValue("@usersid", Convert.ToInt32(sUser));

                        //SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);

                        //sqlCmd.ExecuteNonQuery();
                        InsertEvent(iClient, iAdmin, "Deleted receivers for button: " + sButton.ToString());
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public int MoveGroupMember(int iClient, int iAdmin, int iGroup, string sGroup, string sMembers)
        {
            int oldId = 0;

            try
            {
                sMembers = sMembers.Replace(",", "|");
                string[] ids = sMembers.Split('|');
                string sUser = string.Empty;
                for (int i = 0; i < ids.Length; i++)
                {
                    using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
                    {
                        sUser = ids[i].ToString();
                        SqlCommand sqlCmd = new SqlCommand();
                        sqlCmd.Connection = conn;
                        conn.Open();
                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        sqlCmd.CommandText = "MoveGroupMember";
                        sqlCmd.Parameters.AddWithValue("@clientsid", iClient);
                        sqlCmd.Parameters.AddWithValue("@groupid", iGroup);
                        sqlCmd.Parameters.AddWithValue("@usersid", Convert.ToInt32(sUser));

                        SqlParameter oldGroupId = sqlCmd.Parameters.Add("@oldGroupId", SqlDbType.BigInt);
                        oldGroupId.Direction = ParameterDirection.Output;
                        oldGroupId.Value = 0;

                        SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);
                        sqlCmd.ExecuteNonQuery();
                        oldId = Convert.ToInt32(oldGroupId.Value.ToString());

                        sqlCmd.ExecuteNonQuery();
                        InsertEvent(iClient, iAdmin, "Moved member(s) to group: " + sGroup.ToString());
                    }
                }
                return oldId;
            }
            catch (Exception ex)
            {
                return -1;
            }

        }

        public int UpdateUser(int iClient, int iUser, int iAdmin, int iNewGroup)
        {
            int oldId = 0;

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
            {

                SqlCommand sqlCmd = new SqlCommand();
                sqlCmd.Connection = conn;
                conn.Open();
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.CommandText = "UpdateUser";
                sqlCmd.Parameters.Add("@clientid", SqlDbType.BigInt).Value = iClient;
                sqlCmd.Parameters.Add("@usersid", SqlDbType.BigInt).Value = iUser;
                sqlCmd.Parameters.Add("@adminid", SqlDbType.BigInt).Value = iAdmin;
                sqlCmd.Parameters.Add("@newgroup", SqlDbType.VarChar).Value = iNewGroup;

                SqlParameter oldGroupId = sqlCmd.Parameters.Add("@oldGroupId", SqlDbType.BigInt);
                oldGroupId.Direction = ParameterDirection.Output;
                oldGroupId.Value = 0;

                SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);
                sqlCmd.ExecuteNonQuery();
                oldId = Convert.ToInt32(oldGroupId.Value.ToString());

                InsertEvent(iClient, iAdmin, "Edited record usersid: " + iUser.ToString());
                return oldId;
            }


        }

        public bool UpdateClient(int iClient, int iAdmin, string sClientName, string sConn, int iActive, int iAdminid, string sEmail, string sTimeZone)
        {

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
            {
                string cmd = "exec UpdateClient @clientsid, @clientname, @connections, @active,  @adminsid, @email, @timeZone";
                conn.Open();
                SqlCommand sqlCmd = new SqlCommand(cmd, conn);

                sqlCmd.Parameters.AddWithValue("@clientsid", iClient);
                sqlCmd.Parameters.AddWithValue("@clientname", sClientName);
                sqlCmd.Parameters.AddWithValue("@connections", sConn);
                sqlCmd.Parameters.AddWithValue("@active", iActive);                
                sqlCmd.Parameters.AddWithValue("@adminsid", iAdminid);
                sqlCmd.Parameters.AddWithValue("@email", sEmail);
                sqlCmd.Parameters.AddWithValue("@timeZone", sTimeZone);

                SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);

                sqlCmd.ExecuteNonQuery();
                InsertEvent(iClient, iAdmin, "Edited record client: " + sClientName.ToString());
                return true;
            }

        }

        public void UpdateClientTimezone(int iClient, string sTimezone)
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
            {
                string cmd = "exec UpdateClientTimezone @clientsid, @timezone";
                conn.Open();
                SqlCommand sqlCmd = new SqlCommand(cmd, conn);

                sqlCmd.Parameters.AddWithValue("@clientsid", iClient);
                sqlCmd.Parameters.AddWithValue("@timezone", sTimezone);

                SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);

                sqlCmd.ExecuteNonQuery();

            }
        }

        public void UpdateAdmin(int iClient, int iAdmin, string sUserName, string sEmail, string sFName, string sLName, string sReset)
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
            {
                string cmd = "exec UpdateAdmin @clientsid, @adminid, @email, @fname, @lname, @reset";
                conn.Open();
                SqlCommand sqlCmd = new SqlCommand(cmd, conn);

                sqlCmd.Parameters.AddWithValue("@clientsid", iClient);
                sqlCmd.Parameters.AddWithValue("@adminid", iAdmin);
                sqlCmd.Parameters.AddWithValue("@email", sEmail);
                sqlCmd.Parameters.AddWithValue("@fname", sFName);
                sqlCmd.Parameters.AddWithValue("@lname", sLName);
                sqlCmd.Parameters.AddWithValue("@reset", sReset);

                SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);

                sqlCmd.ExecuteNonQuery();
                InsertEvent(iClient, iAdmin, "Edited admin record: " + sUserName.ToString());
            }
        }

        public void UpdateGlobalAdmin(int iClient, int iAdmin, string sUserName, string sEmail, string sFName, string sLName, int iSelectClient)
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
            {
                string cmd = "exec UpdateGlobalAdmin @clientsid, @adminid, @email, @fname, @lname, @selclient";
                conn.Open();
                SqlCommand sqlCmd = new SqlCommand(cmd, conn);

                sqlCmd.Parameters.AddWithValue("@clientsid", iClient);
                sqlCmd.Parameters.AddWithValue("@adminid", iAdmin);
                sqlCmd.Parameters.AddWithValue("@email", sEmail);
                sqlCmd.Parameters.AddWithValue("@fname", sFName);
                sqlCmd.Parameters.AddWithValue("@lname", sLName);
                sqlCmd.Parameters.AddWithValue("@selclient", iSelectClient);

                SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);

                sqlCmd.ExecuteNonQuery();
                InsertEvent(iClient, iAdmin, "Edited admin record: " + sUserName.ToString());
            }
        }

        public void UpdatePwd(int iClient, int iAdmin, string sUserName, string sCurrent, string sNew)
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
            {
                string cmd = "exec UpdateAdminPwd @clientsid, @adminid, @newpwd";
                conn.Open();
                SqlCommand sqlCmd = new SqlCommand(cmd, conn);

                sqlCmd.Parameters.AddWithValue("@clientsid", iClient);
                sqlCmd.Parameters.AddWithValue("@adminid", iAdmin);
                sqlCmd.Parameters.AddWithValue("@newpwd", sNew);

                SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);

                sqlCmd.ExecuteNonQuery();
                InsertEvent(iClient, iAdmin, "Updated password for admin record: " + sUserName.ToString());
            }
        }

        public void UpdateGroup(int iClient, int iAdmin, int iGroup, string sGroupName, string sGroupDesc, int iMakeDefault)
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
            {
                string cmd = "exec UpdateGroup @clientsid, @adminid, @groupid, @groupname, @groupdesc, @makedefault";
                conn.Open();
                SqlCommand sqlCmd = new SqlCommand(cmd, conn);

                sqlCmd.Parameters.AddWithValue("@clientsid", iClient);
                sqlCmd.Parameters.AddWithValue("@adminid", iAdmin);
                sqlCmd.Parameters.AddWithValue("@groupid", iGroup);
                sqlCmd.Parameters.AddWithValue("@groupname", sGroupName);
                sqlCmd.Parameters.AddWithValue("@groupdesc", sGroupDesc);
                sqlCmd.Parameters.AddWithValue("@makedefault", iMakeDefault);

                SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);

                sqlCmd.ExecuteNonQuery();
                InsertEvent(iClient, iAdmin, "Edited record group: " + sGroupName.ToString());


            }
        }

        public void UpdateButton(int iClient, int iAdmin, int iButton, string sButtonName, string sButtonTitle, string sButtonMsg, string sButtonIcon, string sSeverity, string sHotKey, string sModKey, string sAudKey, int iGroupMsg, int iGroup, string sSendDelay, int iMuteSound)
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
            {

                string cmd = "exec UpdateButton @clientsid, @adminid, @buttonid, @buttonname, @buttontitle, @buttonmsg, @buttonicon, @severityid, @hotkey, @modkey, @audioid, @groupmsg, @groupid, @senddelay, @mutesound";

                conn.Open();
                SqlCommand sqlCmd = new SqlCommand(cmd, conn);

                sqlCmd.Parameters.AddWithValue("@clientsid", iClient);
                sqlCmd.Parameters.AddWithValue("@adminid", iAdmin);
                sqlCmd.Parameters.AddWithValue("@buttonid", iButton);
                sqlCmd.Parameters.AddWithValue("@buttonname", sButtonName);
                sqlCmd.Parameters.AddWithValue("@buttontitle", sButtonTitle);
                sqlCmd.Parameters.AddWithValue("@buttonmsg", sButtonMsg);
                sqlCmd.Parameters.AddWithValue("@buttonicon", sButtonIcon);
                sqlCmd.Parameters.AddWithValue("@severityid", sSeverity);
                sqlCmd.Parameters.AddWithValue("@hotkey", sHotKey);
                sqlCmd.Parameters.AddWithValue("@modkey", sModKey);
                sqlCmd.Parameters.AddWithValue("@audioid", sAudKey);
                sqlCmd.Parameters.AddWithValue("@groupmsg", iGroupMsg);
                sqlCmd.Parameters.AddWithValue("@groupid", iGroup);
                sqlCmd.Parameters.AddWithValue("@senddelay", sSendDelay);
                sqlCmd.Parameters.AddWithValue("@mutesound", iMuteSound);

                SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);

                sqlCmd.ExecuteNonQuery();

                InsertEvent(iClient, iAdmin, "Edited record button: " + sButtonName.ToString());

            }
        }

        public void UpdateSMSNumber(int iClient, int iAdmin, int iSmsId, string sNumber, string contactName)
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
            {
                string cmd = "exec UpdateSMSNumber @clientsid, @smsid, @number, @contactName";

                conn.Open();
                SqlCommand sqlCmd = new SqlCommand(cmd, conn);

                sqlCmd.Parameters.AddWithValue("@clientsid", iClient);
                sqlCmd.Parameters.AddWithValue("@smsid", iSmsId);
                sqlCmd.Parameters.AddWithValue("@number", sNumber);
                sqlCmd.Parameters.AddWithValue("@contactName", contactName);

                SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);

                sqlCmd.ExecuteNonQuery();

                InsertEvent(iClient, iAdmin, "Edited SMS Number for row: " + iSmsId.ToString());

            }
        }

        public void UpdateEmailAddress(int iClient, int iAdmin, int iEmailId, string sEmail)
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
            {
                string cmd = "exec UpdateEmailAddress @clientsid, @emailsid, @number";

                conn.Open();
                SqlCommand sqlCmd = new SqlCommand(cmd, conn);

                sqlCmd.Parameters.AddWithValue("@clientsid", iClient);
                sqlCmd.Parameters.AddWithValue("@emailsid", iEmailId);
                sqlCmd.Parameters.AddWithValue("@number", sEmail);


                SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);

                sqlCmd.ExecuteNonQuery();

                InsertEvent(iClient, iAdmin, "Edited Email Address for row: " + iEmailId.ToString());

            }
        }

        public void InsertAdminUser(int iClient, int iUserId, string sAdminName, int iAdminType, string sEmailAddress, string sFirstName, string sLastName)
        {

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
            {
                string cmd = "exec InsertAdminUser @clientsid, @adminname, @admintype, @email, @fname, @lname, @clientname";
                string clientname = string.Empty;
                conn.Open();
                SqlCommand sqlCmd = new SqlCommand(cmd, conn);
                sqlCmd.Parameters.AddWithValue("@clientsid", iClient);
                sqlCmd.Parameters.AddWithValue("@adminname", sAdminName);
                sqlCmd.Parameters.AddWithValue("@admintype", iAdminType);
                sqlCmd.Parameters.AddWithValue("@email", sEmailAddress);
                sqlCmd.Parameters.AddWithValue("@fname", sFirstName);
                sqlCmd.Parameters.AddWithValue("@lname", sLastName);
                sqlCmd.Parameters.Add("@clientname", SqlDbType.VarChar, 75).Direction = ParameterDirection.Output;

                SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);

                sqlCmd.ExecuteNonQuery();
                clientname = sqlCmd.Parameters["@clientname"].Value.ToString();

                InsertEvent(1, 1, "Inserted Admin user for client: " + clientname.ToString());
            }
        }

        public string InsertClient(string sClientName, int iConn, int iActive, string sAdminName, string sEmail, string sScrubbedName, string sTimezone)
        {
            string ClientGUID = string.Empty;

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
            {
                SqlCommand sqlCmd = new SqlCommand();
                sqlCmd.Connection = conn;
                conn.Open();
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.CommandText = "InsertClient";
                sqlCmd.Parameters.AddWithValue("@clientname", sClientName);
                sqlCmd.Parameters.AddWithValue("@connections", iConn);
                sqlCmd.Parameters.AddWithValue("@active", iActive);                
                sqlCmd.Parameters.AddWithValue("@adminname", sAdminName);
                sqlCmd.Parameters.AddWithValue("@email", sEmail);
                sqlCmd.Parameters.AddWithValue("@scrubbedname", sScrubbedName);
                sqlCmd.Parameters.AddWithValue("@timzone", sTimezone);

                SqlParameter newClientGuid = sqlCmd.Parameters.Add("@newClientGuid", SqlDbType.UniqueIdentifier);
                newClientGuid.Direction = ParameterDirection.Output;
                newClientGuid.Value = new Guid();

                SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);

                sqlCmd.ExecuteNonQuery();

                ClientGUID = newClientGuid.Value.ToString();


                InsertEvent(1, 1, "Inserted client: " + sClientName.ToString());
            }

            return ClientGUID;

        }

        public int InsertButton(int iClient, int iAdmin, int iGroup, string sButtonName, string sButtonTitle, string sButtonMsg, string sButtonIcon, string sSeverity, string sHotKey, string sModKey, string sAudKey, int iGroupMsg, string sSendDelay, int iMuteSound)
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
            {
                string cmd = "exec InsertButton @clientsid, @adminid, @groupid, @buttonname, @buttontitle, @buttonmsg, @buttonicon, @severity, @hotkey, @modkey, @audio, @groupmsg, @senddelay, @mutesound";

                conn.Open();
                SqlCommand sqlCmd = new SqlCommand(cmd, conn);

                sqlCmd.Parameters.AddWithValue("@clientsid", iClient);
                sqlCmd.Parameters.AddWithValue("@adminid", iAdmin);
                sqlCmd.Parameters.AddWithValue("@groupid", iGroup);
                sqlCmd.Parameters.AddWithValue("@buttonname", sButtonName);
                sqlCmd.Parameters.AddWithValue("@buttontitle", sButtonTitle);
                sqlCmd.Parameters.AddWithValue("@buttonmsg", sButtonMsg);
                sqlCmd.Parameters.AddWithValue("@buttonicon", sButtonIcon);
                sqlCmd.Parameters.AddWithValue("@severity", sSeverity);
                sqlCmd.Parameters.AddWithValue("@hotkey", sHotKey);
                sqlCmd.Parameters.AddWithValue("@modkey", sModKey);
                sqlCmd.Parameters.AddWithValue("@audio", sAudKey);
                sqlCmd.Parameters.AddWithValue("@groupmsg", iGroupMsg);
                sqlCmd.Parameters.AddWithValue("@senddelay", sSendDelay);
                sqlCmd.Parameters.AddWithValue("@mutesound", iMuteSound);

                SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);

                int iButtonId = Convert.ToInt32(sqlCmd.ExecuteScalar());

                InsertEvent(iClient, iAdmin, "Inserted button: " + sButtonName.ToString());

                return iButtonId;
            }
        }

        public void InsertSMSNumber(int iClient, int iAdmin, int iButton, string sNumber)
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
            {
                string cmd = "exec InsertSMSNumber @clientsid, @buttonsid, @number";

                conn.Open();
                SqlCommand sqlCmd = new SqlCommand(cmd, conn);

                sqlCmd.Parameters.AddWithValue("@clientsid", iClient);
                sqlCmd.Parameters.AddWithValue("@buttonsid", iButton);
                sqlCmd.Parameters.AddWithValue("@number", sNumber);

                SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);

                sqlCmd.ExecuteNonQuery();

                InsertEvent(iClient, iAdmin, "Inserted SMS Number: " + sNumber.ToString());
            }
        }

        public void InsertMassSMS(int iClient, int iAdmin, int iButton, string[] numbers)
        {
            string sNumber = string.Empty;
            for (int i = 0; i < numbers.Length; i++)
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
                {
                    sNumber = numbers[i].ToString();
                    string cmd = "exec InsertSMSNumber @clientsid, @buttonsid, @number";

                    conn.Open();
                    SqlCommand sqlCmd = new SqlCommand(cmd, conn);

                    sqlCmd.Parameters.AddWithValue("@clientsid", iButton);
                    sqlCmd.Parameters.AddWithValue("@buttonsid", iButton);
                    sqlCmd.Parameters.AddWithValue("@number", sNumber);

                    SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);

                    sqlCmd.ExecuteNonQuery();
                }

                InsertEvent(iClient, iAdmin, "Inserted SMS Number: " + sNumber.ToString());
            }
        }

        public void InsertMassEmailAddress(int iClient, int iAdmin, int iButton, string[] emails)
        {
            string sEmail = string.Empty;
            string sBadEmailList = string.Empty;
            for (int i = 0; i < emails.Length; i++)
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
                {
                    sEmail = emails[i].ToString();

                    if (utility.ValidateEmailAddress(sEmail, 100))
                    {
                        string cmd = "exec InsertEmail @clientsid, @buttonsid, @email";

                        conn.Open();
                        SqlCommand sqlCmd = new SqlCommand(cmd, conn);

                        sqlCmd.Parameters.AddWithValue("@clientsid", iClient);
                        sqlCmd.Parameters.AddWithValue("@buttonsid", iButton);
                        sqlCmd.Parameters.AddWithValue("@email", sEmail);

                        SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);

                        sqlCmd.ExecuteNonQuery();

                        InsertEvent(iClient, iAdmin, "Inserted Email Address: " + sEmail.ToString());
                    }

                    else
                    {
                        sBadEmailList += sEmail + ", ";
                    }

                }


            }

            InsertEvent(iClient, iAdmin, "Invalid Email Addresses: " + sBadEmailList.ToString());

        }

        public void InsertEmailAddress(int iClient, int iAdmin, int iButton, string sEmail)
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
            {

                string cmd = "exec InsertEmail @clientsid, @buttonsid, @email";

                conn.Open();
                SqlCommand sqlCmd = new SqlCommand(cmd, conn);

                sqlCmd.Parameters.AddWithValue("@clientsid", iClient);
                sqlCmd.Parameters.AddWithValue("@buttonsid", iButton);
                sqlCmd.Parameters.AddWithValue("@email", sEmail);

                SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);

                sqlCmd.ExecuteNonQuery();
            }

            InsertEvent(iClient, iAdmin, "Inserted Email Address: " + sEmail.ToString());
        }

        public int InsertGroup(int iClient, int iAdmin, string sGroupName, string sGroupDesc, string sDefaultGroup)
        {
            int newId = 0;

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
            {

                SqlCommand sqlCmd = new SqlCommand();
                sqlCmd.Connection = conn;
                conn.Open();
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.CommandText = "InsertGroup";
                sqlCmd.Parameters.Add("@clientid", SqlDbType.BigInt).Value = iClient;
                sqlCmd.Parameters.Add("@adminid", SqlDbType.BigInt).Value = iAdmin;
                sqlCmd.Parameters.Add("@groupname", SqlDbType.VarChar).Value = sGroupName;
                sqlCmd.Parameters.Add("@groupdesc", SqlDbType.VarChar).Value = sGroupDesc;
                sqlCmd.Parameters.Add("@defaultgroup", SqlDbType.VarChar).Value = sDefaultGroup;
                //sqlCmd.Parameters.Add("@copygroupid", SqlDbType.VarChar).Value = iCopyGroupId;

                SqlParameter newGroupId = sqlCmd.Parameters.Add("@newGroupId", SqlDbType.BigInt);
                newGroupId.Direction = ParameterDirection.Output;
                newGroupId.Value = 0;

                SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);

                sqlCmd.ExecuteNonQuery();

                newId = Convert.ToInt32(newGroupId.Value.ToString());

                InsertEvent(iClient, iAdmin, "Inserted group: " + sGroupName.ToString());
            }

            return newId;
        }

        public void InsertMachineReceivers(int iClient, int iAdmin, int iButton, string sButton, string sReceivers)
        {
            //todo fix this loop

            sReceivers = sReceivers.Replace(",", "|");
            string[] ids = sReceivers.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
            string sUser = string.Empty;
            int iGroup = 0;
            for (int i = 0; i < ids.Length; i++)
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
                {

                    sUser = ids[i].ToString();

                    SqlCommand sqlCmd = new SqlCommand();
                    sqlCmd.Connection = conn;
                    conn.Open();
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    sqlCmd.CommandText = "InsertMachineReceivers";
                    sqlCmd.Parameters.AddWithValue("@buttonid", iButton);
                    sqlCmd.Parameters.AddWithValue("@usersid", Convert.ToInt32(sUser));

                    SqlParameter groupId = sqlCmd.Parameters.Add("@groupid", SqlDbType.BigInt);
                    groupId.Direction = ParameterDirection.Output;
                    groupId.Value = 0;

                    SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);

                    sqlCmd.ExecuteNonQuery();

                    iGroup = Convert.ToInt32(groupId.Value.ToString());

                    QueueClass q = new QueueClass();
                    q.CreateGroupQueueMsg(iGroup.ToString(), iGroup.ToString(), "update");

                }

                InsertEvent(iClient, iAdmin, "Updated receivers for button: " + sButton.ToString());
            }
        }

        public void InsertEvent(int iClient, int iAdmin, string sDesc)
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
            {
                string cmd = "exec ApplicationEventLog @clientsid, @adminid, @desc, @createdon";
                conn.Open();
                SqlCommand sqlCmd = new SqlCommand(cmd, conn);

                sqlCmd.Parameters.AddWithValue("@clientsid", iClient);
                sqlCmd.Parameters.AddWithValue("@adminid", iAdmin);
                sqlCmd.Parameters.AddWithValue("@desc", sDesc);
                sqlCmd.Parameters.AddWithValue("@createdon", DateTime.UtcNow);

                SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);

                sqlCmd.ExecuteNonQuery();
            }
        }

        private string GetClientTimezone(int iClient)
        {
            string timeZone = string.Empty;

            System.Collections.Generic.Dictionary<int, string> timeZones = HttpContext.Current.Cache["_ClientTimezones"] as System.Collections.Generic.Dictionary<int, string>;

            if (timeZones != null)
            {
                if (timeZones.ContainsKey(iClient))
                {
                    timeZone = timeZones[iClient];
                }
                else
                {
                    DataTable dtClient = GetClient(iClient);
                    if (dtClient != null)
                    {
                        object clientTimezone = dtClient.Rows[0]["TimeZone"];
                        if (clientTimezone != null)
                        {
                            timeZone = clientTimezone.ToString();
                            timeZones.Add(iClient, timeZone);
                            HttpContext.Current.Cache["_ClientTimezones"] = timeZones;
                        }
                    }
                }
            }
            else
            {
                timeZones = new System.Collections.Generic.Dictionary<int, string>();
                DataTable dtClient = GetClient(iClient);
                if (dtClient != null)
                {
                    object clientTimezone = dtClient.Rows[0]["TimeZone"];
                    if (clientTimezone != null)
                    {
                        timeZone = clientTimezone.ToString();
                        timeZones.Add(iClient, timeZone);
                        HttpContext.Current.Cache["_ClientTimezones"] = timeZones;
                    }
                }
            }

            return timeZone;
        }


        public void ResetPwd(string sUserName, string sNew)
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
            {
                string cmd = "exec ResetPwd @username, @newpwd";
                conn.Open();
                SqlCommand sqlCmd = new SqlCommand(cmd, conn);

                sqlCmd.Parameters.AddWithValue("@username", sUserName);
                sqlCmd.Parameters.AddWithValue("@newpwd", sNew);

                SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);

                sqlCmd.ExecuteNonQuery();
                //InsertEvent(iClient, iAdmin, "Reset password for admin record: " + sUserName.ToString());
            }
        }

        public bool IsValidEmail(string sUserName)
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
            {
                string cmd = "exec IsValidEmail @username";
                conn.Open();
                SqlCommand sqlCmd = new SqlCommand(cmd, conn);

                sqlCmd.Parameters.AddWithValue("@username", sUserName);

                SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);

                return Convert.ToInt32(sqlCmd.ExecuteScalar()) > 0;
            }
        }

        public string GetAdminName(string sEmail)
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
            {
                string cmd = "exec GetAdminName @email";
                conn.Open();
                SqlCommand sqlCmd = new SqlCommand(cmd, conn);

                sqlCmd.Parameters.AddWithValue("@email", sEmail);

                SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);

                return sqlCmd.ExecuteScalar().ToString();
            }
        }

        public DataTable GetGroupButtons(int iClient, int iGroup)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
            {
                string cmd = "exec GetGroupButtons @clientsid, @groupid";
                conn.Open();
                SqlCommand sqlCmd = new SqlCommand(cmd, conn);

                sqlCmd.Parameters.AddWithValue("@clientsid", iClient);
                sqlCmd.Parameters.AddWithValue("@groupid", iGroup);

                SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);

                sqlDa.Fill(dt);
            }
            return dt;
        }

    }
}

