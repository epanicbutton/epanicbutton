﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.IO;

namespace epanicbutton
{
    public class ErrorHandler
    {
        public void ErrorCatch(string sApp, string sClient, string sUser, string sCategory, string sMessageInto, string sMessageDetail)
        {
            try
            {//AppName,UserIdName,Category,MessageIntro,MessageDetails
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
                {
                    SqlCommand cmd = new SqlCommand();

                    cmd.Connection = conn;

                    conn.Open();

                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "ApplicationErrorLog";
                    cmd.Parameters.Add("@AppName", SqlDbType.NVarChar, 60).Value = sApp;
                    cmd.Parameters.Add("@ClientsId", SqlDbType.NVarChar, 60).Value = sClient;
                    cmd.Parameters.Add("@UserIdName", SqlDbType.NVarChar, 50).Value = sUser;
                    cmd.Parameters.Add("@Category", SqlDbType.NVarChar, 50).Value = sCategory;
                    cmd.Parameters.Add("@MessageIntro", SqlDbType.NVarChar, 500).Value = sMessageInto;
                    cmd.Parameters.Add("@MessageDetails", SqlDbType.NVarChar).Value = sMessageDetail;

                    // Execute the command. 
                    cmd.ExecuteNonQuery();
                }
            }
            catch (System.Exception x)
            {
                //cant log to database
                string err = x.Message.ToString();
                ErrorWrite(err);
            }
        }

        public void ErrorWrite(string sErr)
        {
            string file = HttpContext.Current.Server.MapPath("\\logs\\AppError.txt");
            string hdr = "===============================================================";
            StreamWriter writer = new StreamWriter(file, true);
            writer.WriteLine(hdr);
            writer.WriteLine("Exception Date/Time = " + System.DateTime.Now.ToLocalTime().ToString());
            writer.WriteLine("Exception = " + sErr.ToString());

            writer.Close();
        }


    }
}