﻿using System;
using System.Data;
using System.Data.SqlClient;


namespace epanicbutton
{
    public partial class Login : System.Web.UI.Page
    {
        DataClass dc = new DataClass();
        ErrorHandler error = new ErrorHandler();
        Session session = new Session();
        UserAccount ua = new UserAccount();
        string password = string.Empty;
        string client = string.Empty;
        string guid = string.Empty;
        string user = string.Empty;


        protected void Page_Load(object sender, EventArgs e)
        {
            Session.RemoveAll();

            lblValidationError.Visible = false;

            if (Session["autologout"] != null)
            {
                lblLoggedOut.Visible = true;
                Session["autologout"] = null;
            }

            if (IsPostBack)
            {
                try
                {
                    bool login = UserGetPassword(this.txtUserId.Text);

                    if (UserAccount.VerifyHash(this.txtPassword.Text, "MD5", password))  //todo: protect injection
                    {
                        Session["loggedin"] = "yes";
                        Session["client"] = client;
                        Session["admin"] = user;
                        Session["guid"] = guid;

                        Response.Redirect("groups.aspx", true);
                    }
                    else
                    {
                        lblValidationError.Visible = true;
                    }
                }

                    catch (System.Threading.ThreadAbortException)
                    {
                        //just catch the redirect thread 
                    }
                    catch (System.Exception ex)
                    {
                        error.ErrorCatch("Login.aspx", "1","1", "UserGetPassword", ex.Message, ex.StackTrace);
                        //Response.Redirect("err.aspx", true);
                    }
            }
        }

        //public static string ComputeHash(string plainText, string hashAlgorithm, byte[] saltBytes)
        //{
        //    // If salt is not specified, generate it on the fly.
        //    if (saltBytes == null)
        //    {
        //        // Define min and max salt sizes.
        //        int minSaltSize = 4;
        //        int maxSaltSize = 8;

        //        // Generate a random number for the size of the salt.
        //        Random random = new Random();
        //        int saltSize = random.Next(minSaltSize, maxSaltSize);

        //        // Allocate a byte array, which will hold the salt.
        //        saltBytes = new byte[saltSize];

        //        // Initialize a random number generator.
        //        RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();

        //        // Fill the salt with cryptographically strong byte values.
        //        rng.GetNonZeroBytes(saltBytes);
        //    }

        //    // Convert plain text into a byte array.
        //    byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);

        //    // Allocate array, which will hold plain text and salt.
        //    byte[] plainTextWithSaltBytes =
        //            new byte[plainTextBytes.Length + saltBytes.Length];

        //    // Copy plain text bytes into resulting array.
        //    for (int i = 0; i < plainTextBytes.Length; i++)
        //        plainTextWithSaltBytes[i] = plainTextBytes[i];

        //    // Append salt bytes to the resulting array.
        //    for (int i = 0; i < saltBytes.Length; i++)
        //        plainTextWithSaltBytes[plainTextBytes.Length + i] = saltBytes[i];

        //    // Because we support multiple hashing algorithms, we must define
        //    // hash object as a common (abstract) base class. We will specify the
        //    // actual hashing algorithm class later during object creation.
        //    HashAlgorithm hash;

        //    // Make sure hashing algorithm name is specified.
        //    if (hashAlgorithm == null)
        //        hashAlgorithm = "";

        //    // Initialize appropriate hashing algorithm class.
        //    switch (hashAlgorithm.ToUpper())
        //    {
        //        case "SHA1":
        //            hash = new SHA1Managed();
        //            break;

        //        case "SHA256":
        //            hash = new SHA256Managed();
        //            break;

        //        case "SHA384":
        //            hash = new SHA384Managed();
        //            break;

        //        case "SHA512":
        //            hash = new SHA512Managed();
        //            break;

        //        default:
        //            hash = new MD5CryptoServiceProvider();
        //            break;
        //    }

        //    // Compute hash value of our plain text with appended salt.
        //    byte[] hashBytes = hash.ComputeHash(plainTextWithSaltBytes);

        //    // Create array which will hold hash and original salt bytes.
        //    byte[] hashWithSaltBytes = new byte[hashBytes.Length +
        //                                        saltBytes.Length];

        //    // Copy hash bytes into resulting array.
        //    for (int i = 0; i < hashBytes.Length; i++)
        //        hashWithSaltBytes[i] = hashBytes[i];

        //    // Append salt bytes to the result.
        //    for (int i = 0; i < saltBytes.Length; i++)
        //        hashWithSaltBytes[hashBytes.Length + i] = saltBytes[i];

        //    // Convert result into a base64-encoded string.
        //    string hashValue = Convert.ToBase64String(hashWithSaltBytes);

        //    // Return the result.
        //    return hashValue;
        //}

        //public static bool VerifyHash(string plainText, string hashAlgorithm, string hashValue)
        //{
        //    // Convert base64-encoded hash value into a byte array.
        //    byte[] hashWithSaltBytes = Convert.FromBase64String(hashValue);

        //    // We must know size of hash (without salt).
        //    int hashSizeInBits, hashSizeInBytes;

        //    // Make sure that hashing algorithm name is specified.
        //    if (hashAlgorithm == null)
        //        hashAlgorithm = "";

        //    // Size of hash is based on the specified algorithm.
        //    switch (hashAlgorithm.ToUpper())
        //    {
        //        case "SHA1":
        //            hashSizeInBits = 160;
        //            break;

        //        case "SHA256":
        //            hashSizeInBits = 256;
        //            break;

        //        case "SHA384":
        //            hashSizeInBits = 384;
        //            break;

        //        case "SHA512":
        //            hashSizeInBits = 512;
        //            break;

        //        default: // Must be MD5
        //            hashSizeInBits = 128;
        //            break;
        //    }

        //    // Convert size of hash from bits to bytes.
        //    hashSizeInBytes = hashSizeInBits / 8;

        //    // Make sure that the specified hash value is long enough.
        //    if (hashWithSaltBytes.Length < hashSizeInBytes)
        //        return false;

        //    // Allocate array to hold original salt bytes retrieved from hash.
        //    byte[] saltBytes = new byte[hashWithSaltBytes.Length -
        //                                hashSizeInBytes];

        //    // Copy salt from the end of the hash to the new array.
        //    for (int i = 0; i < saltBytes.Length; i++)
        //        saltBytes[i] = hashWithSaltBytes[hashSizeInBytes + i];

        //    // Compute a new hash string.
        //    string expectedHashString =
        //                ComputeHash(plainText, hashAlgorithm, saltBytes);

        //    // If the computed hash matches the specified hash,
        //    // the plain text value must be correct.
        //    return (hashValue == expectedHashString);
        //}

        public bool UserGetPassword(string user)
        {
            DataTable dt = new DataTable();
            try
            {
                string strQuery = "exec GetPassword @userid";
                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.Parameters.Add("@userid", SqlDbType.VarChar).Value = user;

                dt = dc.dTable(cmd);

                if (dt.Rows.Count > 0)
                {
                    password = dt.Rows[0]["password"].ToString();

                    session.ClientId = Convert.ToInt32(dt.Rows[0]["clientsid"].ToString());
                    session.UserId = Convert.ToInt32(dt.Rows[0]["adminsid"].ToString());
                    session.ClientGuid = dt.Rows[0]["ClientGuid"].ToString();

                    Session["session"] = session;

                    Session["clientid"] = dt.Rows[0]["clientsid"].ToString();
                    Session["userid"] = dt.Rows[0]["adminsid"].ToString();
                    Session["admintype"] = dt.Rows[0]["admintype"].ToString();
                    Session["changepwd"] = dt.Rows[0]["ChangePassword"].ToString();
                    Session["eula"] = dt.Rows[0]["EulaAccepted"].ToString();
                    Session["ClientName"] = dt.Rows[0]["ClientName"].ToString();
                    Session["ClientGuid"] = dt.Rows[0]["ClientGuid"].ToString();
                    Session["VirtualHost"] = dt.Rows[0]["VirtualHost"].ToString();
                }
                if (dt.Rows.Count == 0)
                {
                    lblValidationError.Text = "User ID or Password is incorrect.";
                    return false;
                }
            }

            catch (System.Exception ex)
            {
                error.ErrorCatch("Login.aspx", "0", "0", "UserGetPassword", ex.Message, ex.StackTrace);
                Response.Redirect("/apperror.aspx", false);

            }

            return true;

        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {

        }

        protected void btnForgot_Click(object sender, EventArgs e)
        {
            Response.Redirect("Recover.aspx", true);
        }

        
    }
}