﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Main.Master" AutoEventWireup="true" CodeBehind="Users.aspx.cs" Inherits="epanicbutton.Grid" %>

<%@ Register Assembly="Trirand.Web" Namespace="Trirand.Web.UI.WebControls" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="header">User Manager</div>
    <div id="content">
        <cc1:JQGrid ID="epbGrid" runat="server" Width="1082" Height="414" PagerSettings-PageSize="18" PagerSettings-PageSizeOptions="false"
            OnRowDeleting="epbGrid_RowDeleting"
            OnRowAdding="epbGrid_RowAdding"
            OnRowEditing="epbGrid_RowEditing"
            MultiSelect="true"
            MultiSelectMode="SelectOnRowClick">
<%--
    20160311 - JH
    Add UserLocation to the table
--%>
            <Columns>
                <cc1:JQGridColumn DataField="id" Editable="false" PrimaryKey="true" Visible="False" Searchable="false" />
                <cc1:JQGridColumn DataField="UserFirstName" HeaderText="First Name" Editable="true" />
                <cc1:JQGridColumn DataField="UserLastName" HeaderText="Last Name" Editable="true" />
                <cc1:JQGridColumn DataField="UserDepartment" HeaderText="Department" Editable="true" />
                <cc1:JQGridColumn DataField="MachineName" HeaderText="Machine Name" Editable="False" ShowToolTip="false" />
                <cc1:JQGridColumn DataField="UserLocation" HeaderText="Location" Editable="False" ShowToolTip="false" />
                <cc1:JQGridColumn DataField="UserEmail" HeaderText="User Email" Editable="False" />
                <cc1:JQGridColumn DataField="UserTelephone" HeaderText="User Telephone" Editable="False" />
                <cc1:JQGridColumn DataField="GroupName" HeaderText="Group Name" Editable="true"
                    EditType="DropDown"
                    EditorControlID="ddlGroups"
                    EditTypeCustomGetValue="getGroupElementValue" />

                <cc1:JQGridColumn DataField="MachineGuid"  HeaderText="Machine Guid" Editable="false" Width="380"/>
            </Columns>

            <SearchDialogSettings MultipleSearch="true" />
            <ToolBarSettings ToolBarPosition="Bottom" ShowEditButton="true" ShowAddButton="false" ShowDeleteButton="false" ShowSearchButton="true" ShowRefreshButton="true">
                <CustomButtons>
                    <cc1:JQGridToolBarButton
                        Text=""
                        ToolTip="Delete User(s)"
                        ButtonIcon="ui-icon-trash"
                        Position="Last"
                        OnClick="delUsers" />
                </CustomButtons>
            </ToolBarSettings>

            <EditDialogSettings CloseAfterEditing="true" Caption="Sender Editor" />
            <DeleteDialogSettings />
            <PagerSettings PageSize="18" PageSizeOptions="[18,20,50,100]" />
            <ClientSideEvents RowSelect="rowSelected" AfterEditDialogShown="hideNav" LoadComplete="setTooltip" />
            <AppearanceSettings HighlightRowsOnHover="true" />
        </cc1:JQGrid>
        <asp:HiddenField runat="server" ID="SelectedRowsState" />
        <asp:DropDownList runat="server" ID="ddlGroups"></asp:DropDownList>
    </div>
    <script type="text/javascript">
        //a few grid inits
        var selectedRows = [];
        var grid = $("#<%= epbGrid.ClientID %>");

        function rowSelected(rowID, isSelected) {
            var x = 0;
            selectedRows[rowID] = isSelected;
            updateSelectedRowsHidden();
        }

        function updateSelectedRowsHidden() {
            var hiddenField = $("#<%= SelectedRowsState.ClientID %>");
            var selectedValues = "";

            for (var row in selectedRows) {
                if (selectedRows[row])
                    selectedValues = selectedValues + row + ",";
            }

            hiddenField.val(selectedValues);

        }

        function delUsers() {
            var selectedRowIDs = $(grid).jqGrid('getGridParam', 'selarrrow');
            deleteSelectedUsers(selectedRowIDs);
            $(grid).trigger('reloadGrid');
        }

        function setTooltip() {
            var ids = grid.jqGrid('getDataIDs');
            console.log('ids', ids)
            for (var i = 0; i < ids.length; i++) {
                var rowid = ids[i];
                var rowData = grid.jqGrid('getRowData', rowid);
                $(grid).setCell(rowid, 'MachineName', '', '', { 'title': rowData.MachineGuid });
            }
        }


    </script>
</asp:Content>
