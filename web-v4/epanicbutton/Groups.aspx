﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Main.Master" AutoEventWireup="true" CodeBehind="Groups.aspx.cs" Inherits="epanicbutton.Groups" %>

<%@ Register Assembly="Trirand.Web" Namespace="Trirand.Web.UI.WebControls" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="header">Group Manager</div>
    <div id="content">
        <cc1:JQGrid ID="epbGrid" runat="server"  Width="1082" Height="414" PagerSettings-PageSizeOptions="false"
            OnRowDeleting="epbGrid_RowDeleting"
            OnRowAdding="epbGrid_RowAdding"
            OnRowEditing="epbGrid_RowEditing">
            <Columns>
                <cc1:JQGridColumn DataField="ID" Editable="false" PrimaryKey="true" Visible="False" Searchable="false" />
                <cc1:JQGridColumn DataField="GroupName" HeaderText="Group Name" Editable="true" Width="100" />
                <cc1:JQGridColumn DataField="GroupDescription" HeaderText="Group Description" Editable="true" Width="250" />
                <cc1:JQGridColumn DataField="CreatedOn" HeaderText="Created On" Editable="false" Searchable="true" Width="250" />
                <cc1:JQGridColumn DataField="ClientDefaultGroup" HeaderText="Default Group" TextAlign="Center" Editable="true" Width="70" CssClass="defRadio"
                    EditType="Custom"
                    EditTypeCustomCreateElement="createDefault"
                    EditTypeCustomGetValue="getDefault" />
                <cc1:JQGridColumn DataField="AddMember" HeaderText="Members" Editable="false" Searchable="false" Width="60" TextAlign="Center" />
            </Columns>
            <ToolBarSettings ToolBarPosition="Bottom" ShowEditButton="true" ShowAddButton="True" ShowDeleteButton="true" ShowSearchButton="true"
                ShowRefreshButton="true" />
            <EditDialogSettings CloseAfterEditing="true" Caption="Edit Group" />
            <SearchDialogSettings MultipleSearch="true" />
            <AddDialogSettings CloseAfterAdding="true" Caption="Add Group" />
            <DeleteDialogSettings />
            <AppearanceSettings HighlightRowsOnHover="true" />
            <PagerSettings PageSize="15" PageSizeOptions="[18,20,50,100]" />
            <ClientSideEvents RowSelect="editRow" AfterEditDialogShown="hideNav" AfterAddDialogShown="hideNavAndAdditional"  />
        </cc1:JQGrid>
        <asp:HiddenField runat="server" ID="defaultGroup" />
    </div>
    

         <asp:DropDownList runat="server" ID="ddlGroups"  CssClass="width"  style="display:none"/> 
    <script type="text/javascript">
        // group functions
        function editRow(id) {
            var grid = $("#<%= epbGrid.ClientID %>");
            var obj = $(grid).jqGrid('getRowData', id);
            var def = obj.ClientDefaultGroup;

           
        }

    </script>
</asp:Content>
