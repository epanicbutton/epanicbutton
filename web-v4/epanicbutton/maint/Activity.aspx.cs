﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace epanicbutton
{
    public partial class Activity : System.Web.UI.Page
    {

        DataClass dc = new DataClass();
        Utility ut = new Utility();
        ErrorHandler error = new ErrorHandler();
        int iClient, iUserId;
        string sAdminType;

        protected void Page_Load(object sender, EventArgs e)
        {

            if ((Session["clientid"] != null) && ((Session["userid"] != null)))
            {
                iClient = Convert.ToInt32(Session["clientid"].ToString());
                iUserId = Convert.ToInt32(Session["userid"].ToString());
                sAdminType = (Session["admintype"].ToString());
            }
            else
            {
                Response.Redirect("/login.aspx");
            }

            if (sAdminType != "Global")
            {
                Response.Redirect("/login.aspx");
            }

            try
            {
                epbGrid.DataSource = dc.GetActivity();
                epbGrid.DataBind();
            }
            catch (System.Exception ex)
            {
                string method = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();
                error.ErrorCatch(this.ToString(), iClient.ToString(), iUserId.ToString(), method, ex.Message, ex.StackTrace);
                Response.Redirect("/apperror.aspx", true);
            }

        }




    }
}