﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Main.Master" AutoEventWireup="true" CodeBehind="Activity.aspx.cs" Inherits="epanicbutton.Activity" %>
<%@ Register Assembly="Trirand.Web" Namespace="Trirand.Web.UI.WebControls" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="header"></div>
    <div id="content">
        <cc1:JQGrid ID="epbGrid" runat="server" Width="1082" Height="414" PagerSettings-PageSize="18" 
            EditDialogSettings-Height="250" EditDialogSettings-Width="450">
            <Columns>
                <cc1:JQGridColumn DataField="id" Editable="false" PrimaryKey="true" Visible="true" Searchable="false" />              
                <cc1:JQGridColumn DataField="UserID" Editable="false" PrimaryKey="false" Visible="true" Searchable="true" />     
                <cc1:JQGridColumn DataField="Catergory" Editable="false" PrimaryKey="false" Visible="true" Searchable="true" />     
                <cc1:JQGridColumn DataField="ActivityDateTime" Editable="false" PrimaryKey="false" Visible="true" Searchable="false" />     
                <cc1:JQGridColumn DataField="MessageLong" Editable="false" PrimaryKey="false" Visible="true" Searchable="true" />            

            </Columns>
            <ToolBarSettings ToolBarPosition="Bottom" ShowEditButton="true" ShowAddButton="false" ShowDeleteButton="false" ShowSearchButton="true" ShowRefreshButton="true" />
            <EditDialogSettings CloseAfterEditing="true" Caption="Client Editor" LoadingMessageText="Loading..." />
            <AddDialogSettings CloseAfterAdding="true" />            
            <AppearanceSettings HighlightRowsOnHover="true" />  
             <ClientSideEvents AfterEditDialogShown="hideNav"/>
        </cc1:JQGrid>
    </div>
</asp:Content>
