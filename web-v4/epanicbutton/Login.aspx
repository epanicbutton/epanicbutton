﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Login.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="epanicbutton.Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    

        <div class="container">
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6 header" align="center">
                    <img src="../images/ePanic-Logo-Long-Black.png" style="height:76px" />
                    <i>ePanic Button</i> System Login
                    <hr />
                </div>
                <div class="col-md-3"></div>
            </div>

            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <asp:TextBox ID="txtUserId" placeholder="USER ID" runat="server" CssClass="form-control"></asp:TextBox><br />
                </div>
                <div class="col-md-3"></div>
            </div>

            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <asp:TextBox ID="txtPassword" placeholder="PASSWORD" runat="server" TextMode="Password" CssClass="form-control" AutoCompleteType="Disabled"></asp:TextBox>
                </div>
                <div class="col-md-3"></div>
            </div>

            <div class="row login-warning">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <asp:Label ID="lblValidationError" runat="server" Text="User ID or Password invalid" ></asp:Label>
                    <asp:Label ID="lblLoggedOut" runat="server" Text="For security reasons, you have been automatically logged out." Visible="False"></asp:Label>

                </div>
                <div class="col-md-4"></div>
            </div>

            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-6">
                            <asp:Button ID="btnLogin" runat="server" Text="Login" OnClick="btnLogin_Click" CssClass="primary-btn form-control" /><br />
                        </div>
                        
                        <div class="col-md-6">
                            <asp:Button ID="btnForgot" runat="server" CssClass="secondary-btn form-control" Text="Forgot password" OnClientClick="forgot();return false;" Visible="true" OnClick="btnForgot_Click" /><br />
                        </div>
                    </div>
                </div>
                <div class="col-md-3"></div>
            </div>
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6" style="align-content:center">
                    <a class="twitter-timeline"  href="https://twitter.com/ePanicButton" data-widget-id="636935664529838082">Tweets by @ePanicButton</a>
            <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
                <div class="col-md-3"></div>

                </div>
            </div>
        </div>



        
    <script>
        function forgot() {
            window.location('recover.aspx');
        }

    </script>
    
</asp:Content>
