﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace epanicbutton
{
    public partial class WebForm1 : System.Web.UI.Page
    {

        ErrorHandler error = new ErrorHandler();
        DataClass dc = new DataClass();
        int iClient = 2;

  

        protected void Page_Load(object sender, EventArgs e)
        {
            if(Session["loggedin"] == null)
            {
                Response.Redirect("login.aspx", true);
            }

            jqGrid.DataSource = GetData();
            jqGrid.DataBind();
        }

        private DataTable GetData()
        {
            DataTable dt = dc.GetUsers(iClient);
            return dt;     
        }


        protected void jqGrid_RowEditing(object sender, Trirand.Web.UI.WebControls.JQGridRowEditEventArgs e)
        {
            DataTable dt = GetData();
            dt.PrimaryKey = new DataColumn[] { dt.Columns["Id"] };
            DataRow rowEdited = dt.Rows.Find(e.RowKey);

            rowEdited["UserLastName"] = e.RowData["UserLastName"];
            rowEdited["UserFirstName"] = e.RowData["UserFirstName"];

            jqGrid.DataSource = GetData();
            jqGrid.DataBind();
        }

        protected void jqGrid_RowDeleting(object sender, Trirand.Web.UI.WebControls.JQGridRowDeleteEventArgs e)
        {

        }

        protected void jqGrid_RowAdding(object sender, Trirand.Web.UI.WebControls.JQGridRowAddEventArgs e)
        {

        }


       
    }
}