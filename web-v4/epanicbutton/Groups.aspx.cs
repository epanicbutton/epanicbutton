﻿using System;
using System.Data;
using System.Transactions;
using System.Web.UI.WebControls;

namespace epanicbutton
{
    public partial class Groups : System.Web.UI.Page
    {

        DataClass dc = new DataClass();
        ErrorHandler error = new ErrorHandler();
        Session session = new Session();
        int iClient, iUserId;

        protected void Page_Load(object sender, EventArgs e)
        {

            if ((Session["clientid"] != null) && ((Session["userid"] != null)))
            {
                iClient = Convert.ToInt32(Session["clientid"].ToString());
                iUserId = Convert.ToInt32(Session["userid"].ToString());
            }
            else
            {
                Response.Redirect("login.aspx");
            }

            //For clone or copy group option
            DataTable dtGroups = dc.GetGroups(iClient);
            LoadGroups(dtGroups);

            epbGrid.DataSource = dtGroups;
            epbGrid.DataBind();

            //Session["groupid"] = "288";
        }

        private void LoadGroups(DataTable dtGroups)
        {
            if (dtGroups != null && dtGroups.Rows != null && dtGroups.Rows.Count > 0)
            {
                DataTable dtGroupCopy = dtGroups.Clone();
                DataRow row = dtGroupCopy.NewRow();
                row["ID"] = -1;
                row["GroupName"] = "-- Select Group --";
                dtGroupCopy.Rows.Add(row);

                foreach (DataRow item in dtGroups.Rows)
                {
                    row = dtGroupCopy.NewRow();
                    row["ID"] = item["ID"];
                    row["GroupName"] = item["GroupName"];
                    dtGroupCopy.Rows.Add(row);
                }

                ddlGroups.DataValueField = "ID";
                ddlGroups.DataTextField = "GroupName";
                ddlGroups.DataSource = dtGroupCopy;
                ddlGroups.DataBind();
            }

        }

        private int GetButtonId(DataTable dtButtons, string sButtonName)
        {
            foreach (DataRow item in dtButtons.Rows)
            {
                if (item["ButtonName"].ToString().ToLower() == sButtonName.ToLower())
                {
                    return Convert.ToInt32(item["Id"]);
                }
            }
            return -1;
        }
        private void CopyButtonsFromExistingGroup(int iClient, int iUserId, int iExistingUserGroupId, int iNewUserGroupID)
        {

            //1) Get the buttons of the user group selected from copy buttons
            //2) SMS, Email & Machines
            DataTable dtExistingButtons = dc.GetGroupButtons(iClient, iExistingUserGroupId);
            if (dtExistingButtons != null && dtExistingButtons.Rows != null && dtExistingButtons.Rows.Count > 0)
            {
                DataTable dtSecurityAndSupport = dc.GetGroupButtons(iClient, iNewUserGroupID);

                foreach (DataRow button in dtExistingButtons.Rows)
                {
                    int iButtonId = -1;
                    if (button["Id"] != null)
                    {
                        int.TryParse(button["Id"].ToString(), out iButtonId);
                    }

                    int iNewButtonId = -1;

                    //if (button["ButtonName"] != null && button["ButtonName"].ToString().ToLower() == "security")
                    //{
                    //    iNewButtonId = GetButtonId(dtSecurityAndSupport, "security");
                    //}
                    //else if (button["ButtonName"] != null && button["ButtonName"].ToString().ToLower() == "support")
                    //{
                    //    iNewButtonId = GetButtonId(dtSecurityAndSupport, "support");
                    //}
                    //else
                    //{
                    //Copy buttons
                    iNewButtonId = dc.InsertButton(iClient, iUserId, iNewUserGroupID, button["ButtonName"].ToString(), button["ButtonTitle"].ToString(), button["ButtonMessage"].ToString(),
                         button["ButtonIcon"].ToString(), button["Severity"].ToString(), button["HotKeyCode"].ToString(), button["HotKeyModifier"].ToString(), button["AudibleAlarmLevel"].ToString(), Convert.ToInt32(button["GroupMessage"]),
                         button["SendDelay"].ToString(), Convert.ToInt32(button["Mute"]));
                    //}


                    //Copy SMS Receivers
                    DataTable dtExistingSMSReceivers = dc.GetSMSReceivers(iClient, iButtonId);
                    if (dtExistingSMSReceivers != null && dtExistingSMSReceivers.Rows != null && dtExistingSMSReceivers.Rows.Count > 0)
                    {
                        string[] smses = new string[dtExistingSMSReceivers.Rows.Count];
                        int i = 0;
                        foreach (DataRow sms in dtExistingSMSReceivers.Rows)
                        {
                            if (sms["Number"] != null)
                            {
                                smses[i++] = sms["Number"].ToString();
                            }
                        }
                        dc.InsertMassSMS(iClient, iUserId, iNewButtonId, smses);
                    }

                    //Copy emails

                    DataTable dtExistingEmailReceivers = dc.GetEmailReceivers(iClient, iButtonId);
                    if (dtExistingEmailReceivers != null && dtExistingEmailReceivers.Rows != null && dtExistingEmailReceivers.Rows.Count > 0)
                    {
                        string[] emails = new string[dtExistingEmailReceivers.Rows.Count];
                        int i = 0;
                        foreach (DataRow email in dtExistingEmailReceivers.Rows)
                        {
                            if (email["EmailAddress"] != null)
                            {
                                emails[i++] = email["EmailAddress"].ToString();
                            }
                        }
                        dc.InsertMassEmailAddress(iClient, iUserId, iNewButtonId, emails);
                    }

                    //Copy Desktop Receivers

                    DataTable dtExistingMachineReceivers = dc.GetMachineReceivers(iClient, iButtonId);
                    if (dtExistingMachineReceivers != null && dtExistingMachineReceivers.Rows != null && dtExistingMachineReceivers.Rows.Count > 0)
                    {
                        string machines = string.Empty;

                        foreach (DataRow machine in dtExistingMachineReceivers.Rows)
                        {
                            if (machine["Id"] != null)
                            {
                                machines += machine["Id"].ToString() + "|";
                            }
                        }
                        dc.InsertMachineReceivers(iClient, iUserId, iNewButtonId, button["ButtonName"].ToString(), machines);
                    }
                }
            }

        }

        protected void epbGrid_RowAdding(object sender, Trirand.Web.UI.WebControls.JQGridRowAddEventArgs e)
        {
            try
            {
                DataTable dt = dc.GetGroups(iClient);
                DataRow row = dt.NewRow();

                string sGroupName = e.RowData["GroupName"];
                string sGroupDesc = e.RowData["GroupDescription"];
                string sDefaultGroup = e.RowData["ClientDefaultGroup"];
         //       int iCopyGroupId = -1;

          //      int.TryParse(e.RowData["Copy buttons"], out iCopyGroupId);

                if (row != null)
                {
                    int iGroup = dc.InsertGroup(iClient, iUserId, sGroupName, sGroupDesc, sDefaultGroup);
                    QueueClass q = new QueueClass();
                    q.CreateGroupQueueMsg(iGroup.ToString(), iGroup.ToString(), "update");

                }

                epbGrid.DataSource = dt;
                epbGrid.DataBind();
            }
            catch (System.Exception ex)
            {
                string method = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();
                error.ErrorCatch(this.ToString(), iClient.ToString(), iUserId.ToString(), method, ex.Message, ex.StackTrace);
                Response.Redirect("/apperror.aspx", false);

            }
        }

        protected void epbGrid_RowEditing(object sender, Trirand.Web.UI.WebControls.JQGridRowEditEventArgs e)
        {
            try
            {
                DataTable dt = dc.GetGroups(iClient);
                int iGroup = Convert.ToInt32(e.RowData[3].ToString());

                dt.PrimaryKey = new DataColumn[] { dt.Columns["Id"] };
                DataRow drow = dt.Rows.Find(e.RowKey);

                string sGroupName = e.RowData["GroupName"];
                string sGroupDesc = e.RowData["GroupDescription"];
                int iMakeDefault = Convert.ToInt32(e.RowData["ClientDefaultGroup"].ToString());

                if (drow != null)
                {
                    int iRow = Convert.ToInt32(drow.ItemArray[0].ToString());
                    dc.UpdateGroup(iClient, iUserId, iGroup, sGroupName, sGroupDesc, iMakeDefault);

                    QueueClass q = new QueueClass();
                    q.CreateGroupQueueMsg(iGroup.ToString(), iGroup.ToString(), "update");
                }

                epbGrid.DataSource = dc.GetGroups(iClient);
                epbGrid.DataBind();

            }
            catch (System.Exception ex)
            {
                string method = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();
                error.ErrorCatch(this.ToString(), iClient.ToString(), iUserId.ToString(), method, ex.Message, ex.StackTrace);
                Response.Redirect("/apperror.aspx", false);

            }
        }

        protected void epbGrid_RowDeleting(object sender, Trirand.Web.UI.WebControls.JQGridRowDeleteEventArgs e)
        {
            try
            {
                DataTable dt = dc.GetGroups(iClient);
                dt.PrimaryKey = new DataColumn[] { dt.Columns["Id"] };
                DataRow drow = dt.Rows.Find(e.RowKey);

                if (drow != null)
                {
                    string sGroupId = drow.ItemArray[0].ToString();
                    string sGroupName = drow.ItemArray[1].ToString();
                    dc.DeleteGroup(iClient, iUserId, sGroupName, sGroupId);

                    QueueClass q = new QueueClass();
                    q.CreateGroupQueueMsg(sGroupId, sGroupId, "update");
                }


                epbGrid.DataSource = dc.GetGroups(iClient);
                epbGrid.DataBind();

            }
            catch (System.Exception ex)
            {

                ErrorHandler eh = new ErrorHandler();
                eh.ErrorWrite(ex.ToString());

                string method = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();
                error.ErrorCatch(this.ToString(), iClient.ToString(), iUserId.ToString(), method, ex.Message, ex.StackTrace);
                Response.Redirect("/apperror.aspx", false);

            }
        }

    }
}