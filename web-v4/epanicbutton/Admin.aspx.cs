﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Security.Cryptography;

namespace epanicbutton
{
    public partial class Admin : System.Web.UI.Page
    {

        DataClass dc = new DataClass();
        Utility ut = new Utility();
        Session session = new Session();
        ErrorHandler error = new ErrorHandler();
        int iClient, iUserId;
        string password = string.Empty;
        string sAdminType = string.Empty;


        protected void Page_Load(object sender, EventArgs e)
        {
                if ((Session["clientid"] != null) && ((Session["userid"] != null)))
                {
                    iClient = Convert.ToInt32(Session["clientid"].ToString());
                    iUserId = Convert.ToInt32(Session["userid"].ToString());
                    sAdminType = (Session["admintype"].ToString());
                }
                else
                {
                    Response.Redirect("login.aspx", true);
                    return;
                }

                this.lblAccountMsg.Visible = false;
                this.lblPasswordMsg.Visible = false;

                if (!IsPostBack)
                {
                    DataTable clients = new DataTable();

                    if (sAdminType == "Global")
                    {
                        ddlClients.Visible = true;
                        lblClients.Visible = true;
                        lblCode.Visible = true;
                        txtCode.Visible = true;
                        btnCode.Visible = true;

                        epbGrid.ToolBarSettings.ShowDeleteButton = true;

                        clients = dc.GetClients();
                        ddlClients.DataSource = clients;
                        ddlClients.DataValueField = "id";
                        ddlClients.DataTextField = "name";
                        ddlClients.DataBind();

                        ddlClientList.DataSource = clients;
                        ddlClientList.DataValueField = "id";
                        ddlClientList.DataTextField = "name";
                        ddlClientList.DataBind();

                        ddlClients.SelectedValue = iClient.ToString();

                        epbGrid.DataSource = dc.GetAdmins(0);
                        epbGrid.DataBind();

                        ddlTimeZone.Visible = false;
                        trTimezone.Visible = false;
                    }
                    else
                    {
                        clients = dc.GetUserClient(iClient);
                        ddlClients.DataSource = clients;
                        ddlClients.DataValueField = "id";
                        ddlClients.DataTextField = "name";
                        ddlClients.DataBind();

                        ddlClientList.DataSource = clients;
                        ddlClientList.DataValueField = "id";
                        ddlClientList.DataTextField = "name";
                        ddlClientList.DataBind();

                        ddlClients.SelectedValue = iClient.ToString();
                        ddlClientList.SelectedValue = iClient.ToString();

                        epbGrid.DataSource = dc.GetAdmins(iClient);
                        epbGrid.DataBind();

                        trTimezone.Visible = true;
                        ddlTimeZone.Visible = true;
                        ddlTimeZone.DataSource = Utility.AllTimeZones();
                        ddlTimeZone.DataValueField = "ID";
                        ddlTimeZone.DataTextField = "DisplayName";
                        ddlTimeZone.DataBind();

                        //Set the clients time zone..
                        DataTable dtClient = dc.GetClient(iClient);
                        if (dtClient != null && dtClient.Rows != null && dtClient.Rows.Count > 0)
                        {
                            object oTimezone = dtClient.Rows[0]["TimeZone"];
                            if (oTimezone != null)
                            {
                                ddlTimeZone.SelectedValue = oTimezone.ToString();
                            }
                        }

                    }

                    DataTable dt = dc.GetSingleAdmin(iClient, iUserId);
                    if (dt.Rows.Count > 0)
                    {
                        lblLogin.Text = dt.Rows[0]["AdminName"].ToString();
                        this.txtEmail.Text = dt.Rows[0]["EmailAddress"].ToString();
                        this.txtLastName.Text = dt.Rows[0]["LastName"].ToString();
                        this.txtFirstName.Text = dt.Rows[0]["FirstName"].ToString();
                    }

                    if (Session["changepwd"] != null)
                    {
                        if (Session["changepwd"].ToString() == "True")
                        {
                            lblPasswordMsg.Visible = true;
                            lblPasswordMsg.Text = "You must change your password to continue.";
                            Page.ClientScript.RegisterStartupScript(this.GetType(), "messgebox", "displayPasswordMsg();", true);
                        }
                    }
                }
           
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string email, fname, lname, admin;
                email = ut.MakeSafeParameter(txtEmail.Text.ToString());
                fname = ut.MakeSafeParameter(txtFirstName.Text.ToString());
                lname = ut.MakeSafeParameter(txtLastName.Text.ToString());
                admin = lblLogin.Text.ToString();

                if (sAdminType == "Global")
                {
                    int iselectClient = Convert.ToInt16(ddlClients.SelectedValue.ToString());

                    dc.UpdateGlobalAdmin(iClient, iUserId, admin, email, fname, lname, iselectClient);
                }
                else
                {
                    dc.UpdateAdmin(iClient, iUserId, admin, email, fname, lname, "0");
                    dc.UpdateClientTimezone(iClient, ddlTimeZone.SelectedValue);
                }

                this.lblAccountMsg.Visible = true;
                this.lblAccountMsg.Text = "Account details successfully updated.";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "messgebox", "displayPasswordMsg();", true);

            }
            catch (System.Exception ex)
            {
                string method = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();
                error.ErrorCatch(this.ToString(), iClient.ToString(), iUserId.ToString(), method, ex.Message, ex.StackTrace);
                Response.Redirect("/apperror.aspx", true);

            }
        }

        private bool ValidPassword(string sPwd, string sUser)
        {
            bool uservalid = false;
            bool pwdvalid = false;

            uservalid = UserGetPassword(sUser);

            if (UserAccount.VerifyHash(sPwd, "MD5", password))
            {
                pwdvalid = true;
            }

            return pwdvalid;
        }

        protected void btnUpdatePwd_Click(object sender, EventArgs e)
        {
            this.lblPasswordMsg.Visible = false;
            //(?=^.{8,}$)(?=.*\d)(?=.*[!@#$%^&*]+)(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$


            try
            {
                string currentpwd, newpwd, admin, email;
                currentpwd = ut.MakeSafeParameter(this.txtPassword.Text.ToString());
                newpwd = ut.MakeSafeParameter(this.txtNew.Text.ToString());
                admin = lblLogin.Text.ToString();
                email = txtEmail.Text.ToString();

                //check current password entered
                if (this.txtNew.Text != this.txtConfirm.Text)
                {
                    this.lblPasswordMsg.Visible = true;
                    this.lblPasswordMsg.Text = "New password and confirm password are not the same.";
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "messgebox", "displayPasswordMsg();", true);
                }

                else if (!ut.ValidatePasswordStrength(newpwd, 8))
                {
                    this.lblPasswordMsg.Visible = true;
                    this.lblPasswordMsg.Text = "New password is not strong enough.";
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "messgebox", "displayPasswordMsg();", true);
                }

                else if (ValidPassword(currentpwd, admin))
                {
                    string msg = "Your admin password for ePanicButton has been updated successfully.";
                    newpwd = UserAccount.ComputeHash(newpwd, "MD5", null);


                    dc.UpdatePwd(iClient, iUserId, admin, currentpwd, newpwd);

                    Utility.SendSimpleMessage(email, msg);

                    this.lblPasswordMsg.Visible = true;
                    this.lblPasswordMsg.Text = "Password successfully updated.  A confirmation email has been sent.";
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "messgebox", "displayPasswordMsg();", true);
                    Session["changepwd"] = null;
                }

                else
                {
                    this.lblPasswordMsg.Visible = true;
                    this.lblPasswordMsg.Text = "Current password is incorrect.";
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "messgebox", "displayPasswordMsg();", true);
                }
            }


            catch (System.Exception ex)
            {
                string method = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();
                error.ErrorCatch(this.ToString(), iClient.ToString(), iUserId.ToString(), method, ex.Message, ex.StackTrace);
                Response.Redirect("/apperror.aspx", true);

            }
        }

        //todo move to general class and re-use login
        public bool UserGetPassword(string user)
        {
            DataTable dt = new DataTable();
            try
            {
                string strQuery = "exec GetPassword @userid";
                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.Parameters.Add("@userid", SqlDbType.VarChar).Value = user;

                dt = dc.dTable(cmd);

                if (dt.Rows.Count > 0)
                {
                    password = dt.Rows[0]["password"].ToString();

                    session.ClientId = Convert.ToInt32(dt.Rows[0]["clientsid"].ToString());
                    session.UserId = Convert.ToInt32(dt.Rows[0]["adminsid"].ToString());

                    Session["session"] = session;

                    Session["clientid"] = dt.Rows[0]["clientsid"].ToString();
                    Session["userid"] = dt.Rows[0]["adminsid"].ToString();

                }
                if (dt.Rows.Count == 0)
                {
                    this.lblPasswordMsg.Text = "User ID or Password is incorrect.";
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "messgebox", "displayPasswordMsg();", true);
                    return false;
                }
            }

            catch (System.Exception ex)
            {
                error.ErrorCatch("Login.aspx", "0", "0", "UserGetPassword", ex.Message, ex.StackTrace);
                Response.Redirect("/apperror.aspx", false);

            }

            return true;

        }

        protected void epbGrid_RowEditing(object sender, Trirand.Web.UI.WebControls.JQGridRowEditEventArgs e)
        {
            try
            {
                string msg = "Your ePanicButton password has been set to eP@nic123.  You will be required to change it the next time you login.";

                DataTable dt = new DataTable();

                if (sAdminType == "Global")
                {
                    dt = dc.GetAdmins(0);
                }
                else
                {
                    dt = dc.GetAdmins(iClient);
                }

                dt.PrimaryKey = new DataColumn[] { dt.Columns["Id"] };
                DataRow drow = dt.Rows.Find(e.RowKey);

                int iThisClient = Convert.ToInt32(e.RowData[0]);
                string admin = e.RowData["AdminName"];
                string email = e.RowData["EmailAddress"];
                string fname = e.RowData["FirstName"];
                string lname = e.RowData["LastName"];
                string reset = e.RowData["ChangePassword"];

                if (drow != null)
                {
                    int iRow = Convert.ToInt32(drow.ItemArray[0].ToString());
                    dc.UpdateAdmin(iThisClient, iRow, admin, email, fname, lname, reset);
                }

                Utility.SendSimpleMessage(email, msg);

                if (sAdminType == "Global")
                {
                    epbGrid.DataSource = dc.GetAdmins(0);

                }
                else
                {
                    epbGrid.DataSource = dc.GetAdmins(iClient);
                }
                epbGrid.DataBind();


            }
            catch (System.Exception ex)
            {
                string method = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();
                error.ErrorCatch(this.ToString(), iClient.ToString(), iUserId.ToString(), method, ex.Message, ex.StackTrace);
                Response.Redirect("/apperror.aspx", false);

            }
        }

        protected void epbGrid_RowDeleting(object sender, Trirand.Web.UI.WebControls.JQGridRowDeleteEventArgs e)
        {
            try
            {
                DataTable dt = new DataTable();

                if (sAdminType == "Global")
                {
                    dt = dc.GetAdmins(0);
                }
                else
                {
                    dt = dc.GetAdmins(iClient);
                }

                dt.PrimaryKey = new DataColumn[] { dt.Columns["Id"] };
                DataRow drow = dt.Rows.Find(e.RowKey);

                if (drow != null)
                {
                    int iAdminToRemove = Convert.ToInt32(drow.ItemArray[0].ToString());
                    int iReturn = dc.DeleteAdmin(iClient, iAdminToRemove, iUserId);
                }


                if (sAdminType == "Global")
                {
                    epbGrid.DataSource = dc.GetAdmins(0);

                }
                else
                {
                    epbGrid.DataSource = dc.GetAdmins(iClient);
                }
                epbGrid.DataBind();


            }
            catch (System.Exception ex)
            {
                string method = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();
                error.ErrorCatch(this.ToString(), iClient.ToString(), iUserId.ToString(), method, ex.Message, ex.StackTrace);
                Response.Redirect("/apperror.aspx", false);

            }
        }

        protected void epbGrid_RowAdding(object sender, Trirand.Web.UI.WebControls.JQGridRowAddEventArgs e)
        {
            try
            {
                DataTable dt = dc.GetAdmins(iClient);

                // dt.PrimaryKey = new DataColumn[] { dt.Columns["Id"] };
                DataRow row = dt.NewRow();

                iClient = Convert.ToInt16(e.RowData[0]);

                string sAdminName = e.RowData["AdminName"];
                string sEmailAddress = e.RowData["EmailAddress"];
                string sFirstName = e.RowData["FirstName"];
                string sLastName = e.RowData["LastName"].ToString();


                if (row != null)
                {
                    dc.InsertAdminUser(iClient, iUserId, sAdminName, 3, sEmailAddress, sFirstName, sLastName);

                    Utility.SendWelcomeMessage(sEmailAddress, sAdminName);
                }
                epbGrid.DataSource = dt;
                epbGrid.DataBind();
            }


            catch (System.Exception ex)
            {
                string method = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();
                error.ErrorCatch(this.ToString(), iClient.ToString(), iUserId.ToString(), method, ex.Message, ex.StackTrace);
            }
        }

        protected void btnCode_Click(object sender, EventArgs e)
        {
            this.txtCode.Text = Utility.Base64Encode(this.txtCode.Text);
        }

    }
}