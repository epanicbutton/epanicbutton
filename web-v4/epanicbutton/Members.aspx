﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Main.Master" AutoEventWireup="true" CodeBehind="Members.aspx.cs" Inherits="epanicbutton.Members" %>

<%@ Register Assembly="Trirand.Web" Namespace="Trirand.Web.UI.WebControls" TagPrefix="cc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="header">
        <asp:Label ID="lblMemberHeader" runat="server" Text=""></asp:Label>
    </div>

    <div id="msg" class="ui-state-highlight">
        <asp:Label ID="lblMsg" runat="server" Visible="False"></asp:Label></div>

    <div id="lhdr">Current Members</div>
    <div id="lcontent">
        <cc1:JQGrid ID="epbGrid" runat="server" Width="491" Height="385" PagerSettings-PageSize="18">
            <Columns>
                <cc1:JQGridColumn DataField="id" Editable="false" PrimaryKey="true" Visible="False" Searchable="false" />
                <cc1:JQGridColumn DataField="UserLastName" HeaderText="Last Name" Editable="false" />
                <cc1:JQGridColumn DataField="UserFirstName" HeaderText="First Name" Editable="false" />
                <cc1:JQGridColumn DataField="UserLocation" HeaderText="User Location" Editable="false" />
                <cc1:JQGridColumn DataField="GroupName" HeaderText="Group Name" Editable="false" />
                <cc1:JQGridColumn DataField="UserDepartment" HeaderText="Department" Editable="false" />
            </Columns>
            <ToolBarSettings ToolBarPosition="Bottom" ShowSearchButton="true" ShowRefreshButton="true" />
            <PagerSettings PageSize="18" PageSizeOptions="[18,20,50,100]" />
            <AppearanceSettings HighlightRowsOnHover="true" />
            <ClientSideEvents RowSelect="crSelected" AfterEditDialogShown="hideNav" LoadComplete="crRestore" />
            <SearchDialogSettings MultipleSearch="true" />
        </cc1:JQGrid>
    </div>


    <div id="mhdr"></div>
    <div id="mcontent">
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <asp:Button ID="btnDelMember" runat="server" Text="<< Move " Width="85" OnClick="btnDelMember_Click" />

    </div>

    <div id="rhdr">Available Members</div>
    <div id="rcontent">
        <cc1:JQGrid ID="epbGridMember" runat="server" Width="491" Height="385" PagerSettings-PageSize="18" MultiSelect="true" MultiSelectMode="SelectOnRowClick">
            <Columns>
                <cc1:JQGridColumn DataField="id" Editable="false" PrimaryKey="true" Visible="False" Searchable="false" />
                <cc1:JQGridColumn DataField="UserLastName" HeaderText="Last Name" Editable="false" />
                <cc1:JQGridColumn DataField="UserFirstName" HeaderText="First Name" Editable="false" />
                <cc1:JQGridColumn DataField="UserLocation" HeaderText="User Location" Editable="false" />
                <cc1:JQGridColumn DataField="GroupName" HeaderText="Group Name" Editable="false" />
                <cc1:JQGridColumn DataField="UserDepartment" HeaderText="Department" Editable="false" />


            </Columns>
            <ToolBarSettings ToolBarPosition="Bottom" ShowSearchButton="true" ShowRefreshButton="true" />
            <PagerSettings PageSize="18" PageSizeOptions="[18,20,50,100]" />
            <AppearanceSettings HighlightRowsOnHover="true" />
            <ClientSideEvents RowSelect="nrSelected" AfterEditDialogShown="hideNav" LoadComplete="arRestore" />
            <SearchDialogSettings MultipleSearch="true" />
        </cc1:JQGrid>

    </div>

    <asp:HiddenField runat="server" ID="crSelectedRowsState" />
    <asp:HiddenField runat="server" ID="nrSelectedRowsState" />
    <script type="text/javascript">
        var selectedNrRows = [];
        var selectedCrRows = [];
        var grid = $("#<%= epbGrid.ClientID %>");
        var amgrid = $("#<%= epbGridMember.ClientID %>");

        function crSelected(rowID, isSelected) {
            selectedCrRows[rowID] = isSelected;
            crSelectedRowsHidden();
        }

        function nrSelected(rowID, isSelected) {
            selectedNrRows[rowID] = isSelected;
            nrSelectedRowsHidden();
        }


        function crRestore() {
            $('.cbox').prop("checked", false);
            for (var row in selectedCrRows) {
                if (selectedCrRows[row])
                    grid.setSelection(row);
            }
        }

        function arRestore() {
            $('.cbox').prop("checked", false);
            for (var row in selectedNrRows) {
                if (selectedNrRows[row])
                    amgrid.setSelection(row);
            }
        }

        function crSelectedRowsHidden() {
            var hiddenField = $("#<%= crSelectedRowsState.ClientID %>");
            var selectedValues = "";

            for (var row in selectedCrRows) {
                if (selectedCrRows[row])
                    selectedValues = selectedValues + row + "|";
            }

            hiddenField.val(selectedValues);
        }

        function nrSelectedRowsHidden() {
            var hiddenField = $("#<%= nrSelectedRowsState.ClientID %>");
            var selectedValues = "";

            for (var row in selectedNrRows) {
                if (selectedNrRows[row])
                    selectedValues = selectedValues + row + "|";
            }

            hiddenField.val(selectedValues);
        }

        $("#<%= btnDelMember.ClientID %>").click(function () {
            var hiddenField = $("#<%= nrSelectedRowsState.ClientID %>");
            var selectedValues = $(amgrid).jqGrid('getGridParam', 'selarrrow');
            hiddenField.val(selectedValues);
        });

    </script>


</asp:Content>
