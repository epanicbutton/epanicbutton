﻿using System;
using System.Data;

namespace epanicbutton
{
    public partial class Grid : System.Web.UI.Page
    {
        DataClass dc = new DataClass();
        ErrorHandler error = new ErrorHandler();
        int iClient, iUserId, iButton, iGroup;


        protected void Page_Load(object sender, EventArgs e)
        {

            if ((Session["clientid"] != null) && ((Session["userid"] != null)))
            {
                iClient = Convert.ToInt32(Session["clientid"].ToString());
                iUserId = Convert.ToInt32(Session["userid"].ToString());
            }
            else
            {
                Response.Redirect("login.aspx");
            }
            try
            {
                epbGrid.DataSource = dc.GetUsers(iClient);
                epbGrid.DataBind();

                ddlGroups.DataValueField = "ID";
                ddlGroups.DataTextField = "GroupName";
                ddlGroups.DataSource = LoadGroups(iClient);
                ddlGroups.DataBind();
            }
            catch (System.Exception ex)
            {
                string method = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();
                error.ErrorCatch(this.ToString(), iClient.ToString(), iUserId.ToString(), method, ex.Message, ex.StackTrace);
                Response.Redirect("/apperror.aspx", false);
               

            }

        }

        protected void epbGrid_RowAdding(object sender, Trirand.Web.UI.WebControls.JQGridRowAddEventArgs e)
        {
            
        }

        protected void epbGrid_RowEditing(object sender, Trirand.Web.UI.WebControls.JQGridRowEditEventArgs e)
        {
            try
            {

                DataTable dt = dc.GetUsers(iClient);
                int iGroup = Convert.ToInt32(e.RowData[3].ToString());

                dt.PrimaryKey = new DataColumn[] { dt.Columns["Id"] };
                DataRow drow = dt.Rows.Find(e.RowKey);

                if (drow != null)
                {
                    int iRow = Convert.ToInt32(drow.ItemArray[0].ToString());
                    int iOldGroupID = dc.UpdateUser(iClient, iRow, iUserId, iGroup);

                    QueueClass q = new QueueClass();
                    q.CreateGroupQueueMsg(iOldGroupID.ToString(), iGroup.ToString(), "usermove");
                }

                epbGrid.DataSource = dc.GetUsers(iClient);
                epbGrid.DataBind();

            }
            catch (System.Exception ex)
            {
                string method = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();
                error.ErrorCatch(this.ToString(), iClient.ToString(), iUserId.ToString(), method, ex.Message, ex.StackTrace);
                Response.Redirect("/apperror.aspx", false);
            }
        }

        protected void epbGrid_RowDeleting(object sender, Trirand.Web.UI.WebControls.JQGridRowDeleteEventArgs e)
        {

            try
            {
                DataTable dt = dc.GetUsers(iClient);
                dt.PrimaryKey = new DataColumn[] { dt.Columns["Id"] };
                DataRow drow = dt.Rows.Find(e.RowKey);

                if (drow != null)
                {
                    int row = Convert.ToInt32(drow.ItemArray[0].ToString());
                    dc.DeleteUsers(iClient, iUserId, row);
                }


                epbGrid.DataSource = dc.GetUsers(iClient);
                epbGrid.DataBind();

            }
            catch (System.Exception ex)
            {
                string method = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();
                error.ErrorCatch(this.ToString(), iClient.ToString(), iUserId.ToString(), method, ex.Message, ex.StackTrace);
                Response.Redirect("/apperror.aspx", false);
               
            }
        }

        protected DataTable LoadGroups(int iClient)
        {
            DataTable dtGroups = dc.GetGroups(iClient);
            return dtGroups;
        }

    }
}