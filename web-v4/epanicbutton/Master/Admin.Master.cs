﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace epanicbutton.Master
{
    public partial class Admin : System.Web.UI.MasterPage
    {
        int iAdminType = 1;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["loggedin"] == null)
            { 
                Response.Redirect("~/login.aspx", true);
            }
            else
            {
                if (iAdminType == 1)
                {
                    this.bAccount.Visible = true;
                }
            }

            if (Session["ClientName"] != null)
            {
                lblClientName.Text = " - " + Session["ClientName"].ToString();
            }

            if (Session["changepwd"] != null)
            {
                if (Session["changepwd"].ToString() == "True")
                {
                    //  Response.Redirect("~/admin.aspx", true);
                }
            }
        }

        protected void bGroups_Click(object sender, EventArgs e)
        {
            Response.Redirect("/groups.aspx", true);
        }

        protected void bUsers_Click(object sender, EventArgs e)
        {
            Response.Redirect("/users.aspx", true);
        }

        protected void bButtons_Click(object sender, EventArgs e)
        {
            Session["groupid"] = null;
            Response.Redirect("/buttons.aspx", true);
        }

        protected void bLogout_Click(object sender, EventArgs e)
        {
            Response.Redirect("/logout.aspx", true);
        }

        protected void bAccount_Click(object sender, EventArgs e)
        {
            Response.Redirect("/accounts.aspx", true);
        }

        protected void bEvents_Click(object sender, EventArgs e)
        {
            Response.Redirect("/events.aspx", true);
        }

        protected void bAdmin_Click(object sender, EventArgs e)
        {
            Response.Redirect("/admin.aspx", true);
        }

        protected void bReceivers_Click(object sender, EventArgs e)
        {
            Response.Redirect("/receivers.aspx", true);
        }

        protected void bMessages_Click(object sender, EventArgs e)
        {
            Response.Redirect("/Msg.aspx", true);
        }
    }     
}
