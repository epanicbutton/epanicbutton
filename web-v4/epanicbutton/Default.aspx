﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Main.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="epanicbutton.WebForm1" %>

<%@ Register Assembly="Trirand.Web" Namespace="Trirand.Web.UI.WebControls" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">



    <script type="text/javascript">
        function addRow() {

            alert('add');
            var grid = jQuery("#<%= jqGrid.ID %>");
            grid.editGridRow("new", grid.addDialogOptions);
        }

        function editRow() {
            alert('edit');
            var grid = jQuery("#<%= jqGrid.ID %>");
            var rowKey = grid.getGridParam("selrow");
            var editOptions = grid.getGridParam('editDialogOptions');
            if (rowKey) {
                grid.editGridRow(rowKey, editOptions);
            }
            else {
                alert("No rows are selected");
            }
        }

        function delRow() {
            alert('delete');
            var grid = jQuery("#<%= jqGrid.ID %>");
            var rowKey = grid.getGridParam("selrow");
            if (rowKey) {
                grid.delGridRow(rowKey, grid.delDialogOptions);
            }
            else {
                alert("No rows are selected");
            }
        }

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div id="message"></div>



    <div id="content">
        <cc1:JQGrid ID="jqGrid" runat="server" Width="1068" Height="460" PagerSettings-PageSize="20"
            OnRowDeleting="jqGrid_RowDeleting"
            OnRowAdding="jqGrid_RowAdding"
            OnRowEditing="jqGrid_RowEditing">
            <Columns>
                <cc1:JQGridColumn DataField="Id" Visible="True" Width="20" />
                <cc1:JQGridColumn DataField="UserLastName" />
                <cc1:JQGridColumn DataField="UserFirstName" />
                <cc1:JQGridColumn DataField="GroupName" />
            </Columns>
            <ToolBarSettings ShowInlineEditButton="true" ShowDeleteButton="true" ShowRefreshButton="true" />
            <EditDialogSettings CloseAfterEditing="true" Caption="The Edit Dialog" />
            <AddDialogSettings CloseAfterAdding="true" />
        </cc1:JQGrid>
    </div>
</asp:Content>
