﻿
<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Main.Master" AutoEventWireup="true" CodeBehind="Accounts.aspx.cs" Inherits="epanicbutton.Accounts" %>

<%@ Register Assembly="Trirand.Web" Namespace="Trirand.Web.UI.WebControls" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="header">Account Manager</div>
    <div id="content">
        <cc1:JQGrid ID="epbGrid" runat="server" Width="1082" Height="414" PagerSettings-PageSize="18"
            OnRowEditing="epbGrid_RowEditing"
            OnRowAdding="epbGrid_RowAdding">
            <Columns>
                <cc1:JQGridColumn DataField="Id" Editable="false" PrimaryKey="false" Visible="false" Searchable="true" />
                <cc1:JQGridColumn DataField="AdminsId" Editable="false" PrimaryKey="true" Visible="false" Searchable="true" />
                <cc1:JQGridColumn DataField="ClientName" HeaderText="Client Name" Editable="true" PrimaryKey="false" Visible="true" Searchable="true">
                    <EditClientSideValidators>
                        <cc1:RequiredValidator />
                        <cc1:CustomValidator ValidationFunction="dupeClientChk" />
                    </EditClientSideValidators>
                </cc1:JQGridColumn>
                <cc1:JQGridColumn DataField="RegistrationId" HeaderText="Registration ID" Editable="false" PrimaryKey="false" Visible="true" Searchable="true" Width="300" />
                <cc1:JQGridColumn DataField="TotalConnections" HeaderText="Users" Editable="false" PrimaryKey="false" Visible="false" Searchable="true" Width="50" TextAlign="Center" />
<%--
    20160311 - JH
    Comment out until this field is really used 
                <cc1:JQGridColumn DataField="InactiveDate" HeaderText="Expiration" Editable="true" PrimaryKey="false" Visible="true" Searchable="true" Width="75" TextAlign="Center" />
--%>
                <cc1:JQGridColumn DataField="AdminName" HeaderText="Admin Name" Editable="true" PrimaryKey="false" Visible="true" Searchable="true" TextAlign="Left">
                    <EditClientSideValidators>
                        <cc1:RequiredValidator />
                        <cc1:CustomValidator ValidationFunction="dupeAdminChk" />
                    </EditClientSideValidators>
                </cc1:JQGridColumn>
                <cc1:JQGridColumn DataField="EmailAddress" HeaderText="Admin Email" Editable="true" PrimaryKey="false" Visible="true" Searchable="true" TextAlign="Left" />
                <cc1:JQGridColumn DataField="Emails" HeaderText="Emails" Editable="false" EditDialogLabel="Email Messaging Count" PrimaryKey="false" Visible="true" Searchable="false" TextAlign="Center" Width="75" />
                <cc1:JQGridColumn DataField="Machines" HeaderText="Desktops" Editable="false" EditDialogLabel="Desktop Messaging Count" PrimaryKey="false" Visible="true" Searchable="false" TextAlign="Center" Width="75" />
                <cc1:JQGridColumn DataField="SMS" HeaderText="Texts" Editable="false" EditDialogLabel="Text Messaging Count" PrimaryKey="false" Visible="true" Searchable="false" TextAlign="Center" Width="75" />

                <cc1:JQGridColumn DataField="Active" HeaderText="Active" Editable="true" PrimaryKey="false" Visible="true" Searchable="true" Width="75" TextAlign="Center"
                    EditType="Custom"
                    EditTypeCustomCreateElement="createActive"
                    EditTypeCustomGetValue="getActive" />

                <cc1:JQGridColumn DataField="TimeZone" Editable="True" PrimaryKey="false" Visible="false" Searchable="false"
                    EditType="DropDown"
                    EditorControlID="ddlTimeZones" />

            </Columns>
            <ToolBarSettings ToolBarPosition="Bottom" ShowEditButton="false" ShowAddButton="false" ShowDeleteButton="false" ShowSearchButton="true" ShowRefreshButton="true" />
            <EditDialogSettings CloseAfterEditing="true" Caption="Client Editor" Width="500" LoadingMessageText="Loading..." />
            <AddDialogSettings CloseAfterAdding="true" Width="500" />
            <AppearanceSettings HighlightRowsOnHover="true" />
            <ClientSideEvents RowSelect="editRow" AfterAddDialogShown="accountAdd" AfterEditDialogShown="accountEdit" />

            <%---%>
        </cc1:JQGrid>
    </div>
    <asp:DropDownList runat="server" ID="ddlTimeZones" CssClass="width" />
    <style>
        .width {
            width: 300px !important;
        }
    </style>
    <script type="text/javascript">
        // group functions
        function editRow(id) {
            var grid = $("#<%= epbGrid.ClientID %>");
            var obj = $(grid).jqGrid('getRowData', id);
            var def = obj.AdminType;
        }

        function dupeClientChk(value, column) {
            if (value.length > 100)
                return [false, column + ": maximum length of client name is 100!"];

            if ($("#ClientName")[0].className.indexOf('disabled') > 0) {
                return ["", true];
            }

            else {
                var result, msg;
                $.ajax({
                    type: "POST",
                    url: '/ajax/AdminUtilities.aspx/ClientExist',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ ClientName: value }),
                    async: false,
                    success: function (data) {
                        if (data.d.Success) {
                            msg = "";
                            result = true;
                        }
                        else {
                            msg = "A client is already defined with that name.";
                            result = false;
                        }
                    },
                    error: function () {
                        msg = "An error occurred attempting to validate your request";
                        result = false
                    }
                });
                return [result, msg];
            }
        }

        function dupeAdminChk(value, column) {
            var address = $('#EmailAddress').val();
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if (address == '' || !re.test(address)) {
                return [false, 'Please enter a valid email address!'];
            }
            else if (value.length > 40) {
                return [false, column + ": maximum length of admin name length is 40!"];
            }
            else if ($("#AdminName")[0].className.indexOf('disabled') > 0) {
                return ["", true];
            }
            else {
                var result, msg;
                $.ajax({
                    type: "POST",
                    url: '/ajax/AdminUtilities.aspx/AdminExist',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ AdminName: value }),
                    async: false,
                    success: function (data) {
                        if (data.d.Success) {
                            msg = "";
                            result = true;
                        }
                        else {
                            msg = "Admin Name must be unique";
                            result = false;
                        }
                    },
                    error: function () {
                        msg = "An error occurred attempting to validate your request";
                        result = false
                    }
                });

                return [result, msg];
            }
        }
        

    </script>

</asp:Content>

