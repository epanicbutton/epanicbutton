﻿using System;
using System.Data;

namespace epanicbutton
{
    public partial class Members : System.Web.UI.Page
    {
        DataClass dc = new DataClass();
        ErrorHandler error = new ErrorHandler();
        Session session = new Session();
        int iClient, iUserId, iGroup;
        string sGroup;


        protected void Page_Load(object sender, EventArgs e)
        {

            if ((Session["clientid"] != null) && ((Session["userid"] != null)))
            {
                iClient = Convert.ToInt32(Session["clientid"].ToString());
                iUserId = Convert.ToInt32(Session["userid"].ToString());
            }
            else
            {
                Response.Redirect("login.aspx");
            }
            if (Request.QueryString["g"] != null)
            {
                try
                {
                    string s = Request.QueryString["g"].ToString();
                    bool result = int.TryParse(s, out iGroup);

                    if (result)
                        iGroup = Convert.ToInt32(s);

                    DataTable group = dc.GetSingleGroup(iClient, iGroup);
                    if (group.Rows.Count < 1)
                    {
                        Response.Redirect("logout.aspx");
                    }
                    sGroup = group.Rows[0]["GroupName"].ToString();
                    this.lblMemberHeader.Text = "Member Manger for Group: " + sGroup ;
                }

                catch (System.Threading.ThreadAbortException)
                {
                    //just catch the redirect thread 
                }
                catch (System.Exception ex)
                {
                    string method = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();
                    error.ErrorCatch(this.ToString(), iClient.ToString(), iUserId.ToString(), method, ex.Message, ex.StackTrace);
                    Response.Redirect("/apperror.aspx", false);

                }

            }
            lblMsg.Visible = false;

            if (!IsPostBack)
            {
                try
                {
                    epbGrid.DataSource = dc.GetGroupMembers(iClient, iGroup);
                    epbGrid.DataBind();

                    epbGridMember.DataSource = dc.GetAvailableMembers(iClient, iGroup);
                    epbGridMember.DataBind();

                }
                catch (System.Exception ex)
                {
                    string method = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();
                    error.ErrorCatch(this.ToString(), iClient.ToString(), iUserId.ToString(), method, ex.Message, ex.StackTrace);
                    Response.Redirect("/apperror.aspx", false);

                }
            }

        }


        protected void btnDelMember_Click(object sender, EventArgs e)
        {
            string nr = nrSelectedRowsState.Value.TrimEnd('|');

            try
            {
                if (nr != "")
                {
                    DataTable dt = dc.GetGroupMembers(iClient, iGroup);

                    dt.PrimaryKey = new DataColumn[] { dt.Columns["Id"] };

                    int iOldGroup = dc.MoveGroupMember(iClient, iUserId, iGroup, sGroup, nr);

                    QueueClass q = new QueueClass();
                    q.CreateGroupQueueMsg(iOldGroup.ToString(), iGroup.ToString(), "usermove");

                    epbGrid.DataSource = dc.GetGroupMembers(iClient, iGroup);
                    epbGrid.DataBind();
                }
                else
                {
                    lblMsg.Visible = true;
                    lblMsg.Text = "You must select at least one recipient to remove.";
                }

            }
            catch (System.Exception ex)
            {
                string method = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();
                error.ErrorCatch(this.ToString(), iClient.ToString(), iUserId.ToString(), method, ex.Message, ex.StackTrace);
                Response.Redirect("/apperror.aspx", false);

            }
        }
    }
}