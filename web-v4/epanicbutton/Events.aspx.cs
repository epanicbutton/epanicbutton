﻿using System;
using System.Data;
using System.IO;

namespace epanicbutton
{
    public partial class Events : System.Web.UI.Page
    {
        DataClass dc = new DataClass();
        ErrorHandler error = new ErrorHandler();
        Session session = new Session();
        int iClient, iUserId;

        protected void Page_Load(object sender, EventArgs e)
        {

            if ((Session["clientid"] != null) && ((Session["userid"] != null)))
            {
                iClient = Convert.ToInt32(Session["clientid"].ToString());
                iUserId = Convert.ToInt32(Session["userid"].ToString());
            }
            else
            {
                Response.Redirect("login.aspx");
            }
            try
            {
                DataTable dt = dc.GetEvents(iClient);
                //Change the created time based on end users timezone ( CreatedOn)
                string sTimezone = GetClientTimeZone();
                if (!string.IsNullOrEmpty(sTimezone))
                {
                    UpdateEventToLocalClientTimeZone(dt, sTimezone);
                }

                epbGrid.DataSource = dt;
                epbGrid.DataBind();
            }
            catch (System.Exception ex)
            {
                string method = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();
                error.ErrorCatch(this.ToString(), iClient.ToString(), iUserId.ToString(), method, ex.Message, ex.StackTrace);
                Response.Redirect("/apperror.aspx", false);

            }
        }

        private static void UpdateEventToLocalClientTimeZone(DataTable dt, string sTimezone)
        {
            TimeZoneInfo timeZoneInfo = Utility.GetTimeZoneInfo(sTimezone);
            foreach (DataRow row in dt.Rows)
            {
                row["CreatedOn"] = Utility.ConvertTimeFromUtc(Convert.ToDateTime(row["CreatedOn"]), timeZoneInfo);
            }
        }

        protected void downloadCSV_Click(object sender, EventArgs e)
        {
            String uniqueFileName = MapPath("~") + Path.GetRandomFileName() + ".tmp";

            try
            {
                DataTable dt = dc.GetEvents(iClient);
                string sTimezone = GetClientTimeZone();
                if (!string.IsNullOrEmpty(sTimezone))
                {
                    UpdateEventToLocalClientTimeZone(dt, sTimezone);
                }
                
                String outputFileName = "ePanic_events_" + Session["ClientGuid"] + ".csv";

                using (StreamWriter file = new StreamWriter(uniqueFileName))
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        String line = row["Description"] + "," + row["AdminName"] + "," + row["CreatedOn"];
                        file.WriteLine(line);
                    }

                }

                Response.AddHeader("Content-Disposition", "attachment; filename=" + outputFileName);
                Response.WriteFile(uniqueFileName);
                Response.Flush();

            }
            catch (System.Exception ex)
            {
                string method = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();
                error.ErrorCatch(this.ToString(), iClient.ToString(), iUserId.ToString(), method, ex.Message, ex.StackTrace);
                Response.Redirect("/apperror.aspx", false);

            }

            finally
            {
                File.Delete(uniqueFileName);
            }
        }

    
        private string GetClientTimeZone()
        {
            DataTable dtClient = dc.GetClient(iClient);
            string sTimezone = string.Empty;
            if (dtClient != null && dtClient.Rows != null && dtClient.Rows.Count > 0)
            {
                if (dtClient.Rows[0]["TimeZone"] != null)
                {
                    sTimezone = dtClient.Rows[0]["TimeZone"].ToString();
                }
            }

            return sTimezone;
        }
    }
}