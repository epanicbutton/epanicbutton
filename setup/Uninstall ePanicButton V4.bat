taskkill /f /im ePanicClient.exe
taskkill /f /im ePanicButtonReceiver.exe
taskkill /f /im EpbHostedDispatcherService.exe
 
MsiExec.exe /x{8131769B-9D0D-4C16-B76D-3102B3A43A34} /quiet

net stop "ePanicButton Message Dispatcher"
sc delete "ePanicButtonDispatcher"
 
Reg Delete HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Microsoft\Tracing\ePanicButtonReceiver_RASAPI32 /f
Reg Delete HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Microsoft\Tracing\ePanicButtonReceiver_RASMANCS /f
Reg Delete HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Microsoft\Tracing\ePanicClient_RASAPI32 /f
Reg Delete HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Microsoft\Tracing\ePanicClient_RASMANCS /f
Reg Delete HKEY_LOCAL_MACHINE\SYSTEM\ControlSet002\services\ePanicButtonDispatcher /f
 
cd c:\
RD /S /Q C:\ProgramData\ePanicButton