taskkill /f /im ePanicClient.exe
taskkill /f /im ePanicButtonReceiver.exe
taskkill /f /im EpbHostedDispatcherService.exe

MsiExec.exe /x{3C8FDA30-22C6-4CB4-BFAD-D852231CAA63} /quiet

net stop "ePanicButton Message Dispatcher"
sc delete "ePanicButtonDispatcher"
 
Reg Delete HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Microsoft\Tracing\ePanicButtonReceiver_RASAPI32 /f
Reg Delete HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Microsoft\Tracing\ePanicButtonReceiver_RASMANCS /f
Reg Delete HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Microsoft\Tracing\ePanicClient_RASAPI32 /f
Reg Delete HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Microsoft\Tracing\ePanicClient_RASMANCS /f
Reg Delete HKEY_LOCAL_MACHINE\SYSTEM\ControlSet002\services\ePanicButtonDispatcher /f

cd c:\
RD /S /Q C:\ProgramData\ePanicButton