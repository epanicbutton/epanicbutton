taskkill /f /im ePanicClient.exe
taskkill /f /im ePanicButtonReceiver.exe
taskkill /f /im EpbHostedDispatcherService.exe
 
MsiExec.exe /x{805A7F32-001D-4C59-BCD8-63BCCD251E63} /quiet

net stop "ePanicButton Message Dispatcher"
sc delete "ePanicButtonDispatcher"
 
Reg Delete HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Microsoft\Tracing\ePanicButtonReceiver_RASAPI32 /f
Reg Delete HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Microsoft\Tracing\ePanicButtonReceiver_RASMANCS /f
Reg Delete HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Microsoft\Tracing\ePanicClient_RASAPI32 /f
Reg Delete HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Microsoft\Tracing\ePanicClient_RASMANCS /f
Reg Delete HKEY_LOCAL_MACHINE\SYSTEM\ControlSet002\services\ePanicButtonDispatcher /f
reg delete "HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Microsoft\Tracing\EpbHostedDispatcherService_RASAPI32" /f
reg delete "HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Microsoft\Tracing\EpbHostedDispatcherService_RASMANCS" /f
 
cd c:\
RD /S /Q C:\ProgramData\ePanicButton