############
EPANICBUTTON
############
- Admin Portal
 - https://prod.epanicbutton.com/login.aspx
 - http://qa.epanicbutton.com/login.aspx

- Service Bus Portal
	- portal.azure.com

- RDP
	- Credentials on lastpass
	- Production Sever
		- eq.epanicbutton.com:16548
	- QA Server
		- qa.epanicbutton.com:16548

- Contacts
	- Johnny Lee: jlee@peaceatwork.org
	- Andrew Wing: andrew.wing@wingweb.com

#################
INSTALLER PROJECT
#################
- Visual Studio 2010 Installer Project
- Pre-build event signs each individual .exe
- Post-build event renames the built .msi to "ePanic_.msi" and signs the .msi
- Client Guid added after "_" in the msi filename before installing.
- Custom 'Commit' Actions to get directory/msi filename the installer is run from and to optionally take in "Sharepath" command line arguement to allow installs from UNC shares
- Deployment
	- Build solution in VS 2010
	- Add '...\epanicbuttonSetup\epbInstaller\Release\ePanic_.msi' to 'ePanicInstaller.zip'
	- Add local .zip to ePanicInstaller.zip in ePanic folder on Google Drive
		- Inside the Drive right click the zip, choose manage versions, choose upload new version
	- The link to the .zip should stay the same between versions but if it is needed: Right click .zip inside Drive, choose 'Get Shareable Link' then send the link to Johnny

####################
EPANICBUTTON PROJECT
####################
- Running in Visual Studio 2012
	- Build output of all projects should go to shared folder (ex. C:\Temp\Build)
	- Solution must be set to multi-project start
 		- EpbHostedDispatcherService, ePanicButtonReceiver, & ePanicClient 
 	- Before running, correct config files must be placed into shared build directory
 		- EpbSharedLib.dll.config
 		- EpbHostedDispatcherService.exe.config
 		- ePanicClient.exe.config
 		- ePanicButtonReceiver.exe.config
 		- UserInfo.xml
- Deploying
	- Build .msi with installer project

###########
WEB PROJECT
###########
- MVC project for admin portal interface
- "web-v4" project in ePanic Suite 3.0 in TFS
- Deploy first to QA server for testing

###########
WCF PROJECT
###########
- Web service to communicate user/button changes made on admin portal to desktop aplication as well as handle email and text notifications
- "wcf-v4" project in ePanic Suite 3.0 in TFS
- Deploy first to QA server for testing

