﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace EPanicButtonReceiver
{
    [RunInstaller(true)]
    public partial class RecieverInstaller : System.Configuration.Install.Installer
    {
        public RecieverInstaller()
        {
            InitializeComponent();
        }

        public override void Commit(IDictionary savedState)
        {
            string appRoot = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            string exePath = Path.Combine(appRoot, "ePanicButtonReceiver.exe");

            base.Commit(savedState);
            Process.Start(exePath);
        }

        public override void Uninstall(IDictionary savedState)
        {
            string appRoot = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            string[] files =
                {
                    @"ePanicButtonReceiver.exe.config",
                    @"ePanicClient.exe.config",
                    @"EpbHostedDispatcherService.exe.config",
                    @"UserInfo.xml"
                };

            foreach (string file in files)
            {
                File.Delete(Path.Combine(appRoot, file));
            }

            base.Uninstall(savedState);
        }
    }
}
