﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using EPanicButtonReceiver.Views;
using EpbSharedLib.Enums;
using EpbSharedLib.SharedClasses;
using EventAggregator;
using FirstFloor.ModernUI.Presentation;

namespace EPanicButtonReceiver.ViewModels
{
    internal class MainWindowViewModel : NotifyPropertyChanged
    {
        private string mostRecentMessageGuid = string.Empty;
        private DateTime mostRecentDateTime;

        public MainWindowViewModel()
        {
            this.Subscribe(Constants.AlertNotification, this.HandleShowAlertMessage);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Opens Main Window
        /// </summary>
        public void ShowMainWindow()
        {
            if (Application.Current.MainWindow == null)
            {
                Application.Current.MainWindow = new MainWindow();
            } 
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private void HandleShowAlertMessage(EventMessage eventMessage)
        {
            this.ShowMainWindow();

            TimeSpan elaspedTime = DateTime.Now - this.mostRecentDateTime;

            var mnWindow = (MainWindow)Application.Current.MainWindow;

            mnWindow.Show();

            EpbSharedLib.Entities.Alert msg = eventMessage.GetValue<EpbSharedLib.Entities.Alert>("Dispatch");

            if (msg.AlertMsgSource == MessageSource.RaiseAlertMessage ||
                msg.AlertMsgSource == MessageSource.DataGrid)
            {
                if (msg.AlertMsgSource == MessageSource.DataGrid)
                {
                    mnWindow.ContentSource = new Uri("/Pages/AlertView.xaml", UriKind.Relative);
                    return;
                }

                this.mostRecentDateTime = DateTime.Now;

                this.Send(Constants.AlertNotificationUpdate,
                    new KeyValuePair<string, object>(
                        "Dispatch", ((App)Application.Current).AlertMessage));

                if (elaspedTime.TotalSeconds > 5)
                {
                    mnWindow.ContentSource = new Uri("/Pages/AlertView.xaml", UriKind.Relative);
                }
                else
                {
                    this.Send(Constants.UpdateDataGridRequest);
                }  
            }
             
        }
    }
}
