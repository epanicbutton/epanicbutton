﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Xml.Serialization;
using EPanicButtonReceiver.Models;
using EpbSharedLib.SharedClasses;
using EpbSharedLib.Utilities;
using EventAggregator;
using FirstFloor.ModernUI.Presentation;
using EpbSharedLib.Enums;

namespace EPanicButtonReceiver.ViewModels
{
    internal class AlertsDataGridViewModel : NotifyPropertyChanged
    {
        private readonly DataGrid mainDg;
        private static readonly CacheHelper objCache = new CacheHelper();

        public AlertsDataGridViewModel(DataGrid alertsDg)
        {
            mainDg = alertsDg;
            ObservableCollection<Models.Alert> alertsData = this.GetData();

            alertsDg.MouseDoubleClick += this.AlertsDgMouseDoubleClick;
            alertsDg.Loaded += this.AlertsDgLoaded;

            //Bind the DataGrid to the Alert data
            alertsDg.DataContext = alertsData;

            this.Subscribe(Constants.UpdateDataGridRequest, HandleUpdateDataGridRequest);
        }

        private void HandleUpdateDataGridRequest(EventMessage eventMessage)
        {
            ObservableCollection<Models.Alert> alertsData = this.GetData();
            mainDg.DataContext = alertsData;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Gets the Update Command for the ViewModel
        /// </summary>
        //   public ICommand GridRowDblClickCommand { get; private set; }
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private void AlertsDgLoaded(object sender, RoutedEventArgs e)
        {
            this.mainDg.ColumnFromDisplayIndex(1).SortDirection =
                ListSortDirection.Descending;
        }

        private ObservableCollection<Models.Alert> GetData()
        {
            string alertsListDataFile = AlertsListHelpers.GetAlertsListFilePath();

            EpbSharedLib.Entities.Alert[] objRes = null;
            var alerts = new ObservableCollection<Models.Alert>();

            var cachedObjRes = objCache.GetCachedItem("ALERTS_LIST") as EpbSharedLib.Entities.Alert[];

            if (File.Exists(alertsListDataFile) || cachedObjRes != null)
            {
                if (cachedObjRes == null)
                {
                    XmlSerializer deserializer = new XmlSerializer(typeof(EpbSharedLib.Entities.Alert[]));

                    using (StreamReader reader = new StreamReader(alertsListDataFile))
                    {
                        objRes = (EpbSharedLib.Entities.Alert[])deserializer.Deserialize(reader);
                        reader.Close();
                    }

                    objCache.AddToCache("ALERTS_LIST", objRes, EpbSharedLib.SharedClasses.CachePriority.Default);
                }
                else
                {
                    objRes = cachedObjRes;
                }

                int acount = 0;

                foreach (var a in objRes.OrderBy(s => s.AlertDateTime))
                {
                    alerts.Insert(0, SetAlertProperties(a));
                }
            }

            return alerts;
        }

        private Models.Alert SetAlertProperties(EpbSharedLib.Entities.Alert a)
        {
            Models.Alert insertedAlert = new Models.Alert();

            insertedAlert.MessageGuid = a.MessageGuid;
            insertedAlert.AlertTitle = a.AlertTitle;
            insertedAlert.AlertDescription = a.AlertDescription;
            insertedAlert.AlertMemo = a.AlertMemo;
            insertedAlert.AlertId = a.AlertId;
            insertedAlert.AlertDateTime = a.AlertDateTime;
            insertedAlert.SenderName = a.SenderName;
            insertedAlert.SenderMachineName = a.SenderMachineName;
            insertedAlert.SenderDepartment = a.SenderDepartment;
            insertedAlert.SenderLocation = a.SenderLocation;
            insertedAlert.Confirmed = a.Confirmed;
            insertedAlert.SenderTelephone = a.SenderTelephone;
            insertedAlert.SenderEmail = a.SenderEmail;
            insertedAlert.SeverityLevel = a.SeverityLevel;
            insertedAlert.MessageGuid = a.MessageGuid;

            return insertedAlert;
        }

        private void AlertsDgMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var dg = (DataGrid)sender;
            var ca = dg.CurrentItem as Models.Alert;

            //-- The grid was clicked 
            //-- but nothing is selected
            if (ca == null)
            {
                return;
            }

            var editAlert = new EpbSharedLib.Entities.Alert();

            editAlert.AlertDateTime = ca.AlertDateTime;
            editAlert.AlertDescription = ca.AlertDescription;
            editAlert.AlertMemo = ca.AlertMemo;
            editAlert.AlertTitle = ca.AlertTitle;
            editAlert.SenderName = ca.SenderName;
            editAlert.SenderEmail = ca.SenderEmail;
            editAlert.SenderId = ca.SenderId;
            editAlert.SenderLocation = ca.SenderLocation;
            editAlert.SenderDepartment = ca.SenderDepartment;
            editAlert.SenderMachineName = ca.SenderMachineName;
            editAlert.SenderTelephone = ca.SenderTelephone;
            editAlert.SeverityLevel = ca.SeverityLevel;
            editAlert.MessageGuid = ca.MessageGuid;
            editAlert.AlertMemo = ca.AlertMemo;
            editAlert.AlertGuid = ca.AlertGuid;
            editAlert.Confirmed = ca.Confirmed;
            editAlert.AlertMsgSource = MessageSource.DataGrid;

            ((App)Application.Current).AlertAudioEnabled = false;
            ((App)Application.Current).AlertMessage = editAlert;

            this.Send(Constants.AlertNotification,
                new KeyValuePair<string, object>(
                    "Dispatch", ((App)Application.Current).AlertMessage));
        }
    }
}