﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Timers;
using System.Windows;
using System.Windows.Input;
using EPanicButtonReceiver.Commands;
using EPanicButtonReceiver.Views;
using EasyNetQ;
using EpbSharedLib.DispatcherSrvc;
using EpbSharedLib.Entities;
using EpbSharedLib.Enums;
using EpbSharedLib.MQ;
using EpbSharedLib.SharedClasses;
using EpbSharedLib.Utilities;
using EventAggregator;
using FirstFloor.ModernUI.Presentation;
using Hardcodet.Wpf.TaskbarNotification;
using FirstFloor.ModernUI.Windows.Controls;
using Ninject;

namespace EPanicButtonReceiver.ViewModels
{
    internal class NotifyIconViewModel : NotifyPropertyChanged
    {
        private readonly ISettings objSettings;
        public IAdvancedBus MessageBus;
        public IMQPublisher publisher ;
        private string buttonIDs = string.Empty;
        private readonly EpbSharedLib.SharedClasses.IDispatcherProxy objProxy;
        private MqConfig MqConfig;
        private readonly App theApp;
        public bool InitialStart = true;
        public bool Stopping = false;

        private IMQConsumer consumer = null;


        public void Init(){
            IocBootstrapper bootstrapper = new IocBootstrapper();
            bootstrapper.Initialize();
            publisher = bootstrapper.Container.Get<IMQPublisher>();
            consumer = bootstrapper.Container.Get<IMQConsumer>();
        }
        

        public NotifyIconViewModel(ISettings settings,
            IDispatcherProxy dispatcherProxy)
        {
            Init();
            ELogger.LogMethodStart();

            this.objSettings = settings;
            this.objProxy = dispatcherProxy;
            this.theApp = ((App)Application.Current);

            this.ShowMainWindowCommand = new NotifyIconCommandProcessor(this, SharedCommands.TrayIcon.ShowMainWindow);
            this.ExitAppCommand = new NotifyIconCommandProcessor(this, SharedCommands.TrayIcon.Exit);

            //ELogger.Log("Getting MQ configuration.");
            GetMqSettings();

            this.Subscribe(Constants.ConfirmReceipt, this.HandleConfirmReceipt);

            ELogger.Log("Processing User info.");
            ProcessUserInfo();

            ELogger.LogMethodEnd();
        }

        private void GetMqSettings()
        {
            int retryCount = 0;
            int retryLimit = 6;
            int retryPauseInMilli = 6000;

            bool retry = true;

            while (retry)
            {
                if (retryCount > 0)
                {
                    TaskbarIcon tb = this.theApp.NotifyIcon;

                    tb.CloseBalloon();
                    tb.ShowBalloonTip("Loading", "Loading settings (" + retryCount + ")...", tb.Icon);

                    ELogger.Log(string.Format("{0} - Retrying GetUserInfo in {1} seconds.", retryCount, retryPauseInMilli));
                    Thread.Sleep(retryPauseInMilli);
                }

                retry = false;

                try
                {
                    this.MqConfig = this.objProxy.GetQueueConfiguration();

                    //TOD: Move to shared lib next full release.
                    //Duplicated code to avoid shared library update.
                    //if (string.IsNullOrEmpty(this.MqConfig.HostName) ||
                    //    string.IsNullOrEmpty(this.MqConfig.VirtualHost) ||
                    //    string.IsNullOrEmpty(this.MqConfig.ResponseExchangeName) ||
                    //    string.IsNullOrEmpty(this.MqConfig.RequestExchangeName))
                    //{
                    //    string mqSettings = string.Format("\nHostName: {0}, vHost: {1}, ResXName: {2}, ReqXName: {3}",
                    //        this.MqConfig.HostName,
                    //        this.MqConfig.VirtualHost,
                    //        this.MqConfig.ResponseExchangeName,
                    //        this.MqConfig.RequestExchangeName);

                    //    throw new Exception("Unable to load MQ settings from local service." + mqSettings);
                    //}
                }
                catch (Exception ex)
                {
                    ELogger.Log(ex.Message);
                    retryCount++;

                    if (retryCount < retryLimit)
                    {
                        retry = true;
                    }
                    else
                    {
                        ModernDialog.ShowMessage("Unable to load MQ settings. Please make sure the local Dispatcher\nservice is running and restart the ePanicButton Receiver.", "Application Error", MessageBoxButton.OK);
                        ExitApplication();
                    }
                }

            }
        }


        internal void OnProfileChange()
        {
            if (objSettings.VerboseLoggingOn)
            {
                ELogger.Log("\r\n\r\nUserInfo has been updated. Reprocessing started...");
            }

            if (objSettings.VerboseLoggingOn)
            {
                ELogger.Log("\r\n\r\nStopping consumer thread...");
            }
            this.Stop();

            System.Timers.Timer t = new System.Timers.Timer();

            t.Interval = 500;
            t.Elapsed += new System.Timers.ElapsedEventHandler(this.ProcessAfterProfileMods);
            t.Start();
        }

        private void ProcessAfterProfileMods(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                ((System.Timers.Timer)sender).Stop();
                ((System.Timers.Timer)sender).Dispose();

                Application.Current.Dispatcher.Invoke(new System.Action(() =>
                {
                    //CHANGE: !this.consumer.IsConnected
                    if (this.consumer.IsConnected)
                    {
                        ProcessUserInfo();
                    }
                    else
                    {
                        StartConsuming();
                    }

                }));
            }
            catch (Exception ex)
            {
                ELogger.Log("ProcessAfterProfileMods: " + ex.Message);
            }
        }

        private void HandleConfirmReceipt(EventMessage eventMessage)
        {
            Models.Alert msg = eventMessage.GetValue<Models.Alert>("Dispatch");
            try
            {
                this.SendReceiptConfirmation(msg, this.publisher);
            }
            catch (Exception ex)
            {
                if (this.objSettings.VerboseLoggingOn)
                {
                    ELogger.Log(ex.Message);
                }
            }
        }

        private void SendReceiptConfirmation(Models.Alert msg, IMQPublisher mQPublisher)
        {
            var alertConfirm = new EpbSharedLib.Entities.AlertConfirmation();
            string strippedGuid = msg.SenderMachineGuid.Replace("-", "");
            alertConfirm.MessageGuid = msg.MessageGuid;

            UserData userInfo = this.GetUserInfo();
            if (userInfo != null)
            {
                alertConfirm.ConfirmSenderId = userInfo.Id;
            }
            try
            {
                mQPublisher.RoutingKey = msg.SenderMachineGuid;
                var message = EpbSharedLib.Utilities.Serialization.SerializeObject(alertConfirm);
                mQPublisher.Send(message, strippedGuid);

                if (this.objSettings.VerboseLoggingOn)
                {
                    ELogger.Log(string.Format("Sent confirm message '{0}'at {1}.", msg.MessageGuid, DateTime.Now));
                }
            }
            catch (Exception)
            {
            }

            return;
        }

        private void MessageBus_Connected()
        {
            ELogger.Log("Connected to MQ Server...");

            this.InitialStart = false;
            this.publisher.CreateExchangesAndQueues();

            Application.Current.Dispatcher.Invoke(new System.Action(() =>
            {
                this.Send(Constants.MqConnectionDetected);
            }));

            System.Timers.Timer t = new System.Timers.Timer();
            t.Interval = 2000;
            t.Elapsed += new ElapsedEventHandler(this.DoStartConsuming);
            t.Start();
        }

        private void DoStartConsuming(object sender, ElapsedEventArgs e)
        {
            ((System.Timers.Timer)sender).Stop();
            ((System.Timers.Timer)sender).Dispose();

            TaskbarIcon tb = this.theApp.NotifyIcon;

            if (!EpbSharedLib.Utilities.Helpers.IsProcessOpen("ePanicClient"))
            {
                tb.CloseBalloon();
                tb.ShowBalloonTip("Connected", "Connected to ePanicButton server.", tb.Icon);
            }

            StartConsuming();
            
        }

        private void MessageBus_Disconnected()
        {
            ELogger.Log("Connection to MQ Server lost...");

            TaskbarIcon tb = this.theApp.NotifyIcon;
            this.Stop();

            if (!EpbSharedLib.Utilities.Helpers.IsProcessOpen("ePanicClient"))
            {
                tb.CloseBalloon();
                tb.ShowBalloonTip("Disconnected", "Unable to connect to ePanicButton server!", tb.Icon);
            }

            Application.Current.Dispatcher.Invoke(new System.Action(() =>
            {
                this.Send(Constants.MqConnectionLost);
            }));
        }

        private UserData GetUserInfo()
        {
            UserData userData = null;
            try
            {
                var objDispProxy = new EpbSharedLib.SharedClasses.DispatcherProxy();
                userData = objDispProxy.GetUserData(false);
            }
            catch (Exception ex)
            {
                if (this.objSettings.VerboseLoggingOn)
                {
                    ELogger.Log(string.Format("Unable to read user data from dispatcher.\r\n{0}", ex.Message));
                }
            }

            return userData;
        }

        public void StartConsuming()
        {
            this.buttonIDs = this.GetButtonIds(); 

            //if (string.IsNullOrEmpty(buttonIDs))
            //{
            //    Application.Current.Dispatcher.Invoke(new System.Action(() =>
            //    {
            //        this.ShowMainWindow();
            //        Application.Current.MainWindow.Hide();
            //        ModernDialog.ShowMessage("Unable to load buttons. Please make sure the local Dispatcher\nservice is running and restart the ePanicButton receiver.", "Application Error", MessageBoxButton.OK);

            //        ExitApplication();
            //    }));

            //    return;
            //}

            //if (string.IsNullOrEmpty(this.buttonIDs))
            //{
            //    this.buttonIDs = objSettings.MachineGuid;
            //}
            //else
            //{
            //    this.buttonIDs += "," + objSettings.MachineGuid;
            //}

            if (objSettings.VerboseLoggingOn)
            {
                ELogger.Log("\r\n\r\nStarting consumer thread...\nUsing button Id's: "+ buttonIDs);
            }


            this.Consume();
            return;
        }

        private void Stop()
        {
            try
            {
                if (consumer != null)
                {
                    consumer.Dispose();
                }

                Stopping = true;
            }
            catch (Exception ex)
            {
                ELogger.Log(ex.Message);

            }
        }

        private string GetButtonIds()
        {
            var usrData = this.objProxy.GetUserDataNoCache();
            int retryCount = 0;

            if (usrData != null && !string.IsNullOrEmpty(usrData.ButtonIDs))
            {
                return usrData.ButtonIDs;
            }

            string buttonIds = string.Empty;

            //-----------------------------------------------------------
            // Continue to try until thread ends or userDate is not null
            //-----------------------------------------------------------
            while (usrData == null && !Stopping)
            {
                retryCount++;
                if (this.objSettings.VerboseLoggingOn)
                {
                    ELogger.Log(String.Format("[{0}] Waiting for network connection...", retryCount));
                }

                Thread.Sleep(3000);
                usrData = this.objProxy.GetUserDataNoCache();

                if (((App)Application.Current) == null || ((App)Application.Current).ShuttingDown == true)
                {
                    this.Stop();
                    return string.Empty;
                }

                // Read from Cache after server reads failed.
                if (retryCount == 10)
                {
                    usrData = this.objProxy.GetUserData(false);
                    retryCount = 0;

                    if (usrData == null || string.IsNullOrEmpty(usrData.ButtonIDs))
                    {
                        break;
                    }

                    buttonIds = usrData.ButtonIDs;
                    break;
                }
            }

        
            return buttonIds;
        }

        private void Consume()
        {

            if (!this.publisher.IsConnected)
            {
                ELogger.Log("Not connected to MQ. Exiting consumer.");
                return;
            }

            try
            {
                IList<string> buttonList = !string.IsNullOrEmpty(this.buttonIDs) ? this.buttonIDs.Split(',') : null;

                if (buttonList != null && buttonList.Any())
                {
                    //System.Action<EpbSharedLib.Entities.Alert> 
                    var  receiveAction = new Action<object>((msg) =>
                        {
                        var obj = msg as Alert;
                        if (obj.MessageGuid.Equals("update_receiver"))
                        {
                            OnProfileChange();
                        }
                        else
                        {
                            //-- Reset the Alert Audio
                            ((App)Application.Current).AlertAudioEnabled = true;

                            string curPath = string.Format("{0}\\", Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location));
                            string alertsListDataFile = string.Format("{0}AlertsList.xml", curPath);

                            obj.AlertMsgSource = MessageSource.RaiseAlertMessage;
                            this.Send(Constants.AlertNotification,
                                new KeyValuePair<string, object>(
                                    "Dispatch", obj));

                            AlertsListHelpers.UpdateAlertsListXml(alertsListDataFile, obj);
                        }
                    });

                 
                    //ELogger.LogFormat("Listening for the following button IDs: {0}", string.Join(",", buttonList));

                }
            }
            catch (Exception ex)
            {
                ELogger.Log(ex);
            }
            finally
            {
            }

        }

        private void AlertHandler(object sender)
        {
            var obj = sender as Alert;
            if (obj.MessageGuid.Equals("update_receiver"))
            {
                OnProfileChange();
            }
            else
            {
                //-- Reset the Alert Audio
                ((App)Application.Current).AlertAudioEnabled = true;

                string curPath = string.Format("{0}\\", Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location));
                string alertsListDataFile = string.Format("{0}AlertsList.xml", curPath);

                obj.AlertMsgSource = MessageSource.RaiseAlertMessage;
                this.Send(Constants.AlertNotification,
                    new KeyValuePair<string, object>(
                        "Dispatch", obj));

                AlertsListHelpers.UpdateAlertsListXml(alertsListDataFile, obj);
                new AlertViewViewModel(obj);
            }
        }

        private void ProcessUserInfo()
        {
            try
            {
                this.MqConfig = this.objProxy.GetQueueConfiguration();
            }
            catch (Exception ex)
            {
                ELogger.Log(ex);
            }


            System.Timers.Timer t = new System.Timers.Timer();
            t.Interval = 1000;
            t.Elapsed += new ElapsedEventHandler(this.DoProcessUserInfo);
            t.Start();
        }

        private void DoProcessUserInfo(object sender, ElapsedEventArgs e)
        {
            ((System.Timers.Timer)sender).Stop();
            ((System.Timers.Timer)sender).Dispose();

            this.StartConsuming();
        }

        #region ICommands

        /// <summary>
        /// Exit Application
        /// </summary>
        public ICommand ExitAppCommand { get; private set; }

        /// <summary>
        /// Show Main Window Command
        /// </summary>
        public ICommand ShowMainWindowCommand { get; private set; }

        #endregion

        /// <summary>
        /// Opens Main Window
        /// </summary>
        public void ShowMainWindow()
        {
            if (Application.Current.MainWindow == null)
            {
                Application.Current.MainWindow = new MainWindow();

            }

            Application.Current.MainWindow.Show();

            //Bring window to front
            Application.Current.MainWindow.Topmost = true;

        }

        /// <summary>
        /// Exit ePanic Receiver
        /// </summary>
        public void ExitApplication()
        {
            // This message should be handled by the various 
            // processes and threads. Clean up tasks should 
            // be initiated.
            // Give other tasks a chance to finish up 
            this.Stop();

            try
            {
                if (this.MessageBus != null)
                {
                    this.MessageBus.Dispose();
                }
            }
            catch
            {
            }

            ((App)Application.Current).ShuttingDown = true;
            this.Send(Constants.ApplicationShutdown);
            GC.SuppressFinalize(this);
        }

        public EventHandler MqConnected { get; set; }
    }
}