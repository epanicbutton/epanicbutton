﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Animation;
using System.Windows.Threading;
using System.Xml;
using EPanicButtonReceiver.Classes;
using EPanicButtonReceiver.Commands;
using EPanicButtonReceiver.Views;
using EpbSharedLib.Entities;
using EpbSharedLib.Enums;
using EpbSharedLib.SharedClasses;
using EventAggregator;
using FirstFloor.ModernUI.Presentation;
using FirstFloor.ModernUI.Windows;
using FirstFloor.ModernUI.Windows.Controls;
using NAudio.Wave;
using EpbSharedLib.Utilities;

namespace EPanicButtonReceiver.ViewModels
{
    internal class AlertViewViewModel : NotifyPropertyChanged, IContent
    {
        private static WaveOut waveOut = null;
        private static DispatcherTimer stopAudioTimer = null;
        private static readonly CacheHelper objCache = new CacheHelper();

        public AlertViewViewModel(EpbSharedLib.Entities.Alert alertMessage)
        {
            if (Application.Current.MainWindow == null)
            {
                Application.Current.MainWindow = new MainWindow();
            }

            this.EnableAlertSound = ((App)Application.Current).AlertAudioEnabled;

            this.ShowAlert(alertMessage);

            var mnWindow = (MainWindow)Application.Current.MainWindow;

            mnWindow.Closed += this.MnWindowClosed;
            mnWindow.RequestBringIntoView += this.MnWindowRequestBringIntoView;


            this.UpdateCommand = new AlertViewCommandProcessor(this, SharedCommands.AlertView.UpdateMemo);
            this.ConfirmReceiptCommand = new AlertViewCommandProcessor(this, SharedCommands.AlertView.ConfirmReceipt);

            this.Subscribe(Constants.ConfirmReceiptSuccess, this.HandleConfirmReceiptSuccess);
            this.Subscribe(Constants.AlertNotificationUpdate, this.HandleShowAlertMessage);

        }

        private void HandleShowAlertMessage(EventMessage eventMessage)
        {
            var mnWindow = (MainWindow)Application.Current.MainWindow;
            EpbSharedLib.Entities.Alert msg = eventMessage.GetValue<EpbSharedLib.Entities.Alert>("Dispatch");

            if (msg.AlertMsgSource == MessageSource.RaiseAlertMessage)
            {
                this.DoCleanup();
                Application.Current.MainWindow.Activate();
                mnWindow.ContentSource = new Uri("/XAMLResources/AlertsDataGrid.xaml", UriKind.Relative);
            }
        }

        private void HandleConfirmReceiptSuccess(EventMessage eventMessage)
        {
            var msg = eventMessage.GetValue<AlertConfirmation>("Dispatch");

            this.ObjAlert.Confirmed = true;
            StartUpdateConfirm_Task(msg.MessageGuid);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public EPanicButtonReceiver.Models.Alert ObjAlert { get; set; }

        public bool EnableAlertSound { get; set; }

        /// <summary>
        /// Gets the Update Command for the ViewModel
        /// </summary>
        public ICommand UpdateCommand { get; private set; }

        /// <summary>
        /// Gets the Confirm Receipt Command for the ViewModel
        /// </summary>
        public ICommand ConfirmReceiptCommand { get; private set; }

        public async Task<int> StartUpdateConfirm_Task(string msg)
        {
            await Task.Run(() => this.UpdateConfirmed(msg));

            return 1;
        }

        public void UpdateMemo()
        {
            string alertsListFilePath = AlertsListHelpers.GetAlertsListFilePath();
            XmlDocument xml = new XmlDocument();

            try
            {


                xml.Load(alertsListFilePath);

                var element = xml.SelectSingleNode(string.Format("//Alert/AlertMemo[../MessageGuid='{0}']", this.ObjAlert.MessageGuid));

                if (element != null)
                {
                    element.InnerText = this.ObjAlert.AlertMemo;

                    xml.Save(alertsListFilePath);

                    objCache.RemoveCachedItem("ALERTS_LIST");

                    var mnWindow = (MainWindow)Application.Current.MainWindow;

                    Application.Current.Dispatcher.Invoke((Action)delegate()
                    {
                        mnWindow.ContentSource = new Uri("/XAMLResources/AlertsDataGrid.xaml", UriKind.Relative);
                        this.DoCleanup();
                    });

                    return;
                }
            }
            catch (Exception error)
            {
                ModernDialog.ShowMessage(error.Message, FirstFloor.ModernUI.Resources.NavigationFailed, MessageBoxButton.OK);
            }
        }
        public void UpdateConfirmed(string messageGuid)
        {
            string alertsListFilePath = AlertsListHelpers.GetAlertsListFilePath();

            try
            {
                XmlDocument xml = new XmlDocument();

                xml.Load(alertsListFilePath);

                var element = xml.SelectSingleNode(string.Format("//Alert/Confirmed[../MessageGuid='{0}']", messageGuid));

                if (element != null)
                {
                    element.InnerText = "true";

                    xml.Save(alertsListFilePath);
                    objCache.RemoveCachedItem("ALERTS_LIST");

                    //var mnWindow = (MainWindow)Application.Current.MainWindow;

                    Application.Current.Dispatcher.Invoke((Action)delegate()
                    {
                        //mnWindow.ContentSource = new Uri("/XAMLResources/AlertsDataGrid.xaml", UriKind.Relative);
                        this.DoCleanup();
                    });

                    return;
                }
            }
            catch (Exception error)
            {
                ModernDialog.ShowMessage(error.Message, FirstFloor.ModernUI.Resources.NavigationFailed, MessageBoxButton.OK);
            }

            return;
        }
        public void OnFragmentNavigation(FirstFloor.ModernUI.Windows.Navigation.FragmentNavigationEventArgs e)
        {
        }

        public void OnNavigatedFrom(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
        }

        public void OnNavigatedTo(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
        }

        public void OnNavigatingFrom(FirstFloor.ModernUI.Windows.Navigation.NavigatingCancelEventArgs e)
        {
            // ask user if navigating away is ok
            if (ModernDialog.ShowMessage("Navigate away?", "navigate",
                System.Windows.MessageBoxButton.YesNo) == MessageBoxResult.No)
            {
                e.Cancel = true;
            }
        }

        internal void ConfirmReceipt()
        {
            if (this.ObjAlert == null) { return; }

            this.ObjAlert.Confirmed = true;

            this.Send(Constants.ConfirmReceipt,
                new KeyValuePair<string, object>(
                    "Dispatch", this.ObjAlert));
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private static void OnTimedEvent(object sender, EventArgs e)
        {
            StopAlertAudio();
        }

        private void MnWindowRequestBringIntoView(object sender, RequestBringIntoViewEventArgs e)
        {
            Application.Current.Dispatcher.Invoke((Action)delegate()
            {
                Application.Current.MainWindow.Activate();
            });

            this.DoCleanup();
        }

        private void MnWindowClosed(object sender, EventArgs e)
        {
            this.DoCleanup();
        }

        private void DoCleanup()
        {


        }

        /// <summary>
        /// Populate the Alert object and start audio loop if enabled.
        /// </summary>
        /// <param name="msg">Object of type EpbSharedLib.Entities.Alert</param>
        /// <seealso cref="EpbSharedLib.Entities.Alert"/>
        private void ShowAlert(EpbSharedLib.Entities.Alert msg)
        {
            if (msg.MessageGuid == null)
            {
                return;
            }

            if (this.ObjAlert == null)
            {
                this.ObjAlert = new Models.Alert();
            }

            this.ObjAlert.AlertTitle = string.Empty;
            this.ObjAlert.Confirmed = msg.Confirmed;
            this.ObjAlert.AlertDateTime = msg.AlertDateTime;
            this.ObjAlert.AlertTitle = msg.AlertTitle;
            this.ObjAlert.AlertDescription = msg.AlertDescription;
            this.ObjAlert.SenderDepartment = msg.SenderDepartment;
            this.ObjAlert.SenderLocation = msg.SenderLocation;
            this.ObjAlert.SeverityLevel = msg.SeverityLevel.ToUpper();
            this.ObjAlert.SenderName = msg.SenderName;
            this.ObjAlert.SenderMachineName = msg.SenderMachineName;
            this.ObjAlert.SenderMachineGuid = msg.SenderMachineGuid;
            this.ObjAlert.SenderId = msg.SenderId;
            this.ObjAlert.OriginalMemo = msg.AlertMemo;
            this.ObjAlert.AlertMemo = msg.AlertMemo;
            this.ObjAlert.MessageGuid = msg.MessageGuid;

            SetSeverityLevelColor(msg);

            //Bring window to front
            Application.Current.MainWindow.Activate();

            if (!string.IsNullOrEmpty(this.ObjAlert.SeverityLevel) && this.EnableAlertSound)
            {
                ((App)Application.Current).AlertAudioEnabled = false;

                if (!msg.Mute)
                {
                    this.PlayAlertAudio(this.ObjAlert.SeverityLevel);
                }
            }

            Application.Current.MainWindow.Topmost = true;

        }

        private void SetSeverityLevelColor(Alert msg)
        {
            string sevLevel = msg.SeverityLevel.ToLower().Trim();

            switch (sevLevel)
            {
                case "low":
                    this.ObjAlert.SeverityLevelColor = "#00a138";
                    break;
                case "medium":
                    this.ObjAlert.SeverityLevelColor = "#ffbd00";
                    break;
                case "high":
                    this.ObjAlert.SeverityLevelColor = "#ff000f";
                    break;
            }
        }

        /// <summary>
        /// Play severity level audio using NAudio library.
        /// </summary>
        /// <param name="sevLevel">"Low", "Medium" or "High" Severity Level</param>
        public void PlayAlertAudio(string sevLevel)
        {
            string waveFile = string.Empty;

            sevLevel = sevLevel.ToLower().Trim();
            string appPath = AppDomain.CurrentDomain.BaseDirectory;

            switch (sevLevel)
            {
                case "low":
                    waveFile = string.Format("{0}Media\\AlertLow.wav", appPath);
                    break;
                case "medium":
                    waveFile = string.Format("{0}Media\\AlertNormal.wav", appPath);
                    break;
                case "high":
                    waveFile = string.Format("{0}Media\\AlertHigh.wav", appPath);
                    break;
            }

            try
            {
                if (!string.IsNullOrEmpty(waveFile))
                {
                    WaveFileReader reader = new WaveFileReader(waveFile);
                    LoopStream loop = new LoopStream(reader);
                    waveOut = new WaveOut();
                    waveOut.Init(loop);

                    // Start timer that will stop audio after 2 minutes.
                    stopAudioTimer = new System.Windows.Threading.DispatcherTimer();
                    stopAudioTimer.Tick += new EventHandler(OnTimedEvent);
                    stopAudioTimer.Interval = new TimeSpan(0, 2, 0);
                    stopAudioTimer.Start();

                    waveOut.Play();
                }
            }
            catch (Exception ex)
            {
                ELogger.Log(string.Format("{0}\r\n\r\n{1}", waveFile, ex.Message));
            }
        }

        public static void StopAlertAudio()
        {
            try
            {
                if (stopAudioTimer != null)
                {
                    stopAudioTimer.Stop();
                }
            }
            finally
            {
                if (waveOut != null && waveOut.PlaybackState == PlaybackState.Playing)
                {
                    waveOut.Stop();
                    waveOut.Dispose();
                    waveOut = null;
                }
            }
        }
    }
}