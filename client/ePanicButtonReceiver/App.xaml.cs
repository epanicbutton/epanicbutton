﻿using System;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using EPanicButtonReceiver.Pages;
using EPanicButtonReceiver.ViewModels;
using EPanicButtonReceiver.Views;
using EasyNetQ;
using EpbSharedLib.MQ;
using EpbSharedLib.SharedClasses;
using EventAggregator;
using FirstFloor.ModernUI.Windows.Controls;
using Hardcodet.Wpf.TaskbarNotification;
using Ninject;
using System.IO;
using EpbSharedLib.Enums;
using System.Collections.Generic;
using EpbSharedLib.Utilities;

namespace EPanicButtonReceiver
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application, IDisposable
    {
        public EpbSharedLib.Entities.Alert AlertMessage = new EpbSharedLib.Entities.Alert();
        public TaskbarIcon NotifyIcon;
        public bool AlertAudioEnabled = false;
        public bool ProfileUpdateInprogress = false;
        public bool ShuttingDown = false;

        private static ISettings objSettings;
        private MainWindowViewModel viewModel;
        public IAdvancedBus MessageBus { get; set; }
        private EpbSharedLib.SharedClasses.IDispatcherProxy objProxy;
        private bool loopbackMode = false;

        private DateTime startTime = DateTime.Now;

        private App()
        {

            IocBootstrapper bootstrapper = new IocBootstrapper();
            bootstrapper.Initialize();
            var consumer = bootstrapper.Container.Get<IMQConsumer>();
            consumer.RaiseAlert += consumer_RaiseAlert;

            var machineGuid = ConfigurationManager.AppSettings["MACHINE_GUID"].ToString();
            var alertSub = machineGuid + ".alert";

            if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["LoopbackMode"]) &&  
                ConfigurationManager.AppSettings["LoopbackMode"].ToLower() == "true")
                loopbackMode = true;

            consumer.Start(new string[] { "AlertSub" }, new string[] { alertSub });

            objSettings = bootstrapper.Container.Get<ISettings>();
            objProxy = bootstrapper.Container.Get<IDispatcherProxy>();
        }

        void consumer_RaiseAlert(EpbSharedLib.Entities.Alert alert)
        {
            if ((!loopbackMode && alert.SenderMachineGuid == objSettings.MachineGuid) || alert.AlertDateTime < startTime)
                return;

            if (alert.MessageGuid.Equals("update_receiver"))
            {
                NotifyIconViewModel vm = this.NotifyIcon.DataContext as NotifyIconViewModel;
                vm.OnProfileChange();
            }
            else
            {
                Current.Dispatcher.Invoke((Action) delegate
                    {
                        //-- Reset the Alert Audio
                        ((App)Application.Current).AlertAudioEnabled = true;
                        
                        var control = new AlertView();
                        control.DataContext = new AlertViewViewModel(alert); 
                        var window = new ModernDialog
                            {
                                Content = control
                            };

                        window.Topmost = true;
                        window.ShowDialog();

                        AlertViewViewModel.StopAlertAudio();

                        string curPath = string.Format("{0}\\",
                                                       Path.GetDirectoryName(
                                                           System.Reflection.Assembly.GetEntryAssembly().Location));
                        string alertsListDataFile = string.Format("{0}AlertsList.xml", curPath);
                        alert.AlertMsgSource = MessageSource.RaiseAlertMessage;

                        AlertsListHelpers.UpdateAlertsListXml(alertsListDataFile, alert);

                    });
            }
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            if (EpbSharedLib.Utilities.Helpers.ProcessIsRunning("ePanicReceiver") == true)
            {
                System.Windows.Application.Current.Shutdown();
                return;
            }



            base.OnStartup(e);



            if (objSettings.VerboseLoggingOn)
            {
                ELogger.Log("Application starting...");
            }

            this.Subscribe(Constants.ApplicationShutdown, this.HandleApplicationShutdown);

            //-- create the notifyicon (it's a resource 
            //-- declared in NotifyIconResources.xaml 
            this.NotifyIcon = (TaskbarIcon)this.FindResource("NotifyIcon");
            this.NotifyIcon.DataContext = new ViewModels.NotifyIconViewModel(objSettings, objProxy);

            MainWindow mainWindow = new MainWindow();
            this.viewModel = new MainWindowViewModel();
            mainWindow.DataContext = this.viewModel;

            this.Subscribe(Constants.AlertNotification, this.HandleShowAlertMessage);

            NoSleep.Start();
            
            if (objSettings.VerboseLoggingOn)
            {
                ELogger.Log("Application started.");
            }
        }

        private async void HandleApplicationShutdown(EventMessage obj)
        {
            if (objSettings != null && objSettings.VerboseLoggingOn)
            {
                ELogger.Log("Exiting application");
            }

            NoSleep.Stop();

            Task<int> shutdownTask = ShutdonwDelayAsync();
            int retVal = await shutdownTask;

            if (objSettings != null && objSettings.VerboseLoggingOn)
            {
                ELogger.Log("Application exit result = " + retVal);
            }

            if (this.NotifyIcon != null)
            {
                this.NotifyIcon.Dispose();
            }
            Application.Current.Shutdown();
        }

        protected async override void OnExit(ExitEventArgs e)
        {
            base.OnExit(e);
        }

        public async Task<int> ShutdonwDelayAsync()
        {
            await Task.Delay(1500);
            return 1;
        }

        private void HandleShowAlertMessage(EventMessage eventMessage)
        {
            try
            {
                EpbSharedLib.Entities.Alert msg = eventMessage.GetValue<EpbSharedLib.Entities.Alert>("Dispatch");

                this.AlertMessage = msg;
            }
            catch (Exception ex)
            {
                ELogger.Log(ex.Message);
            }
        }

        public void Dispose()
        {
            if (AlertMessage != null)
            {
                AlertMessage.Dispose();
            }

            GC.SuppressFinalize(this);
        }
    }
}