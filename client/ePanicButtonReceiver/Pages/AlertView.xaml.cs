﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using EPanicButtonReceiver.ViewModels;

namespace EPanicButtonReceiver.Pages
{
    /// <summary>
    /// Interaction logic for AlertView.xaml
    /// </summary>
    public partial class AlertView : UserControl
    { 
        public AlertView()
        {
            this.InitializeComponent(); 
            this.DataContext = new AlertViewViewModel(((App)Application.Current).AlertMessage); 
        }
    }
}