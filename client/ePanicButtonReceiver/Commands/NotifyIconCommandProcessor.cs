﻿using System;
using System.Linq;
using System.Windows.Input;
using EPanicButtonReceiver.ViewModels;
using EpbSharedLib.Enums;

namespace EPanicButtonReceiver.Commands
{
    internal class NotifyIconCommandProcessor : ICommand
    {
        private readonly NotifyIconViewModel viewModel;
        private readonly SharedCommands.TrayIcon commandAction;

        public NotifyIconCommandProcessor(NotifyIconViewModel viewModel, SharedCommands.TrayIcon commandAction)
        {
            this.viewModel = viewModel;
            this.commandAction = commandAction;
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }

        public bool CanExecute(object parameter)
        {
            switch (this.commandAction)
            {
                case SharedCommands.TrayIcon.ShowMainWindow:
                    return true; 
                    break;
                case SharedCommands.TrayIcon.Exit:
                    return true;
                    break; 
                default:
                    return true;
                    break;
            }
        }

        /// <summary>
        /// Execute action based on current command.
        /// </summary>
        /// <param name="parameter"></param>
        public void Execute(object parameter)
        {
            switch (this.commandAction)
            {
                case SharedCommands.TrayIcon.ShowMainWindow:
                    this.viewModel.ShowMainWindow();
                    break;
                case SharedCommands.TrayIcon.Exit:
                    this.viewModel.ExitApplication();
                    break;
                default:
                    break;
            }
        }
    }
}
