﻿using System;
using System.Linq;
using System.Windows.Input;
using EpbSharedLib.Enums;
using EPanicButtonReceiver.ViewModels;

namespace EPanicButtonReceiver.Commands
{
    internal class AlertViewCommandProcessor : ICommand
    {
        private readonly AlertViewViewModel viewModel;
        private readonly SharedCommands.AlertView commandAction;

        public AlertViewCommandProcessor(AlertViewViewModel viewModel, SharedCommands.AlertView commandAction)
        {
            this.viewModel = viewModel;
            this.commandAction = commandAction;
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }

        public bool CanExecute(object parameter)
        {
            if (viewModel.ObjAlert == null)
            {
                return false;
            }

            switch (this.commandAction)
            {
                case SharedCommands.AlertView.UpdateMemo:
                    return !string.IsNullOrEmpty(viewModel.ObjAlert.AlertMemo) &&
                            viewModel.ObjAlert.OriginalMemo != viewModel.ObjAlert.AlertMemo;
                    break;
                case SharedCommands.AlertView.ConfirmReceipt: 

                    DateTime startTime = viewModel.ObjAlert.AlertDateTime;
                    DateTime endTime = DateTime.Now;
                    TimeSpan span = endTime.Subtract(startTime);

                    if (viewModel.ObjAlert.Confirmed)
                    {
                        return false;
                    }
                    else
                    {
                        return (span.Hours < 1 && span.Minutes < 10);
                    }
                    break;
                default:
                    return true;
                    break;
            }
        }

        /// <summary>
        /// Execute action based on current command.
        /// </summary>
        /// <param name="parameter"></param>
        public void Execute(object parameter)
        {
            switch (this.commandAction)
            {
                case SharedCommands.AlertView.UpdateMemo:
                    this.viewModel.UpdateMemo();
                    break;
                case SharedCommands.AlertView.ConfirmReceipt:
                    this.viewModel.ConfirmReceipt();
                    break;
                default:
                    break;
            }
        }
    }
}
