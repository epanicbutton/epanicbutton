﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using EasyNetQ;
using EasyNetQ.DI;
using EpbSharedLib.MQ;
using EpbSharedLib.SharedClasses;
using EventAggregator;
using Ninject;

//using Ninject;

namespace EPanicButtonReceiver.Classes
{
    public sealed class ZeroMqSubscriber : IDisposable
    {
        private static Settings objSettings;
        private readonly ManualResetEvent stopEvent = new ManualResetEvent(false);
        private readonly object locker = new object(); 
        private EpbSharedLib.DispatcherSrvc.UserData userInfo = null;

        private Thread workerThread;

        private bool exitSubscriptionUpdate = false;
        private bool exitApplication = false;
        private RabbitMqConsumer rabbitConsumer;

        public ZeroMqSubscriber()
        {
            objSettings = new EpbSharedLib.SharedClasses.Settings();
            this.SetUserData();

            this.Subscribe(Constants.ConfirmReceipt, this.HandleConfirmReceipt);
            this.Subscribe(Constants.ApplicationShutdown, this.HandleApplicationShutdown);

            // Container creation and custom logger registration
            IKernel cointainer = new StandardKernel();
            cointainer.Bind<IEasyNetQLogger>().To<RabbitMqLogger>();

            // Register Ninject as IoC Container for EasyNetQ
            cointainer.RegisterAsEasyNetQContainerFactory();

            //-- to prevent blocking of the UI thread. 
            this.StartConsuming();
        }

        public void Stop()
        {
            if (((App)Application.Current) == null || ((App)Application.Current).ProfileUpdateInprogress)
            {
                this.exitSubscriptionUpdate = true;
                return;
            }

            this.stopEvent.Set();

            try
            {
                this.workerThread.Join();
            }
            catch (Exception ex)
            {
                if (objSettings.VerboseLoggingOn)
                {
                    ELogger.Log(ex.Message);
                }
            }
        }

        public void StartConsuming()
        {
            this.exitSubscriptionUpdate = false;
            this.stopEvent.Reset();
            this.workerThread = new Thread(this.Consume);
            this.workerThread.Start();
            return;
        }

        public void Dispose()
        {
            this.stopEvent.Dispose();
            GC.SuppressFinalize(this);
        }

        private void HandleApplicationShutdown(EventMessage obj)
        {
            this.exitApplication = true;
            this.Stop();
            if (objSettings.VerboseLoggingOn)
            {
                ELogger.Log("ZeroMqSubscriber: Processing application shutdown.");
            }

            this.Dispose();
        }

        private void SetUserData()
        {
            try
            {
                var objDispProxy = new EpbSharedLib.SharedClasses.DispatcherProxy();
                this.userInfo = objDispProxy.GetUserData(false);
            }
            catch (Exception ex)
            {
                if (objSettings.VerboseLoggingOn)
                {
                    ELogger.Log(string.Format("Unable to read user data from dispatcher.\r\n{0}", ex.Message));
                }
            }
        }

        private void HandleConfirmReceipt(EventMessage eventMessage)
        {
            Models.Alert msg = eventMessage.GetValue<Models.Alert>("Dispatch");

            this.SendReceiptConfirmation_Task(msg);
        }

        private async void SendReceiptConfirmation_Task(Models.Alert msg)
        {
            try
            {
                await Task.Run(() => this.SendReceiptConfirmation(msg));
            }
            catch (Exception ex)
            {
                if (objSettings.VerboseLoggingOn)
                {
                    ELogger.Log(ex.Message);
                }
            }

            return;
        }

        private void SendReceiptConfirmation(Models.Alert msg)
        {
            //var alertConfirm = new EpbSharedLib.Entities.AlertConfirmation();
            //alertConfirm.MessageGuid = msg.MessageGuid;
            //if (this.userInfo != null)
            //{
            //    alertConfirm.ConfirmSenderId = this.userInfo.Id;
            //}
            //string recMachineName = msg.SenderMachineName;
            //var confirmSocket = zmqContext.CreateSocket(SocketType.REQ);
            //try
            //{
            //    recMachineName = string.Format("tcp://{0}:20161", recMachineName);
            //    confirmSocket.Connect(recMachineName);
            //    var sendStat = confirmSocket.Send(EpbSharedLib.Utilities.Serialization.SerializeObject(alertConfirm), Encoding.UTF8);
            //    var rcvdMsg = confirmSocket.Receive(Encoding.UTF8, new TimeSpan(0, 0, 0, 1));
            //    if (sendStat.ToString().ToLower() == "sent" && !string.IsNullOrEmpty(rcvdMsg))
            //    {
            //        Application.Current.Dispatcher.Invoke(new System.Action(() =>
            //        {
            //            this.Send(Constants.ConfirmReceiptSuccess,
            //                new KeyValuePair<string, object>(
            //                    "Dispatch", alertConfirm));
            //        }));
            //        if (ObjSettings.VerboseLoggingOn)
            //        {
            //            ELogger.Log(string.Format("Sent confirm message '{0}'at {1}.", msg.MessageGuid, DateTime.Now));
            //        }
            //    }
            //}
            //catch (ZmqException ex)
            //{
            //    ELogger.Log(string.Format("Confirmation Error (HandleConfirmReceipt): {0}", ex.Message));
            //}
            //finally
            //{
            //    if (confirmSocket != null)
            //    {
            //        confirmSocket.Close();
            //    }
            //    alertConfirm = null;
            //}
            return;
        }

        private void Consume()
        {
            int retryCount = 0;

            ((App)Application.Current).ProfileUpdateInprogress = true;
            EpbSharedLib.SharedClasses.DispatcherProxy objProxy = new EpbSharedLib.SharedClasses.DispatcherProxy();
            var usrData = objProxy.GetUserDataNoCache();

            //-----------------------------------------------------------
            // Continue to try until thread ends or userDate is not null
            //-----------------------------------------------------------
            while (usrData == null && !this.stopEvent.WaitOne(0))
            {
                retryCount++;
                if (objSettings.VerboseLoggingOn)
                {
                    ELogger.Log(String.Format("[{0}] Waiting for network connection...", retryCount));
                }

                Thread.Sleep(3000);
                usrData = objProxy.GetUserDataNoCache();

                if (((App)Application.Current) == null || ((App)Application.Current).ShuttingDown == true)
                {
                    if (((App)Application.Current) != null)
                    {
                        ((App)Application.Current).ProfileUpdateInprogress = false;
                    }

                    this.Stop();
                    return;
                }
                // Read from Cache after server reads failed.
                if (retryCount == 3)
                {
                    usrData = objProxy.GetUserData(false);
                    retryCount = 0;
                }
            }

            try
            {
                if (usrData == null || string.IsNullOrEmpty(usrData.ButtonIDs))
                {
                    ((App)Application.Current).ProfileUpdateInprogress = false;
                    return;
                }
            }
            catch (Exception ex1)
            {
                ELogger.Log(string.Format("DoConnectAndSubscribe() - if (usrData == null || usrData.buttons == null) -|- {0}", ex1.Message));
            }

            try
            {
                ((App)Application.Current).ProfileUpdateInprogress = false;
                IList<string> buttonList = !string.IsNullOrEmpty(usrData.ButtonIDs) ? usrData.ButtonIDs.Split(',') : null;
                if (buttonList != null && buttonList.Any())
                {
                  //  this.rabbitConsumer = new RabbitMqConsumer(buttonList);
                }

                while (!this.stopEvent.WaitOne(0))
                {
                }
            }
            catch (Exception ex)
            {
                ELogger.Log(ex);
            }
            finally
            {
                if (rabbitConsumer != null)
                {
                    this.rabbitConsumer.Stop();
                }
            }
        }
    }
}