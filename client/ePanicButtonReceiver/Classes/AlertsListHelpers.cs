﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using EpbSharedLib.SharedClasses;

namespace EPanicButtonReceiver.Classes
{
    public class AlertsListHelpers
    {
        private static readonly CacheHelper objCache = new CacheHelper();

        public static string GetAlertsListFilePath()
        {
            string curPath = string.Format("{0}\\", Path.GetDirectoryName(Assembly.GetEntryAssembly().Location));
            string alertsListDataFile = string.Format("{0}AlertsList.xml", curPath);

            return alertsListDataFile;

        }
        public static void UpdateAlertsListXml(string alertsListDataFile, EpbSharedLib.Entities.Alert msg)
        {
            msg.AlertMemo = string.Empty;

            try
            {
                EpbSharedLib.Entities.Alert[] objRes = null;

                if (File.Exists(alertsListDataFile))
                {
                    XmlSerializer deserializer = new XmlSerializer(typeof(EpbSharedLib.Entities.Alert[]));

                    StreamReader reader = new StreamReader(alertsListDataFile);
                    objRes = (EpbSharedLib.Entities.Alert[])deserializer.Deserialize(reader);
                    reader.Close(); 
                } 

                
                if (objRes == null)
                {
                    objRes = new EpbSharedLib.Entities.Alert[1];
                    objRes[0] = msg;
                }
                else
                {
                      
                    List<EpbSharedLib.Entities.Alert> listOfStr = new List<EpbSharedLib.Entities.Alert>(objRes.OrderByDescending(s => s.AlertDateTime).Take(10)); 

                    listOfStr.Insert(0, msg);
                    objRes = listOfStr.ToArray();

                    objCache.RemoveCachedItem("ALERTS_LIST");
                    objCache.AddToCache("ALERTS_LIST", objRes, EpbSharedLib.SharedClasses.CachePriority.Default);
                }

                System.IO.StreamWriter file = new System.IO.StreamWriter(alertsListDataFile);

                try
                {
                    System.Xml.Serialization.XmlSerializer x = new System.Xml.Serialization.XmlSerializer(objRes.GetType());
                    x.Serialize(file, objRes); 
                }
                catch (Exception ex2)
                {
                    ELogger.Log(ex2.Message);
                }
                finally
                {
                    file.Close(); 
                }
            }
            catch (Exception ex)
            {
                ELogger.Log(ex.Message);
            }
             
        }
    }
}
