﻿using System;
using System.Linq;
using System.Configuration;
using EasyNetQ;
using EasyNetQ.DI;
using EpbSharedLib.MQ;
using EpbSharedLib.SharedClasses;
using Ninject;

namespace EPanicButtonReceiver
{
    public class IocBootstrapper
    {
        public IocBootstrapper()
        {
        }
  
        public IKernel Container { get; private set; }

        public void Initialize()
        {
            ELogger.Log("Creating IOC container.");

            ISettings objSettings = new EpbSharedLib.SharedClasses.Settings();
            IDispatcherProxy objProxy = new EpbSharedLib.SharedClasses.DispatcherProxy();

            // Container creation and custom logger registration
            this.Container = new StandardKernel();

            ELogger.Log("Binding container types.");

            //if (objSettings.VerboseLoggingOn)
            //{
            //    this.Container.Bind<IEasyNetQLogger>().To<RabbitMqLogger>();
            //}
               
            var connStr = ConfigurationManager.AppSettings["AzureConnectionString"];   
            // var connStr = "Endpoint=sb://epanic.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=HHIKaBivRusxq/fZb9sRTpOx+773mCZMxfK0/hglZBo=";
            IMQConsumer consumer = AzureConsumer.Create(connStr);
            IMQPublisher publisher = new AzurePublisher(connStr, objProxy);

            Container.Bind<IMQPublisher>().ToConstant(publisher);
            Container.Bind<IMQConsumer>().ToConstant(consumer);
            this.Container.Bind<ISettings>().To<Settings>();
            //   Container.Bind<IMQConsumer>().To<RabbitMqConsumer>();
            //    Container.Bind<IMQPublisher>().To<RabbitMqPublisher>();
            this.Container.Bind<IDispatcherProxy>().To<DispatcherProxy>();

            // Register Ninject as IoC Container for EasyNetQ
            this.Container.RegisterAsEasyNetQContainerFactory();
        }
    }
}