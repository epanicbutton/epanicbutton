﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;
using FirstFloor.ModernUI.Presentation;

namespace EPanicButtonReceiver.Models 
{
    public class Alert : NotifyPropertyChanged 
    {
        private int alertId;
        private DateTime alertDateTime;
        private string alertGuid;
        private string alertTitle;
        private string senderName;
        private string senderMachineName;
        private string senderMachineGuid;
        private string senderLocation;
        private string senderDepartment;
        private string senderTelephone;
        private string senderEmail;
        private int senderId;
        private string severityLevel;
        private string alertDescription;
        private string alertMemo;
        private bool confirmed;
        private bool enableAudio;
        private string messageGuid;
        public  string OriginalMemo;
        private string severityLevelColor;

        public Alert()
        {
        }

        public string SeverityLevelColor
        {
            get
            {
                return this.severityLevelColor;
            }
            set
            {
                this.severityLevelColor = value;
                this.OnPropertyChanged("SeverityLevelColor");
            }
        }

        public string SenderMachineGuid
        {
            get
            {
                return this.senderMachineGuid;
            }
            set
            {
                this.senderMachineGuid = value;
                this.OnPropertyChanged("SenderMachineGuid");
            }
        }

        public string MessageGuid
        {
            get
            {
                return this.messageGuid;
            }
            set
            {
                this.messageGuid = value;
                this.OnPropertyChanged("MessageGuid");
            }
        }

        public bool Confirmed
        {
            get
            {
                return this.confirmed;
            }
            set
            {
                this.confirmed = value;
                this.OnPropertyChanged("Confirmed");
            }
        }

        public bool EnableAudio
        {
            get
            {
                return this.enableAudio;
            }
            set
            {
                this.enableAudio = value;
                this.OnPropertyChanged("EnableAudio");
            }
        }

        public string AlertMemo
        {
            get
            {
                return this.alertMemo;
            }
            set
            {
                this.alertMemo = value; 
                this.OnPropertyChanged("AlertMemo");
            }
        }

        public string AlertDescription
        {
            get
            {
                return this.alertDescription;
            }
            set
            {
                this.alertDescription = value;
                this.OnPropertyChanged("AlertDescription");
            }
        }

        public string SeverityLevel
        {
            get
            {
                return this.severityLevel;
            }
            set
            {
                this.severityLevel = value;
                this.OnPropertyChanged("SeverityLevel");
            }
        }

        public int SenderId
        {
            get
            {
                return this.senderId;
            }
            set
            {
                this.senderId = value;
                this.OnPropertyChanged("SenderId");
            }
        }

        public string SenderDepartment
        {
            get
            {
                return this.senderDepartment;
            }
            set
            {
                this.senderDepartment = value;
                this.OnPropertyChanged("SenderDepartment");
            }
        }

        public string SenderLocation
        {
            get
            {
                return this.senderLocation;
            }
            set
            {
                this.senderLocation = value;
                this.OnPropertyChanged("SenderLocation");
            }
        }

        public string SenderName
        {
            get
            {
                return this.senderName;
            }
            set
            {
                this.senderName = value;
                this.OnPropertyChanged("SenderName");
            }
        }

        public string SenderMachineName
        {
            get
            {
                return this.senderMachineName;
            }
            set
            {
                this.senderMachineName = value;
                this.OnPropertyChanged("SenderMachineName"); 
            }
        }

        public string AlertTitle
        {
            get
            {
                return this.alertTitle;
            }
            set
            {
                this.alertTitle = value;
                this.OnPropertyChanged("AlertTitle");
            }
        }

        public string AlertGuid
        {
            get
            {
                return this.alertGuid;
            }
            set
            {
                this.alertGuid = value;
                this.OnPropertyChanged("AlertGuid");
            }
        }

        public int AlertId
        {
            get
            {
                return this.alertId;
            }
            set
            {
                this.alertId = value;
                this.OnPropertyChanged("AlertId");
            }
        }

        public DateTime AlertDateTime
        {
            get
            {
                return this.alertDateTime;
            }
            set
            {
                this.alertDateTime = value;
                this.OnPropertyChanged("AlertDateTime");
            }
        }

        public string FormattedAlertDateTime
        {
            get
            {
                return string.Format("{0}\r\n{1}", this.alertDateTime.ToShortDateString(), this.alertDateTime.ToShortTimeString());
            }
        }

        public string SenderTelephone
        {
            get
            {
                return this.senderTelephone;
            }
            set
            {
                this.senderTelephone = value;
                this.OnPropertyChanged("SenderTelephone");
            }
        }

        public string SenderEmail
        {
            get
            {
                return this.senderEmail;
            }
            set
            {
                this.senderEmail = value;
                this.OnPropertyChanged("SenderEmail");
            }
        }

        public string SenderUserInfo
        {
            get
            {
                return string.Format("{0}\r\n{1}\r\n{2}", this.senderName, this.senderEmail, this.senderTelephone);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName)); 

            }
        }
    }
}