﻿using System;
using System.Linq;
using System.Configuration;
using System.IO;

namespace WinServiceTester
{
    class Program
    {
        //private static string clientID = ConfigurationManager.AppSettings.Get("CLIENT_GUID");
        //private static string machineID = ConfigurationManager.AppSettings.Get("MACHINE_GUID");
        private static string clientID = "14CE6671-A1F2-4A1D-BA50-8635F429AA05";
        private static string machineID = "248c2355-643e-4e6f-b300-e50f2f1ba39a";

        static void Main(string[] args)
        {           

            var client = new ServiceReference1.DispatcherServiceClient("BasicHttpBinding_IDispatcherService");
            var directClient = new ServiceReference2.IepbwsClient("BasicHttpBinding_Iepbws");

            var output1 = directClient.GetUserInfo(clientID.ToString(), machineID.ToString());            
            var output = client.GetUserInfo(clientID.ToString(), machineID.ToString(), true);
            try
            {
                string file = "wcf_test.txt";
                string hdr = "===============================================================";
                StreamWriter writer = new StreamWriter(file, true);
                writer.WriteLine(hdr);
                writer.WriteLine("Test Date/Time = " + System.DateTime.Now.ToLocalTime().ToString());
                writer.WriteLine("Result = " + output[0].nodeuserdata[0].ClientName.ToString());
                writer.WriteLine("Result = " + output[0].nodeuserdata[0].UserLastName.ToString());

                writer.Close();
            }

            catch (Exception x){

                string file = "wcf_test_err.txt";
                string hdr = "===============================================================";
                StreamWriter writer = new StreamWriter(file, true);
                writer.WriteLine(hdr);
                writer.WriteLine("Test Date/Time = " + System.DateTime.Now.ToLocalTime().ToString());
                writer.WriteLine("Result = " + x.ToString());

                writer.Close();

            }
        }
    }
}
