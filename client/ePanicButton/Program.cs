﻿using System;
using System.Text;
using System.IO;
using System.Xml;
using System.Diagnostics;
using System.ServiceProcess;
using System.Configuration;
using Microsoft.Win32;



namespace ePanicButton
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
               if (args.Length == 2)
                {
                    if (args[0].ToUpper().Equals(@"INSTALL") == true)
                    {
                        var clientId = args[1];
                        File.AppendAllText(@"C:\ProgramData\ePanicButton\InstallLog.txt", "CLIENT ID For INSTALL:" + args[1] + "\r\n");
                        DoWork.InstallConfiguartionFiles(clientId);
                        DoWork.StopAll();
                        DoWork.StartAll(true);
                    }
                }
            }

            catch( Exception ex)
            {
                File.AppendAllText(@"C:\ProgramData\ePanicButton\InstallLog.txt", ex.Message + "" + ex.StackTrace);
            }
        }
    }



    /*
    **  Static class which does all the auxiliary work by the “Launcher” / “Installer”
    */
    public static class DoWork
    {
        static string ProgramDirectoryLocation = @"C:\ProgramData\ePanicButton\";

        /*
        **  Logic to handle CLI is valid
        */
        #region ValidateCLI section
        /*
        **  Validate the command line is in a good syntax
        */
        public static int ValidateCLI(string[] args)
        {
            return (0);
        }
        #endregion


        /*
        **  Logic to GUIDs
        */
        #region GUID section
        /*
        **  Simple class to generate a GUID and return a string value all upper case
        */
        public static string GenerateGUID()
        {
            Guid guid = Guid.NewGuid();
            return (guid.ToString().ToUpper());
        }

        /*
        **  Time is preventing finding an existing OS GUID specific to this computer - such that it will be the same # each time
        **  One suggestion is to grab the following registry key
        **      HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Cryptography\MachineGuid
        **
        **  Revisit this method when there is more time
        */
        static string _machineGUID = null;
        public static string GetMachineGUID()
        {
            if (_machineGUID == null) {
                if (_machineGUID == null)
                    _machineGUID = DoWork.GenerateGUID();
            }
            return (_machineGUID);
        }
        #endregion


        /*
        **  Logic to handle creation of various configuration files
        */
        #region InstallConfiguartionFiles section
        /*
        **  Define a struct to handle configuration files stored as resources embedded in the executable
        */
        struct ConfigurationFiles
        {
            public string Name;
            public string Contents;

            public ConfigurationFiles(string name, string contents)
            {
                Name = name;
                Contents = contents;
            }
        };

        /*
        **  Create an array of configuration resource files
        */
        static ConfigurationFiles[] ConfigFiles =
        {
            new ConfigurationFiles(ProgramDirectoryLocation + @"ePanicButtonReceiver.exe.config",       Properties.Resources.ePanicButtonReceiver_exe_config),
            new ConfigurationFiles(ProgramDirectoryLocation + @"ePanicClient.exe.config",               Properties.Resources.ePanicClient_exe_config),
            new ConfigurationFiles(ProgramDirectoryLocation + @"EpbHostedDispatcherService.exe.config", Properties.Resources.EpbHostedDispatcherService_exe_config),
            new ConfigurationFiles(ProgramDirectoryLocation + @"UserInfo.xml",                          Properties.Resources.UserInfo_xml),
        };

        /*
        **  XPath location to the nodes to update
        */
        static string CLIENT_GUID = @"/configuration/appSettings/add[@key='CLIENT_GUID']";
        static string MACHINE_GUID = @"/configuration/appSettings/add[@key='MACHINE_GUID']";


        public static void Log(string mesg)
        {
            File.AppendAllText(@"C:\ProgramData\ePanicButton\InstallLog.txt", mesg + "\r\n");
        }

        /*
        **  Determine if all the configuration files exist
        **      If it does leave it alone
        **      If it does not create the file
        */
        public static void InstallConfiguartionFiles(string clientGUID)
        {
            bool b = Directory.Exists(ProgramDirectoryLocation);
            DirectoryInfo di = Directory.CreateDirectory(ProgramDirectoryLocation);

            /*
            **  Iterate over all the config files
            */
            for (int i=0; i<ConfigFiles.Length; i++)
            {

                try
                {
                    /*
                    **  If the file is not there create the file from the embedded resource file
                    */

                    if (File.Exists(ConfigFiles[i].Name) == false)
                    {

                        /*
                        **  Put the contents of the resource file into an XML document
                        **  Locate the CLIENT_GUID & MACHINE_GUID nodes
                        **  Create the file
                        */
                        XmlDocument doc = new XmlDocument();
                        doc.PreserveWhitespace = true;
                        doc.LoadXml(ConfigFiles[i].Contents);
                        XmlElement eClientGUID = doc.SelectSingleNode(CLIENT_GUID) as XmlElement;
                        XmlElement eMachineGUID = doc.SelectSingleNode(MACHINE_GUID) as XmlElement;

                        if (eClientGUID != null)
                            eClientGUID.Attributes["value"].Value = clientGUID;
                        if (eMachineGUID != null)
                            eMachineGUID.Attributes["value"].Value = GetMachineGUID();

                        using (XmlTextWriter tw = new XmlTextWriter(ConfigFiles[i].Name, Encoding.UTF8))
                        {
                            tw.Formatting = Formatting.Indented;
                            doc.Save(tw);
                            tw.Close();
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log(ex.Message + "" + ex.StackTrace);
                    
                }
            }
            return;
        }
        #endregion

        /*
        **  Logic to stop/kill and start running processes
        */
        #region Start/Stop processes section
        /*
        **  List of the processes and services which need to be started/stopped
        */
        static string[] ProcessList = { @"ePanicClient", @"ePanicButtonReceiver" };
        static string[] ServiceList = { @"ePanicButtonDispatcher" };

        public static void StopAll()
        {
            for (int i=0; i<ProcessList.Length; i++)
            {
                foreach (var p in Process.GetProcessesByName(ProcessList[i]))
                    p.Kill();
            }

            for (int i = 0; i<ServiceList.Length; i++)
            {
                try
                {
                    ServiceController sc = new ServiceController(ServiceList[i]);

                    if (sc == null) { Log("sc is null"); continue; }
                    Log("Stopping Service Status: " + sc.Status.ToString());

                    sc.WaitForStatus(ServiceControllerStatus.Running, TimeSpan.FromSeconds(30));
                    if (sc != null && sc.Status != ServiceControllerStatus.Stopped)
                        sc.Stop();

                    sc.WaitForStatus(ServiceControllerStatus.Stopped, TimeSpan.FromSeconds(20));
                }
                catch(Exception ex)
                {
                    Log("Failed attempting to stop service: " + ex.Message);
                }
            }
            return;
        }

        public static void StartAll(bool includeService=false)
        {

            int iDelay = 0;

            try
            {
                string sDelay = ConfigurationManager.AppSettings["StartDelay"] ?? "5";
                iDelay = Convert.ToInt32(sDelay) * 1000;
            }
            catch
            {
                ;
            }
            if (includeService)
            {
                for (int i = 0; i < ServiceList.Length; i++)
                {
                    try
                    {
                        ServiceController sc = new ServiceController(ServiceList[i]);
                        if (sc == null)
                        {
                            Log("sc == null");
                            continue;
                        }
                        Log("Starting Service Status: " + sc.Status.ToString());
                        if (sc.Status != ServiceControllerStatus.Running)
                            sc.Start();
                    }
                    catch (Exception ex)
                    {
                        Log("Failure to Stop Service: " + ex.Message);
                    }
                }
                System.Threading.Thread.Sleep(10000);
            }

            for (int i = 0; i < ProcessList.Length; i++)
            {
                Process[] p = Process.GetProcessesByName(ProcessList[i]);
                
                Log("Starting Process: " + ProcessList[i]);
                if (p == null)
                {
                    Log("p == null:" + ProcessList[i]);
                }
                if (p != null && p.Length == 0)
                    Process.Start(ProgramDirectoryLocation + ProcessList[i]);
                
                System.Threading.Thread.Sleep(iDelay);
            }
            return;
        }
        #endregion
    }
}



#region notes
/*
            ManagementObject os = new ManagementObject("Win32_OperatingSystem=@");
            ManagementClass cpu = new ManagementClass("Win32_Processor");
            ManagementClass csp = new ManagementClass("Win32_ComputerSystemProduct");

            ManagementObjectCollection moc = cpu.GetInstances();

            string serial = (string) os["SerialNumber"];
            serial = (string) cpu["processorID"];
            serial = (string)csp["UUID"];

            //foreach (ManagementObject mo in os)
            //{
            //        Console.WriteLine("{0} = {1} {{2}}", mo["Name"], mo["Value"], mo["Type"]);
            //}

            return (serial);
*/
#endregion
