﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ePanicButton.Properties {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("ePanicButton.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot;?&gt;
        ///&lt;configuration&gt;
        ///	&lt;configSections&gt;
        ///		&lt;section type=&quot;log4net.Config.Log4NetConfigurationSectionHandler, log4net&quot; name=&quot;log4net&quot;&gt;&lt;/section&gt;
        ///	&lt;/configSections&gt;
        ///	&lt;appSettings&gt;
        ///		&lt;add key=&quot;APP_VERSION&quot; value=&quot;4.0.0.0&quot;&gt;&lt;/add&gt;
        ///		&lt;add key=&quot;CLIENT_GUID&quot; value=&quot;&quot;&gt;&lt;/add&gt;
        ///		&lt;add key=&quot;LOGGING_LEVEL&quot; value=&quot;Verbose&quot;&gt;&lt;/add&gt;
        ///		&lt;add key=&quot;MACHINE_GUID&quot; value=&quot;&quot;&gt;&lt;/add&gt;
        ///		&lt;add key=&quot;SUB_UPDATE_INTERVAL&quot; value=&quot;15&quot;&gt;&lt;/add&gt;
        ///	&lt;/appSettings&gt;
        ///	&lt;log4net&gt;
        ///		&lt;appender type=&quot;log4net.Appe [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string ePanicButtonReceiver_exe_config {
            get {
                return ResourceManager.GetString("ePanicButtonReceiver_exe_config", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot;?&gt;
        ///&lt;configuration&gt;
        ///	&lt;configSections&gt;
        ///		&lt;section type=&quot;log4net.Config.Log4NetConfigurationSectionHandler, log4net&quot; name=&quot;log4net&quot;&gt;&lt;/section&gt;
        ///	&lt;/configSections&gt;
        ///	&lt;appSettings&gt;
        ///		&lt;add key=&quot;APP_VERSION&quot; value=&quot;4.0.0.0&quot;&gt;&lt;/add&gt;
        ///		&lt;add key=&quot;CLIENT_GUID&quot; value=&quot;&quot;&gt;&lt;/add&gt;
        ///		&lt;add key=&quot;LOGGING_LEVEL&quot; value=&quot;Verbose&quot;&gt;&lt;/add&gt;
        ///		&lt;add key=&quot;MACHINE_GUID&quot; value=&quot;&quot;&gt;&lt;/add&gt;
        ///		&lt;add key=&quot;SUB_UPDATE_INTERVAL&quot; value=&quot;60&quot;&gt;&lt;/add&gt;
        ///	&lt;/appSettings&gt;
        ///	&lt;log4net&gt;
        ///		&lt;appender type=&quot;log4net.Appe [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string ePanicClient_exe_config {
            get {
                return ResourceManager.GetString("ePanicClient_exe_config", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot;?&gt;
        ///&lt;configuration&gt;
        ///	&lt;configSections&gt;
        ///		&lt;section type=&quot;log4net.Config.Log4NetConfigurationSectionHandler, log4net&quot; name=&quot;log4net&quot;&gt;&lt;/section&gt;
        ///	&lt;/configSections&gt;
        ///	&lt;appSettings&gt;
        ///		&lt;add value=&quot;true&quot; key=&quot;log4net.Internal.Quiet&quot;&gt;&lt;/add&gt;
        ///	&lt;/appSettings&gt;
        ///	&lt;log4net&gt;
        ///		&lt;appender type=&quot;log4net.Appender.RollingFileAppender&quot; name=&quot;ELoggerAppender&quot;&gt;
        ///			&lt;appendToFile value=&quot;true&quot;&gt;&lt;/appendToFile&gt;
        ///			&lt;datePattern value=&quot;.yyyyMMdd-HH&quot;&gt;&lt;/datePattern&gt;
        ///			&lt;file type=&quot;log4net.Util.P [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string EpbHostedDispatcherService_exe_config {
            get {
                return ResourceManager.GetString("EpbHostedDispatcherService_exe_config", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Icon similar to (Icon).
        /// </summary>
        internal static System.Drawing.Icon IconGreen {
            get {
                object obj = ResourceManager.GetObject("IconGreen", resourceCulture);
                return ((System.Drawing.Icon)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;?xml version=&quot;1.0&quot; encoding=&quot;utf-8&quot;?&gt;
        ///&lt;ArrayOfNodeData xmlns:xsi=&quot;http://www.w3.org/2001/XMLSchema-instance&quot; xmlns:xsd=&quot;http://www.w3.org/2001/XMLSchema&quot;&gt;
        ///  &lt;NodeData&gt;
        ///    &lt;ExtensionData /&gt;
        ///    &lt;nodeuserdata&gt;
        ///      &lt;UserData&gt;
        ///        &lt;ExtensionData /&gt;
        ///        &lt;AllowMoveEnabled&gt;1&lt;/AllowMoveEnabled&gt;
        ///        &lt;AllowsUserToChangeProfileEnabled&gt;1&lt;/AllowsUserToChangeProfileEnabled&gt;
        ///        &lt;ButtonIDs&gt;&lt;/ButtonIDs&gt;
        ///        &lt;ClientName&gt;&lt;/ClientName&gt;
        ///        &lt;ClientProfileRefreshEnabled&gt;1&lt;/ClientProfileRef [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string UserInfo_xml {
            get {
                return ResourceManager.GetString("UserInfo_xml", resourceCulture);
            }
        }
    }
}
