﻿
using EpbDispatcherLib.EPBService;
using EpbSharedLib.Enums;
using System;
using System.Linq;
using System.ServiceModel; 
 
namespace EpbDispatcherLib
{
    [ServiceContract] 
    public interface IDispatcherService
    {
        [OperationContract]
        NodeData[] GetUserInfo(string clientGuid, string machineGuid, bool refresh);

        [OperationContract]
        void UpdateUserInfo(string clientGuid, string machineGuid);

        [OperationContract]
        bool SetUserProfile(EpbSharedLib.Entities.UserInfo userInfo);

        [OperationContract]
        string ServiceTest(string inTest);

        [OperationContract]
        bool EventProcess(EpbSharedLib.Enums.EventType eventType, string eventData);

        [OperationContract]
        string GetDispatcherPath();
    }
}
