﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml.Serialization;
using EpbDispatcherLib.EPBService;
using EpbSharedLib.Entities;
using EpbSharedLib.Enums;
using EpbSharedLib.SharedClasses;
using System.Diagnostics;
using System.Text;

namespace EpbDispatcherLib
{
    public class DispatcherService : IDispatcherService
    {
        private IepbwsClient epbWs;
        private static Settings objSettings;

        /// <summary>Helper function to attach a debugger to the running service.</summary>
        [Conditional("DEBUG")]
        static void DebugMode()
        {
            //if (!Debugger.IsAttached)
            //    Debugger.Launch();

            //Debugger.Break();
        }

        public DispatcherService()
        {
            DebugMode();
            objSettings = new EpbSharedLib.SharedClasses.Settings();

            ELogger.Log("Verbose Logging: " + objSettings.VerboseLoggingOn);

            if (objSettings.VerboseLoggingOn)
            {
                ELogger.Log("In DispatcherService()...");
            }

            try
            {
                epbWs = new IepbwsClient();
                ELogger.Log("Connected to ePanicButton Server." );
            }
            catch (Exception ex)
            {
                ELogger.Log("Unable to conntect to service: " + ex.Message);
            }

        }
         
        /// <summary>
        /// Sets the user profile.
        /// </summary>
        /// <param name="userInfo">The user info.</param>
        /// <returns>Success or Failure as Boolean</returns>
        public bool SetUserProfile(EpbSharedLib.Entities.UserInfo userInfo)
        {
            if (objSettings.InLocalTestMode)
            {
                ELogger.Log("Profile not updated. Currently in local test mode.");

                if (objSettings.VerboseLoggingOn)
                {
                    ELogger.Log("In DispatcherService()...");
                }

                return true;
            }

            if (objSettings.VerboseLoggingOn)
            {
                ELogger.LogMethodStart();
            }

            bool retVal = false;

            try
            {
                var o = new UpdateUserData();

                o.FirstName = userInfo.FirstName;
                o.LastName = userInfo.LastName;
                o.Telephone = userInfo.Telephone;
                o.EmailAddress = userInfo.EmailAddress;
                o.Department = userInfo.Department;
                o.Location = userInfo.Location;
                o.MachineName = userInfo.MachineName;
                o.MacAddress = userInfo.MacAddress;
                o.MachineIp = userInfo.MachineIp;
                o.MachineGuid = userInfo.MachineGuid;
                o.ClientGuid = userInfo.ClientGuid;
                o.ApplicationVersion = userInfo.ApplicationVersion;
                o.OsVersion = userInfo.OsVersion;
                o.ServicePack = userInfo.ServicePack;
                o.UserName = userInfo.UserName;

                retVal = epbWs.SetUserInfo(o);
            }
            catch (Exception ex)
            {
                ELogger.Log(ex.Message);
            }

            if (objSettings.VerboseLoggingOn)
            {
                ELogger.LogMethodEnd();
            }

            return retVal;
        }

        /// <summary>
        /// Get User Info by client GUID and machine GUID
        /// </summary>
        /// <param name="clientGuid">Client GUID as String</param>
        /// <param name="machineGuid">Machine GUID as String</param>
        /// <param name="refresh">True to read from web service, False to load from config XML file.</param>
        /// <returns>NodeData collection</returns>
        public EpbDispatcherLib.EPBService.NodeData[] GetUserInfo(string clientGuid, string machineGuid, bool refresh)
        {

            if (objSettings.VerboseLoggingOn)
            {
                ELogger.Log("GetUserInfo( " + clientGuid + " , " + machineGuid + " ' " + refresh + " ) ");
            } 

            EpbDispatcherLib.EPBService.NodeData[] objRet = null;

            string userInfoDataFile = EpbSharedLib.Utilities.Helpers.GetUserInfoFilePath();

            try
            {  
                if ((refresh || !File.Exists(userInfoDataFile)) && !objSettings.InLocalTestMode)
                {
                    //---------------------------------------
                    //-- Get data from Service
                    //--------------------------------------- 
                    objRet = RefreshConfigDataFromService(clientGuid, machineGuid, objRet, userInfoDataFile);

                    if (objSettings.VerboseLoggingOn)
                    {
                        ELogger.Log("DispatcherService::Refreshing...");
                    }
                }
                else
                { 
                    if (objSettings.InLocalTestMode)
                    {
                        ELogger.Log("Currently in local test mode. Loading from file.");
                    }
                    try
                    {
                        //---------------------------------------
                        //-- Get data from XML data file
                        //---------------------------------------
                        objRet = LoadConfigFromXmlFile(objRet, userInfoDataFile);
                    }
                    catch (Exception ex2)
                    {
                        ELogger.Log(ex2.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                // Log the exception.
                ELogger.Log(ex.Message);
            }

            if (objSettings.VerboseLoggingOn)
            {
                ELogger.LogMethodEnd();
            }

            return objRet;
        }
        /// <summary>
        /// Send updated user info to the server.
        /// </summary>
        /// <param name="clientGuid">Client GUID as string</param>
        /// <param name="machineGuid">Machine GUID as string</param>
        public void UpdateUserInfo(string clientGuid, string machineGuid)
        {
            if (objSettings.VerboseLoggingOn)
            {
                ELogger.LogMethodStart();
            }

            if (objSettings.InLocalTestMode)
            {
                ELogger.Log("Currently in local test mode. Skipping server update.");
                return;
            }

            EpbDispatcherLib.EPBService.NodeData[] objRet = null;

            string userInfoDataFile = EpbSharedLib.Utilities.Helpers.GetUserInfoFilePath();

            try
            {
                //---------------------------------------
                //-- Get data from Service
                //---------------------------------------
                objRet = RefreshConfigDataFromService(clientGuid, machineGuid, objRet, userInfoDataFile);
            }
            catch (Exception ex)
            {
                // Log the exception.
                ELogger.Log(ex.Message);
            }

            if (objSettings.VerboseLoggingOn)
            {
                ELogger.LogMethodEnd();
            }
        }

        private static EpbDispatcherLib.EPBService.NodeData[] LoadConfigFromXmlFile(EpbDispatcherLib.EPBService.NodeData[] objRet, string userInfoDataFile)
        {
            if (objSettings.VerboseLoggingOn)
            {
                ELogger.LogMethodStart();
                ELogger.Log("Loading XML config");
            }

            if (!File.Exists(userInfoDataFile))
            {
                ELogger.Log("LoadConfigFromXmlFile: UserInfo does not exist");
                return null;
            }

            EpbDispatcherLib.EPBService.NodeData[] objRes = null;
            XmlSerializer serializer = new XmlSerializer(typeof(EpbDispatcherLib.EPBService.NodeData[]));

            using (var fs = new FileStream(userInfoDataFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            using (StreamReader reader = new StreamReader(fs, Encoding.Default))
            {
                // read the stream
                objRes = (EpbDispatcherLib.EPBService.NodeData[])serializer.Deserialize(reader);
                reader.Close();
            }  

            objRet = objRes;

            if (objSettings.VerboseLoggingOn)
            {
                ELogger.LogMethodEnd();
            }

            return objRet;
        }

        private EpbDispatcherLib.EPBService.NodeData[] RefreshConfigDataFromService(string clientGuid, string machineGuid, EpbDispatcherLib.EPBService.NodeData[] objRet, string userInfoDataFile)
        {
            if (objSettings.VerboseLoggingOn)
            {
                ELogger.LogMethodStart();
                ELogger.Log("Getting data from web service.");
                ELogger.LogFormat("clientGuid: {0} machineGuid: {1}", clientGuid, machineGuid);
            }

            try
            {
                EpbDispatcherLib.EPBService.NodeData[] objRes = null;

                try
                {
                    objRes = epbWs.GetUserInfo(clientGuid, machineGuid);
                }
                catch (Exception ex)
                {
                    ELogger.Log("Unable to call epbWs.GetUserInfo(clientGuid, machineGuid).\r\n" + ex.Message);
                    return null;
                }
                if (objRes == null)
                {
                    if (objSettings.VerboseLoggingOn)
                    {
                        ELogger.Log("epbWs.GetUserInfo returned null");
                    }
                }

                if (objRes != null && objRes.Count() > 0)
                {
                    if (objSettings.VerboseLoggingOn)
                    {
                        ELogger.Log("Serializing new UserInfo XML to file system.");
                        ELogger.Log("objRes.Count: " + objRes.Count().ToString());
                        ELogger.Log("nodeuserdata.Count: " + objRes[0].nodeuserdata.Count());
                    }

                    objRet = objRes;

                    System.Xml.Serialization.XmlSerializer x = new System.Xml.Serialization.XmlSerializer(objRes.GetType());

                    if (objRes != null && objRes.Count() > 0 && objRes[0].nodeuserdata != null)
                    {
                        if (objRes[0].nodeuserdata.Count() > 0)
                        {
                            using (System.IO.StreamWriter file = new System.IO.StreamWriter(userInfoDataFile))
                            {
                                x.Serialize(file, objRes);
                                file.Close();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ELogger.Log(ex.Message);
            }

            if (objSettings.VerboseLoggingOn)
            {
                ELogger.LogMethodEnd();
            }

            return objRet;
        }

        public string ServiceTest(string inTest)
        {
            return "Received at " + DateTime.Now + " -:- " + inTest;
        }
        /// <summary>
        /// Send event data to server.
        /// </summary>
        /// <param name="eventType">EventType enum<see cref="EpbSharedLib.Enums.EventType"/></param>
        /// <param name="events">Event data as CSV string <see cref="EpbSharedLib.SharedClasses.ExtensionMethods"/></param>
        /// <returns>true for success, false for failure</returns>
        public bool EventProcess(EpbSharedLib.Enums.EventType eventType, string eventData)
        {
            bool retVal = false;

            if (objSettings.InLocalTestMode)
            {
                ELogger.Log("Currently in local test mode. Not sending event to server.");
                return true;
            }

            try
            {
                if (objSettings.VerboseLoggingOn)
                {
                    ELogger.Log(string.Format("[Adding Event] Type: {0}, Data: {1} ", eventType, eventData));
                }

                epbWs.EventProcess((int)eventType, eventData);
                retVal = true;
            }
            catch (Exception ex)
            {
                ELogger.Log(ex.Message);
            }

            return retVal; 

        }

        /// <summary>
        /// Get Dispatcher directory path.
        /// </summary>
        /// <returns>Directory path as a string.</returns>
        public string GetDispatcherPath()
        {
            return Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
        }


    }
}