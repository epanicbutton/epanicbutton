﻿using System;
using System.Linq;

namespace EpbSharedLib.Enums
{
    public enum EventType
    {
        AlertButtonPressed = 1,
        ConfirmButtonPressed = 2
    }
}
