﻿using System;
using System.Linq;

namespace EpbSharedLib.Enums
{
    public class SharedCommands
    {
        public enum TrayIcon
        {
            About,
            CloseWindow,
            ModifyProfile,
            ResetIcon,
            ShowMainWindow,
            Exit,
            Update
        }

        public enum PanicButtons
        {
            None,
            Loaded,
            Unloaded,
            Closing,
            Clicked,
            MouseEnter,
            MouseLeave
        }

        public enum UserInfo
        {
            Update,
            Cancel
        }

        public enum Grid
        {
            ViewEditRow 
        }

        public enum AlertView
        {
            UpdateMemo,
            ConfirmReceipt
        }
    }
}