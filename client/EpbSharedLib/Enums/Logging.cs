﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EpbSharedLib.Enums
{
    public enum Logging
    {
        ExceptionsOnly,
        Verbose
    }
}
