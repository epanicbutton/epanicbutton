﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EpbSharedLib.Enums
{
    public enum MessageSource
    {
        RaiseAlertMessage,
        DataGrid,
        None
    }
}
