﻿using System;
using System.Linq;

namespace EpbSharedLib.Enums
{ 
    /// <summary>
    /// Alert Severity Enum
    /// </summary>
    public enum Severity
    {
        High,
        Medium,
        Low 
    }
}