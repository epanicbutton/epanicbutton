﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using EpbSharedLib.Interfaces;
using log4net;

namespace EpbSharedLib.Logging
{
    public class FileLogger : ILogger
    {
        private readonly ILog logger; 

        public FileLogger()
        {  
            // Initialize log4net.
            log4net.Config.XmlConfigurator.
            ConfigureAndWatch(new FileInfo(
            Path.GetDirectoryName(
                      Assembly.GetAssembly(typeof(FileLogger)).Location)
                      + @"\" + "log4net.xml"));

            //log4net.Config.XmlConfigurator.ConfigureAndWatch(new FileInfo("c:\\e\\log4net.xml"));

            logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
 
        }
        public bool Add(string msg, string msgDetail)
        {
            this.logger.Info(msg + " : " + msgDetail);
            return false;
        }

        public bool Add(string msgSource, string msg, string msgDetail)
        {
            // TODO: Implement this method
            return false;
        }
        public bool Add(string msgSource, Exception ex)
        {
            // TODO: Implement this method
            this.logger.Error(msgSource  ,ex);
            return false;
        }
    }
}