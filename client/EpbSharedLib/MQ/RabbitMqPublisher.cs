﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EasyNetQ;
using EpbSharedLib.SharedClasses;
using Microsoft.Build.Framework;
using RabbitMQ.Client;

namespace EpbSharedLib.MQ
{
    public class RabbitMqPublisher : MQPublisher 
    {
        public bool Enabled;
        private readonly IRabbitConnection connection;

        public RabbitMqPublisher(IRabbitConnection connection, IDispatcherProxy objProxy):base(objProxy)
        {
            this.connection = connection;
            if (!this.connection.MessageBus.IsConnected)
            {
                return;
            }
            this.CreateExchangesAndQueues(); 
        }

        public string RoutingKey { get; set; }

        public override bool IsConnected { get { return this.connection.MessageBus.IsConnected; } }

        public override void Send(string message, string keyValue)
        {
            var properties = new MessageProperties(); 

            var body = Encoding.UTF8.GetBytes(message);
            string publisherRoutingKey = string.Format(this.RoutingKey, keyValue.ToString());
         
            try
            {
               this.connection.MessageBus.Publish(this.connection.MachineExchange, publisherRoutingKey, false, false, properties, body); 
            }
            catch (Exception ex)
            {
                ELogger.Log(ex);
            }
        }

        //public void CreateExchangesAndQueues()
        //{
        //    this.MqConfig = this.ObjProxy.GetQueueConfiguration();
        //    //-- Create response/request topic exchange
        //    this.ResponseExchange = this.MessageBus.ExchangeDeclare(this.MqConfig.ResponseExchangeName, ExchangeType.Topic);
        //    this.RequestExchange = this.MessageBus.ExchangeDeclare(this.MqConfig.RequestExchangeName, ExchangeType.Fanout);

        //    //-- Create machine exchange
        //    this.MachineExchange = this.MessageBus.ExchangeDeclare(this.MqConfig.PrivateExchangeName,
        //        ExchangeType.Fanout, false, true, false, false);

        //    //-- Bind machine exchange to main topic exchange
        //    this.MessageBus.Bind(this.MachineExchange, this.RequestExchange, "#");
        //    this.MessageBus.Bind(this.RequestExchange, this.ResponseExchange, "#");

        //    this.MachineQueue = this.MessageBus.QueueDeclare(
        //        this.MqConfig.PrivateQueueName, passive: false, durable: false,
        //        exclusive: false, autoDelete: true, perQueueTtl: 5000);

        //    this.MachineConfirmQueue = this.MessageBus.QueueDeclare(
        //        this.MqConfig.PrivateConfirmQueueName, passive: false, durable: false,
        //        exclusive: false, autoDelete: true);//, perQueueTtl: 20000);//, perQueueTtl: 200000, expires: 200000);   

        //    this.MachineUpdateQueue = this.MessageBus.QueueDeclare(
        //        this.MqConfig.PrivateUpdateQueueName, passive: false, durable: false,
        //        exclusive: false, autoDelete: true);
        //}

        public override void CreateTopic(string topic)
        {
            //-- Create response/request topic exchange
            this.connection.MessageBus.ExchangeDeclare(topic, ExchangeType.Topic);
        }

        public override void CreateQueue(string queue, int ttl=5000)
        {
          this.connection.MessageBus.QueueDeclare(
                queue, passive: false, durable: false,
                exclusive: false, autoDelete: true, perQueueTtl: ttl);
        }

        public override void Dispose()
        { 
            try
            {
               
            }
            catch (Exception ex)
            {
                ELogger.Log(ex);
            }
        } 
    }
}