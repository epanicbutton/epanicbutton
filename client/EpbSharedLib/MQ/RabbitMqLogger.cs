﻿using System;
using System.Linq;
using EasyNetQ;
using EpbSharedLib.SharedClasses;

namespace EpbSharedLib.MQ
{
    public class RabbitMqLogger : IEasyNetQLogger
    {
        public void DebugWrite(string format, params object[] args)
        {
            ELogger.Log(string.Format(format, args));
        }

        public void InfoWrite(string format, params object[] args)
        {
            ELogger.Log(string.Format(format, args));
        }

        public void ErrorWrite(string format, params object[] args)
        {
            ELogger.Log(string.Format(format, args));
        }

        public void ErrorWrite(Exception exception)
        {
            ELogger.Log(exception);
        }
    }
}