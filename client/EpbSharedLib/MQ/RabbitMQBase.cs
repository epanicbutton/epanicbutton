﻿using System;
using System.Linq;
using EasyNetQ.Topology;
using EpbSharedLib.Entities;
using EasyNetQ;
using EpbSharedLib.SharedClasses;

namespace EpbSharedLib.MQ
{
    public abstract class RabbitMQBase : IDisposable
    {
        protected readonly EpbSharedLib.SharedClasses.DispatcherProxy objProxy = new EpbSharedLib.SharedClasses.DispatcherProxy();
        protected MqConfig mqConfig;
        protected IAdvancedBus messageBus;
        protected IExchange responseExchange;
        protected IExchange requestExchange;
        protected IExchange machineExchange;
        protected IQueue machineQueue;
        public bool Enabled; 

        public RabbitMQBase()
        {
            this.mqConfig = this.objProxy.GetQueueConfiguration();
            Initialize();
        }

        private void Initialize()
        {
            messageBus = this.GetQueueConnection(mqConfig);

            //-- Create response/request topic exchange
            responseExchange = messageBus.ExchangeDeclare(mqConfig.ResponseExchangeName, ExchangeType.Topic);
            requestExchange = messageBus.ExchangeDeclare(mqConfig.RequestExchangeName, ExchangeType.Fanout);

            //-- Create machine exchange
            machineExchange = messageBus.ExchangeDeclare(mqConfig.PrivateExchangeName,
                ExchangeType.Fanout, false,  true, false, false);

            //-- Bind machine exchange to main topic exchange
            messageBus.Bind(machineExchange, requestExchange, "#");
            messageBus.Bind(requestExchange, responseExchange, "#");

            //-- Create machine queue   
            CreateMachineQueue(); 
        }
  
        private void CreateMachineQueue()
        {  
            //-- Create machine queue   
            machineQueue = messageBus.QueueDeclare(
                mqConfig.PrivateQueueName, passive: false, durable: false,
                exclusive: false, autoDelete: true);//, perQueueTtl: 20000);//, perQueueTtl: 200000, expires: 200000);  
        }

        protected void ResetMachineQueue()
        {
            if (machineQueue != null)
            {
                messageBus.QueueDelete(machineQueue); 
                ELogger.Log("Resetting rec queue..." + machineQueue.GetHashCode());
            } 
        }

        void IDisposable.Dispose()
        {
            if (messageBus != null)
            {
                messageBus.Dispose();
            }
        }
         
        protected IAdvancedBus ConnectionFactory
        {
            get
            {
                if (messageBus == null)
                {
                    messageBus = GetQueueConnection(mqConfig);
                }
                return messageBus;
            }
        }

        private IAdvancedBus GetQueueConnection(MqConfig mqConfig)
        {
            IAdvancedBus bus = RabbitHutch.CreateBus(
                string.Format("host={0};virtualHost={1};username={2};password={3}",
                       mqConfig.HostName, mqConfig.VirtualHost, mqConfig.UserName, mqConfig.Password)).Advanced;

            return bus;
        }

    }
}