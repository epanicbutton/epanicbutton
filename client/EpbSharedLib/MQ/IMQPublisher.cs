using Newtonsoft.Json;
using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using EpbSharedLib.SharedClasses;
using EpbSharedLib.Entities;
using System.Text.RegularExpressions;

namespace EpbSharedLib.MQ
{
    public interface IMQPublisher : IDisposable
    {
        string RoutingKey { get; set; } 
        void Send(string message, string keyValue);
        void CreateTopic(string topic);
        void CreateQueue(string queue, int ttl);
        void CreateQueues(string[] queues, int ttl = 5000);
        bool IsConnected { get; }
        void CreateExchangesAndQueues();
    }
    public abstract class MQPublisher : IMQPublisher{
        public string RoutingKey { get; set; }
        public abstract void Send(string message, string keyValue);
        public abstract void CreateTopic(string topic);
        public abstract void CreateQueue(string queue, int ttl = 5000);
        public abstract bool IsConnected { get; }
        public abstract void Dispose();
        protected IDispatcherProxy objProxy;
        protected ISettings objSettings = null;

        public void CreateExchangesAndQueues()
        {
            CreateTopic(this.objSettings.ClientGuid);
        }

        public void CreateQueues(string[] queues, int ttl)
        {
            foreach (var queue in queues)
                CreateQueue(queue, ttl);
        }

        public MQPublisher(IDispatcherProxy objProxy)
        {
            this.objProxy = objProxy;
        }
    }

    public class MultiMqPublish : MQPublisher
    {
        private readonly IMQPublisher[] publishers = null;

        public MultiMqPublish(IDispatcherProxy objProxy, params IMQPublisher[] publishers):base(objProxy)
        {
            this.publishers = publishers;
        }
        
        public string RoutingKey { get; set; }

        public override void Send(string message, string keyValue)
        {
            if (publishers.Count() == 0) throw new Exception("No publishers defined to multipublish");
            foreach (var pub in this.publishers)
                pub.Send(message, keyValue);
        }

        public override void CreateTopic(string topic)
        {
            foreach (var pub in this.publishers)
                pub.CreateTopic(topic);
        }
        
        public override void CreateQueue(string queue, int ttl)
        {
            foreach (var pub in this.publishers)
                pub.CreateQueue(queue, ttl);
        }
        public override bool IsConnected { get {
            return !this.publishers.Any(x => !x.IsConnected);
        } }

        public override void Dispose()
        {
            foreach (var pub in publishers)
            {
                pub.Dispose();
            }
        }
    }
}