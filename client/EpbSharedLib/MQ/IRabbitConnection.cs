using System;
using EasyNetQ;
using EasyNetQ.Topology;
using EpbSharedLib.Entities;
using EpbSharedLib.SharedClasses;

namespace EpbSharedLib.MQ
{
    public interface IRabbitConnection
    {
        IExchange ResponseExchange { get; }

        IExchange RequestExchange { get; }

        IExchange MachineExchange { get; }

        IQueue MachineQueue { get; }

        IQueue MachineConfirmQueue { get; }

        IQueue MachineUpdateQueue { get; }

        IDispatcherProxy ObjProxy { get; }

        IAdvancedBus MessageBus { get; }

        IAdvancedBus GetQueueConnection(MqConfig mqConfig, ISettings objSettings, AdvancedBusEventHandlers connectedHandler);

        MqConfig MqConfig { get;  }

        void ResetMachineQueue();

        void Dispose();
    }
}