﻿using System;
using System.Linq;
using EasyNetQ;
using EasyNetQ.Topology;
using EpbSharedLib.Entities;
using EpbSharedLib.SharedClasses;
using Microsoft.ServiceBus;
using Microsoft.ServiceBus.Messaging;
using System.Threading.Tasks;

namespace EpbSharedLib.MQ
{
    public class RabbitConnection : IDisposable, IRabbitConnection
    {
        public MqConfig MqConfig { get; private set; }

        public RabbitConnection(IDispatcherProxy dispatcherProxy)
        {
            this.ObjProxy = dispatcherProxy;
        }

        public IExchange ResponseExchange { get; private set; }

        public IExchange RequestExchange { get; private set; }

        public IExchange MachineExchange { get; private set; }

        public IQueue MachineQueue { get; private set; }

        public IQueue MachineConfirmQueue { get; private set; }

        public IQueue MachineUpdateQueue { get; private set; }

        public IDispatcherProxy ObjProxy { get; private set; }

        public IAdvancedBus MessageBus { get; set; }

        public IAdvancedBus GetQueueConnection(MqConfig mqConfig, ISettings objSettings, AdvancedBusEventHandlers connectedHandler)
        {
            this.MqConfig = mqConfig;

            if (string.IsNullOrEmpty(this.MqConfig.HostName) || string.IsNullOrEmpty(this.MqConfig.VirtualHost))
            {
                ELogger.Log("MQ settings not found.");
                return null;
            }

            if (this.MessageBus == null || !this.MessageBus.IsConnected)
            {
                string decoded = EpbSharedLib.Utilities.Helpers.Base64Decode(this.MqConfig.Password);   

                ConnectionConfiguration connectionConfig = new ConnectionConfiguration
                {
                    PrefetchCount = 1,
                    Hosts = new[] { new HostConfiguration { Host = this.MqConfig.HostName } },
                    UserName = this.MqConfig.UserName,
                    Password = decoded,
                    VirtualHost = this.MqConfig.VirtualHost,
                    PersistentMessages = false,
                    RequestedHeartbeat = ushort.Parse(objSettings.HeartBeatInterval),
                    Timeout = 0,
                    Platform = objSettings.MachineGuid
                };

                ELogger.Log("**** Connecting to MQ server...");
                IAdvancedBus bus = RabbitHutch.CreateBus(connectionConfig, connectedHandler, new Action<IServiceRegister>(this.RegisterServices)).Advanced;
             
                this.MessageBus = bus;
            }
             
            return this.MessageBus;
        }
  
        public void ResetMachineQueue()
        {
            try
            {
                if (this.MachineQueue != null)
                {
                    this.MessageBus.QueuePurge(this.MachineQueue);
                    ELogger.Log(string.Format("Resetting rec queue...{0}", this.MachineQueue.GetHashCode()));
                }
            }
            catch
            {
            }
        }

       

        public void Dispose()
        {
            try
            {
                if (this.MessageBus != null)
                {
                    this.MessageBus.Dispose();
                }
            }
            catch (Exception ex)
            {
                ELogger.Log(ex);
            }
        }

        private void RegisterServices(IServiceRegister obj)
        {
        }
    }
}