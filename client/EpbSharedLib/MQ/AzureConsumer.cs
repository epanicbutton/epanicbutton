using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO.MemoryMappedFiles;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using EpbSharedLib.SharedClasses;
using Microsoft.ServiceBus;
using Microsoft.ServiceBus.Messaging;
using Ninject;
using EpbSharedLib.Entities;

namespace EpbSharedLib.MQ
{
    public class AzureConsumer : MqConsumer
    {
        private IList<string> keyList;
        //private IDisposable consumer = null;
        private string[] bindingKeyTemplates = null;

        private string TopicName;
        private string ConnectionString;
        private readonly NamespaceManager namespaceManager = null;
        private Dictionary<string, SubscriptionClient> subscriptionClients = new Dictionary<string, SubscriptionClient>();

        private static AzureConsumer theConsumer = null;
        private static object locker = new object();

        public static IMQConsumer Create(string connectString)
        {
            lock (locker)
            {
                if (theConsumer == null)
                    theConsumer = new AzureConsumer(connectString);



                return theConsumer;
            }
        }

        public override void Start(string[] keys, string[] values)
        {
            try
            {
                for (var i = 0; i < keys.Length; i++)
                {
                    Subscribe(keys[i], values[i]);
                }
            }
            catch (Exception ex)
            {
                ELogger.LogFormat("Error starting subscription: {0} : {1}", ex.Message, ex.StackTrace);
                throw;
            }
        }

        private void Subscribe(string key, string sub)
        {
            var clientGuid = this.objSettings.ClientGuid;

            CreateSubscription(sub, clientGuid);

            if (subscriptionClients.ContainsKey(key) == false)
            {

                this.subscriptionClients.Add(key,
                                             SubscriptionClient.CreateFromConnectionString(this.ConnectionString,
                                                                                           clientGuid,
                                                                                           sub, ReceiveMode.PeekLock));
                this.subscriptionClients[key].OnMessage(ReceiveAction);
            }
        }

        private void CreateSubscription(string sub, string clientGuid)
        {
            if (!this.namespaceManager.SubscriptionExists(clientGuid, sub))
            {
                SubscriptionDescription sd = new SubscriptionDescription(clientGuid, sub);
                sd.DefaultMessageTimeToLive = new TimeSpan(12, 0, 0);
                this.namespaceManager.CreateSubscription(sd);
            }
        }

        private Action<BrokeredMessage> ReceiveAction
        {
            get
            {
                Action<BrokeredMessage> receiveAction = message =>
                    {
                        if (message.Label != null &&
                            message.ContentType != null &&
                            message.ContentType.Equals("application/json", StringComparison.InvariantCultureIgnoreCase))
                        {
                            var body = message.GetBody<string>();

                            byte[] contents = Encoding.UTF8.GetBytes(body);

                            DoConsume(contents);

                            message.Complete();
                        }
                        else
                        {
                            message.DeadLetter("ProcessingError", "Don't know what to do with this message");
                        }
                    };
                return receiveAction;
            }
        }

        public override bool IsConnected { get { return true; } }

        public override IDisposable StartConsuming(IList<string> bindingKeys, List<string> bindingKeyTemplates,
                                                   Action<object> onReceiveAction)
        {
            return this;
        }

        //    try
        //    {
        //        this.keyList = bindingKeys;

        //        var clientGuid = this.objSettings.ClientGuid;
        //        var machineGuid = this.objSettings.MachineGuid;
        //        var alertSub = ".alert";
        //        var confirmSub = machineGuid + ".confirm";
        //        var updateSub = machineGuid + ".update";

        //        if (!bindingKeyTemplates.Contains("alert.*.{0}"))
        //            return this;

        //     //   if (bindingKeyTemplates.Contains("alert.*.{0}"))
        //        {
        //            if (this.namespaceManager.SubscriptionExists(clientGuid, alertSub))
        //            {
        //                this.namespaceManager.DeleteSubscription(clientGuid, alertSub);
        //            }

        //            string filterBase = "keyValue = '";
        //            string filterOr = " OR ";
        //            string filterString = "";
        //            filterString += filterBase;
        //            for (int i = 0; i < this.keyList.Count; i++)
        //            {
        //                if (i != 0)
        //                    filterString = filterString + filterOr + filterBase;
        //                filterString = filterString + keyList[i] + "'";
        //            }
        //            SqlFilter filter = new SqlFilter(filterString);

        //            SubscriptionDescription sd = new SubscriptionDescription(clientGuid, alertSub);
        //            sd.DefaultMessageTimeToLive = new TimeSpan(12, 0, 0);
        //            this.namespaceManager.CreateSubscription(sd);
        //            if (subscriptionClients.ContainsKey("Alert") == false)
        //            {

        //                this.subscriptionClients.Add("Alert", SubscriptionClient.CreateFromConnectionString(this.ConnectionString, clientGuid, alertSub, ReceiveMode.PeekLock));
        //                this.subscriptionClients["Alert"].OnMessage(
        //                    message =>
        //                    {
        //                        if (message.Label != null &&
        //                            message.ContentType != null &&
        //                            message.ContentType.Equals("application/json", StringComparison.InvariantCultureIgnoreCase))
        //                        {
        //                            var body = message.GetBody<string>();
        //                         //   Test("Alert");
        //                            byte[] contents = Encoding.UTF8.GetBytes(body);
        //                            DoConsume<Alert>(contents, onReceiveAction);

        //                            message.Complete();

        //                        }
        //                        else
        //                        {
        //                            message.DeadLetter("ProcessingError", "Don't know what to do with this message");
        //                        }
        //                    }
        //                    );

        //            }
        //        }
        //        // else if (bindingKeyTemplates.Contains("confirm." + machineGuid + "."))
        //        {
        //            if (this.namespaceManager.SubscriptionExists(clientGuid, confirmSub))
        //            {
        //                this.namespaceManager.DeleteSubscription(clientGuid, confirmSub);
        //            }

        //            SubscriptionDescription sd = new SubscriptionDescription(clientGuid, confirmSub);
        //            sd.DefaultMessageTimeToLive = new TimeSpan(12, 0, 0);
        //            string strippedGuid = this.objSettings.MachineGuid.Replace("-", "");
        //            string filterString = "keyValue = '" + strippedGuid + "'";
        //            //SqlFilter filter = new SqlFilter(filterString);
        //            //this.namespaceManager.CreateSubscription(sd, filter);
        //            this.namespaceManager.CreateSubscription(sd);

        //            if (subscriptionClients.ContainsKey("Confirm") == false)
        //            {
        //                this.subscriptionClients.Add("Confirm", SubscriptionClient.CreateFromConnectionString(this.ConnectionString, clientGuid, confirmSub, ReceiveMode.PeekLock));
        //                this.subscriptionClients["Confirm"].OnMessage(
        //                    message =>
        //                        {

        //                        if (message.Label != null &&
        //                            message.ContentType != null &&
        //                            message.ContentType.Equals("application/json", StringComparison.InvariantCultureIgnoreCase))
        //                        {
        //                            var body = message.GetBody<string>();

        //                            byte[] contents = Encoding.UTF8.GetBytes(body);
        //                            if (message.Label == strippedGuid)
        //                            {
        //                                DoConsume<AlertConfirmation>(contents, onReceiveAction);
        //                                message.Complete();
        //                            }
        //                        }
        //                        else
        //                        {
        //                            message.DeadLetter("ProcessingError", "Don't know what to do with this message");
        //                        }
        //                    }
        //                    );
        //            }
        //        }
        //        //else if (bindingKeys.Contains("ConfigUpdate"))
        //        {
        //            if (!this.namespaceManager.SubscriptionExists(clientGuid, updateSub))
        //            {
        //                SubscriptionDescription sd = new SubscriptionDescription(clientGuid, updateSub);
        //                sd.DefaultMessageTimeToLive = new TimeSpan(12, 0, 0);
        //                this.namespaceManager.CreateSubscription(sd);
        //            }
        //            if (subscriptionClients.ContainsKey("Update") == false)
        //            {
        //                this.subscriptionClients.Add("Update", SubscriptionClient.CreateFromConnectionString(this.ConnectionString, clientGuid, updateSub, ReceiveMode.PeekLock));
        //                this.subscriptionClients["Update"].OnMessage(
        //                    message =>
        //                    {
        //                        // EVERY SUBSCRIPTION INVOKES THIS LAMDA EXPRESSION.....
        //                        if (message.Label != null &&
        //                            message.ContentType != null &&
        //                            message.ContentType.Equals("application/json", StringComparison.InvariantCultureIgnoreCase))
        //                        {
        //                            var body = message.GetBody<string>();

        //                            byte[] contents = Encoding.UTF8.GetBytes(body);
        //                            // SO THIS onReceiveAction WILL ALWAYS BE FOR UPDATE.

        //                                 DoConsume<ConfigUpdate>(contents, onReceiveAction);

        //                             message.Complete();

        //                         }
        //                         else
        //                         {
        //                             message.DeadLetter("ProcessingError", "Don't know what to do with this message");
        //                         }
        //                     }
        //                     );
        //             }
        //         }
        //    }
        //    catch (Exception ex)
        //    {
        //        ELogger.Log(ex);
        //    }

        //    return this;
        //}

        public override void Empty()
        {
            //if (!this.namespaceManager.SubscriptionExists(this.objSettings.ClientGuid, this.objSettings.MachineGuid))
            //{
            //    SubscriptionDescription sd = new SubscriptionDescription(this.objSettings.ClientGuid, this.objSettings.MachineGuid);
            //    sd.DefaultMessageTimeToLive = new TimeSpan(12, 0, 0);
            //    this.namespaceManager.CreateSubscription(sd);
            //}

            //this.subscriptionClients.Add("Empty", SubscriptionClient.CreateFromConnectionString(this.ConnectionString, this.objSettings.ClientGuid, this.objSettings.MachineGuid, ReceiveMode.PeekLock));
            //while (subscriptionClients["Empty"].Peek() != null)
            //{
            //    var message = subscriptionClients["Empty"].Receive();
            //    message.Complete();
            //}
        }
        public class AzureSetting : ISettings
        {
            public string ApplicationVersion { get; private set; }

            public bool InLocalTestMode { get; private set; }

            public string ClientGuid { get; set; }

            public string MachineGuid { get; set; }

            public string SubscriptionUpdateInterval { get; set; }

            public string HeartBeatInterval { get; set; }

            /// <summary>
            /// ExceptionsOnly / Verbose
            /// </summary>
            public string LoggingLevel { get; set; }

            public AzureSetting()
            {
                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["LOCAL_TEST_MODE"]))
                {
                    string testMode = ConfigurationManager.AppSettings["LOCAL_TEST_MODE"].Trim();
                    this.InLocalTestMode = testMode == "1";
                }
                else
                {
                    this.InLocalTestMode = false;
                }

                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["CLIENT_GUID"]))
                {
                    this.ClientGuid = ConfigurationManager.AppSettings["CLIENT_GUID"].Trim();
                }

                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["MACHINE_GUID"]))
                {
                    this.MachineGuid = ConfigurationManager.AppSettings["MACHINE_GUID"].Trim();
                }

                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["APP_VERSION"]))
                {
                    this.ApplicationVersion = ConfigurationManager.AppSettings["APP_VERSION"].Trim();
                }

                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["SUB_UPDATE_INTERVAL"]))
                {
                    this.SubscriptionUpdateInterval = ConfigurationManager.AppSettings["SUB_UPDATE_INTERVAL"].Trim();
                }

                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["LOGGING_LEVEL"]))
                {
                    this.LoggingLevel = ConfigurationManager.AppSettings["LOGGING_LEVEL"].Trim();
                }
                else
                {
                    this.LoggingLevel = Enums.Logging.Verbose.ToString();
                }

                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["HEARTBEAT_INTERVAL"]))
                {
                    this.HeartBeatInterval = ConfigurationManager.AppSettings["HEARTBEAT_INTERVAL"].Trim();
                }
            }

            public bool VerboseLoggingOn
            {
                get
                {
                    return (LoggingLevel == EpbSharedLib.Enums.Logging.Verbose.ToString());
                }
            }
        }

        [Inject]
        public AzureConsumer(string connectionString)
        {
            this.ConnectionString = connectionString;
            this.namespaceManager = NamespaceManager.CreateFromConnectionString(this.ConnectionString);
            this.objSettings = new AzureSetting();
        }

        public override void Dispose()
        {

        }

        public string Test(String s)
        {
            return s;
        }



        //[Obsolete]
        //public override IDisposable StartConsuming<T>(IList<string> bindingKeys, string[] bindingKeyTemplates, System.Action<T> onReceiveAction)
        //{

        //    try
        //    {
        //        this.keyList = bindingKeys;

        //        var clientGuid = this.objSettings.ClientGuid;
        //        var machineGuid = this.objSettings.MachineGuid;
        //        var alertSub = machineGuid + ".alert";
        //        var confirmSub = machineGuid + ".confirm";
        //        var updateSub = machineGuid + ".update";

        //        if (typeof(T).Name.Equals("Alert"))
        //        {
        //            alertAction = onReceiveAction as Action<Alert>;
        //            if (this.namespaceManager.SubscriptionExists(clientGuid, alertSub))
        //            {
        //                this.namespaceManager.DeleteSubscription(clientGuid, alertSub);
        //            }

        //            string filterBase = "keyValue = '";
        //            string filterOr = " OR ";
        //            string filterString = "";
        //            filterString += filterBase;
        //            for (int i = 0; i < this.keyList.Count; i++)
        //            {
        //                if (i != 0)
        //                    filterString = filterString + filterOr + filterBase;
        //                filterString = filterString + keyList[i] + "'";
        //            }
        //            SqlFilter filter = new SqlFilter(filterString);

        //            SubscriptionDescription sd = new SubscriptionDescription(clientGuid, alertSub);
        //            sd.DefaultMessageTimeToLive = new TimeSpan(12, 0, 0);
        //            this.namespaceManager.CreateSubscription(sd, filter);
        //            if (subscriptionClients.ContainsKey("Alert") == false)
        //            {

        //                //Action.SetAction("Alert", onReceiveAction as Action<BrokeredMessage>);
        //                this.subscriptionClients.Add("Alert", SubscriptionClient.CreateFromConnectionString(this.ConnectionString, clientGuid, alertSub, ReceiveMode.PeekLock));
        //                this.subscriptionClients["Alert"].OnMessage(
        //                    message =>
        //                        {
        //                            if (message.Label != null &&
        //                                message.ContentType != null &&
        //                                message.ContentType.Equals("application/json", StringComparison.InvariantCultureIgnoreCase))
        //                            {
        //                                var body = message.GetBody<string>();
        //                                Test("Alert");                
        //                                byte[] contents = Encoding.UTF8.GetBytes(body);
        //                                DoConsume<T>(contents, onReceiveAction);

        //                                message.Complete();

        //                            }
        //                            else
        //                            {
        //                                message.DeadLetter("ProcessingError", "Don't know what to do with this message");
        //                            }
        //                        }
        //                    );

        //            }
        //        }
        //        else if (typeof(T).Name.Equals("AlertConfirmation"))
        //        {
        //            if (this.namespaceManager.SubscriptionExists(clientGuid, confirmSub))
        //            {
        //                this.namespaceManager.DeleteSubscription(clientGuid, confirmSub);
        //            }

        //            SubscriptionDescription sd = new SubscriptionDescription(clientGuid, confirmSub);
        //            sd.DefaultMessageTimeToLive = new TimeSpan(12, 0, 0);
        //            string strippedGuid = this.objSettings.MachineGuid.Replace("-", "");
        //            string filterString = "keyValue = '" + strippedGuid + "'";
        //            //SqlFilter filter = new SqlFilter(filterString);
        //            //this.namespaceManager.CreateSubscription(sd, filter);
        //            this.namespaceManager.CreateSubscription(sd);

        //            if (subscriptionClients.ContainsKey("Confirm") == false)
        //            {
        //                this.subscriptionClients.Add("Confirm", SubscriptionClient.CreateFromConnectionString(this.ConnectionString, clientGuid, confirmSub, ReceiveMode.PeekLock));
        //                this.subscriptionClients["Confirm"].OnMessage(
        //                    message =>
        //                        {
        //                            if (message.Label != null &&
        //                                message.ContentType != null &&
        //                                message.ContentType.Equals("application/json", StringComparison.InvariantCultureIgnoreCase))
        //                            {
        //                                var body = message.GetBody<string>();

        //                                byte[] contents = Encoding.UTF8.GetBytes(body);
        //                                if (message.Label == strippedGuid)
        //                                {
        //                                    DoConsume<T>(contents, onReceiveAction);
        //                                    message.Complete();
        //                                }
        //                            }
        //                            else
        //                            {
        //                                message.DeadLetter("ProcessingError", "Don't know what to do with this message");
        //                            }
        //                        }
        //                    );      
        //            }              
        //        }
        //        else if (typeof(T).Name.Equals("ConfigUpdate"))
        //        {
        //            if (!this.namespaceManager.SubscriptionExists(clientGuid, updateSub))
        //            {
        //                SubscriptionDescription sd = new SubscriptionDescription(clientGuid, updateSub);
        //                sd.DefaultMessageTimeToLive = new TimeSpan(12, 0, 0);
        //                this.namespaceManager.CreateSubscription(sd);
        //            }
        //            if (subscriptionClients.ContainsKey("Update") == false)
        //            {
        //                this.subscriptionClients.Add("Update", SubscriptionClient.CreateFromConnectionString(this.ConnectionString, clientGuid, updateSub, ReceiveMode.PeekLock));
        //                this.subscriptionClients["Update"].OnMessage(
        //                    message =>
        //                        {
        //                            // EVERY SUBSCRIPTION INVOKES THIS LAMDA EXPRESSION.....
        //                            if (message.Label != null &&
        //                                message.ContentType != null &&
        //                                message.ContentType.Equals("application/json", StringComparison.InvariantCultureIgnoreCase))
        //                            {
        //                                var body = message.GetBody<string>();
        //                                byte[] contents = Encoding.UTF8.GetBytes(body);
        //                                // SO THIS onReceiveAction WILL ALWAYS BE FOR UPDATE.
        //                                if (body.Contains("<Alert "))
        //                                    DoConsume(contents, alertAction);
        //                                else
        //                                    DoConsume<T>(contents, onReceiveAction);

        //                                message.Complete();

        //                            }
        //                            else
        //                            {
        //                                message.DeadLetter("ProcessingError", "Don't know what to do with this message");
        //                            }
        //                        }
        //                    ); 
        //            }                
        //        }               
        //    }
        //    catch (Exception ex)
        //    {
        //        ELogger.Log(ex);
        //    }

        //    return this;
        //}

    }
}