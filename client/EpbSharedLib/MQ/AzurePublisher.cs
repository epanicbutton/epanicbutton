using System;
using System.Configuration;
using EpbSharedLib.SharedClasses;
using Microsoft.ServiceBus;
using Microsoft.ServiceBus.Messaging;
using Ninject;

namespace EpbSharedLib.MQ
{
    public class AzurePublisher : MQPublisher
    {
        private string ConnectionString;
        private readonly NamespaceManager namespaceManager = null;

        [Inject]
        public AzurePublisher(string connectionString, IDispatcherProxy objProxy):base(objProxy)
        {
            this.ConnectionString = connectionString;
            this.namespaceManager = NamespaceManager.CreateFromConnectionString(this.ConnectionString);
            this.objSettings = new AzureSetting();
        }

        public class AzureSetting : ISettings
        {
            public string ApplicationVersion { get; private set; }

            public bool InLocalTestMode { get; private set; }

            public string ClientGuid { get; set; }

            public string MachineGuid { get; set; }

            public string SubscriptionUpdateInterval { get; set; }

            public string HeartBeatInterval { get; set; }

            /// <summary>
            /// ExceptionsOnly / Verbose
            /// </summary>
            public string LoggingLevel { get; set; }

            public AzureSetting()
            {
                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["LOCAL_TEST_MODE"]))
                {
                    string testMode = ConfigurationManager.AppSettings["LOCAL_TEST_MODE"].Trim();
                    this.InLocalTestMode = testMode == "1";
                }
                else
                {
                    this.InLocalTestMode = false;
                }

                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["CLIENT_GUID"]))
                {
                    this.ClientGuid = ConfigurationManager.AppSettings["CLIENT_GUID"].Trim();
                }

                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["MACHINE_GUID"]))
                {
                    this.MachineGuid = ConfigurationManager.AppSettings["MACHINE_GUID"].Trim();
                }

                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["APP_VERSION"]))
                {
                    this.ApplicationVersion = ConfigurationManager.AppSettings["APP_VERSION"].Trim();
                }

                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["SUB_UPDATE_INTERVAL"]))
                {
                    this.SubscriptionUpdateInterval = ConfigurationManager.AppSettings["SUB_UPDATE_INTERVAL"].Trim();
                }

                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["LOGGING_LEVEL"]))
                {
                    this.LoggingLevel = ConfigurationManager.AppSettings["LOGGING_LEVEL"].Trim();
                }
                else
                {
                    this.LoggingLevel = Enums.Logging.Verbose.ToString();
                }

                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["HEARTBEAT_INTERVAL"]))
                {
                    this.HeartBeatInterval = ConfigurationManager.AppSettings["HEARTBEAT_INTERVAL"].Trim();
                }
            }

            public bool VerboseLoggingOn
            {
                get
                {
                    return (LoggingLevel == EpbSharedLib.Enums.Logging.Verbose.ToString());
                }
            }
        }

        public override bool IsConnected { get { return true; } }

        public override void Send(string message, string keyValue)
        {
            var client = TopicClient.CreateFromConnectionString(this.ConnectionString, this.objSettings.ClientGuid); 
            var brokeredMessage = new BrokeredMessage(message) 
                {
                    ContentType = "application/json",
                    Label = keyValue,
                    ReplyTo = this.objSettings.MachineGuid,  
                    Properties = 
                        {
                            { "keyValue", keyValue }
                        }
                };            

            try
            {
                client.SendAsync(brokeredMessage);
            }
            catch (Exception ex)
            {
                ELogger.Log(ex);
            }
        }

        new public void CreateExchangesAndQueues()
        {
            CreateTopic(this.objSettings.ClientGuid);

            //var mqConfig = objProxy.GetQueueConfiguration();
            //CreateQueue(mqConfig.PrivateQueueName);
            //CreateQueue(mqConfig.PrivateConfirmQueueName);
            //CreateQueue(mqConfig.PrivateUpdateQueueName);

            //CreateTopic(mqConfig.ResponseExchangeName);
            //CreateTopic(mqConfig.RequestExchangeName);
            //CreateTopic(mqConfig.PrivateExchangeName);
        }

        public override void CreateTopic(string topicName)
        {
            TopicDescription td = new TopicDescription(topicName);
            td.MaxSizeInMegabytes = 5120;
            td.DefaultMessageTimeToLive = new TimeSpan(12, 0, 0);

            if (!this.namespaceManager.TopicExists(topicName))
            {
                this.namespaceManager.CreateTopic(td);
            }           
        }
       
        public override void CreateQueue(string queue, int ttl = 5000)
        {
            if (!this.namespaceManager.QueueExists(queue))
            {
                this.namespaceManager.CreateQueue(queue);
            }
        }

        public override void Dispose()
        {
            // client.Close();
        }
    }
}