﻿using System;
using System.Linq;
using EasyNetQ;
using EasyNetQ.Topology;
using EpbSharedLib.Entities;
using EpbSharedLib.SharedClasses;

namespace EpbSharedLib.MQ
{
    public class MqBase
    {

        public bool Enabled; 

        protected MqBase(IAdvancedBus messageBus, IDispatcherProxy dispatcherProxy)
        { 
            this.MessageBus = messageBus; 
        }

        public IAdvancedBus MessageBus { get; set; }


    }
}