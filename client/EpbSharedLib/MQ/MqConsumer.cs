﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows;
using System.Xml.Serialization;
using EpbSharedLib.Entities;
using EpbSharedLib.SharedClasses;
using Microsoft.ServiceBus.Messaging;

namespace EpbSharedLib.MQ
{
    public static class ActionDictionary
    {
        private static Dictionary<string, Action<BrokeredMessage>> actionDictionary;

        public static void SetAction(string actionName, Action<BrokeredMessage> action)
        {
            if (actionDictionary == null)
                actionDictionary = new Dictionary<string, Action<BrokeredMessage>>();

            actionDictionary.Add(actionName, action);
        }

        public static Action<BrokeredMessage> GetAction(string actionName)
        {
            return actionDictionary[actionName];
        }
    };



    public abstract class MqConsumer : IMQConsumer
    {
        protected readonly ConfigUpdate updateMessage = new ConfigUpdate();
        public abstract void Dispose();
        //public abstract IDisposable StartConsuming<T>(IList<string> bindingKeys, string[] bindingKeyTemplates, System.Action<T> onReceiveAction);
        public abstract IDisposable StartConsuming(IList<string> bindingKeys, List<string> bindingKeyTemplates, Action<object> onReceiveAction);
        protected ISettings objSettings = null;

        public static Action<Alert> alertAction = null;

        public event RaiseAlertHandler RaiseAlert;
        public event RaiseUpdateConfigHandler RaiseUpdateConfig;
        public event RaiseAlertConfirmationHandler RaiseAlertConfirmation;

        public void Stop()
        {
            try
            {
                this.Dispose();
            }
            catch (Exception ex)
            {
                ELogger.Log(ex);
            }
        }


        public abstract void Start(string[] keys, string[] values);

        public abstract bool IsConnected { get; }

        public abstract void Empty();

        private object Deserialize(Type t, string message)
        {
            var serializer = new XmlSerializer(t);
            using (TextReader reader = new StringReader(message))
            {
                return serializer.Deserialize(reader);
            }
        }

        protected void DoConsume(byte[] body)
        {

            var message = Encoding.UTF8.GetString(body);

            try
            {
                if (message.Length > 0)
                {

                    if (message.ToLower().Equals(UpdateActions.Update))
                    {
                        updateMessage.UpdateAction = UpdateActions.Update;
                        updateMessage.ActionArgs = "";
                        if (this.RaiseUpdateConfig != null)
                        {
                            RaiseUpdateConfig(updateMessage);
                        }
                    }
                    else if (message.ToLower().Contains(UpdateActions.GroupChange))
                    {
                        updateMessage.UpdateAction = UpdateActions.GroupChange;
                        IList<string> actionArgs = message.Split(',');
                        updateMessage.ActionArgs = actionArgs[1];
                        if (this.RaiseUpdateConfig != null)
                        {
                            RaiseUpdateConfig(updateMessage);
                        }
                    }
                    else if (message.ToLower().Equals(UpdateActions.UpdateReceiver))
                    {
                        Alert alert = new Alert();
                        alert.MessageGuid = message;
                        if (RaiseAlert != null)
                            RaiseAlert(alert);
                    }
                    else if (message.Contains("<AlertConfirmation "))
                    {
                        if (this.RaiseAlertConfirmation != null)
                        {
                            var msg = Deserialize(typeof(AlertConfirmation), message) as AlertConfirmation;
                            RaiseAlertConfirmation(msg);
                        }
                    }
                    else if (message.Contains("<Alert "))
                    {
                        if (this.RaiseAlert != null)
                        {
                            var msg = Deserialize(typeof(Alert), message) as Alert;
                            RaiseAlert(msg);
                        }
                    }
                }
                /*
                    if (message.ToLower().Equals(UpdateActions.UpdateReceiver))
                    {
                        Alert alert = new Alert();
                        alert.MessageGuid = message;
                        Action<Alert> thing = doConsumeAction as Action<Alert>;
                        if (thing != null)
                            Application.Current.Dispatcher.Invoke(thing, alert);
                    }

                    if (message.ToLower().Contains(UpdateActions.GroupChange))
                    {
                        updateMessage.UpdateAction = UpdateActions.GroupChange;
                        IList<string> actionArgs = message.Split(',');
                        updateMessage.ActionArgs = actionArgs[1];
                        Application.Current.Dispatcher.Invoke(doConsumeAction , updateMessage);
                    }

                    if (message.IndexOf("<AlertConfirmation ") >= 0)
                    {
                        var serializer = new XmlSerializer(typeof(AlertConfirmation));
                        AlertConfirmation msg;

                        using (TextReader reader = new StringReader(message))
                        {
                            msg = (AlertConfirmation)serializer.Deserialize(reader);
                        }

                        Application.Current.Dispatcher.Invoke(doConsumeAction, msg);
                        return;
                    }

                    if (message.IndexOf("<Alert ") >= 0)
                    {
                        var serializer = new XmlSerializer(typeof(Alert));
                        Alert msg;

                        using (TextReader reader = new StringReader(message))
                        {
                            msg = (Alert)serializer.Deserialize(reader);
                        }

                        if (msg.SenderMachineGuid == objSettings.MachineGuid)
                        {
                            return;
                        }

                        Application.Current.Dispatcher.Invoke(doConsumeAction, msg);    
                    }
                }*/
            }
            catch (Exception ex)
            {
            }

            return;
        }
    }
}