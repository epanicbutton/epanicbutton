using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Linq;
using EasyNetQ;
using System.IO;
using Newtonsoft.Json;
using EasyNetQ.Consumer;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Xml;
using EpbSharedLib.Entities;
namespace EpbSharedLib.MQ
{
    //public interface IMQConsumer : IDisposable
    //{
    //    void Stop(); 
    //    //[Obsolete]
    //    //IDisposable StartConsuming<T>(IList<string> bindingKeys, string[] bindingKeyTemplates, System.Action<T> onReceiveAction);

    //    IDisposable StartConsuming(IList<string> bindingKeys, List<string> bindingKeyTemplates, System.Action<object> onReceiveAction);
    //    void Empty();
    //    bool IsConnected { get; }
    //}

    public delegate void RaiseAlertHandler(Alert alert);
    public delegate void RaiseUpdateConfigHandler(ConfigUpdate configUpdate);
    public delegate void RaiseAlertConfirmationHandler(AlertConfirmation alertConfirmation);


    public interface IMQConsumer: IDisposable
    {
        event RaiseAlertHandler RaiseAlert;
        event RaiseUpdateConfigHandler RaiseUpdateConfig;
        event RaiseAlertConfirmationHandler RaiseAlertConfirmation;

        void Stop();
        void Start(string[] keys, string[] values);
        bool IsConnected { get; }
        void Empty();

    }
}
