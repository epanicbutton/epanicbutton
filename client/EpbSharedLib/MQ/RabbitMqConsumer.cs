﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EasyNetQ.Consumer;
using EpbSharedLib.SharedClasses;
using System.Threading; 

namespace EpbSharedLib.MQ
{
    public class RabbitMqConsumer : MqConsumer
    { 
        private IList<string> keyList; 
        private IDisposable consumer = null;
        private string[] bindingKeyTemplates = null;
       
        public RabbitMqConsumer(IRabbitConnection connection, ISettings settings) 
        { 
            this.RabbitConnection = connection;
            this.RabbitConnection.MessageBus.Disconnected += MessageBus_Disconnected;
            this.objSettings = settings;
        }

        
        public override IDisposable StartConsuming(IList<string> bindingKeys, List<string> bindingKeyTemplates, Action<object> onReceiveAction)
        {
            throw new NotImplementedException();
        }

        public override void Empty()
        {
            this.RabbitConnection.MessageBus.QueuePurge(this.RabbitConnection.MachineQueue);
            this.RabbitConnection.MessageBus.QueuePurge(this.RabbitConnection.MachineConfirmQueue);
            this.RabbitConnection.MessageBus.QueuePurge(this.RabbitConnection.MachineUpdateQueue);
        }

        public override void Start(string[] keys, string[] values)
        {
            throw new NotImplementedException();
        }

        public override bool IsConnected
        {

            get
            {
                if (this.RabbitConnection == null) return false;
                    return (!string.IsNullOrEmpty(this.RabbitConnection.MqConfig.VirtualHost));
               
            }
        }
        void MessageBus_Disconnected(object sender, EventArgs e)
        {
            this.Stop();
        }

        public IRabbitConnection RabbitConnection { get; private set; }

        public string[] BindingKeyTemplates
        {
            get
            {
                return this.bindingKeyTemplates;
            }
            set
            {
                this.bindingKeyTemplates = value;
            }
        }

        public override void Dispose()
        {
            RabbitConnection.Dispose();
            this.Stop(); 
        }



        //public override IDisposable StartConsuming<T>(IList<string> bindingKeys, string[] bindingKeyTemplates, System.Action<T> onReceiveAction)
        //{
        //    if (bindingKeys == null || onReceiveAction == null)
        //    { 
        //        return null; 
        //    }

        //    this.bindingKeyTemplates = bindingKeyTemplates;
        //    if (bindingKeyTemplates == null)
        //    {
        //        throw new Exception("Binding Keys are null.");
        //    }

        //    if (onReceiveAction == null)
        //    {
        //        throw new Exception("On receive Action delegate cannot be null.");
        //    }
             
        //    this.keyList = bindingKeys;

        //    try
        //    { 
        //        if (typeof(T).Name.Equals("Alert"))
        //        { 
        //            //-- Bind machine queue to response exchange
        //            foreach (string buttonId in this.keyList)
        //            {
        //                foreach (string bindingKey in this.bindingKeyTemplates)
        //                {
        //                    string formattedBindingKey = string.Format(bindingKey, buttonId);
        //                    this.RabbitConnection.MessageBus.Bind(this.RabbitConnection.ResponseExchange, this.RabbitConnection.MachineQueue, formattedBindingKey);
        //                }
        //            } 

        //            ////-- Start consuming   
        //            this.consumer = this.RabbitConnection.MessageBus.Consume(this.RabbitConnection.MachineQueue, (body, properties, info) => Task.Factory.StartNew(() =>
        //            {
        //                this.DoConsume<T>(body, onReceiveAction);
        //            }), new Action<IConsumerConfiguration>(this.ConsumerConfig));
        //        }

        //        if (typeof(T).Name.Equals("AlertConfirmation"))
        //        {
        //            //-- Bind machine queue to response exchange
        //            foreach (string buttonId in this.keyList)
        //            {
        //                foreach (string bindingKey in this.bindingKeyTemplates)
        //                {
        //                    string formattedBindingKey = string.Format(bindingKey, buttonId);
        //                    this.RabbitConnection.MessageBus.Bind(this.RabbitConnection.ResponseExchange, this.RabbitConnection.MachineConfirmQueue, formattedBindingKey);
        //                }
        //            } 

        //            ////-- Start consuming   
        //            this.consumer = this.RabbitConnection.MessageBus.Consume(this.RabbitConnection.MachineConfirmQueue, (body, properties, info) => Task.Factory.StartNew(() =>
        //            {
        //                this.DoConsume<T>(body, onReceiveAction);
        //            }), new Action<IConsumerConfiguration>(this.ConsumerConfig));
        //        }

        //        if (typeof(T).Name.Equals("ConfigUpdate"))
        //        {
        //            //-- Bind machine queue to response exchange
        //            foreach (string groupId in this.keyList)
        //            {
        //                foreach (string bindingKey in this.bindingKeyTemplates)
        //                {
        //                    string formattedBindingKey = string.Format(bindingKey, groupId);
        //                    ELogger.Log("Config binding: " + formattedBindingKey);
        //                    this.RabbitConnection.MessageBus.Bind(this.RabbitConnection.ResponseExchange, this.RabbitConnection.MachineUpdateQueue, formattedBindingKey);
        //                }
        //            }

        //            ////-- Start consuming   
        //            this.consumer = this.RabbitConnection.MessageBus.Consume(this.RabbitConnection.MachineUpdateQueue, (body, properties, info) => Task.Factory.StartNew(() =>
        //            {
        //                this.DoConsume<T>(body, onReceiveAction);
        //            }), new Action<IConsumerConfiguration>(this.ConsumerConfig));
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ELogger.Log(ex);
        //    }

        //    return this.consumer;
        //}

     

        private void ConsumerConfig(IConsumerConfiguration consumerConfig)
        {
            consumerConfig.WithPrefetchCount(1);
        } 
      
    }
}