﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace EpbSharedLib.Utilities
{
    public static class Serialization
    {
        /// <summary>
        /// Serialize and object to a string.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="toSerialize"></param>
        /// <returns>Object as a String</returns>
        public static string SerializeObject<T>(this T toSerialize)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(toSerialize.GetType());
            StringWriter textWriter = new StringWriter();

            xmlSerializer.Serialize(textWriter, toSerialize);
            return textWriter.ToString();
        }
        /// <summary>
        /// Deserialize string to object
        /// </summary>
        /// <typeparam name="TType"></typeparam>
        /// <param name="input"></param>
        /// <returns></returns>
        public static TType FromString<TType>(string input)
        {
            var byteArray = Encoding.ASCII.GetBytes(input);
            using (var stream = new MemoryStream(byteArray))
            {
                var bf = new BinaryFormatter();
                return (TType)bf.Deserialize(stream);
            }
        }
        /// <summary>
        /// Serialize and object to a string.
        /// </summary>
        /// <typeparam name="TType"></typeparam>
        /// <param name="data"></param>
        /// <returns>Object as a String</returns>
        public static string ToString<TType>(TType data)
        {
            using (var ms = new MemoryStream())
            {
                var bf = new BinaryFormatter();
                bf.Serialize(ms, data);
                return Encoding.ASCII.GetString(ms.GetBuffer());
            }
        }
    }
}
