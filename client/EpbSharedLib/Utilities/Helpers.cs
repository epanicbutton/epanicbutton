﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using EpbSharedLib.SharedClasses;

namespace EpbSharedLib.Utilities
{
    public static class Helpers
    {
        private static readonly Settings objSettings;
        private static readonly string dispPath;

        static Helpers()
        {
            try
            {
                var objProxy = new DispatcherProxy();
                dispPath = objProxy.GetDispatcherDirectoryPath();
            }
            catch (Exception ex)
            {
                dispPath = string.Empty;
                ELogger.Log(string.Format("Helpers(): {0}", ex.Message));
            }

            objSettings = new Settings(); 
        }

        /// <summary>
        /// Get the UserInfo file path.
        /// </summary>
        /// <returns>UserInfo file path as a string.</returns>
        public static string GetUserInfoFilePath()
        {
            string curPath = string.Format("{0}\\",dispPath);
            string userInfoDataFile = string.Format("{0}UserInfo.xml", curPath);

            return userInfoDataFile;
        }

        /// <summary>
        /// Get the UserInfo directory path.
        /// </summary>
        /// <returns>UserInfo directory path as a string.</returns>
        public static string GetUserInfoDirectory()
        { 
            return dispPath;
        }

        /// <summary>
        /// Check for running process by name.
        /// </summary>
        /// <param name="name">Name of process minus '.exe'</param>
        /// <returns>'true' if process is running, 'false' if it is not.</returns>
        public static bool IsProcessOpen(string name)
        {
            try
            {
                foreach (Process clsProcess in Process.GetProcesses())
                {
                    if (clsProcess.ProcessName.Contains(name))
                    {
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                ELogger.Log(string.Format("IsProcessOpen: {0}", ex.Message));
            }

            return false;
        }

        /// <summary>
        /// Check for running instance. If found, prevent 
        /// current instance from starting.
        /// </summary>
        /// <param name="processName">Name of process</param>
        public static bool ProcessIsRunning(string processName)
        {
            try
            { 
                // Get Reference to the current Process
                Process thisProc = Process.GetCurrentProcess();

                // Check how many total processes have the same name as the current one
                if (Process.GetProcessesByName(thisProc.ProcessName).Length > 1)
                {
                    if (objSettings.VerboseLoggingOn)
                    {
                        ELogger.Log("Running instance found. Exiting...");
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {
                ELogger.Log(string.Format("DoSingleInstanceCheck: {0}", ex.Message));
            }

            return false;
        }

        /// <summary>
        /// Get the MDG Hash of a file.
        /// </summary>
        /// <param name="filename">Full file path as string.</param>
        /// <returns>MD5 has as a string.</returns>
        public static string GetFileMD5Hash(string filename)
        {
            using (var md5 = new MD5CryptoServiceProvider())
            {
                var buffer = md5.ComputeHash(File.ReadAllBytes(filename));
                var sb = new StringBuilder();
                for (var i = 0; i < buffer.Length; i++)
                {
                    sb.Append(buffer[i].ToString("x2"));
                }

                if (objSettings.VerboseLoggingOn)
                {
                    ELogger.Log(string.Format("Filename: {0} -:- Hash: {1}", filename, sb.ToString()));
                }

                return sb.ToString();
            }
        }

        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        } 

        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }
    }
}