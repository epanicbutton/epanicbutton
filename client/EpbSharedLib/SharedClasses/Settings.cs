﻿using System;
using System.Configuration;
using System.Linq;

namespace EpbSharedLib.SharedClasses
{
    public class Settings : ISettings  
    {
        public Settings()
        {
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["LOCAL_TEST_MODE"]))
            {
                string testMode = ConfigurationManager.AppSettings["LOCAL_TEST_MODE"].Trim(); 
                this.InLocalTestMode = testMode == "1";
            }
            else
            {
                this.InLocalTestMode = false;
            }

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["CLIENT_GUID"]))
            {
                this.ClientGuid = ConfigurationManager.AppSettings["CLIENT_GUID"].Trim();
            }

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["MACHINE_GUID"]))
            {
                this.MachineGuid = ConfigurationManager.AppSettings["MACHINE_GUID"].Trim();
            }

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["APP_VERSION"]))
            {
                this.ApplicationVersion = ConfigurationManager.AppSettings["APP_VERSION"].Trim();
            }

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["SUB_UPDATE_INTERVAL"]))
            {
                this.SubscriptionUpdateInterval = ConfigurationManager.AppSettings["SUB_UPDATE_INTERVAL"].Trim();
            }

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["LOGGING_LEVEL"]))
            {
                this.LoggingLevel = ConfigurationManager.AppSettings["LOGGING_LEVEL"].Trim();
            }
            else
            {
                this.LoggingLevel = Enums.Logging.Verbose.ToString();
            }

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["HEARTBEAT_INTERVAL"]))
            {
                this.HeartBeatInterval = ConfigurationManager.AppSettings["HEARTBEAT_INTERVAL"].Trim();
            }
        }

        /// <summary>
        /// Verbose Logging Flag
        /// </summary>
        /// <returns>True if on False is off. Config key is "LOGGING_LEVEL"</returns>
        public bool VerboseLoggingOn 
        {
            get
            {
                return (LoggingLevel == EpbSharedLib.Enums.Logging.Verbose.ToString());
            }
        }

        #region Properties

        public string ApplicationVersion { get; private set; }

        public bool InLocalTestMode { get; private set; }

        public string ClientGuid { get; set; }

        public string MachineGuid { get; set; }

        public string SubscriptionUpdateInterval { get; set; } 

        public string HeartBeatInterval { get; set; }

        /// <summary>
        /// ExceptionsOnly / Verbose
        /// </summary>
        public string LoggingLevel { get; set; }
        #endregion
    }
}