﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EpbSharedLib.SharedClasses
{
    public class Constants
    {
         
        public const int AlertNotification = 10002;
        public const int ActionCompleteNotification = 10003;
        public const int ActionUpdateCompleted = 10004;
        public const int AlertByHotKey = 10005;
        public const int ShowAlertMessage = 10006;
        public const int ConfirmReceipt = 10007;
        public const int ConfirmReceiptSuccess = 10008;
        public const int ApplicationShutdown = 10009;
        public const int ShowProfileWindow = 10010;
        public const int AlertNotificationUpdate = 10011;
        public const int NewAlert = 10012;
        public const int UpdateDataGridRequest = 10013;
        public const int MqConnectionLost = 10014;
        public const int MqConnectionDetected= 10015;
        public const int TriggerReceiverUpdate = 10016;
    }
}
