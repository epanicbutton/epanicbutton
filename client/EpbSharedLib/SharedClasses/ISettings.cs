using System.Configuration;

namespace EpbSharedLib.SharedClasses
{
    public interface ISettings
    {
        /// <summary>
        /// Verbose Logging Flag
        /// </summary>
        /// <returns>True if on False is off. Config key is "LOGGING_LEVEL"</returns>
        bool VerboseLoggingOn { get; }

        string ApplicationVersion { get; }

        bool InLocalTestMode { get; }

        string ClientGuid { get; set; }

        string MachineGuid { get; set; }

        string HeartBeatInterval { get; set; }

        string SubscriptionUpdateInterval { get; set; }

        /// <summary>
        /// ExceptionsOnly / Verbose
        /// </summary>
        string LoggingLevel { get; set; }
    }
}