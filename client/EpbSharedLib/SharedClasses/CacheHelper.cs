﻿using System;
using System.Linq;
using System.Runtime.Caching;

namespace EpbSharedLib.SharedClasses
{
    public class CacheHelper
    {
        // Gets a reference to the default MemoryCache instance. 
        private static readonly ObjectCache cache = MemoryCache.Default;
        private CacheItemPolicy policy = null;
        private CacheEntryRemovedCallback callback = null;
        private static readonly Settings objSettings = new Settings();

        public void AddToCache(String cacheKeyName, Object cacheItem,
            CachePriority cacheItemPriority)
        {
            // 
            this.callback = new CacheEntryRemovedCallback(this.CachedItemRemovedCallback);
            this.policy = new CacheItemPolicy();
            this.policy.Priority = (cacheItemPriority == CachePriority.Default) ? CacheItemPriority.Default : CacheItemPriority.NotRemovable;
            //-- We don't want it to expire
            //this.policy.AbsoluteExpiration = DateTimeOffset.Now.AddHours(1.00);

            this.policy.RemovedCallback = this.callback; 

            // Add inside cache 
            cache.Set(cacheKeyName, cacheItem, this.policy);
        }

        public Object GetCachedItem(String cacheKeyName)
        { 
            return cache[cacheKeyName];
        }

        public void RemoveCachedItem(String cacheKeyName)
        { 
            if (cache.Contains(cacheKeyName))
            {
                cache.Remove(cacheKeyName);
            }
        }

        private void CachedItemRemovedCallback(CacheEntryRemovedArguments arguments)
        {
            // Log these values from arguments list 
            String strLog = String.Concat("Reason: ", arguments.RemovedReason.ToString(), " | Key-Name: ", arguments.CacheItem.Key, " | Value-Object: ", arguments.CacheItem.Value.ToString());

            if (objSettings.VerboseLoggingOn)
            {
                ELogger.Log("CachedItemRemovedCallback: " + strLog);
            }
        }
    }
}