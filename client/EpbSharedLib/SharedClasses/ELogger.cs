﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using log4net;

namespace EpbSharedLib.SharedClasses
{
    public class ELogger
    {

        #region Constants

        private const string MethodLogStart = "Start Calling {0}";
        private const string MethodLogEnd = "End Calling {0}";

        #endregion Constants

        #region Constructor

        static ELogger()
        {
            log4net.Config.XmlConfigurator.Configure();
        }

        #endregion Constructor

        #region Log4net Logger

        static ILog LogObject { get { return LogManager.GetLogger("ELogger"); } }

        #endregion Log4net Logger

        #region ELogger Members

        public static void Log(string message)
        {
            LogObject.Info(message);
        }

        public static void Log(Exception ex, string customMessage = "")
        {
            LogObject.ErrorFormat(": {0}: {1} : {2}", customMessage, ex.Message, ex.StackTrace);
        }

        public static void LogFormat(string message, params object[] parameters)
        {

            LogObject.InfoFormat(message, parameters);

        }

        public static void LogMethodStart()
        {
            StackTrace callStack = new StackTrace(true);
            StackFrame stackFrame = callStack.GetFrame(1);
            LogFormat(MethodLogStart, string.Format("{0}::{1}", Path.GetFileName(stackFrame.GetFileName()), stackFrame.GetMethod().Name));
        }
        public static void LogMethodEnd()
        {
            StackTrace callStack = new StackTrace(true);
            StackFrame stackFrame = callStack.GetFrame(1);
            LogFormat(MethodLogEnd, string.Format("{0}::{1}", Path.GetFileName(stackFrame.GetFileName()), stackFrame.GetMethod().Name));
        }

        #endregion
    }
}