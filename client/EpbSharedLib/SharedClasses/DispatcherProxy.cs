﻿using System;
using System.Linq;
using EpbSharedLib.DispatcherSrvc;
using EpbSharedLib.Entities;
using Newtonsoft.Json;

namespace EpbSharedLib.SharedClasses
{
    public class DispatcherProxy : IDispatcherProxy
    {
        public static readonly Settings ObjSettings = new Settings();

        public readonly DispatcherSrvc.DispatcherServiceClient ObjSrvc = null;

        public readonly CacheHelper ObjCache = new CacheHelper();

        public DispatcherProxy()
        {
            this.ObjCache = new CacheHelper();

            try
            {
                ObjSrvc = new DispatcherSrvc.DispatcherServiceClient("BasicHttpBinding_IDispatcherService");
            }
            catch (Exception ex)
            {
                ELogger.Log(ex.Message);

            }

        }
        /// <summary>
        /// Get Dispatcher directory path.
        /// </summary>
        /// <returns>Path as a string.</returns>
        public string GetDispatcherDirectoryPath()
        {
            string dPath = ObjSrvc.GetDispatcherPath();

            return dPath;
        }

        /// <summary>
        /// Return User Data either from Cache or from Dispatcher.
        /// The data will be refreshed from the dispatcher.
        /// </summary> 
        /// <param name="refresh">True/False - Refresh from the web service.</param>
        /// <returns>UserData object</returns>
        public UserData GetUserData(bool refresh)
        {
            UserData oUser = null;

            if (ObjSettings.VerboseLoggingOn)
            {
                ELogger.LogMethodStart();
            }

            try
            {
                oUser = this.RetrieveUserData(refresh);
            }
            catch (Exception ex)
            {
                ELogger.Log(ex.Message);
            }

            if (ObjSettings.VerboseLoggingOn)
            {
                ELogger.LogMethodEnd();
            }

            return oUser;
        }

        public NodeData[] GetNodeData(bool refresh)
        {
            try
            {
                if (ObjSrvc == null) return null;

                return ObjSrvc.GetUserInfo(ObjSettings.ClientGuid, ObjSettings.MachineGuid, refresh);
            }
            catch (Exception ex)
            {
                ELogger.Log(ex.Message);
            }

            return null;
        }

        /// <summary>
        /// Send event data to server.
        /// </summary>
        /// <param name="eventType">EventType enum<see cref="EpbSharedLib.Enums.EventType"/></param>
        /// <param name="events">Event data as CSV string <see cref="EpbSharedLib.SharedClasses.ExtensionMethods"/></param>
        /// <returns>true for success, false for failure</returns>
        public void ReportEvent(EpbSharedLib.Enums.EventType eventType, string eventData)
        {
            try
            {
                if (ObjSrvc == null) return;

                if (ObjSettings.VerboseLoggingOn)
                {
                    ELogger.Log(string.Format("Adding event -:- {0}  -:- Data: {1}", eventType, eventData));
                }

                ObjSrvc.EventProcess(EventType.AlertButtonPressed, eventData);
            }
            catch (Exception ex)
            {
                ELogger.Log(ex.Message);
            }

            return;
        }

        /// <summary>
        /// Get MQ Configuration
        /// </summary>
        /// <returns></returns>
        public MqConfig GetQueueConfiguration()
        {
            MqConfig mqConfig = new MqConfig(ObjSettings.ClientGuid, ObjSettings.MachineGuid);
            NodeData[] nodeData = GetNodeData(false);

            String[] creds = ObjSettings.ClientGuid.Split('-');

            string clientAdmin = creds[0].ToString().ToUpper();
            string clientPwd = creds[4].ToString().ToUpper();

            clientPwd = EpbSharedLib.Utilities.Helpers.Base64Encode(clientPwd);     

            try
            {
                if (nodeData != null && nodeData.Any())
                {
                    if (nodeData[0].nodeuserdata != null && nodeData[0].nodeuserdata.Any())
                    {
                        UserData userData = nodeData[0].nodeuserdata[0]; 
                        mqConfig.RequestExchangeName = userData.RequestExchangeName;
                        mqConfig.ResponseExchangeName = userData.ResponseExchangeName;
                        mqConfig.HostName = userData.MQHostName;
                        mqConfig.Port = 5672;
                        mqConfig.VirtualHost = userData.VHostName;
                        mqConfig.UserName = clientAdmin;
                        mqConfig.Password = clientPwd;
                    }
                }
            }
            catch (Exception ex)
            {
                ELogger.Log(ex);
            }
            return mqConfig;
        }

        /// <summary>
        /// Return User Data either from Cache or from Dispatcher.
        /// The data will not be refreshed by the dispatcher.
        /// </summary>
        /// <returns>UserData object</returns>
        public UserData GetUserData()
        {
            bool refresh = false;
            DispatcherSrvc.UserData oUser;

            try
            {
                oUser = this.RetrieveUserData(refresh);
            }
            catch (Exception ex)
            {
                ELogger.Log(ex.Message);

                return null;
            }
            return oUser;
        }

        /// <summary>
        /// Get User Info from dispatcher with refresh.
        /// </summary>
        /// <param name="oUser">UserData object</param>
        public UserData GetUserDataNoCache()
        {
            DispatcherSrvc.UserData oUser = this.RetrieveUserDataNoCache();
            return oUser;
        }

        /// <summary>
        /// Add User Info to Cache.
        /// </summary>
        /// <param name="oUser">UserData object</param>
        public void CacheUserData(UserData oUser)
        {
            this.ClearUserDataCache();
            this.ObjCache.AddToCache("CLIENT_CONFIG", oUser, CachePriority.Default);
        }

        public void ClearUserDataCache()
        {
            this.ObjCache.RemoveCachedItem("CLIENT_CONFIG");
        }

        private UserData RetrieveUserData(bool refresh)
        {
            DispatcherSrvc.UserData oUser = null;

            try
            {
                oUser = this.ObjCache.GetCachedItem("CLIENT_CONFIG") as DispatcherSrvc.UserData;

                if (oUser == null)
                {
                    ELogger.Log("Refresh: " + refresh);

                    DispatcherSrvc.NodeData[] result = ObjSrvc.GetUserInfo(ObjSettings.ClientGuid, ObjSettings.MachineGuid, refresh);

                    if (result != null && result.Count() > 0)// && result[0].nodeuserdata[0].Id != 1)
                    {
                        if (result[0].nodeuserdata.Count() > 0)
                        {
                            oUser = result[0].nodeuserdata[0];

                            this.CacheUserData(oUser);

                            if (ObjSettings.VerboseLoggingOn)
                            {
                                ELogger.Log("oUser: " + oUser.UserFirstName + "" + oUser.UserLastName);

                                ELogger.Log("Config data read from Dispatcher.");
                            }
                        }
                    }

                }
                else
                {
                    if (ObjSettings.VerboseLoggingOn)
                    {
                        ELogger.Log("Config data read from cache. User: " + oUser.UserFirstName + " " + oUser.UserLastName);
                    }
                }
            }
            catch (Exception ex)
            {
                ELogger.Log(ex.Message);
            }

            return oUser;
        }
        private UserData RetrieveUserDataNoCache()
        {
            DispatcherSrvc.UserData oUser = null;

            if (ObjSettings.VerboseLoggingOn)
            {
                ELogger.LogMethodStart();
            }

            try
            {
                DispatcherSrvc.NodeData[] result = ObjSrvc.GetUserInfo(ObjSettings.ClientGuid, ObjSettings.MachineGuid, true);

                if (result != null && result.Count() > 0)
                {
                    if (result[0].nodeuserdata.Count() > 0)
                    {
                        oUser = result[0].nodeuserdata[0];

                        this.CacheUserData(oUser);

                        if (ObjSettings.VerboseLoggingOn)
                        {
                            ELogger.Log("Config data read from Dispatcher.");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ELogger.Log(ex, "RetrieveUserDataNoCache");
            }

            if (ObjSettings.VerboseLoggingOn)
            {
                ELogger.LogMethodEnd();
            }

            return oUser;
        }
        /// <summary>
        /// Updates the profile.
        /// </summary>
        /// <param name="objUser">Populated EpbSharedLib.Entities.UserInfo object</param>
        /// <returns>Success or Failure as bool.</returns>
        public bool UpdateProfile(DispatcherSrvc.UserInfo objUser)
        {
            if (ObjSettings.VerboseLoggingOn)
            {
                ELogger.LogMethodStart();
            }

            try
            {
                bool set = ObjSrvc.SetUserProfile(objUser);
                string json = JsonConvert.SerializeObject(objUser, Formatting.None);
                ELogger.LogFormat("User profile set: {0}: {1}", set, json);
                return set;
            }
            catch (Exception ex)
            {
                ELogger.Log(ex.Message);

                if (ObjSettings.VerboseLoggingOn)
                {
                    ELogger.LogMethodEnd();
                }

                return false;
            }
        }
    }
}