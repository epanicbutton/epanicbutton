using System;
using System.Linq;
using EpbSharedLib.DispatcherSrvc;
using EpbSharedLib.Entities;

namespace EpbSharedLib.SharedClasses
{
    public interface IDispatcherProxy
    {
        /// <summary>
        /// Get Dispatcher directory path.
        /// </summary>
        /// <returns>Path as a string.</returns>
        string GetDispatcherDirectoryPath();

        /// <summary>
        /// Return User Data either from Cache or from Dispatcher.
        /// The data will be refreshed from the dispatcher.
        /// </summary> 
        /// <param name="refresh">True/False - Refresh from the web service.</param>
        /// <returns>UserData object</returns>
        UserData GetUserData(bool refresh);

        NodeData[] GetNodeData(bool refresh);

        /// <summary>
        /// Send event data to server.
        /// </summary>
        /// <param name="eventType">EventType enum<see cref="EpbSharedLib.Enums.EventType"/></param>
        /// <param name="events">Event data as CSV string <see cref="EpbSharedLib.SharedClasses.ExtensionMethods"/></param>
        /// <returns>true for success, false for failure</returns>
        void ReportEvent(EpbSharedLib.Enums.EventType eventType, string eventData);

        /// <summary>
        /// Get MQ Configuration
        /// </summary>
        /// <returns></returns>
        MqConfig GetQueueConfiguration();

        /// <summary>
        /// Return User Data either from Cache or from Dispatcher.
        /// The data will not be refreshed by the dispatcher.
        /// </summary>
        /// <returns>UserData object</returns>
        UserData GetUserData();

        /// <summary>
        /// Get User Info from dispatcher with refresh.
        /// </summary>
        /// <param name="oUser">UserData object</param>
        UserData GetUserDataNoCache();

        /// <summary>
        /// Add User Info to Cache.
        /// </summary>
        /// <param name="oUser">UserData object</param>
        void CacheUserData(UserData oUser);

        /// <summary>
        /// Clear user info from cache.
        /// </summary>
        void ClearUserDataCache();

        /// <summary>
        /// Updates the profile.
        /// </summary>
        /// <param name="objUser">Populated EpbSharedLib.Entities.UserInfo object</param>
        /// <returns>Success or Failure as bool.</returns>
        bool UpdateProfile(DispatcherSrvc.UserInfo objUser);
    }
}