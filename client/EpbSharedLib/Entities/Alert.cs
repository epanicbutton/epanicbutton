﻿using EpbSharedLib.Enums;
using System;
using System.Linq;

namespace EpbSharedLib.Entities
{
    [Serializable]
    public class Alert : IDisposable 
    {
        public int AlertId { get; set; }

        public DateTime AlertDateTime { get; set; }

        public string AlertGuid { get; set; }

        public string AlertTitle { get; set; }

        public virtual string MessageGuid { get; set; }

        public virtual string SenderName { get; set; }

        public virtual string SenderMachineName { get; set; }

        public virtual string SenderMachineGuid { get; set; }

        public virtual string SenderLocation { get; set; }

        public virtual string SenderDepartment { get; set; }

        public virtual string SenderTelephone { get; set; }

        public virtual string SenderEmail { get; set; }

        public virtual int SenderId { get; set; }

        public virtual string SeverityLevel { get; set; }

        public virtual string AlertDescription { get; set; }

        public virtual string AlertMemo { get; set; }

        public virtual bool Mute { get; set; }

        public virtual bool Confirmed { get; set; }

        public virtual string ConfirmedBy { get; set; }

        public virtual int ReceiptCount { get; set; }

        public MessageSource AlertMsgSource { get; set; }

        public Alert()
        {
            AlertMsgSource = MessageSource.None; 
        }
        public void Dispose()
        { 
        }
    }
}