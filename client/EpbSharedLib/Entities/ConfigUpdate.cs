﻿using System;
using System.Linq;

namespace EpbSharedLib.Entities
{ 
    public class ConfigUpdate : IConfigUpdate  
    {
        public string UpdateAction { get; set; }
        public string ActionArgs { get; set; }
    }
}