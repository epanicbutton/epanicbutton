﻿using System;
using System.Linq;

namespace EpbSharedLib.Entities
{
    public static class UpdateActions
    {
        public const string Update = "update";
        public const string UpdateReceiver = "update_receiver";
        public const string GroupChange = "group_change,";
    }
}