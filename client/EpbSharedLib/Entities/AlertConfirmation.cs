﻿using System;
using System.Linq;

namespace EpbSharedLib.Entities
{
    [Serializable]
    public class AlertConfirmation 
    {
        public virtual string MessageGuid { get; set; } 

        public virtual int ConfirmSenderId { get; set; } 
    }
}