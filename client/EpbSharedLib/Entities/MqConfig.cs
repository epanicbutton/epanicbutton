﻿using System;
using System.Linq;

namespace EpbSharedLib.Entities
{
    public class MqConfig
    { 
        static readonly string queueNameTemplate = "epb4.Q.topic.{0}.ReceiverQueue";
        static readonly string confirmQueueNameTemplate = "epb4.Q.topic.{0}.ConfirmQueue";
        static readonly string updateQueueNameTemplate = "epb4.Q.topic.{0}.UpdateQueue";
        static readonly string exchangeNameTemplate = "epb4.E.topic.{0}.Client";
        static readonly string alertRoutingKeyTemplate = "alert.{0}";
        static readonly string confirmRoutingKeyTemplate = "confirm.{0}";
        static readonly string updateRoutingKeyTemplate = "update.{0}";

        public string HostName { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public string RequestExchangeName { get; set; }

        public string ResponseExchangeName { get; set; }

        public bool IsDurable { get; set; }

        public string VirtualHost { get; set; }

        public int Port { get; set; }

        public string PrivateQueueName { get; private set;}
        public string PrivateConfirmQueueName { get; private set; }
        public string PrivateUpdateQueueName { get; private set; }
        public string PrivateExchangeName { get; private set;}
        public string AlertRoutingKey { get; private set; }
        public string ConfirmRoutingKey { get; private set; }
        public string UpdateRoutingKey { get; private set; }

        public MqConfig(string clientGuid, string machineGuid)
        {
            PrivateQueueName = string.Format(queueNameTemplate, machineGuid);
            PrivateConfirmQueueName = string.Format(confirmQueueNameTemplate, machineGuid);
            PrivateUpdateQueueName = string.Format(updateQueueNameTemplate, machineGuid);
            PrivateExchangeName = string.Format(exchangeNameTemplate, machineGuid);
            AlertRoutingKey = string.Format(alertRoutingKeyTemplate, machineGuid) + ".{0}";
            ConfirmRoutingKey = string.Format(confirmRoutingKeyTemplate, machineGuid) + ".{0}";
            UpdateRoutingKey = string.Format(updateRoutingKeyTemplate, clientGuid) + ".{0}";
        } 
    }
}