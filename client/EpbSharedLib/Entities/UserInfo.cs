﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Net;

namespace EpbSharedLib.Entities
{
    public class UserInfo
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UserInfo" /> class.
        /// </summary>
        public UserInfo()
        {
            OperatingSystem os = Environment.OSVersion;

            this.OsVersion = os.VersionString;
            this.ServicePack = os.ServicePack;
            this.MachineName = Environment.MachineName;
            this.MachineIp = Dns.GetHostAddresses(Environment.MachineName)[0].ToString();
            this.MacAddress = this.GetMacAddress();
            this.UserName = Environment.UserName;
        }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Telephone { get; set; }

        public string EmailAddress { get; set; }

        public string Department { get; set; }

        public string Location { get; set; }

        public string MachineName { get; private set; }

        public string MacAddress { get; private set; }

        public string MachineIp { get; private set; }

        public string MachineGuid { get; set; }

        public string ClientGuid { get; set; }

        public string ApplicationVersion { get; set; }

        public string OsVersion { get; private set; }

        public string ServicePack { get; private set; }

        public string UserName { get; private set; }
        
        private string GetMacAddress()
        {
            try
            {
                ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT * FROM Win32_NetworkAdapterConfiguration where IPEnabled=true");
                IEnumerable<ManagementObject> objects = searcher.Get().Cast<ManagementObject>();
                string mac = (from o in objects orderby o["IPConnectionMetric"] select o["MACAddress"].ToString()).FirstOrDefault();
                return mac;
            }
            catch
            {
                return "undefined";
            }
        }
    }
}