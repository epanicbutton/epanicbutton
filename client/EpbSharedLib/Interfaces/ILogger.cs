﻿using System;
using System.Linq;


namespace EpbSharedLib.Interfaces
{
    internal interface ILogger
    {     
        /// <summary>
        /// Adds message to logging target.
        /// </summary>
        /// <param name="msg">Short message string.</param>
        /// <param name="msgDetail">Message details string.</param>
        /// <returns>Success as true or false</returns>
        bool Add(string msg, string msgDetail); 

        /// <summary>
        /// Adds message to logging target with message origin.
        /// </summary>
        /// <param name="msgSource">Message source as string. This could be the Method or class name.</param>
        /// <param name="msg">Short message string.</param>
        /// <param name="msgDetail">Message details string.</param>
        /// <returns>Success as true or false</returns>
        bool Add(string msgSource, string msg, string msgDetail);

        /// <summary>
        /// Adds Exception to logging target with origin.
        /// </summary>
        /// <param name="msgSource">Message source as string. This could be the Method or class name.</param>
        /// <param name="ex">Exception object</param>
        /// <returns>Success as true or false</returns>
        bool Add(string msgSource, Exception ex);
    }
}