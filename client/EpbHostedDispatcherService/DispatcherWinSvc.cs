﻿using EpbSharedLib.SharedClasses;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceProcess; 

namespace EpbHostedDispatcherService
{
    public partial class DispatcherWinSvc : ServiceBase
    {
        ServiceHost mSvcHost = null;
        bool stopIssued = false;
        bool startSuccessful = false;

        public DispatcherWinSvc()
        {
            InitializeComponent();
        }

        private void StartIt(string[] args)
        {
            EventLog.WriteEntry("EPanicButton Dispatcher Started.");
            ELogger.LogMethodStart();
            Action thread = () =>
            {
                while (!stopIssued && !startSuccessful)
                {
                    ELogger.Log("Reattempting connection...");
                    
                    try
                    {
                        if (mSvcHost != null) mSvcHost.Close();

                        string strAdrHttp = "http://localhost:9001/DispatcherService";
                        string strAdrTcp = "net.tcp://localhost:9002/DispatcherService";

                        Uri[] adrbase = { new Uri(strAdrHttp), new Uri(strAdrTcp) };
                        mSvcHost = new ServiceHost(typeof(EpbDispatcherLib.DispatcherService), adrbase);

                        ServiceMetadataBehavior mBehave = new ServiceMetadataBehavior();
                        mSvcHost.Description.Behaviors.Add(mBehave);


                        BasicHttpBinding httpb = new BasicHttpBinding();
                        mSvcHost.AddServiceEndpoint(typeof(EpbDispatcherLib.IDispatcherService), httpb, strAdrHttp);
                        mSvcHost.AddServiceEndpoint(typeof(IMetadataExchange),
                        MetadataExchangeBindings.CreateMexHttpBinding(), "mex");

                        NetTcpBinding tcpb = new NetTcpBinding();
                        mSvcHost.AddServiceEndpoint(typeof(EpbDispatcherLib.IDispatcherService), tcpb, strAdrTcp);
                        mSvcHost.AddServiceEndpoint(typeof(IMetadataExchange),
                        MetadataExchangeBindings.CreateMexTcpBinding(), "mex");

                        mSvcHost.Open();
                        startSuccessful = true;
                    }
                    catch (Exception ex)
                    {
                        // Log the exception.
                        if (Environment.UserInteractive)
                        {
                            Console.WriteLine(ex.Message);
                        }
                        EventLog.WriteEntry(ex.Message, EventLogEntryType.Error);
                        ELogger.Log("Connection Failed");
                        ELogger.Log(ex.Message);
                    }

                    //--
                    ELogger.LogMethodEnd();
                    System.Threading.Thread.Sleep(5000);
                }
            };
            thread.BeginInvoke(null, null);
        }
        private void ShutIt()
        {
            ELogger.LogMethodStart();
            this.stopIssued = true;
            if (mSvcHost != null)
            {
                mSvcHost.Close();
                mSvcHost = null;

            }

            ELogger.LogMethodEnd();
        }

        public void Startup(string[] args)
        {
            StartIt(args);
        }

        public void Shutdown()
        {
           
        }

        protected override void OnStart(string[] args)
        {
            StartIt(args);
        }

        protected override void OnStop()
        {
            ShutIt();
        } 
    }
}
