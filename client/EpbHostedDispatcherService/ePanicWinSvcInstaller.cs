﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Configuration.Install;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.ServiceProcess;
using System.Threading;
using System.Threading.Tasks;
using EpbDispatcherLib.EPBService;
using EpbSharedLib.SharedClasses;
using Microsoft.ServiceBus;
using System.Xml.Linq;
using Microsoft.Win32;
using System.ServiceModel;

namespace EpbHostedDispatcherService
{
    [RunInstaller(true)]
    public partial class ePanicWinSvcInstaller : System.Configuration.Install.Installer
    {
        public ePanicWinSvcInstaller()
        {
            InitializeComponent();
        }

        protected override void OnBeforeInstall(IDictionary savedState)
        {
            base.OnBeforeInstall(savedState);
        }


        public override void Commit(IDictionary savedState)
        {
            base.Commit(savedState);
            ServiceController sc = new ServiceController(ePanicWinSvcServiceInstaller.ServiceName);
            sc.Start();

            var clientGuid =  GetClientGuid();
           
            if (!string.IsNullOrEmpty(clientGuid))
            {
                var cmd = @"C:\ProgramData\ePanicButton\ePanicButton.exe";
                var installArgs = "INSTALL " + clientGuid;

                ProcessStartInfo installProcessInfo = new ProcessStartInfo(cmd, installArgs);
                installProcessInfo.UseShellExecute = false;
                installProcessInfo.Verb = "runas";


                Process.Start(installProcessInfo);
            }
            sc.Refresh();
        }

        private string GetClientGuid()
        {
            var clientGuid = Context.Parameters["ClientGuid"];

            if (string.IsNullOrEmpty(clientGuid))
            {
                var sourceDir = Context.Parameters["SourceDir"];
                var sharepath = Context.Parameters["Sharepath"];
                // Use sharepath if proveded from cmdline
                if (sharepath != "")
                    sourceDir = sharepath;
                var files = System.IO.Directory.GetFiles(sourceDir).Where(x => x.Contains("ePanic_"));
                var setupFile = files.FirstOrDefault();
                if (setupFile == null)
                    throw new Exception("Setup file must begin with ePanic_");
                var fi = new System.IO.FileInfo(setupFile);
                clientGuid = fi.Name.Replace("ePanic_", "").Replace(".msi", "");
            }

            return clientGuid;
        }

        protected override void OnCommitted(IDictionary savedState)
        {

        }

        protected override void OnBeforeUninstall(IDictionary savedState)
        {
            var configLocation = @"C:\ProgramData\ePanicButton\ePanicClient.exe.config";
            if (!File.Exists(configLocation))
            {
                base.OnBeforeUninstall(savedState);
                return;
            }
            var settingsNode = XElement.Load(configLocation).Element("appSettings");
            var connStr = settingsNode.Elements("add").Where(x => x.Attribute("key").Value == "AzureConnectionString").FirstOrDefault().Attribute("value").Value;
            var clientGuid = settingsNode.Elements("add").Where(x => x.Attribute("key").Value == "CLIENT_GUID").FirstOrDefault().Attribute("value").Value;
            var machineGuid = settingsNode.Elements("add").Where(x => x.Attribute("key").Value == "MACHINE_GUID").FirstOrDefault().Attribute("value").Value;

            var updateSub = machineGuid + ".update";
            var alertSub = machineGuid + ".alert";
            var confirmSub = machineGuid + ".confirm";

            var namespaceManager = NamespaceManager.CreateFromConnectionString(connStr);

            if (namespaceManager.SubscriptionExists(clientGuid, updateSub))
            {
                namespaceManager.DeleteSubscription(clientGuid, updateSub);
            }
            if (namespaceManager.SubscriptionExists(clientGuid, alertSub))
            {
                namespaceManager.DeleteSubscription(clientGuid, alertSub);
            }
            if (namespaceManager.SubscriptionExists(clientGuid, confirmSub))
            {
                namespaceManager.DeleteSubscription(clientGuid, confirmSub);
            }

            base.OnBeforeUninstall(savedState);
        }

        private void ePanicWinSvcProcessInstaller_AfterInstall(object sender, InstallEventArgs e)
        {

        }

        public override void Uninstall(IDictionary savedState)
        {
            string appRoot = @"C:\ProgramData\ePanicButton";
            string[] files =
                {
                    @"ePanicButtonReceiver.exe.config",
                    @"ePanicClient.exe.config",
                    @"EpbHostedDispatcherService.exe.config",
                    @"UserInfo.xml"
                };

            foreach (string file in files)
            {
                File.Delete(Path.Combine(appRoot, file));
            }

            base.Uninstall(savedState);
        }
    }
}
