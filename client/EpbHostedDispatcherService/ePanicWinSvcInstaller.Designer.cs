﻿namespace EpbHostedDispatcherService
{
    partial class ePanicWinSvcInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ePanicWinSvcProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.ePanicWinSvcServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // ePanicWinSvcProcessInstaller
            // 
            this.ePanicWinSvcProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.ePanicWinSvcProcessInstaller.Password = null;
            this.ePanicWinSvcProcessInstaller.Username = null;
            // 
            // ePanicWinSvcServiceInstaller
            // 
            this.ePanicWinSvcServiceInstaller.Description = "ePanicButton Message Dispatcher service manages the delivery of messages, updatin" +
    "g of recipient lists ";
            this.ePanicWinSvcServiceInstaller.DisplayName = "ePanicButton Dispatcher Service";
            this.ePanicWinSvcServiceInstaller.ServiceName = "ePanicButtonDispatcher";
            this.ePanicWinSvcServiceInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;            
            // 
            // ePanicWinSvcInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.ePanicWinSvcProcessInstaller,
            this.ePanicWinSvcServiceInstaller});

        }

        #endregion

        public System.ServiceProcess.ServiceProcessInstaller ePanicWinSvcProcessInstaller;
        public System.ServiceProcess.ServiceInstaller ePanicWinSvcServiceInstaller;
    }
}