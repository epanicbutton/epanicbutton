﻿using EpbSharedLib.SharedClasses;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Text;
using System.Threading.Tasks;

namespace EpbHostedDispatcherService
{
    class ServiceHelper
    {
        ServiceHost mSvcHost = null;

        public void OnStart(string[] args)
        {
            //EventLog.WriteEntry("EPanicButton Dispatcher Started.");

            ELogger.LogMethodStart();

            try
            {
                if (mSvcHost != null) mSvcHost.Close();

                string strAdrHttp = "http://localhost:9001/DispatcherService";
                string strAdrTcp = "net.tcp://localhost:9002/DispatcherService";

                Uri[] adrbase = { new Uri(strAdrHttp), new Uri(strAdrTcp) };
                mSvcHost = new ServiceHost(typeof(EpbDispatcherLib.DispatcherService), adrbase);
                
                ServiceMetadataBehavior mBehave = new ServiceMetadataBehavior();
                mSvcHost.Description.Behaviors.Add(mBehave);


                BasicHttpBinding httpb = new BasicHttpBinding();
                mSvcHost.AddServiceEndpoint(typeof(EpbDispatcherLib.IDispatcherService), httpb, strAdrHttp);
                mSvcHost.AddServiceEndpoint(typeof(IMetadataExchange),
                MetadataExchangeBindings.CreateMexHttpBinding(), "mex");

                NetTcpBinding tcpb = new NetTcpBinding();
                mSvcHost.AddServiceEndpoint(typeof(EpbDispatcherLib.IDispatcherService), tcpb, strAdrTcp);
                mSvcHost.AddServiceEndpoint(typeof(IMetadataExchange),
                MetadataExchangeBindings.CreateMexTcpBinding(), "mex");

                mSvcHost.Open();
            }
            catch (Exception ex)
            {
                // Log the exception.
                //EventLog.WriteEntry(ex.Message, EventLogEntryType.Error);
                ELogger.Log(ex.Message);
            }

            //--
            ELogger.LogMethodEnd();

        }
    }
}
