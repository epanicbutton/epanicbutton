﻿using EpbSharedLib.SharedClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace EpbHostedDispatcherService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        private static void Main(string[] args)
        {
            ELogger.LogMethodStart();

            if (Environment.UserInteractive)
            {
                Console.WriteLine(string.Format("EPanicButton Dispatcher Started: {0}", DateTime.Now));

                var dWinSvc = new DispatcherWinSvc();
                dWinSvc.Startup(Environment.GetCommandLineArgs());
                Console.WriteLine("Press Any key to End");
                Console.ReadKey();
                dWinSvc.Shutdown();
            }
            else
            {
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[] 
                {  
                    new DispatcherWinSvc() 
                };
                ServiceBase.Run(ServicesToRun);
            }

            ELogger.LogMethodEnd();
        }
    }
}
