﻿using System;
using System.Linq;

namespace EPanicClient.Enums
{
    public class Commands
    {
        public enum TrayIcon
        {
            About,
            CloseWindow,
            ModifyProfile,
            ResetIcon,
            ShowMainWindow, 
            Exit,
            Update
        }
        public enum PanicButtons
        {
            None,
            Loaded,
            Unloaded,
            Closing,
            Clicked,
            MouseEnter,
            MouseLeave,
            CancelAlertSend,
            SendAlertNow
        }
        public enum UserInfo
        {
            Update,
            Cancel
        }
    }
}