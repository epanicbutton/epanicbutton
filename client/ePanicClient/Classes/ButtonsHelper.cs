﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Media.Imaging;
using EPanicClient.ViewModels;
using EpbSharedLib.DispatcherSrvc;
using EpbSharedLib.SharedClasses;
using System.Drawing;

namespace EPanicClient.Classes
{
    public sealed class ButtonsHelper
    {
        private readonly EpbSharedLib.SharedClasses.DispatcherProxy objProxy = new EpbSharedLib.SharedClasses.DispatcherProxy();
        private readonly ObservableCollection<PanicButtonViewModel> panicButtons = new ObservableCollection<PanicButtonViewModel>();
        private DateTime msgDateTime = DateTime.Now;

        private App theApp;

        public DateTime MsgDateTime
        {
            get
            {
                return msgDateTime;
            }

            set
            {
                this.msgDateTime = value;
            }
        }

        public ButtonsHelper()
        {
         //   this.RefreshButtons();
        }

        public void RefreshButtons()
        {
            this.theApp = ((App)Application.Current);

            UserData buttonCol = null;

            try
            {
                buttonCol = this.objProxy.GetUserData();
            }
            catch(Exception ex)
            {
                ELogger.Log("ButtonsHelper::GetUserData: " + ex.Message);
                return;
            }

            if (buttonCol == null)
            {
                ELogger.Log("Unable to load buttons. Dispatcher may be unavailable.");
                return;
            }

            ELogger.Log(string.Format("{0} Buttons.", buttonCol.buttons.Length));

            if (buttonCol.buttons.Length == 0)
            {
                buttonCol = this.objProxy.GetUserData(true);
                for(int i = 0; i < buttonCol.buttons.Length; i++)
                {
                    ELogger.Log("Button loaded: " + buttonCol.buttons[i].ButtonId);
                }
                if (buttonCol == null)
                {
                    ELogger.Log("2nd try Unable to load buttons. Dispatcher may be unavailable.");
                    return;
                }

                ELogger.Log(string.Format("{0} Buttons 2nd try.", buttonCol.buttons.Length));
            }

            if (this.panicButtons != null)
            {
            REM_POINT:
                foreach (var panicButtonViewModel in this.panicButtons)
                {
                    Console.WriteLine("Removing button " + panicButtonViewModel.ButtonContent);
                    panicButtons.Remove(panicButtonViewModel);
                    goto REM_POINT;
                }
            }

            this.panicButtons.Clear();
            this.theApp.HotKeyController = new SmartHotKey.HotKey();
            this.theApp.HotKeyController.HotKeyPressed += this.HotkeyPressed;
            this.theApp.HotKeyController.RemoveAllKeys();

            foreach (var button in buttonCol.buttons)
            {
                var newButton = new PanicButtonViewModel { ButtonContent = button.AlertTitle, ButtonId = button.ButtonId };

                newButton.ButtonImage = string.Format("/ePanicClient;Component/Icons/{1}", button.ButtonId, button.IconIndex);

                this.panicButtons.Add(newButton);

                this.WireUpHotKeys(button);
            }
        }

        public ObservableCollection<PanicButtonViewModel> GetPanicButtons()
        {
            return this.panicButtons;
        }

        private void WireUpHotKeys(ButtonData button)
        {
            try
            {
                this.theApp.HotKeyController.AddHotKey(string.Format("{0}+{1}", button.HotKeyCode, button.HotKeyModifier), button.ButtonId);

                Console.WriteLine(string.Format("Adding HotKey {0}+{1}", button.HotKeyCode, button.HotKeyModifier));
            }
            catch (Exception ex)
            {
                ELogger.Log(ex.Message);
            }
        }
        private void HotkeyPressed(object sender, SmartHotKey.HotKeyEventArgs e)
        {
            TimeSpan elaspedTime = DateTime.Now - this.MsgDateTime;

            if (elaspedTime.TotalSeconds < 5)
            {
                return;
            }

            this.MsgDateTime = DateTime.Now;

            var epbVm = new PanicButtonViewModel();
            epbVm.DoButtonAction(e.ButtonId);
        }
    }
}