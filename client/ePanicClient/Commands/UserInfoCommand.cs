﻿using System;
using System.Linq;
using System.Windows.Input;
using EPanicClient.ViewModels;
using EpbSharedLib.SharedClasses;

namespace EPanicClient.Commands
{
    internal class UserInfoCommand : ICommand
    {
        private readonly UserInfoViewModel viewModel;

        /// <summary>
        /// Initializes new instance of UpdateUserInfoCommand
        /// </summary>
        public UserInfoCommand(UserInfoViewModel viewModel)
        {
            this.viewModel = viewModel;
        }

        public bool CanExecute(object parameter)
        {
            return viewModel.UserInfo.IsValid;
        }

        public void Execute(object parameter)
        {
            ELogger.LogMethodStart();

            viewModel.SaveChanges();
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }
    }
}