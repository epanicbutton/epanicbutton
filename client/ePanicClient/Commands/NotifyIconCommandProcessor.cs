﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using EPanicClient.ViewModels;

namespace EPanicClient.Commands
{
    internal class NotifyIconCommandProcessor : ICommand
    {
        private readonly NotifyIconViewModel viewModel;
        private readonly Enums.Commands.TrayIcon commandAction;

        public NotifyIconCommandProcessor(NotifyIconViewModel viewModel, Enums.Commands.TrayIcon commandAction)
        {
            this.viewModel = viewModel;
            this.commandAction = commandAction;
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }

        public bool CanExecute(object parameter)
        { 
            switch (this.commandAction)
            {
                case Enums.Commands.TrayIcon.About:
                    return true;
                    break;
                case Enums.Commands.TrayIcon.ShowMainWindow:
                    return Application.Current.MainWindow == null;
                    break;
                case Enums.Commands.TrayIcon.ModifyProfile:
                    return true;
                    break;
                case Enums.Commands.TrayIcon.Exit:
                    return true;
                    break;
                case Enums.Commands.TrayIcon.ResetIcon:
                    return true;
                    break;
                case Enums.Commands.TrayIcon.Update:
                    return true;
                    break;
                default:
                    return true;
                    break;
            } 
        }
        /// <summary>
        /// Execute action based on current command.
        /// </summary>
        /// <param name="parameter"></param>
        public void Execute(object parameter)
        {
            switch (this.commandAction)
            {
                case Enums.Commands.TrayIcon.About:
                    this.viewModel.ShowAbout();
                    break;
                case Enums.Commands.TrayIcon.ShowMainWindow:
                    this.viewModel.ShowMainWindow();
                    break;
                case Enums.Commands.TrayIcon.ModifyProfile:
                    this.viewModel.ModifyProfile();
                    break;
                case Enums.Commands.TrayIcon.Update:
                    this.viewModel.Update(true);
                    break;
                case Enums.Commands.TrayIcon.Exit:
                    this.viewModel.ExitApplication();
                    break;
                case Enums.Commands.TrayIcon.ResetIcon:
                    this.viewModel.ResetIcon();
                    break;
                default:
                    break;
            }
        }
    }
}
