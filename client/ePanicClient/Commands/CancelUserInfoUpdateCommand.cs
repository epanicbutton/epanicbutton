﻿using System;
using System.Linq;
using System.Windows.Input;
using EPanicClient.ViewModels;

namespace EPanicClient.Commands
{
    internal class CancelUserInfoUpdateCommand : ICommand
    {
        private readonly UserInfoViewModel viewModel;

        /// <summary>
        /// Initializes new instance of UpdateUserInfoCommand
        /// </summary>
        public CancelUserInfoUpdateCommand(UserInfoViewModel viewModel)
        {
            this.viewModel = viewModel;
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            this.viewModel.CancelUpdate();
        }
    }
}
