﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using EPanicClient.ViewModels;

namespace EPanicClient.Commands
{
    internal class EPanicButtonsCommand : ICommand
    {
        private readonly EPanicButtonsViewModel viewModel;
        private readonly PanicButtonViewModel viewModelButton;

        private readonly Enums.Commands.PanicButtons commandAction;

        public EPanicButtonsCommand(EPanicButtonsViewModel viewModel, Enums.Commands.PanicButtons commandAction)
        {
            this.viewModel = viewModel;
            this.commandAction = commandAction;
        }
        public EPanicButtonsCommand(PanicButtonViewModel viewModel, Enums.Commands.PanicButtons commandAction)
        {
            this.viewModelButton = viewModel;
            this.commandAction = commandAction;
        }
        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            switch (this.commandAction)
            {
                case Enums.Commands.PanicButtons.Unloaded:
                    this.viewModel.ProcessUCUnload();
                    break;
                case Enums.Commands.PanicButtons.Loaded:
                    this.viewModel.ProcessUCLoad();
                    break;
                case Enums.Commands.PanicButtons.MouseEnter:
                    this.viewModel.MouseEnter();
                    break;
                case Enums.Commands.PanicButtons.MouseLeave:
                    this.viewModel.MouseLeave();
                    break;
                case Enums.Commands.PanicButtons.Clicked:
                    this.viewModelButton.DoButtonAction(parameter);
                    break; 
                default:
                    break;
            } 
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }
    }
}