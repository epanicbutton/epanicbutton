﻿using System;
using System.Linq;
using System.Windows.Input;
using EPanicClient.ViewModels;

namespace EPanicClient.Commands
{
    public class SendAlertCommandProcessor : ICommand
    {
        private readonly CancelSendWindowViewModel viewModel;
        private readonly Enums.Commands.PanicButtons commandAction;

        /// <summary>
        /// Initializes new instance of SendAlertCommandProcessor
        /// </summary>
        public SendAlertCommandProcessor(CancelSendWindowViewModel viewModel, Enums.Commands.PanicButtons commandAction)
        {
            this.viewModel = viewModel;
            this.commandAction = commandAction;
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        { 
            switch (this.commandAction)
            {
                case Enums.Commands.PanicButtons.CancelAlertSend:
                    this.viewModel.CancelSend();
                    break;
                case Enums.Commands.PanicButtons.SendAlertNow:
                    this.viewModel.SendNow();
                    break; 
                default:
                    break;
            }
        }
    }
}