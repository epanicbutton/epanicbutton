﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Windows;
using EPanicClient.Classes;
using EPanicClient.ViewModels;
using EPanicClient.Views;
using EasyNetQ;
using EpbSharedLib.Entities;
using EpbSharedLib.Enums;
using EpbSharedLib.Logging;
using EpbSharedLib.MQ;
using EpbSharedLib.SharedClasses;
using EpbSharedLib.Utilities;
using EventAggregator;
using Hardcodet.Wpf.TaskbarNotification;
using Ninject;
using FirstFloor.ModernUI.Windows.Controls;
using System.Diagnostics;
using Microsoft.ServiceBus;
using System.Configuration;
using Microsoft.ServiceBus.Messaging;

namespace EPanicClient
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public object Locker = new object();

        public SmartHotKey.HotKey HotKeyController = null;

        public TaskbarIcon notifyIcon;
        public ButtonsHelper GlobalButtonsHelper;
        public EpbSharedLib.Entities.Alert ObjAlert = null;


        public IAdvancedBus MessageBus;

        private static Dictionary<string, string> dictEvent;
        private static ISettings objSettings;
        private readonly FileLogger logger = new FileLogger();

        private EpbSharedLib.Entities.Alert outMsg = new EpbSharedLib.Entities.Alert();

        private bool epanicButtonsVisible = false;
        private bool epanicButtonsHaveFocus = false;
        private bool cancelSendWindowVisible = false;
        private bool groupUpdate = false;

        private DateTime msgDateTime = DateTime.Now;
        private MqConfig mQConfig;
        private EpbSharedLib.SharedClasses.IDispatcherProxy objProxy;

        private IMQConsumer consumer = null;
        private IMQPublisher publisher = null;

        public bool AlertAudioEnabled = false;

        private App()
        {
            IocBootstrapper bootstrapper = new IocBootstrapper();
            bootstrapper.Initialize();
            consumer = bootstrapper.Container.Get<IMQConsumer>();
            publisher = bootstrapper.Container.Get<IMQPublisher>();
            objSettings = bootstrapper.Container.Get<ISettings>();
            this.objProxy = bootstrapper.Container.Get<IDispatcherProxy>();
        }

        public bool CancelSendWindowVisible
        {
            get
            {
                return this.cancelSendWindowVisible;
            }
            set
            {
                this.cancelSendWindowVisible = value;
            }
        }

        public bool EpanicButtonsHaveFocus
        {
            get
            {
                return this.epanicButtonsHaveFocus;
            }
            set
            {
                this.epanicButtonsHaveFocus = value;
            }
        }

        public bool EpanicButtonsVisible
        {
            get
            {
                return this.epanicButtonsVisible;
            }
            set
            {
                this.epanicButtonsVisible = value;
            }
        }


        public DateTime MsgDateTime
        {
            get
            {
                return msgDateTime;
            }

            set
            {
                this.msgDateTime = value;
            }
        }

        public void UpdateCompleted()
        {
            ELogger.LogMethodStart();

            this.Send(Constants.ActionUpdateCompleted);
            ELogger.Log(string.Format("\r\nProfile updated at {0}.", DateTime.Now));

            ELogger.LogMethodEnd();
        }

        public string LocalIPAddress()
        {
            IPHostEntry host;
            string localIP = "";
            host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (IPAddress.Parse(ip.ToString()).AddressFamily == AddressFamily.InterNetwork)
                {
                    localIP = ip.ToString();
                    break;
                }
            }
            return localIP;
        }

        public void ResetSentIcon()
        {
            Stream iconStream = Application.GetResourceStream(new Uri("pack://application:,,,/ePanicClient;component/LocalResources/TrayIcon.ico")).Stream;
            this.notifyIcon.Icon = new System.Drawing.Icon(iconStream);
            iconStream.Dispose();
        }

        public void StartConsuming()
        {
            this.Consume();
            return;
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            ELogger.Log("****************** OnStartup ***********************");

            if (EpbSharedLib.Utilities.Helpers.ProcessIsRunning("ePanicClient") == true)
            {
                ELogger.Log("****************** Shutting Down ***********************");
                System.Windows.Application.Current.Shutdown();
                return;
            }

            base.OnStartup(e);

            //-- create the notifyicon (it's a resource 
            //-- declared in NotifyIconResources.xaml 
            this.notifyIcon = (TaskbarIcon)this.FindResource("NotifyIcon");
            this.notifyIcon.DataContext = new ViewModels.NotifyIconViewModel();

            MainWindow window = new MainWindow();
            var viewModel = new MainWindowViewModel();
            window.DataContext = viewModel;



            this.objProxy = new EpbSharedLib.SharedClasses.DispatcherProxy();

            dictEvent = new Dictionary<string, string>();

            //-- Lock and Pulse when message ready to be sent
            lock (this.Locker)
            {
            }

            this.GlobalButtonsHelper = new ButtonsHelper();

            ELogger.Log("****************** Starting up ***********************");

            try
            {
                //----------------- Check for new install ------------------------- 
                EpbSharedLib.DispatcherSrvc.NodeData[] result = this.objProxy.GetNodeData(false);

                if (result != null && result.Count() > 0)
                {
                    if (result[0].nodeuserdata.Count() > 0 && result[0].nodeuserdata[0].UserFirstName.ToLower() == "[new]")
                    {
                        Application.Current.Dispatcher.Invoke(new System.Action(() =>
                        {
                            this.Send(Constants.ShowProfileWindow);
                        }));
                    }
                }
            }
            catch (Exception ex)
            {
                ELogger.Log(string.Format("OnStartup: {0}", ex.Message));
            }
            this.DoStatrupTasks();

            /*
            this.HotKeyController = new SmartHotKey.HotKey();
            this.HotKeyController.HotKeyPressed += new SmartHotKey.HotKey.HotKeyEventHandler(this.HotkeyPressed);
            */


            window.Hide();



            this.Subscribe(Constants.NewAlert, this.HandleNewAlertMessage);
            this.Subscribe(Constants.ActionUpdateCompleted, this.HandleUpdateAfterProfileMods);
            this.Subscribe(Constants.TriggerReceiverUpdate, this.HandleTriggerReceiverUpdate);
        }

        protected override void OnExit(ExitEventArgs e)
        {
            try
            {
                this.Stop();

                try
                {
                    if (this.MessageBus != null)
                    {
                        this.MessageBus.Dispose();
                    }
                }
                catch
                {
                    ELogger.Log("Failed to dispose of MessageBus");
                }

                if (this.notifyIcon != null)
                {
                    this.notifyIcon.Dispose();
                }
                this.HotKeyController.RemoveAllKeys();
            }
            catch
            {
                ELogger.Log("Failed to stop MessageBus");
            }
            finally
            {
            }

            base.OnExit(e);
        }

        private void HandleTriggerReceiverUpdate(EventMessage obj)
        {
            this.TriggerReceiverUpdate();
        }

        private void HandleUpdateAfterProfileMods(EventMessage eventMessage)
        {
            this.TriggerReceiverUpdate();
        }

        private void HandleUpdateAfterProfileMods()
        {
            this.Stop();
            System.Timers.Timer t = new System.Timers.Timer();
            t.Interval = 1500;
            t.Elapsed += new System.Timers.ElapsedEventHandler(this.ProcessAfterProfileMods);
            t.Start();
        }

        private void ProcessAfterProfileMods(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                ((System.Timers.Timer)sender).Stop();
                ((System.Timers.Timer)sender).Dispose();

                consumer.Empty();
                this.TriggerReceiverUpdate();
                this.StartConsuming();
            }
            catch (Exception ex)
            {
                ELogger.Log(string.Format("ProcessAfterProfileMods: {0}", ex.Message));
            }
        }

        private void TriggerReceiverUpdate()
        {
            try
            {
                publisher.RoutingKey = string.Format("alert.*.{0}", objSettings.MachineGuid);
                publisher.Send("update_receiver", string.Empty);
                if (objSettings.VerboseLoggingOn)
                {
                    ELogger.Log("Triggering receiver update.");
                }
            }
            catch (Exception ex)
            {
                ELogger.Log(ex);
            }

            return;
        }

        private void Consume()
        {
            if (!consumer.IsConnected)
            {
                return;
            }

            try
            {
                ELogger.Log(string.Format("Consuming confirms for machine ID = '{0}'.", objSettings.MachineGuid));
                
                var oUser = this.objProxy.GetUserData();
                string groupId = "";

                if (oUser != null)
                {
                    groupId = oUser.GroupId;
                }

                ELogger.Log(string.Format("Consuming updates for Group ID = '{0}'.", groupId));
                consumer.RaiseUpdateConfig += consumer_RaiseUpdateConfig;
                consumer.RaiseAlertConfirmation += consumer_RaiseAlertConfirmation;
                consumer.Start(new string[] { "UpdateSub", "ConfirmSub"},
                  new[] { string.Format("{0}.update", objSettings.MachineGuid.ToLower()), string.Format("{0}.confirm", objSettings.MachineGuid) });
                
            }
            catch (Exception ex)
            {
                ELogger.Log(ex);
            }
            finally
            {
            }
        }

        void consumer_RaiseAlertConfirmation(AlertConfirmation alertConfirmation)
        {
            //var obj = msg as AlertConfirmation;
            Current.Dispatcher.Invoke(this.SetSentIcon);
        }

        
        private void consumer_RaiseUpdateConfig(ConfigUpdate configUpdate)
        {
            Current.Dispatcher.Invoke((Action) delegate
                {
                    switch (configUpdate.UpdateAction)
                    {
                        case UpdateActions.Update:
                            this.groupUpdate = false;
                            this.Send(Constants.ActionUpdateCompleted);
                            this.TriggerReceiverUpdate();
                            this.RefreshSubscription();
                            break;
                        case UpdateActions.GroupChange:
                            this.objProxy.GetUserDataNoCache();
                            this.Send(Constants.ActionUpdateCompleted);
                            this.groupUpdate = true;
                            this.HandleUpdateAfterProfileMods();
                            break;
                        default:
                            break;
                    }
                });
        }

        private void HandleNewAlertMessage(EventMessage objAlert)
        {
            try
            {
                EpbSharedLib.Entities.Alert msg = objAlert.GetValue<EpbSharedLib.Entities.Alert>("Dispatch");
                this.ResetSentIcon();

                MqConfig mqConfig = new MqConfig(objSettings.ClientGuid, objSettings.MachineGuid);

                publisher.RoutingKey = mqConfig.AlertRoutingKey;

                Application.Current.Dispatcher.Invoke(new System.Action(() =>
                {
                    string message = EpbSharedLib.Utilities.Serialization.SerializeObject(msg);

                    //mQPublisher.Send(message, msg.AlertId.ToString());
                    publisher.Send(message, msg.AlertId.ToString());

                    if (objSettings.VerboseLoggingOn)
                    {
                        ELogger.Log(string.Format("Sending message '{0}'at {1}.", msg.AlertTitle, DateTime.Now));
                    }

                    this.SendAlertToServer(msg);
                }));
            }
            catch (Exception ex)
            {
                ELogger.Log(ex);
            }
        }

        private void SendAlertToServer(Alert tempObj)
        {
            try
            {
                dictEvent.Clear();

                var oUser = this.objProxy.GetUserData();

                if (oUser != null)
                {
                    dictEvent.Add("clientGuid", objSettings.ClientGuid);
                    dictEvent.Add("userId", oUser.NodeId);
                    dictEvent.Add("buttonId", tempObj.AlertId.ToString());

                    this.objProxy.ReportEvent(EventType.AlertButtonPressed, EpbSharedLib.SharedClasses.ExtensionMethods.ToCsv(dictEvent, '|'));
                }
            }
            catch (Exception ex)
            {
                ELogger.Log(ex);
            }
        }

        private void HotkeyPressed(object sender, SmartHotKey.HotKeyEventArgs e)
        {
            TimeSpan elaspedTime = DateTime.Now - this.MsgDateTime;

            if (elaspedTime.TotalSeconds < 5)
            {
                return;
            }

            this.MsgDateTime = DateTime.Now;

            var epbVm = new PanicButtonViewModel();
            epbVm.DoButtonAction(e.ButtonId);
        }

        private void ResetIconTimeout()
        {
            System.Threading.Timer timer = null;
            timer = new System.Threading.Timer((obj) =>
            {
                ResetSentIcon();
                timer.Dispose();
            }, null, 1000 * 60 * 5, System.Threading.Timeout.Infinite);
        }

        private void SetSentIcon()
        {
            Stream iconStream = Application.GetResourceStream(new Uri("pack://application:,,,/ePanicClient;component/LocalResources/TrayIconReceived.ico")).Stream;
            this.notifyIcon.Icon = new System.Drawing.Icon(iconStream);
            iconStream.Dispose();

            // reset icon back to default after 5 minutes
            ResetIconTimeout();
        }

        /// <summary>
        /// Startup tasks:
        /// 1. Add configuration data oject to cache.
        /// </summary>
        private void DoStatrupTasks()
        {
            ELogger.LogMethodStart();
            try
            {
                this.GetConfigData();
            }
            catch (Exception ex)
            {
                ELogger.Log(ex.Message);
            }

            ELogger.Log("Getting MQ configuration.");
            GetMqSettings();
            this.publisher.CreateExchangesAndQueues();
            this.MessageBus_Connected();
            //this.MessageBus = this.GetQueueConnection(this.mQConfig, objSettings, new AdvancedBusEventHandlers(connected: (s, e) =>
            //{
            //    Application.Current.Dispatcher.Invoke(new System.Action(() =>
            //    {
            //        this.RabbitConnection.MessageBus = (IAdvancedBus)s;
            //        this.MessageBus_Connected();
            //    }));

            //},
            //    disconnected: (s, e) =>
            //    {
            //        Application.Current.Dispatcher.Invoke(new System.Action(() =>
            //        {
            //            this.MessageBus_Disconnected();
            //        }));

            //    }));
        }


        private void GetMqSettings()
        {
            int retryCount = 0;
            int retryLimit = 6;
            int retryPauseInMilli = 6000;

            bool retry = true;

            while (retry)
            {
                if (retryCount > 0)
                {
                    TaskbarIcon tb = notifyIcon;

                    tb.CloseBalloon();
                    tb.ShowBalloonTip("Loading", "Loading settings (" + retryCount + ")...", tb.Icon);

                    ELogger.Log(string.Format("{0} - Retrying GetUserInfo in {1} seconds.", retryCount, retryPauseInMilli));
                    Thread.Sleep(retryPauseInMilli);
                }

                retry = false;
                try
                {
                    this.mQConfig = this.objProxy.GetQueueConfiguration();

                    //TOD: Move to shared lib next full release.
                    //Duplicated code to avoid shared library update.
                    //if (string.IsNullOrEmpty(this.mQConfig.HostName) ||
                    //    string.IsNullOrEmpty(this.mQConfig.VirtualHost) ||
                    //    string.IsNullOrEmpty(this.mQConfig.ResponseExchangeName) ||
                    //    string.IsNullOrEmpty(this.mQConfig.RequestExchangeName))
                    //{
                    //    string mqSettings = string.Format("\nHostName: {0}, vHost: {1}, ResXName: {2}, ReqXName: {3}",
                    //                   this.mQConfig.HostName,
                    //                   this.mQConfig.VirtualHost,
                    //                   this.mQConfig.ResponseExchangeName,
                    //                   this.mQConfig.RequestExchangeName);

                    //    throw new Exception("Unable to load MQ settings from local service." + mqSettings);
                    //}
                }
                catch (Exception ex)
                {
                    ELogger.Log(ex.Message);
                    retryCount++;

                    if (retryCount < retryLimit)
                    {
                        retry = true;
                    }
                    else
                    {
                        ModernDialog.ShowMessage("Unable to load MQ settings. Please make sure the local Dispatcher\nservice is running and restart the ePanicButton Client.", "Application Error", MessageBoxButton.OK);
                        Application.Current.Shutdown();
                    }
                }

            }
        }

        private void GetConfigData()
        {
            EpbSharedLib.DispatcherSrvc.NodeData[] result = this.objProxy.GetNodeData(false);

            if (result != null && result.Count() > 0)
            {
                if (result[0].nodeuserdata.Count() > 0)
                {
                    EpbSharedLib.DispatcherSrvc.UserData oUser = result[0].nodeuserdata[0];

                    EpbSharedLib.SharedClasses.CacheHelper objCache = new EpbSharedLib.SharedClasses.CacheHelper();

                    objCache.AddToCache("CLIENT_CONFIG", oUser, EpbSharedLib.SharedClasses.CachePriority.Default);
                    ELogger.Log("Adding configuration object to cache.");
                }
            }
            else
            {
                ELogger.Log("GetUserInfo returned null.");
            }
        }

        private void Stop()
        {
            try
            {
                if (consumer != null)
                {
                    consumer.Dispose();
                }
            }
            catch (Exception ex)
            {
                ELogger.Log(ex.Message);
            }
        }

        private void MessageBus_Connected()
        {
            if (this.groupUpdate)
            {
                return;
            }
            ELogger.Log("Connected to MQ Server...");

            this.UpdateCompleted();

            this.consumer.Empty();
            this.ResetSentIcon();
            this.StartConsuming();

            TaskbarIcon tb = this.notifyIcon;
            tb.CloseBalloon();
            tb.ShowBalloonTip("Connected", "Protected by ePanicButton.", tb.Icon);
        }

        private void MessageBus_Disconnected()
        {
            if (this.groupUpdate)
            {
                return;
            }

            ELogger.Log("Disconnected from ePanic MQ server.");

            this.Stop();
            this.ResetSentIcon();
            TaskbarIcon tb = this.notifyIcon;
            tb.CloseBalloon();
            tb.ShowBalloonTip("Disconnected", "Unable to connect to ePanicButton server!", tb.Icon);

            Application.Current.Dispatcher.Invoke(new System.Action(() =>
            {
                this.Send(Constants.MqConnectionLost);
            }));
        }

        private string GetButtonIds()
        {
            var usrData = this.objProxy.GetUserDataNoCache();
            int retryCount = 0;

            if (usrData != null && !string.IsNullOrEmpty(usrData.ButtonIDs))
            {
                return usrData.ButtonIDs;
            }

            string buttonIds = string.Empty;

            //-----------------------------------------------------------
            // Continue to try until thread ends or userDate is not null
            //-----------------------------------------------------------
            while (usrData == null)
            {
                retryCount++;

                Thread.Sleep(3000);
                ELogger.Log("Attempting to get userData...");
                usrData = this.objProxy.GetUserDataNoCache();

                // Read from Cache after server reads failed.
                if (retryCount == 10)
                {
                    usrData = this.objProxy.GetUserData(false);
                    retryCount = 0;

                    if (usrData == null || string.IsNullOrEmpty(usrData.ButtonIDs))
                    {
                        break;
                    }

                    buttonIds = usrData.ButtonIDs;
                    break;
                }
            }


            return buttonIds;
        }

        private void RefreshSubscription()
        {
            var buttonIds = GetButtonIds();
            IList<string> keyList = buttonIds.Split(',');
            var clientGuid = objSettings.ClientGuid;
            var alertSub = objSettings.MachineGuid + ".alert";
            var namespaceManager = NamespaceManager.CreateFromConnectionString(ConfigurationManager.AppSettings["AzureConnectionString"]);
            if (namespaceManager.SubscriptionExists(clientGuid, alertSub))
            {
                namespaceManager.DeleteSubscription(clientGuid, alertSub);
            }
            string filterBase = "keyValue = '";
            string filterOr = " OR ";
            string filterString = "";
            filterString += filterBase;
            for (int i = 0; i < keyList.Count; i++)
            {
                if (i != 0)
                    filterString = filterString + filterOr + filterBase;
                filterString = filterString + keyList[i] + "'";
            }
            SqlFilter filter = new SqlFilter(filterString);

            SubscriptionDescription sd = new SubscriptionDescription(clientGuid, alertSub);
            sd.DefaultMessageTimeToLive = new TimeSpan(12, 0, 0);
            namespaceManager.CreateSubscription(sd, filter);
        }
    }
}