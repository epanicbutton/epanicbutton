﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using EPanicClient.Views;
using EventAggregator;
using EpbSharedLib.SharedClasses;
using System.Collections.Generic;

namespace EPanicClient.ViewModels
{
    public class PanicButtonViewModel : INotifyPropertyChanged
    {
        private static EpbSharedLib.SharedClasses.DispatcherProxy objDispProxy;
        private readonly App theApp;
        private int alertSendDelay;
        private ISettings objSettings { get; set; }

        #region INotifyPropertyChanged Members

        public string ButtonImage
        {
            get
            {
                return this.buttonImage;
            }
            set
            {
                this.buttonImage = value;
                this.OnPropertyChanged("ButtonImage");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        private void OnPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public PanicButtonViewModel()
        {
            this.alertSendDelay = 0;

            this.PanicButtonClickedCommand = new Commands.EPanicButtonsCommand(this, Enums.Commands.PanicButtons.Clicked);

            this.theApp = ((App)Application.Current);
            objDispProxy = new EpbSharedLib.SharedClasses.DispatcherProxy();
            this.objSettings =  new EpbSharedLib.SharedClasses.Settings();
        }

        public event Action<PanicButtonViewModel> ExecuteButtonEvent = null;

        /// <summary>
        /// Called when the ePanicButton button is clicked
        /// </summary>
        public ICommand PanicButtonClickedCommand { get; private set; }

        private string buttonImage = null;

        private string buttonContent = null;

        public string ButtonContent
        {
            get
            {
                return this.buttonContent;
            }
            set
            {
                this.buttonContent = value;
                this.OnPropertyChanged("ButtonContent");
            }
        }

        private string buttonId = null;

        public string ButtonId
        {
            get
            {
                return this.buttonId;
            }

            set
            {
                this.buttonId = value;
                this.OnPropertyChanged("ButtonId");
            }
        }

        public void DoButtonAction(object parameter)
        {
            alertSendDelay = 0;
            int btnId = Convert.ToInt32(parameter);

            this.buttonId = btnId.ToString();

            var userData = objDispProxy.GetUserData();

            EpbSharedLib.DispatcherSrvc.ButtonData selectedButton = null;

            //-- Find selected button in button collection
            foreach (var button in userData.buttons)
            {
                if (button.ButtonId == this.buttonId)
                {
                    selectedButton = button;
                    break;
                }
            }

            if (selectedButton == null)
            {
                ELogger.Log("Button " + this.buttonId + " is not configured.");
                return;
            }

            if (selectedButton.SendDelay != null)
            {
                int delay;
                bool isNumeric = int.TryParse(selectedButton.SendDelay, out delay);

                if (isNumeric && delay > 0)
                {
                    alertSendDelay = delay;
                }
            }
            var tb = this.theApp.notifyIcon;
            if (alertSendDelay > 0)
            {
                CancelSendWindow cancelSendWindow = new CancelSendWindow(this, selectedButton, alertSendDelay);


                Application.Current.Dispatcher.Invoke(new System.Action(() =>
                {
                    tb.ShowCustomBalloon(cancelSendWindow, PopupAnimation.None, 20000);
                }));
            }
            else
            {
                tb.CloseBalloon();
                this.SendAlertMessage(selectedButton);
            }

            if (this.ExecuteButtonEvent != null)
            {
                this.ExecuteButtonEvent(this);
            }
        }

        /// <summary>
        /// Send alert message using ZMQ.
        /// </summary>
        /// <param name="message">Message to be sent as string</param>
        /// <returns>void</returns>
        public void SendAlertMessage(EpbSharedLib.DispatcherSrvc.ButtonData oBtn)
        {
            EpbSharedLib.DispatcherSrvc.UserData oUser = objDispProxy.GetUserData(false);

            using (var newAlert = new EpbSharedLib.Entities.Alert())
            {
                newAlert.MessageGuid = Guid.NewGuid().ToString();
                newAlert.AlertDateTime = DateTime.Now;
                newAlert.SenderId = oUser.Id;
                newAlert.SenderName = string.Format("{0} {1}", oUser.UserFirstName, oUser.UserLastName);
                newAlert.SeverityLevel = oBtn.Severity;
                newAlert.SenderMachineName = oUser.NodeMachineName;
                newAlert.SenderMachineGuid = objSettings.MachineGuid;
                newAlert.SenderDepartment = oUser.UserDepartment;
                newAlert.AlertTitle = oBtn.ButtonName;
                newAlert.AlertId = int.Parse(oBtn.ButtonId);
                newAlert.AlertDescription = oBtn.AlertMessage;
                newAlert.SenderLocation = oUser.UserLocation;
                newAlert.SenderTelephone = oUser.UserTelephone;
                newAlert.SenderEmail = oUser.UserEmail;

                bool theMuteVal;
                bool hasMutedVal;
                hasMutedVal = bool.TryParse(oBtn.Mute, out theMuteVal);
                newAlert.Mute = (hasMutedVal) ? theMuteVal : false;
 
                Application.Current.Dispatcher.Invoke(new System.Action(() =>
                    {
                        this.Send(Constants.NewAlert,
                            new KeyValuePair<string, object>(
                                "Dispatch", newAlert));
                    }));
            }
            return;
        }
    }
}