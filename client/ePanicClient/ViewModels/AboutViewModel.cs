﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;

namespace EPanicClient.ViewModels
{
    /// <summary>
    /// 
    /// </summary>
    public class AboutViewModel : INotifyPropertyChanged
    { 

        public AboutViewModel()
        { 
            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            FileVersionInfo versionInfo = FileVersionInfo.GetVersionInfo(assembly.Location);

            this.FileVersion = string.Format("Version: {0}", versionInfo.FileVersion);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public string FileVersion { get; private set; } 
    }
}