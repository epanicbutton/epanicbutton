﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using EPanicClient.Classes;
using EPanicClient.Commands;
using EPanicClient.Views;
using EpbSharedLib.SharedClasses;
using EventAggregator;
using FirstFloor.ModernUI.Presentation;
using FirstFloor.ModernUI.Windows.Controls;
using Hardcodet.Wpf.TaskbarNotification;
using EpbSharedLib.MQ;
using Ninject;

namespace EPanicClient.ViewModels
{
    public class NotifyIconViewModel : NotifyPropertyChanged
    {
        private readonly App theApp;
        private static readonly EpbSharedLib.SharedClasses.Settings objSettings = new EpbSharedLib.SharedClasses.Settings(); 
        private static System.Timers.Timer aTimer;
        private bool ToolTipShowing = false;
        public IMQPublisher publisher;
        public IMQConsumer consumer;

        public void Init()
        {
            IocBootstrapper bootstrapper = new IocBootstrapper();
            bootstrapper.Initialize();
            publisher = bootstrapper.Container.Get<IMQPublisher>();
            consumer = bootstrapper.Container.Get<IMQConsumer>();
        }

        public NotifyIconViewModel()
        {
            Init();
            this.theApp = ((App)Application.Current);
            this.WireUpCommandsAndEvents();
            AppearanceManager.Current.PropertyChanged += this.OnAppearanceManagerPropertyChanged;

            TaskbarIcon tb = this.theApp.notifyIcon;
            tb.TrayBalloonTipShown += tb_TrayBalloonTipShown;
            tb.TrayBalloonTipClosed += tb_TrayBalloonTipClosed;  
        }

        void tb_TrayBalloonTipClosed(object sender, RoutedEventArgs e)
        {
            ToolTipShowing = false;
        }

        void tb_TrayBalloonTipShown(object sender, RoutedEventArgs e)
        {
            ToolTipShowing = true;
        }

        private void WireUpCommandsAndEvents()
        {
            this.ShowMainWindowCommand = new NotifyIconCommandProcessor(this, Enums.Commands.TrayIcon.ShowMainWindow);
            this.ModifyProfileCommand = new NotifyIconCommandProcessor(this, Enums.Commands.TrayIcon.ModifyProfile);
            this.ExitAppCommand = new NotifyIconCommandProcessor(this, Enums.Commands.TrayIcon.Exit);
            this.UpdateCommand = new NotifyIconCommandProcessor(this, Enums.Commands.TrayIcon.Update);
            this.AboutCommand = new NotifyIconCommandProcessor(this, Enums.Commands.TrayIcon.About);
            this.ResetIconCommand = new NotifyIconCommandProcessor(this, Enums.Commands.TrayIcon.ResetIcon);
            this.BalloonClosingCommand = new NotifyIconCommandProcessor(this, Enums.Commands.TrayIcon.CloseWindow);

            TaskbarIcon tb = this.theApp.notifyIcon;
            //tb.TrayContextMenuOpen += this.NotifyIconTrayMouseMove;
            //tb.TrayLeftMouseUp += this.NotifyIconTrayMouseMove;
            tb.TrayMouseMove += this.NotifyIconTrayMouseMove; 

            this.Subscribe(Constants.ActionUpdateCompleted, HandleUpdateAfterProfileMods);
            this.Subscribe(Constants.ShowProfileWindow, HandleShowProfileWindow);
        }

        private void HandleShowProfileWindow(EventMessage obj)
        {
            this.ModifyProfile();

            try
            {
                Application.Current.MainWindow.Activate();
            }
            catch (Exception ex)
            {
                ELogger.Log(ex.Message);
            }
        }

        private void HandleUpdateAfterProfileMods(EventMessage eventMessage)
        {
            Update(false);

            try
            {
                if (Application.Current.MainWindow != null)
                {
                    Application.Current.MainWindow.Hide();
                }
            }
            catch (Exception ex)
            {
                ELogger.Log(ex.Message);
            }
        }

        #region Event Handlers

        private void OnAppearanceManagerPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "ThemeSource" || e.PropertyName == "AccentColor")
            {
                TaskbarIcon tb = this.theApp.notifyIcon;
                tb.ContextMenu.UpdateDefaultStyle();
            }
        }

        private void NotifyIconTrayMouseMove(object sender, RoutedEventArgs e)
        {
            if (this.theApp.EpanicButtonsVisible || this.theApp.CancelSendWindowVisible)
            {
                return;
            }

            var tb = this.theApp.notifyIcon; 
            if ((!consumer.IsConnected))
            {
               
                    if (!ToolTipShowing)
                    {
                        tb.ShowBalloonTip("Disconnected", "Not connected to ePanicButton Server!", tb.Icon);
                    }
                    return;
                
            } 
             
            HideBalloonTip();

            EPanicButtons buttonsPopup = new EPanicButtons(); 

            buttonsPopup.Unloaded += this.TbTrayBalloonTipClosed;
            try
            {
                tb.ShowCustomBalloon(buttonsPopup, PopupAnimation.Slide, 20000);
            }
            catch (Win32Exception ex)
            {
                //I really don't know why this throws.
            }

            // Create a timer with a 4 second interval.
            aTimer = new System.Timers.Timer(4000);

            // Hook up the Elapsed event for the timer.
            aTimer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
            aTimer.Enabled = true;
        }
  
        private void HideBalloonTip()
        {
            ToolTipShowing = false;
            var tb = this.theApp.notifyIcon; 
            tb.HideBalloonTip();
        }

        private void TbTrayBalloonTipClosed(object sender, RoutedEventArgs e)
        {
            if (aTimer != null)
            {
                aTimer.Enabled = false;
                aTimer.Close();
                aTimer.Dispose();
            }
        }

        private static void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            if (Application.Current == null)
            {
                return;
            }
            //Console.WriteLine("The Elapsed event was raised at {0}", e.SignalTime);
            TaskbarIcon tb = ((App)Application.Current).notifyIcon;

            if (((App)Application.Current).EpanicButtonsHaveFocus == false)
            {
                tb.CloseBalloon();
            }
        }

        #endregion

        #region ICommands

        /// <summary>
        /// Gets the Update Command for the ViewModel
        /// </summary>
        public ICommand ShowMainWindowCommand { get; private set; }

        /// <summary>
        /// Ballon closing commant
        /// </summary>
        public ICommand BalloonClosingCommand { get; private set; }

        /// <summary>
        /// Opens Main window with the About View visible
        /// </summary>
        public ICommand AboutCommand { get; private set; }

        /// <summary>
        /// Opens Main Windows with User Info view visible
        /// </summary>
        public ICommand ModifyProfileCommand { get; private set; }

        /// <summary>
        /// Resets the Tray Icon
        /// </summary>
        public ICommand ResetIconCommand { get; private set; }

        /// <summary>
        /// Update User Info from Web Service via Disparcher
        /// </summary>
        public ICommand UpdateCommand { get; private set; }

        /// <summary>
        /// Exits the application (Unload)
        /// </summary>
        public ICommand ExitAppCommand { get; private set; }

        #endregion

        #region Command Actions

        public void ShowMainWindow()
        {
            //TODO: You may uncomment the lines below to 
            //open the User Info window upon double click of
            //the tray icon.
            //Application.Current.MainWindow = new MainWindow();
            //Application.Current.MainWindow.Show();
        }

        /// <summary>
        /// Show About Window
        /// </summary>
        public void ShowAbout()
        {
            if (Application.Current.MainWindow == null)
            {
                Application.Current.MainWindow = new MainWindow();
            }

            var mnWindow = (MainWindow)Application.Current.MainWindow;

            mnWindow.Show();
            mnWindow.ContentSource = new Uri("/Views/About.xaml", UriKind.Relative);
        }

        /// <summary>
        /// Open Modify Profile Window
        /// </summary>
        public void ModifyProfile()
        {
            if (Application.Current.MainWindow == null)
            {
                Application.Current.MainWindow = new MainWindow();
            }

            Application.Current.MainWindow.Show();

            //-- Bring window to front
            Application.Current.MainWindow.Activate();

        }

        public void ResetIcon()
        {
            this.theApp.ResetSentIcon();
        }

        /// <summary>
        /// Update from ePanic Server
        /// </summary>
        public async Task Update(bool showBalloon)
        {
            try
            {

                UpdateCompletedBalloon updateCompletedBalloon = new UpdateCompletedBalloon();
              
                 
                var tb = this.theApp.notifyIcon;

                await this.DoProfileUpdateAsync();

                //-- We need to update button to reinit hotkeys
                theApp.GlobalButtonsHelper.RefreshButtons();

                if (showBalloon)
                {
                    tb.ShowCustomBalloon(updateCompletedBalloon, PopupAnimation.Slide, 4000); 
                }

                Application.Current.Dispatcher.Invoke(new System.Action(() =>
                {
                    this.Send(Constants.TriggerReceiverUpdate);
                }));
            }
            catch (Exception ex)
            {
                ELogger.Log(ex.Message);

                if (showBalloon)
                {
                    ModernDialog.ShowMessage("Unable to retrieve update from Dispatcher.\r\n\r\nError:\r\n" + ex.Message, "Application Error", MessageBoxButton.OK);
                }
            }
        }

        public async Task<int> DoProfileUpdateAsync()
        {
           
            var disp = new EpbSharedLib.SharedClasses.DispatcherProxy();
            var result = await Task.Run(() => disp.GetNodeData(true));

            if (result != null && result.Count() > 0)
            {
                if (result[0].nodeuserdata.Count() > 0)
                {
                    disp.CacheUserData(result[0].nodeuserdata[0]);
                }
            }

            if (objSettings.VerboseLoggingOn)
            {
                ELogger.Log("Profile update completed...");
            }

            return 1;
        }

        /// <summary>
        /// Exit ePanic Client
        /// </summary>
        public void ExitApplication()
        {
            Application.Current.Shutdown();
        }
        #endregion
    }
}