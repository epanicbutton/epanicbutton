﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input; 
using FirstFloor.ModernUI.Presentation;

namespace EPanicClient.ViewModels
{
    public class EPanicButtonsViewModel : NotifyPropertyChanged
    {
        readonly App theApp; 

        public EPanicButtonsViewModel()
        {
            
            MouseEnterPanicButtonsCommand = new Commands.EPanicButtonsCommand(this, Enums.Commands.PanicButtons.MouseEnter);
            MouseLeavePanicButtonsCommand = new Commands.EPanicButtonsCommand(this, Enums.Commands.PanicButtons.MouseLeave);
            UCUnloadedCommand = new Commands.EPanicButtonsCommand(this, Enums.Commands.PanicButtons.Unloaded);
            UCLoadedCommand = new Commands.EPanicButtonsCommand(this, Enums.Commands.PanicButtons.Loaded);  
             
            theApp = ((App)Application.Current); 

            if (theApp.GlobalButtonsHelper != null)
                this.GetPanicButtonsDelegate =  theApp.GlobalButtonsHelper.GetPanicButtons; 

            this.GetPanicButtonsAction(); 

            if (panicButtonsCol != null && panicButtonsCol.Count > 0)
            {
                theApp.EpanicButtonsVisible = true;
            }
        }
       
        public void HandleHotKeyAlert(int butId)
        { 
            //-- Find selected button in button collection
            foreach (var button in panicButtonsCol)
            {
                if (int.Parse(button.ButtonId) == butId)
                {
                    button.DoButtonAction(butId);
                    break;
                }
            }

        }

        #region ICommands
        
        /// <summary>
        /// Gets the Update Command for the ViewModel
        /// </summary>
        public ICommand MouseEnterPanicButtonsCommand { get; private set; }

        public ICommand MouseLeavePanicButtonsCommand { get; private set; }
        
        /// <summary>
        /// Called when the ePanicButton button container is closed
        /// </summary>
        public ICommand UCUnloadedCommand { get; private set; }

        /// <summary>
        /// Called when the ePanicButton button container is loaded
        /// </summary>
        public ICommand UCLoadedCommand { get; private set; }



        #endregion
        
        public void MouseEnter()
        {
            theApp.EpanicButtonsHaveFocus = true;
        }

        public void MouseLeave()
        {
            theApp.EpanicButtonsHaveFocus = false;
        } 
        public void ProcessUCUnload()
        {
            theApp.EpanicButtonsVisible = false;
        }
        public void ProcessUCLoad()
        {
            theApp.EpanicButtonsVisible = true;
        }

        public Func<ObservableCollection<PanicButtonViewModel>> GetPanicButtonsDelegate = null;

        ObservableCollection<PanicButtonViewModel> panicButtonsCol = null;

        public ObservableCollection<PanicButtonViewModel> PanicButtonsCol
        {
            get
            {
                return panicButtonsCol;
            }

            set
            {
                if (panicButtonsCol == value)
                    return;

                if (panicButtonsCol != null)
                {
                    foreach (var panicButtonViewModel in panicButtonsCol)
                    {
                        DisconnectButtonViewModel(panicButtonViewModel);
                    }
                }

                panicButtonsCol = value;

                if (panicButtonsCol != null)
                {
                    foreach (var panicButtonViewModel in panicButtonsCol)
                    {
                        ConnectButtonViewModel(panicButtonViewModel);
                    }
                }

                OnPropertyChanged("PanicButtonsCol");
            }
        }

        void DispatchButtonAction(PanicButtonViewModel panicButtonViewModel)
        {
            DisconnectButtonViewModel(panicButtonViewModel);

            //How to remove a button 
            //PanicButtonsCol.Remove(panicButtonViewModel);

            if (string.IsNullOrEmpty(panicButtonViewModel.ButtonId)) return;

            int bId = int.Parse(panicButtonViewModel.ButtonId);


        }

        void ConnectButtonViewModel(PanicButtonViewModel panicButtonViewModel)
        {
            panicButtonViewModel.ExecuteButtonEvent += DispatchButtonAction;
        }

        void DisconnectButtonViewModel(PanicButtonViewModel panicButtonViewModel)
        {
            panicButtonViewModel.ExecuteButtonEvent -= DispatchButtonAction;
        }

        public void GetPanicButtonsAction()
        {
            PanicButtonsCol = GetPanicButtonsDelegate();

            IsAddButtonActionEnabled = true;
        }

        bool isAddButtonActionEnabled = false;

        public bool IsAddButtonActionEnabled
        {
            get
            {
                return isAddButtonActionEnabled;
            }

            set
            {
                isAddButtonActionEnabled = value;

                OnPropertyChanged("IsAddButtonActionEnabled");
            }
        }

        public void AddPanicButtonAction()
        {
            PanicButtonViewModel newButtonVM = new PanicButtonViewModel { ButtonContent = null, ButtonId = null };

            ConnectButtonViewModel(newButtonVM);

            PanicButtonsCol.Add(newButtonVM);
        }
    }
}