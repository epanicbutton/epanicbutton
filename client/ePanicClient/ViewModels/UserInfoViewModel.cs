﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using EPanicClient.Commands;
using EPanicClient.Models;
using EpbSharedLib.SharedClasses;
using EventAggregator;
using FirstFloor.ModernUI.Presentation;
using FirstFloor.ModernUI.Windows.Controls;

namespace EPanicClient.ViewModels
{
    internal class UserInfoViewModel : NotifyPropertyChanged
    {
        private static Settings objSettings;
        private static EpbSharedLib.SharedClasses.DispatcherProxy objDispProxy;

        private bool isVisible = false;

        /// <summary>
        /// Initializes new instance of UserInfoViewModel
        /// </summary>
        public UserInfoViewModel()
        {
            ELogger.LogMethodStart();

            objSettings = new EpbSharedLib.SharedClasses.Settings();
            objDispProxy = new EpbSharedLib.SharedClasses.DispatcherProxy();

            this.UserInfo = new UserInfo();
            this.UpdateCommand = new UserInfoCommand(this);
            this.CancelCommand = new CancelUserInfoUpdateCommand(this);

            try
            {
                EpbSharedLib.DispatcherSrvc.UserData oUser = objDispProxy.GetUserData(false);

                if (oUser != null)
                {
                    if (!string.IsNullOrEmpty(oUser.UserFirstName) && oUser.UserFirstName.ToLower() == "[new]")
                    {
                        oUser.UserFirstName = string.Empty;

                        Application.Current.Dispatcher.Invoke(new System.Action(() =>
                        {
                            this.Send(Constants.ShowProfileWindow);
                        }));
                    }

                    this.UserInfo.FirstName = oUser.UserFirstName.Trim();
                    this.UserInfo.LastName = oUser.UserLastName.Trim();
                    this.UserInfo.Department = oUser.UserDepartment.Trim();
                    this.UserInfo.Location = oUser.UserLocation.Trim();
                    this.UserInfo.EmailAddress = oUser.UserEmail.ToLower().Trim();
                    this.UserInfo.Telephone = oUser.UserTelephone.Trim();
                }
            }
            catch(Exception ex)
            {
                ELogger.Log(ex.Message);
                //ModernDialog.ShowMessage(ex.Message , "Application Error", MessageBoxButton.OK);  
            }

            ELogger.LogMethodEnd();

        }

        public UserInfo UserInfo { get; set; }
        private bool isSaving = false;

        public bool IsSaving
        {
            get
            {
                return !this.isVisible;
            }
            set
            {
                this.isSaving = value;
                this.OnPropertyChanged("IsSaving"); 
            }
        }

        public bool IsVisible
        {
            get
            {
                return this.isVisible;
            }
            set
            {
                this.isVisible = value; 

                this.OnPropertyChanged("IsVisible"); 
            }
        }
         

        /// <summary>
        /// Gets the Update Command for the ViewModel
        /// </summary>
        public ICommand UpdateCommand { get; private set; }

        /// <summary>
        /// Cancel Profile Update
        /// </summary>
        public ICommand CancelCommand { get; private set; }

        /// <summary>
        /// Save changes made to the UserInfo instance
        /// </summary>
        public async void SaveChanges()
        {
            ELogger.LogMethodStart();

            try
            {

                var oInfo = new EpbSharedLib.DispatcherSrvc.UserInfo();

                oInfo.EmailAddress = this.UserInfo.EmailAddress.Trim();
                oInfo.ApplicationVersion = objSettings.ApplicationVersion;
                oInfo.FirstName = this.UserInfo.FirstName.Trim();
                oInfo.LastName = this.UserInfo.LastName.Trim();
                oInfo.Department = this.UserInfo.Department.Trim();
                oInfo.Location = this.UserInfo.Location.Trim();
                oInfo.Telephone = this.UserInfo.Telephone.Trim();
                oInfo.ApplicationVersion = objSettings.ApplicationVersion;
                oInfo.MachineGuid = objSettings.MachineGuid;
                oInfo.ClientGuid = objSettings.ClientGuid;

                this.isVisible = true;
                OnPropertyChanged("IsVisible");
                OnPropertyChanged("IsSaving");

                await this.StartSave(oInfo);
            }
            catch (Exception ex)
            {
                ELogger.Log(ex.Message);
            }

            ELogger.LogMethodEnd();
            
        }

        public void DoSave(EpbSharedLib.DispatcherSrvc.UserInfo objUser)
        {
            ELogger.LogMethodStart();

            objDispProxy.UpdateProfile(objUser);

            Thread.Sleep(1000);

            Application.Current.Dispatcher.Invoke(new System.Action(() =>
            { 
                this.isVisible = false;
                OnPropertyChanged("IsVisible");
                OnPropertyChanged("IsSaving"); 
            }));
        }

        public async Task<bool> StartSave(EpbSharedLib.DispatcherSrvc.UserInfo objUser)
        {
            ELogger.LogMethodStart();

            await Task.Run(() => {
                ELogger.LogMethodStart();

                this.DoSave(objUser);
                //-- We need to update the profile and close the window
                Application.Current.Dispatcher.Invoke(new System.Action(() =>
                {
                    this.Send(Constants.ActionUpdateCompleted);
                }));
            });
            return true;
        }

        /// <summary>
        /// Cancel Update
        /// </summary>
        public void CancelUpdate()
        {
            Application.Current.MainWindow.Close();
        }
 
    }
}
