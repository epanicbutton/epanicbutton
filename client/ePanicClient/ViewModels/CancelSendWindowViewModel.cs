﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using Hardcodet.Wpf.TaskbarNotification;

namespace EPanicClient.ViewModels
{
    public class CancelSendWindowViewModel : INotifyPropertyChanged
    {
        private readonly DispatcherTimer timer;
        private readonly PanicButtonViewModel panicButtonViewModel;
        private readonly EpbSharedLib.DispatcherSrvc.ButtonData sendButton;
        private readonly App theApp;

        private string alertSendDelay;
        private int sendDelayVal;
        private string sendDelay;

        public CancelSendWindowViewModel(PanicButtonViewModel pButtonViewModel,
            EpbSharedLib.DispatcherSrvc.ButtonData sendButton, int sendDelay)
        {
            this.CancelAlertCommand = new Commands.SendAlertCommandProcessor(this, Enums.Commands.PanicButtons.CancelAlertSend);
            this.SendAlertNowCommand = new Commands.SendAlertCommandProcessor(this, Enums.Commands.PanicButtons.SendAlertNow);

            this.panicButtonViewModel = pButtonViewModel;
            this.sendButton = sendButton;
            this.alertSendDelay = sendDelay.ToString();
            this.sendDelayVal = sendDelay;
            this.theApp = ((App)Application.Current);
            this.theApp.CancelSendWindowVisible = true;
            this.theApp.notifyIcon.HideBalloonTip();
            this.timer = new DispatcherTimer(TimeSpan.FromSeconds(1), DispatcherPriority.Normal, this.OnTimerTick, Application.Current.Dispatcher);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Cancels alert send after send delay if set
        /// </summary>
        public ICommand CancelAlertCommand { get; private set; }

        /// <summary>
        /// Cancels alert send after send delay if set
        /// </summary>
        public ICommand SendAlertNowCommand { get; private set; }

        public string SendDelay
        {
            get
            {
                return this.alertSendDelay;
            }
            set
            {
                this.sendDelay = value;
            }
        }

        internal void CancelSend()
        {
            TaskbarIcon tb = this.theApp.notifyIcon;
            tb.ShowBalloonTip("", "Alert send cancelled", tb.Icon);
            this.timer.Stop();
            this.theApp.CancelSendWindowVisible = false;
            tb.CloseBalloon();
        }

        internal void SendNow()
        {
            StopTimerAndSendAlert(true);
        }
 
        private void StopTimerAndSendAlert(bool showFeedback)
        {
            this.timer.Stop();
            TaskbarIcon tb = this.theApp.notifyIcon;
            tb.CloseBalloon();
            this.panicButtonViewModel.SendAlertMessage(this.sendButton);
            this.theApp.CancelSendWindowVisible = false;

            if (showFeedback)
            {
                tb.ShowBalloonTip("", "Alert has been sent", tb.Icon);
            }
        }

        private void OnPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private void OnTimerTick(object sender, EventArgs e)
        {
            //-- fire a property change event for the timestamp 
            if (this.timer != null)
            {
                this.sendDelayVal--;

                if (this.sendDelayVal > 0)
                {
                    this.alertSendDelay = this.sendDelayVal.ToString();
                    Application.Current.Dispatcher.BeginInvoke(new Action(() => this.OnPropertyChanged("SendDelay")));
                }
                else
                {
                    StopTimerAndSendAlert(true);
                }
            }
        }
    }
}