﻿using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using FirstFloor.ModernUI.Presentation;
using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using EpbSharedLib;
using Hardcodet.Wpf.TaskbarNotification;

namespace EPanicClient.ViewModels
{
    internal class MainWindowViewModel : NotifyPropertyChanged
    {
        private Uri mainContentSource;
        private readonly EpbSharedLib.Logging.GlobalLogger logger;

        public Uri MainContentSource
        {
            get { return this.mainContentSource; }
            set
            {
                if (this.mainContentSource != value)
                {
                    this.mainContentSource = value;

                    //- Set the top right window graphic geometry
                    SetMainWindowIcon();
                    OnPropertyChanged("MainContentSource");
                }

                mainContentSource = value;
            }
        }

        private void SetMainWindowIcon()
        {
            try
            {
                ResourceDictionary dict = new ResourceDictionary();
                string winSettings = "/ePanicClient.Resources;component/externals.xaml";

                if (this.mainContentSource.ToString().IndexOf("Settings") >= 0)
                {
                    winSettings = "/ePanicClient.Resources;component/settings_externals.xaml";
                }
                else if (this.mainContentSource.ToString().IndexOf("UserInfo") >= 0)
                {
                    winSettings = "/ePanicClient.Resources;component/externals.xaml";
                }

                dict.Source = new Uri(winSettings, UriKind.Relative);

                Application.Current.Resources.MergedDictionaries.RemoveAt(0);
                Application.Current.Resources.MergedDictionaries.Insert(0, dict);
            }
            catch (Exception ex)
            {
                logger.Add("MainWindowViewModel::SetMainWindowIcon()", ex);

            }
        }
       
        public MainWindowViewModel()
        {
            logger = new EpbSharedLib.Logging.GlobalLogger();
             
            try
            {
                ResourceDictionary dict = new ResourceDictionary();
                dict.Source = new Uri("/ePanicClient.Resources;component/externals.xaml", UriKind.Relative);

                Application.Current.Resources.MergedDictionaries.RemoveAt(0);
                Application.Current.Resources.MergedDictionaries.Insert(0, dict);

                this.mainContentSource = new Uri("/Views/UserInfo.xaml", UriKind.Relative);
            }
            catch (Exception ex)
            {
                logger.Add("MainWindowViewModel::MainWindowViewModel()", ex);

            }
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
