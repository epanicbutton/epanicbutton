﻿using System;
using System.Configuration;
using System.Linq;
using EasyNetQ;
using EasyNetQ.DI;
using EpbSharedLib.MQ;
using EpbSharedLib.SharedClasses;
using Ninject;

namespace EPanicClient
{
    public class IocBootstrapper
    {
        public IKernel Container { get; private set; }
        public IocBootstrapper()
        {

        }

        public void Initialize()
        {
            ISettings objSettings = new EpbSharedLib.SharedClasses.Settings();
            IDispatcherProxy objProxy = new EpbSharedLib.SharedClasses.DispatcherProxy();

            ELogger.Log("Creating IOC container.");
            // Container creation and custom logger registration
            Container = new StandardKernel();

            ELogger.Log("Binding container types.");

            //if (objSettings.VerboseLoggingOn)
            //{
            //    Container.Bind<IEasyNetQLogger>().To<RabbitMqLogger>();
            //}

            var connStr = ConfigurationManager.AppSettings["AzureConnectionString"];  

            IMQConsumer consumer = AzureConsumer.Create(connStr);
            IMQPublisher publisher = new AzurePublisher(connStr, objProxy);

            Container.Bind<IMQPublisher>().ToConstant(publisher);
            Container.Bind<IMQConsumer>().ToConstant(consumer);
            Container.Bind<ISettings>().To<Settings>(); 
            Container.Bind<IDispatcherProxy>().To<DispatcherProxy>();

            // Register Ninject as IoC Container for EasyNetQ
            Container.RegisterAsEasyNetQContainerFactory();
        }
    }
}
