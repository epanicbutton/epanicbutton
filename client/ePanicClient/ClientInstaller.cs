﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace EPanicClient
{
    [RunInstaller(true)]
    public partial class ClientInstaller : System.Configuration.Install.Installer
    {
        public ClientInstaller()
        {
            InitializeComponent();
        }

        public override void Commit(IDictionary savedState)
        {
            string appRoot = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            string exePath = Path.Combine(appRoot, "ePanicClient.exe");

            base.Commit(savedState);
            Process.Start(exePath);
        }
    }
}
