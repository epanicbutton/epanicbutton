﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EPanicClient.Models
{
    public class PanicButton
    {
        public string Content;
        public int ID;
        public string Type;
        public string ButtonImage;
    }
}
