﻿using System;
using System.ComponentModel;
using System.Text.RegularExpressions;
using FirstFloor.ModernUI.Presentation;

namespace EPanicClient.Models
{
    internal class UserInfo : NotifyPropertyChanged, IDataErrorInfo
    {
        /// <summary>
        /// Properties
        /// </summary>
        private String firstName;
        private String lastName;
        private String telephone;
        private String emailAddress;
        private String department;
        private String location;

        /// <summary>
        ///  Initializes new instance of UserInfo class
        /// </summary>
        public UserInfo()
        {
        }

        #region INotifyPropertyChanged handler

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region Property Getters and Setters

        public string Location
        {
            get
            {
                return this.location;
            }
            set
            {
                this.location = value;
                this.OnPropertyChanged("Location");
            }
        }

        public string Department
        {
            get
            {
                return this.department;
            }
            set
            {
                this.department = value;
                this.OnPropertyChanged("Department");
            }
        }

        public string EmailAddress
        {
            get
            {
                return this.emailAddress;
            }
            set
            {
                this.emailAddress = value;
                this.OnPropertyChanged("EmailAddress");
            }
        }

        public string Telephone
        {
            get
            {
                return this.telephone;
            }
            set
            {
                this.telephone = value;
                this.OnPropertyChanged("Telephone");
            }
        }

        /// <summary>
        /// Gets or sets FirstName property
        /// </summary>
        public String FirstName
        {
            get
            {
                return this.firstName;
            }
            set
            {
                this.firstName = value;
                this.OnPropertyChanged("FirstName");
            }
        }

        /// <summary>
        /// Gets or sets LastName property
        /// </summary>
        public String LastName
        {
            get
            {
                return this.lastName;
            }
            set
            {
                this.lastName = value;
                this.OnPropertyChanged("LastName");
            }
        }

        #endregion

        #region Validation

        /// <summary>
        /// Iterate through all fields that requre validation and 
        /// return true if they are all valid.
        /// </summary>
        public bool IsValid
        {
            get
            {
                foreach (string property in validatedProperties)
                {
                    if (this.GetValidationError(property) != null)
                    {
                        return false;
                    }
                }
                return true;
            }
        }

        /// <summary>
        /// Collection of the all controls to be validated.
        /// </summary>
        private static readonly string[] validatedProperties =
        {
            "FirstName",
            "LastName",
            "Telephone",
            "EmailAddress",
            "Department",
            "Location"
        };

        public string this[string propertyName]
        {
            get
            {
                return this.GetValidationError(propertyName);
            }
        }

        private string GetValidationError(string propertyName)
        {
            string error = null;

            switch (propertyName)
            {
                case "FirstName":
                    error = this.ValidateStringValue(this.FirstName);
                    break;
                case "LastName":
                    error = this.ValidateStringValue(this.LastName);
                    break;
                case "Telephone":
                    error = this.ValidateStringValue(this.Telephone);
                    break;
                case "EmailAddress":
                    error = this.ValidateEmailAddress(this.EmailAddress, 100);
                    break;
                case "Department":
                    error = this.ValidateStringValue(this.Department, 100);
                    break;
                case "Location":
                    error = this.ValidateStringValue(this.Location);
                    break;
            }

            return error;
        }

        private const string ErrorIndicator = "!";

        private string ValidateStringValue(string stringValue)
        {
            if (String.IsNullOrWhiteSpace(stringValue))
            {
                return ErrorIndicator;
            }

            return null;
        }
        private string ValidateStringValue(string stringValue, int maxLength)
        {
            if (String.IsNullOrWhiteSpace(stringValue) || stringValue.Length > maxLength)
            {
                return ErrorIndicator;
            }

            return null;
        }
        private string ValidateEmailAddress(string emailString, int maxLength)
        {
            if (String.IsNullOrWhiteSpace(emailString))
                return ErrorIndicator;

            bool isEmail = Regex.IsMatch(emailString, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z");


            if (isEmail && emailString.Length <= maxLength)
            {
                return null;
            }
            else
            {
                return ErrorIndicator;
            }
        }

        public string Error
        {
            get
            {
                return null;
            }
        }

        #endregion
    }
}