﻿using System;
using System.Linq;
using System.Windows.Controls;

namespace EPanicClient.Views
{
    /// <summary>
    /// Interaction logic for ePanicButtons.xaml
    /// </summary>
    public partial class EPanicButtons : UserControl
    { 
        public EPanicButtons()
        {
            this.InitializeComponent(); 
            this.DataContext = new EPanicClient.ViewModels.EPanicButtonsViewModel(); 
        }
    }
}