﻿using System;
using System.Linq;
using System.Windows.Controls;

namespace EPanicClient.Views
{
    /// <summary>
    /// Interaction logic for About.xaml
    /// </summary>
    public partial class About : UserControl
    {
        public About()
        {
            this.InitializeComponent();
            this.DataContext = new EPanicClient.ViewModels.AboutViewModel();
        }
    }
}