﻿using System;
using System.Linq;
using System.Windows.Controls;
using EPanicClient.ViewModels;

namespace EPanicClient.Views
{
    /// <summary>
    /// Interaction logic for CancelSendWindow.xaml
    /// </summary>
    public partial class CancelSendWindow : UserControl
    {
        public CancelSendWindow(PanicButtonViewModel pbViewModel,
                                EpbSharedLib.DispatcherSrvc.ButtonData sendButton,
                                int sendDelay)
        {
            this.InitializeComponent();
            this.DataContext = new EPanicClient.ViewModels.CancelSendWindowViewModel(pbViewModel, sendButton, sendDelay); 
        }
    }
}