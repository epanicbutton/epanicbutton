﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TestWin
{
    public partial class Form1 : Form
    {
        
        public Form1()
        {
            InitializeComponent();
            this.hotKey1.HotKeyPressed += new SmartHotKey.HotKey.HotKeyEventHandler(hotKey1_HotKeyPressed);
           
        }

        void hotKey1_HotKeyPressed(object sender, SmartHotKey.HotKeyEventArgs e)
        {
            MessageBox.Show("You Pressed: " + e.HotKey);
        }

      

        private void hotKey1_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                //try out all combination of keys from System.Windows.Forms.Keys enum based on your requirement
                //some combination will not work,so select your best combination from the list.
                //Note: Don't forget to remove all registered hotkeys(by using RemoveKey(strHotKey) or RemoveAllKeys()) while closing the application or unloading the form.
                //refer current form's Dispose method to remove the registered hot keys.
                
                //normal keys
                this.hotKey1.AddHotKey("S");
                this.hotKey1.AddHotKey("A");
               
                //normal keys with keymodifiers
                this.hotKey1.AddHotKey("Control+Shift+Alt+9");
                this.hotKey1.AddHotKey("Control+Shift+E");
                
                //function keys
                this.hotKey1.AddHotKey("F2");
                this.hotKey1.AddHotKey("F7");

                //function keys with keymodifiers
                this.hotKey1.AddHotKey("Shift+F1");

                //other keys[Home,End,PageDown,PageUp and Delete]
                this.hotKey1.AddHotKey("Shift+End");
                this.hotKey1.AddHotKey("Alt+Delete");

                //windows keys with normal keys and keymodifiers
                //hot key with windows key combination not working properly,so please avoid to use this conbination or try fix the issue and use it(don't forget to share information if you fixed the issue).
                this.hotKey1.AddHotKey("RWin+D"); //Right windows key
                this.hotKey1.AddHotKey("Control+RWin+S");
                this.hotKey1.AddHotKey("LWin+S"); //Left windows key

                //remove all keys
                //this.hotKey1.RemoveAllKeys();
                //remove specific key
                //this.hotKey1.RemoveKey("Shift+End");
            }
            catch (Exception exErr) { MessageBox.Show(exErr.Message); }

        }
    }
}
