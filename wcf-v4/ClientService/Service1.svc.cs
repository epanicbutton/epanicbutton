﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace ClientService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class Service1 : IService1
    {
        private int nextId = 1;
        private List<NodeData> nodedata = new List<NodeData>();
        private List<UserData> userdata = new List<UserData>();
        private List<ButtonData> buttondata = new List<ButtonData>();

        DataClass dc = new DataClass();


        public List<NodeData> GetUserInfo(string clientGUID, string machineGUID)
        {

            //c = client.clientguid
            //m = node.machineguid

            //string clientGUID = "695A06E3-5E87-458A-B902-C58450554E70";
            //string machineGUID = "F4355035-BF68-4CD9-93F5-828CA477C7E5";

            string strQuery = "exec uspGetByGUID @clientguid, @machineguid";
            SqlCommand cmd = new SqlCommand(strQuery);
            cmd.Parameters.Add("@clientguid", SqlDbType.NVarChar).Value = clientGUID;
            cmd.Parameters.Add("@machineguid", SqlDbType.NVarChar).Value = machineGUID;

            NodeData nd = new NodeData();
            DataSet ds = dc.GetDataSet(cmd);
            DataTable dtUser = ds.Tables[0];

            foreach (DataRow dr in dtUser.Rows)
            {
                AddUserData(new UserData
                {
                    ClientName = dr["ClientName"].ToString(),
                    GroupName = dr["GroupName"].ToString(),
                    NodeName = dr["NodeName"].ToString(),
                    NodeMachineName = dr["NodeMachineName"].ToString(),
                    MacAddress = dr["MacAddress"].ToString(),
                    IPAddress = dr["IPAddress"].ToString(),
                    UserLastName = dr["UserLastName"].ToString(),
                    UserFirstName = dr["UserFirstName"].ToString()

                });
            }

            DataTable dtButton = ds.Tables[1];
            nextId = 1;

            foreach (DataRow dr in dtButton.Rows)
            {
                AddButtonData(new ButtonData
                {
                    NodeId = dr["NodeId"].ToString(),
                    ButtonId = dr["ButtonId"].ToString(),
                    ButtonName = dr["ButtonName"].ToString(),
                    IconIndex = dr["IconIndex"].ToString(),
                    HotKey = dr["HotKey"].ToString(),
                    HotKeyEnabled = dr["HotKeyEnabled"].ToString(),
                    HotKeyCode = dr["HotKeyCode"].ToString(),
                    HotKeyModifier = dr["HotKeyModifier"].ToString()
                });
            }

            nd.nodebuttondata = buttondata;
            nd.nodeuserdata = userdata;
            nodedata.Add(nd);

            return nodedata;
          
        }       
     

        public bool SetUserInfo(string clientGUID, string machineGUID, string FirstName, string LastName)
        {
            return false;
        }

        public ButtonData AddButtonData(ButtonData d)
        {
            if (d == null)
            {
                throw new ArgumentNullException("d");
            }

            d.Id = nextId++;
            buttondata.Add(d);
            return d;
        }

        public UserData AddUserData(UserData d)
        {
            if (d == null)
            {
                throw new ArgumentNullException("d");
            }

            d.Id = nextId++;
            userdata.Add(d);
            return d;
        }


    }
}
