﻿using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace ClientService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "Iepbws" in both code and config file together.
    [ServiceContract]
    public interface Iepbws
    {
        [OperationContract]
        [WebInvoke(Method = "GET",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "UserData")]

        List<NodeData> GetUserInfo(string clientGUID, string machineGUID);
        //68A033B4-C3A3-45EB-8A4E-11815937DEC0
        //3C798235-83FC-4A87-A23F-0DCA0294FC33

        [OperationContract]
        bool SetUserInfo(UpdateUserData updateuserdata);

        [OperationContract]
        bool RemUserInfo(string machineGUID);

        [OperationContract]
        bool EventProcess(int eventType, string events);

        [OperationContract]
        bool SendSMS(string toNumber);


    }
}
