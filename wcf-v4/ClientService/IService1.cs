﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace ClientService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IService1
    {
        [OperationContract]
        List<NodeData> GetUserInfo(string clientGUID, string machineGUID);

        [OperationContract]
        bool SetUserInfo(string clientGUID, string machineGUID, string FirstName, string LastName);

        // TODO: Add your service operations here
    }

}
