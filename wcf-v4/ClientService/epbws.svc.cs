﻿using MessageMedia.Api;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace ClientService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "epbws" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select epbws.svc or epbws.svc.cs at the Solution Explorer and start debugging.
    public class epbws : Iepbws
    {
        private int nId = 1, bId = 1;
        private string buttonids = string.Empty;
        private List<NodeData> nodedata = new List<NodeData>();
        private List<UserData> userdata = new List<UserData>();
        private List<ButtonData> buttondata = new List<ButtonData>();
        private UpdateUserData ud = new UpdateUserData();

        DataClass dc = new DataClass();
        LogHandler lh = new LogHandler();
        ExternalMessage em = new ExternalMessage();

        private static bool logtype = bool.Parse(ConfigurationManager.AppSettings["LogVerbose"]);

        public List<ClientService.NodeData> GetUserInfo(string clientGUID, string machineGUID)
        {
            if (logtype)
            {
                lh.LogActivity("epbws", "Iepbws", "GetUserInfo", "GetUserInfo called by clientGUID:" + clientGUID + " | machineGUID:" + machineGUID);
            }

            try
            {
                // if (dc.TestClientGuid(clientGUID))
                // {
                #region svcCall
                string strQuery = "exec uspGetByGUID @clientguid, @machineguid";
                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.Parameters.Add("@clientguid", SqlDbType.NVarChar).Value = clientGUID;
                cmd.Parameters.Add("@machineguid", SqlDbType.NVarChar).Value = machineGUID;

                NodeData nd = new NodeData();
                DataSet ds = dc.GetDataSet(cmd);
                DataTable dtUser = ds.Tables[0];
                DataTable dtButton = ds.Tables[1];
                //DataTable dtButtons = ds.Tables[2];

                if (dtUser.Rows.Count > 0)
                {
                    buttonids = dtUser.Rows[0]["ButtonIDs"].ToString();
                }

                foreach (DataRow dr in dtButton.Rows)
                {
                    AddButtonData(new ButtonData
                    {
                        NodeId = dr["NodeId"].ToString(),
                        ButtonId = dr["ButtonId"].ToString(),
                        ButtonName = dr["ButtonName"].ToString(),
                        AlertTitle = dr["AlertTitle"].ToString(),
                        AlertMessage = dr["AlertMessage"].ToString(),
                        Severity = dr["Severity"].ToString(),
                        AudibleAlarmLevel = dr["AudibleAlarmLevel"].ToString(),
                        IconIndex = dr["IconIndex"].ToString(),
                        HotKey = dr["HotKey"].ToString(),
                        HotKeyEnabled = dr["HotKeyEnabled"].ToString(),
                        HotKeyCode = dr["HotKeyCode"].ToString(),
                        HotKeyModifier = dr["HotKeyModifier"].ToString(),
                        SendDelay = dr["SendDelay"].ToString(),
                        Mute = dr["Mute"].ToString()
                    });
                }

                foreach (DataRow dr in dtUser.Rows)
                {
                    AddUserData(new UserData
                    {
                        ClientName = dr["ClientName"].ToString(),
                        GroupName = dr["GroupName"].ToString(),
                        GroupId = dr["GroupId"].ToString(),
                        NodeId = dr["NodeId"].ToString(),
                        NodeName = dr["NodeName"].ToString(),
                        NodeDescription = dr["NodeDescription"].ToString(),
                        NodeMachineName = dr["NodeMachineName"].ToString(),
                        MacAddress = dr["MacAddress"].ToString(),
                        IPAddress = dr["IPAddress"].ToString(),
                        UserLastName = dr["UserLastName"].ToString(),
                        UserFirstName = dr["UserFirstName"].ToString(),
                        UserDepartment = dr["UserDepartment"].ToString(),
                        UserEmail = dr["UserEmail"].ToString(),
                        UserLocation = dr["UserLocation"].ToString(),
                        UserTelephone = dr["UserTelephone"].ToString(),
                        UserSelfTestInterval = dr["UserSelfTestInterval"].ToString(),
                        UserEnableSelfTest = dr["UserEnableSelfTest"].ToString(),
                        RemoteCommandsProcessingInterval = dr["RemoteCommandsProcessingInterval"].ToString(),
                        ClientProfileRefreshEnabled = dr["ClientProfileRefreshEnabled"].ToString(),
                        ClientUnloadingEnabled = dr["ClientUnloadingEnabled"].ToString(),
                        AllowsUserToChangeProfileEnabled = dr["AllowsUserToChangeProfileEnabled"].ToString(),
                        AllowMoveEnabled = dr["AllowMoveEnabled"].ToString(),
                        CreateDate = dr["CreateDate"].ToString(),
                        buttons = buttondata,
                        ButtonIDs = buttonids,
                        MQHostName = dr["MQHostName"].ToString(),
                        RequestExchangeName = dr["RequestExchangeName"].ToString(),
                        ResponseExchangeName = dr["ResponseExchangeName"].ToString(),
                        VHostName = dr["VHostName"].ToString(),
                        MQPort = dr["MQPort"].ToString()
                    });
                }
                nd.nodeuserdata = userdata;
                nodedata.Add(nd);
                #endregion
                //  }
            }

            catch (Exception ex)
            {
                ErrHandler eh = new ErrHandler();
                string method = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();
                lh.LogActivity("epbws", "GetUserInfo", ex.Message, ex.StackTrace);
            }

            return nodedata;
        }

        public bool SetUserInfo(UpdateUserData ud)
        {
            string clientGUID = ud.ClientGuid.ToString();
            string ip = ud.MachineIp.ToString();
            string mac = ud.MacAddress.ToString();
            string machineGUID = ud.MachineGuid.ToString();
            string osversion = ud.OsVersion.ToString();
            string ospatch = ud.ServicePack.ToString();
            string firstname = ud.FirstName.ToString();
            string lastname = ud.LastName.ToString();
            string location = ud.Location.ToString();
            string department = ud.Department.ToString();
            string phone = ud.Telephone.ToString();
            string email = ud.EmailAddress.ToString();
            string version = ud.ApplicationVersion.ToString();
            string machineName = ud.MachineName.ToString();
            string username = ud.UserName.ToString();

            try
            {
                string strQuery = "exec uspSetByGUID @clientguid, @machineguid, @ip, @mac, @osversion, @ospatch, @firstname, @lastname, @location, @department, @phone, @email, @version, @machineName, @username";
                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.Parameters.Add("@clientguid", SqlDbType.NVarChar).Value = clientGUID;
                cmd.Parameters.Add("@machineguid", SqlDbType.NVarChar).Value = machineGUID;
                cmd.Parameters.Add("@ip", SqlDbType.NVarChar).Value = ip;
                cmd.Parameters.Add("@mac", SqlDbType.NVarChar).Value = mac;
                cmd.Parameters.Add("@osversion", SqlDbType.NVarChar).Value = osversion;
                cmd.Parameters.Add("@ospatch", SqlDbType.NVarChar).Value = ospatch;
                cmd.Parameters.Add("@firstname", SqlDbType.NVarChar).Value = firstname;
                cmd.Parameters.Add("@lastname", SqlDbType.NVarChar).Value = lastname;
                cmd.Parameters.Add("@location", SqlDbType.NVarChar).Value = location;
                cmd.Parameters.Add("@department", SqlDbType.NVarChar).Value = department;
                cmd.Parameters.Add("@phone", SqlDbType.NVarChar).Value = phone;
                cmd.Parameters.Add("@email", SqlDbType.NVarChar).Value = email;
                cmd.Parameters.Add("@version", SqlDbType.NVarChar).Value = version;
                cmd.Parameters.Add("@machineName", SqlDbType.NVarChar).Value = machineName;
                cmd.Parameters.Add("@username", SqlDbType.NVarChar).Value = username;

                bool success = dc.WriteData(cmd);
            }


            catch (Exception ex)
            {
                ErrHandler eh = new ErrHandler();
                string method = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();
                eh.ErrorCatch(this.ToString(), "epbws", method, ex.Message, ex.StackTrace);
                lh.LogActivity("epbws", "SetUserInfo", ex.Message, ex.StackTrace);

                return false;
            }

            return true;
        }

        public bool EventProcess(int eventtype, string events)
        {
            //1 = alert button pressed: clientGuid, userId, buttonId            

            string clientGuid = string.Empty;
            string userid = string.Empty;
            string buttonid = string.Empty;
            bool result = false;


            //christopher.ieng@messagemedia.com.au

            try
            {
                Dictionary<string, string> resultDictionary = events.ToValueByKey('|', '"');
                //http://www.codeproject.com/Articles/310451/Passing-a-set-of-name-value-pairs-to-a-service-met

                switch (eventtype)
                {
                    case 1:  //an alert button was clicked

                        if (resultDictionary.ContainsKey("clientGuid"))
                        {
                            clientGuid = resultDictionary["clientGuid"];
                        }
                        if (resultDictionary.ContainsKey("userId"))
                        {
                            userid = resultDictionary["userId"];
                        }
                        if (resultDictionary.ContainsKey("buttonId"))
                        {
                            buttonid = resultDictionary["buttonId"];
                        }

                        if ((userid != string.Empty) && (buttonid != string.Empty) && (clientGuid != string.Empty))
                        {
                            AlertSender sender = new AlertSender();
                            sender.clientGuid = clientGuid.ToString();
                            sender.userId = userid.ToString();
                            sender.buttonId = buttonid.ToString();

                            ExternalMessage em = new ExternalMessage();
                            result = em.SendExternalMessage(sender);
                            if (logtype)
                            {
                                lh.LogActivity("epbws", "Iepbws", "EventProcess:Alert Event", "EventProcess called by clientGuid:" + clientGuid + " usserid:" + userid + " | buttonid:" + buttonid);
                                lh.LogEvent(sender);
                            }
                        }

                        else
                        {
                            em.ePanicAdminAlert(clientGuid);
                            ErrHandler eh = new ErrHandler();
                            eh.ErrorCatch("epbws", "1", "EventProcess", "ClientGuid, User or Button was blank. clientGuid: " + clientGuid + " | userid: " + userid + " | buttonid: " + buttonid, "");
                            result = false;
                        }

                        break;

                    default:
                        if (logtype)
                        {//log that we got here but do nothing
                            lh.LogActivity("epbws", "Iepbws", "EventProcess:Undefined Event", "EventProcess called by usserid:" + userid + " | buttonid:" + buttonid);
                        }
                        break;

                }

            }
            catch (Exception ex)
            {
                em.ePanicAdminAlert(clientGuid);

                ErrHandler eh = new ErrHandler();
                string method = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();
                eh.ErrorCatch(this.ToString(), "epbws", method, ex.Message, ex.StackTrace);
                lh.LogActivity("epbws", "EventProcess", ex.Message, ex.StackTrace);

                result = false;
            }

            return result;

        }

        public bool SendSMS(string toNumber)
        {

            //IRestResponse me = SendSimpleMessage();
            //return true;

            bool sendresult = false;
            if (logtype)
            {
                lh.LogActivity("epbws", "Iepbws", "SendSMS", "SendSMS request to: " + toNumber);
            }
            try
            {
                string time = System.DateTime.Now.ToLongTimeString();
                string date = System.DateTime.Now.ToLongDateString();
                MessageMediaSoapClient client = new MessageMediaSoapClient("ePanicButton003", "h8Q67j83");
                var result = client.SendMessage("13366014982", toNumber, "Simple SMS Send from ePanic Alert System [MM]. Date: " + date + " Time: " + time, 11111);
                sendresult = true;

            }

            catch (Exception ex)
            {
                ErrHandler eh = new ErrHandler();
                string method = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();
                eh.ErrorCatch(this.ToString(), "epbws", method, ex.Message, ex.StackTrace);
                lh.LogActivity("epbws", this.ToString(), ex.Message, ex.StackTrace);
                sendresult = false;
            }

            return sendresult;

        }

        private UserData AddUserData(UserData d)
        {
            if (d == null)
            {
                throw new ArgumentNullException("d");
            }

            d.Id = nId++;
            userdata.Add(d);
            return d;
        }

        private ButtonData AddButtonData(ButtonData d)
        {
            if (d == null)
            {
                throw new ArgumentNullException("d");
            }

            d.Id = bId++;
            buttondata.Add(d);
            return d;
        }

        public static IRestResponse SendSimpleMessage()
        {
            try
            {
                RestClient client = new RestClient();
                client.BaseUrl = "https://api.mailgun.net/v2";
                client.Authenticator =
                        new HttpBasicAuthenticator("api",
                                                   "key-1pn00e50pzbzmxfg8-0eb57zso2g1xl6");
                RestRequest request = new RestRequest();
                request.AddParameter("domain",
                                     "rs90224.mailgun.org", ParameterType.UrlSegment);
                request.Resource = "{domain}/messages";
                request.AddParameter("from", "Excited User <andrew.wing@epanicbutton.com>");
                request.AddParameter("to", "andrew.wing@wingweb.com");
                request.AddParameter("to", "drew.wing@gmail.com");
                request.AddParameter("subject", "Hello");
                request.AddParameter("text", "Testing some Mailgun awesomness!");
                request.Method = Method.POST;
                return client.Execute(request);
            }
            catch (Exception x)
            {
                string e = x.Message.ToString();
                return null;

            }
        }

        public bool RemUserInfo(string machineGUID)
        {
            bool result = false;
            try
            {
                string strQuery = "exec uspRemUserInfo @machineguid";
                SqlCommand cmd = new SqlCommand(strQuery);                
                cmd.Parameters.Add("@machineguid", SqlDbType.NVarChar).Value = machineGUID;

                NodeData nd = new NodeData();
                result = dc.ExecSql(cmd);                

            }
            catch (Exception ex)
            {
                result = false;
                ErrHandler eh = new ErrHandler();
                string method = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();
                eh.ErrorCatch(this.ToString(), "epbws", method, ex.Message, ex.StackTrace);
                lh.LogActivity("epbws", "EventProcess", ex.Message, ex.StackTrace);

            }

            return result;

        }
    }
}


