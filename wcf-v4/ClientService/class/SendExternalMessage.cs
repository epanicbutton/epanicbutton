﻿using MessageMedia.Api;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Net.Mail;

namespace ClientService
{
    public class ExternalMessage
    {
        DataClass dc = new DataClass();
        LogHandler lh = new LogHandler();

        private static string userId = ConfigurationManager.AppSettings.Get("Username");
        private static string password = ConfigurationManager.AppSettings.Get("Password");

        private static string sentFromNumber = ConfigurationManager.AppSettings.Get("SentFromNumber");
        private static string sentToNumber = ConfigurationManager.AppSettings.Get("SentToNumber");

        private static string mailgunKey = (ConfigurationManager.AppSettings["MailGunPriKey"]);
        private static string mailgunDomain = (ConfigurationManager.AppSettings["MailGunDomain"]);


        public class PanicMessage
        {
            public int id { get; set; }
            public string to { get; set; }
            public string firstname { get; set; }
            public string lastname { get; set; }
            public string department { get; set; }
            public string location { get; set; }
            public string phone { get; set; }
            public string useremail { get; set; }
            public string subject { get; set; }
            public string message { get; set; }
            public string severity { get; set; }
            public string cc { get; set; }
            public List<string> bcc { get; set; }
            public List<string> sms { get; set; }
            public int hasattachment { get; set; }
            public object attachment { get; set; }
        }

        public void ePanicAdminAlert(string clientGuid)
        {
            //fires when a SMS or Email alert is error trapped.  This is considered a catestrophic 
            //error due to the fact that an error occurred and a message may not have been delivered.
            //todo - make this a dynamic list of numbers
            string sentFromNumber = ConfigurationManager.AppSettings.Get("SentFromNumber");
            string sentToNumber1 = ConfigurationManager.AppSettings.Get("AdminNumber1");
            string sentToNumber2 = ConfigurationManager.AppSettings.Get("AdminNumber2");
            string sentToNumber3 = ConfigurationManager.AppSettings.Get("AdminNumber3");
            string messageTxt = "Critical Failure Message From ePanicService ClientGuid: " + clientGuid;

#if DEBUG
            messageTxt = "TEST TEST Critical Failure Message From ePanicService ClientGuid: " + clientGuid;
#endif

            try
            {
               // uint messageId = 999999999;

                //var result = client.SendMessage(sentFromNumber, sentToNumber1, messageTxt, messageId);
                //result = client.SendMessage(sentFromNumber, sentToNumber2, messageTxt, messageId);
                //result = client.SendMessage(sentFromNumber, sentToNumber3, messageTxt, messageId);
            }
            catch (SystemException)
            {
                string smtp, addr, uid, pwd;
                smtp = (ConfigurationManager.AppSettings["Mail.Server"]);
                addr = (ConfigurationManager.AppSettings["Mail.FromEmail"]);
                uid = (ConfigurationManager.AppSettings["Mail.User"]);
                pwd = (ConfigurationManager.AppSettings["Mail.Pwd"]);

                // to += ",jlee@peaceatwork.org,ganderson@nyasoft.com";

                SmtpClient smtpClient = new SmtpClient();
                NetworkCredential basicCredential = new NetworkCredential(uid, pwd);
                MailMessage message = new MailMessage();
                MailAddress fromAddress = new MailAddress(addr);

                smtpClient.Host = smtp;
                smtpClient.UseDefaultCredentials = false;
                smtpClient.Credentials = basicCredential;

                message.From = fromAddress;
                message.Subject = messageTxt;
                //Set IsBodyHtml to true means you can send HTML email.
                message.IsBodyHtml = false;

                //format the message
                string messagebody = messageTxt;

                message.Body = messagebody;
                //message.CC.Add("andrew.wing@wingweb.com");
                message.CC.Add("jlee@peaceatwork.org");
                message.CC.Add("andrew.wing@wingweb.com");
                //message.Bcc.Add(panicemailmessage.bcc);

                message.Priority = MailPriority.High;

                smtpClient.Send(message);
            }
            catch (Exception ex)
            {
                ErrHandler eh = new ErrHandler();
                string method = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();
                eh.ErrorCatch(this.ToString(), "epbws", method, ex.Message, ex.StackTrace);
            }
        }

        public bool SendExternalMessage(AlertSender sender)
        {
            bool sendresult = false;
            string date = System.DateTime.Now.ToShortDateString();
            string time = " - " + System.DateTime.Now.ToShortTimeString();

            PanicMessage panicsmsmessage = GetSMSReceivers(sender);
            PanicMessage panicemailmessage = GetEmailReceivers(sender);

            #region sms
            if (panicsmsmessage.sms != null)
            {
                try
                {
                    int smscount = panicsmsmessage.sms.Count;
                    uint messageid = 123456789;

                    string messageHdr = "ePanic Alert\r";
                    string message = messageHdr + panicsmsmessage.subject.ToString() + "\r";
                    message += panicsmsmessage.message.ToString() + "\r";    
                    message += panicsmsmessage.firstname + " " + panicsmsmessage.lastname + "\r";
                    message += "Loc: " + panicsmsmessage.location + "\r";
                    message += "Dept: " + panicsmsmessage.department + "\r";
                    message += "Phone: ";
                    message += panicsmsmessage.phone + "\r";

                    if (message == null)
                    {
                        //badness occurred
                    }

                    MessageMediaSoapClient client = new MessageMediaSoapClient(userId, password);

                    var result = client.SendMessages(sentFromNumber, panicsmsmessage.sms, message, messageid);
                    //for (int x = 0; x < smscount;x++ )
                    //{                        
                    //    var result = client.SendMessage(sentFromNumber, panicsmsmessage.sms[x].ToString(), message, messageid);

                    //    lh.LogActivity("epbws", "Iepbws", "SendExternalMessage", "SendExternalMessage(" + panicsmsmessage.sms[x].ToString() + ")");

                    //}
                    sendresult = true;

                }
                catch (Exception ex)
                {
                    ErrHandler eh = new ErrHandler();
                    string method = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();
                    eh.ErrorCatch(this.ToString(), "epbws", method, ex.Message, ex.StackTrace);
                    sendresult = false;
                }
            }


            #endregion

            #region email
            if (panicemailmessage.to != null)
            {               

                string subject = panicemailmessage.subject;

                //format the message
                string messagebody = panicemailmessage.message + "           \r\n \r\n";

                messagebody += panicemailmessage.firstname + " " + panicemailmessage.lastname + " has pressed the ";
                messagebody += panicemailmessage.subject + " button!                  \r\n \r\n";
                messagebody += "Location: " + panicemailmessage.location + "           \r\n \r\n";
                messagebody += "Department: " + panicemailmessage.department + "           \r\n \r\n";
                messagebody += "Phone: " + panicemailmessage.phone + "           \r\n \r\n";
                messagebody += "Email: " + panicemailmessage.useremail + "           \r\n \r\n";

                try
                {
                    SendAPIMessage(panicemailmessage, messagebody, subject);                   
                    sendresult = true;
                }
                catch (Exception ex)
                {
                    //Error, could not send the message
                    //Response.Write(ex.Message);
                    string x = ex.Message.ToString();
                    sendresult = false;
                }
            }
            #endregion

            return sendresult;

        }

        public void SendResetPasswordEmail(string to, string login, string password)
        {
            string subject = ConfigurationManager.AppSettings["Mail.ResetPassword_Subject"] ?? "ePanicService. New temporary password.";
            string msgText = string.Format("You need to change your temporary password after login.\nUsername: {0}\nPassword: {1}", login, password);
            //SendEmail(to, subject, msgText);
        }

        private PanicMessage GetEmailReceivers(AlertSender sender)
        {
            DataTable dt = new DataTable();
            PanicMessage message = new PanicMessage();
            try
            {
                string strQuery = "exec GetButtonEmails @clientguid,  @usersid, @buttonsid ";
                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.Parameters.Add("@clientguid", SqlDbType.VarChar).Value = sender.clientGuid.ToString();
                cmd.Parameters.Add("@usersid", SqlDbType.BigInt).Value = Convert.ToInt32(sender.userId.ToString());
                cmd.Parameters.Add("@buttonsid", SqlDbType.BigInt).Value = Convert.ToInt32(sender.buttonId.ToString());

                dt = dc.GetDataTable(cmd);

                if (dt.Rows.Count > 0)
                {
                    List<string> bcc = new List<string>();

                    message.firstname = dt.Rows[0]["UserFirstName"].ToString().TrimEnd();
                    message.lastname = dt.Rows[0]["UserLastName"].ToString().TrimEnd();
                    message.department = dt.Rows[0]["UserDepartment"].ToString().TrimEnd();
                    message.location = dt.Rows[0]["UserLocation"].ToString().TrimEnd();
                    message.useremail = dt.Rows[0]["UserEmail"].ToString().TrimEnd();
                    message.phone = dt.Rows[0]["UserTelephone"].ToString().TrimEnd();
                    message.subject = dt.Rows[0]["ButtonName"].ToString().TrimEnd();
                    message.message = dt.Rows[0]["ButtonMessage"].ToString().TrimEnd();
                    message.severity = dt.Rows[0]["Severity"].ToString().TrimEnd();
                    message.to = "alert@epanicbutton.com";


                    foreach (DataRow row in dt.Rows)
                    {
                        bcc.Add(row["ToEmail"].ToString().TrimEnd());
                    }
                    message.bcc = bcc;
                }
            }

            catch (Exception ex)
            {
                ErrHandler eh = new ErrHandler();
                string method = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();
                eh.ErrorCatch(this.ToString(), "epbws", method, ex.Message, ex.StackTrace);
                message = null;
            }

            return message;

        }

        private PanicMessage GetSMSReceivers(AlertSender sender)
        {
            DataTable dt = new DataTable();
            PanicMessage message = new PanicMessage();
            try
            {
                string strQuery = "exec GetButtonSMS @clientguid, @usersid, @buttonsid ";
                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.Parameters.Add("@clientguid", SqlDbType.VarChar).Value = sender.clientGuid.ToString();
                cmd.Parameters.Add("@usersid", SqlDbType.BigInt).Value = Convert.ToInt32(sender.userId.ToString());
                cmd.Parameters.Add("@buttonsid", SqlDbType.BigInt).Value = Convert.ToInt32(sender.buttonId.ToString());

                dt = dc.GetDataTable(cmd);

                if (dt.Rows.Count > 0)
                {
                    List<string> sms = new List<string>();

                    message.firstname = dt.Rows[0]["UserFirstName"].ToString().TrimEnd();
                    message.lastname = dt.Rows[0]["UserLastName"].ToString().TrimEnd();
                    message.department = dt.Rows[0]["UserDepartment"].ToString().TrimEnd();
                    message.location = dt.Rows[0]["UserLocation"].ToString().TrimEnd();
                    message.useremail = dt.Rows[0]["UserEmail"].ToString().TrimEnd();
                    message.phone = dt.Rows[0]["UserTelephone"].ToString().TrimEnd();
                    message.subject = dt.Rows[0]["ButtonTitle"].ToString().TrimEnd();
                    message.message = dt.Rows[0]["ButtonMessage"].ToString().TrimEnd();
                    message.severity = dt.Rows[0]["Severity"].ToString().TrimEnd();

                    foreach (DataRow row in dt.Rows)
                    {
                        sms.Add(row["ToSMS"].ToString().TrimEnd());
                    }
                    message.sms = sms;
                }
            }

            catch (Exception ex)
            {
                ErrHandler eh = new ErrHandler();
                string method = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();
                eh.ErrorCatch(this.ToString(), "epbws", method, ex.Message, ex.StackTrace);
                message = null;
            }

            return message;

        }

        public static IRestResponse SendAPIMessage(PanicMessage panicemailmessage, string Message, string Subject)
        {
        
            try
            {
                RestClient client = new RestClient();
                client.BaseUrl = "https://api.mailgun.net/v2";
                client.Authenticator = new HttpBasicAuthenticator("api", mailgunKey);
                RestRequest request = new RestRequest();
                request.AddParameter("domain", mailgunDomain, ParameterType.UrlSegment);
                request.Resource = "{domain}/messages";
                request.AddParameter("from", "ePanic Button Alert <alert@epanicbutton.com>");
                request.AddParameter("to", "alert@epanicbutton.com");
                
                foreach (string email in panicemailmessage.bcc)
                    request.AddParameter("bcc", email);                
                
                request.AddParameter("subject", Subject);
                request.AddParameter("text", Message);
                request.Method = Method.POST;
                return client.Execute(request);
            }
            catch (Exception x)
            {
                string e = x.Message.ToString();
                return null;
            }
        }
    }
}