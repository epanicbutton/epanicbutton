﻿using System.Collections.Generic;

namespace ClientService
{
    public class NodeData
    {
        public List<UserData> nodeuserdata { get; set; }
    }

    public class UpdateUserData
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Telephone { get; set; }
        public string EmailAddress { get; set; }
        public string Department { get; set; }
        public string Location { get; set; }
        public string MachineName { get; set; }
        public string MachineIp { get; set; }
        public string MacAddress { get; set; }
        public string MachineGuid { get; set; }
        public string ClientGuid { get; set; }
        public string ApplicationVersion { get; set; }
        public string OsVersion { get; set; }
        public string ServicePack { get; set; }
        public string UserName { get; set; }
    }

    public class UserData
    {
        public int Id { get; set; }
        public string ClientName { get; set; }
        public string GroupName { get; set; }
        public string GroupId { get; set; }
        public string NodeId { get; set; }
        public string NodeName { get; set; }
        public string NodeDescription { get; set; }
        public string NodeMachineName { get; set; }
        public string MacAddress { get; set; }
        public string IPAddress { get; set; }
        public string UserLastName { get; set; }
        public string UserFirstName { get; set; }
        public string UserDepartment { get; set; }
        public string UserEmail { get; set; }
        public string UserLocation { get; set; }
        public string UserTelephone { get; set; }
        public string UserSelfTestInterval { get; set; }
        public string UserEnableSelfTest { get; set; }
        public string RemoteCommandsProcessingInterval { get; set; }
        public string ClientProfileRefreshEnabled { get; set; }
        public string ClientUnloadingEnabled { get; set; }
        public string AllowsUserToChangeProfileEnabled { get; set; }
        public string AllowMoveEnabled { get; set; }
        public string CreateDate { get; set; }
        public List<ButtonData> buttons { get; set; }
        public string ButtonIDs { get; set; }
        public string MQHostName { get; set; }
        public string RequestExchangeName { get; set; }
        public string ResponseExchangeName { get; set; }
        public string VHostName { get; set; }
        public string MQPort { get; set; }
        
    }

    public class ButtonData
    {
        public int Id { get; set; }
        public string NodeId { get; set; }
        public string ButtonId { get; set; }
        public string ButtonName { get; set; }
        public string AlertTitle { get; set; }
        public string AlertMessage { get; set; }
        public string Severity { get; set; }
        public string AudibleAlarmLevel { get; set; }
        public string IconIndex { get; set; }
        public string HotKey { get; set; }
        public string HotKeyEnabled { get; set; }
        public string HotKeyCode { get; set; }
        public string HotKeyModifier { get; set; }
        public string SendDelay { get; set; }
        public string Mute { get; set; }
    }

    public class ReceiverData
    {
        public int Id { get; set; }
        public string ButtonId { get; set; }
        public string AlertId { get; set; }        
        public string ReceiverIP { get; set; }        
        public string ReceiverNodeName { get; set; }
        public string ReceiverMachineName { get; set; }        
    }

    public class EmailData
    {
        public int Id { get; set; }
        public string NodeId { get; set; }
        public string ButtonId { get; set; }
        public string SendTo { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        public string Severity { get; set; }
    }
}