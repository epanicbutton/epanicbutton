﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Web;

namespace ClientService
{
    public class LogHandler
    {
        public void LogActivity(string sApp, string sUser, string sCategory, string sMessageDetail)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
                {
                    SqlCommand cmd = new SqlCommand();

                    cmd.Connection = conn;

                    conn.Open();

                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "ApplicationActivityLog";
                    cmd.Parameters.Add("@AppName", SqlDbType.NVarChar, 60).Value = sApp;
                    cmd.Parameters.Add("@UserIdName", SqlDbType.NVarChar, 50).Value = sUser;
                    cmd.Parameters.Add("@Category", SqlDbType.NVarChar, 50).Value = sCategory;
                    cmd.Parameters.Add("@MessageDetails", SqlDbType.NVarChar).Value = sMessageDetail;

                    // Execute the command. 
                    cmd.ExecuteNonQuery();
                }
            }
            catch (System.Exception x)
            {
                //cant log to database
                string err = x.Message.ToString();
                ErrorWrite(err);
            }
        }

        public void ErrorWrite(string sErr)
        {
            string file = HttpContext.Current.Server.MapPath("\\logs\\epanicsvcerror.txt");
            string hdr = "===============================================================";
            StreamWriter writer = new StreamWriter(file, true);
            writer.WriteLine(hdr);
            writer.WriteLine("Exception Date/Time = " + System.DateTime.Now.ToLocalTime().ToString());
            writer.WriteLine("Exception = " + sErr.ToString());

            writer.Close();
        }

        public void LogEvent(AlertSender sender)
        {

            try
            {
                DataClass dc = new DataClass();
                int userid = Convert.ToInt32(sender.userId.ToString());
                int buttonid = Convert.ToInt32(sender.buttonId.ToString());
                string query = "GetSingleUser @usersid, @buttonsid;";
                SqlCommand cmd = new SqlCommand(query);
                cmd.Parameters.Add("@usersid", SqlDbType.BigInt).Value = userid;
                cmd.Parameters.Add("@buttonsid", SqlDbType.BigInt).Value = buttonid;

                DataTable dt = dc.GetDataTable(cmd);
                string firstname = dt.Rows[0]["UserFirstName"].ToString();
                string lastname = dt.Rows[0]["UserLastName"].ToString();
                int clientsid = Convert.ToInt32(dt.Rows[0]["ClientsId"].ToString());
                int adminsid = Convert.ToInt32(dt.Rows[0]["AdminsId"].ToString());
                string clientname = dt.Rows[0]["ClientName"].ToString();
                string machinename = dt.Rows[0]["MachineName"].ToString();
                string buttonname = dt.Rows[0]["ButtonName"].ToString();
                string severity = dt.Rows[0]["Severity"].ToString();

                dc.InsertEvent(clientsid, adminsid, "Alert process triggered by " + firstname + " " + lastname + ".  Button name: " + buttonname + " / Severity: " + severity);

            }
            catch (System.Exception x)
            {
                //cant log to database
                string err = x.Message.ToString();
                ErrorWrite(err);
            }

        }
    }
}