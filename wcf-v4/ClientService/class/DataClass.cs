﻿using ClientService;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

public class DataClass
{
    public DataSet GetDataSet(SqlCommand cmd)
    {
        using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
        {
            //go get the form info and data 
            DataSet ds = new DataSet();
            SqlDataAdapter sda = new SqlDataAdapter();
            cmd.CommandType = CommandType.Text;
            cmd.Connection = conn;

            sda.SelectCommand = cmd;
            sda.Fill(ds);

            return ds;
        }
    }

    public DataTable GetDataTable(SqlCommand cmd)
    {
        using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
        {
            //go get the form info and data 
            DataTable dt = new DataTable();
            SqlDataAdapter sda = new SqlDataAdapter();
            cmd.CommandType = CommandType.Text;
            cmd.Connection = conn;

            sda.SelectCommand = cmd;
            sda.Fill(dt);

            return dt;
        }
    }

    public bool WriteData(SqlCommand cmd)
    {
        bool result = false;
        using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
        {

            cmd.Connection = conn;
            conn.Open();

            DataTable dt = new DataTable();
            SqlDataAdapter sda = new SqlDataAdapter();
            cmd.CommandType = CommandType.Text;
            cmd.Connection = conn;

            sda.SelectCommand = cmd;
            cmd.ExecuteNonQuery();

            result = true;
        }

        return result;
    }

    public void InsertEvent(int iClient, int iAdmin, string sDesc)
    {
        using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
        {
            string cmd = "exec ApplicationEventLog @clientsid, @adminid, @desc, @createdon";
            conn.Open();
            SqlCommand sqlCmd = new SqlCommand(cmd, conn);

            sqlCmd.Parameters.AddWithValue("@clientsid", iClient);
            sqlCmd.Parameters.AddWithValue("@adminid", iAdmin);
            sqlCmd.Parameters.AddWithValue("@desc", sDesc);
            sqlCmd.Parameters.AddWithValue("@createdon", DateTime.UtcNow);

            SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);

            sqlCmd.ExecuteNonQuery();
        }
    }


    public bool ExecSql(SqlCommand cmd)
    {
        bool logtype = bool.Parse(ConfigurationManager.AppSettings["LogVerbose"]);
        bool result = false;
        try
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
            {                
                cmd.CommandType = CommandType.Text;
                cmd.Connection = conn;
                conn.Open();
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                cmd.ExecuteNonQuery();
                conn.Close();               

                result = true;
            }
        }
        catch (Exception ex)
        {
            LogHandler lh = new LogHandler();
            lh.LogActivity("epbws", "Iepbws", "ExecSql", ex.Message.ToString());
        }
        return result;
    }



    public bool TestClientGuid(string clientGUID)
    {
        LogHandler lh = new LogHandler();
        bool result = true;
        bool logtype = bool.Parse(ConfigurationManager.AppSettings["LogVerbose"]);

        string strQuery = "exec uspTestClient @clientguid";
        SqlCommand cmd = new SqlCommand(strQuery);
        cmd.Parameters.Add("@clientguid", SqlDbType.NVarChar).Value = clientGUID;

        //client exist?
        DataTable dtClient = GetDataTable(cmd);
        if (dtClient.Rows.Count == 0)
        {
            result = false;
            if (logtype)
            {
                lh.LogActivity("epbws", "Iepbws", "TestClientGuid", "Invalid Client ID:" + clientGUID);
            }
        }

        //client active?
        else
        {
            if (!Convert.ToBoolean(dtClient.Rows[0]["Active"].ToString()))
            {
                result = false;
                if (logtype)
                {
                    lh.LogActivity("epbws", "Iepbws", "TestClientGuid", "Inactive Client ID:" + clientGUID);
                }
            }

        }

        return result;
    }
}