﻿using System.Collections.Generic;
using System.Text;

namespace ClientService
{
    internal static class ExtensionMethods
    {
        /// <summary>
        /// Converts the subject Dictionary into a comma-separated-value string.
        /// </summary>
        /// <param name="valueByKey">The subject Dictionary.</param>
        /// <param name="separator">(Optional) non-default separator character.</param>
        /// <param name="quote">(Optional) quote character in which the value members of name/value pairs should be wrapped.</param>
        /// <returns>A comma-separated-value string that corresponds to the subject Dictionary.</returns>
        internal static string ToCsv(this Dictionary<string, string> valueByKey, char separator = ',', char quote = '\x00')
        {
            StringBuilder sb = new StringBuilder();
            foreach (string key in valueByKey.Keys)
            {
                if (sb.Length > 0)
                {
                    sb.Append(separator);
                }
                if (quote == '\x00')
                {
                    sb.AppendFormat("{0}={1}", key, valueByKey[key]);
                }
                else
                {
                    sb.AppendFormat("{0}={1}{2}{3}", key, quote, valueByKey[key], quote);
                }
            }
            return sb.ToString();
        }

        /// <summary>
        /// Parses a comma-separated-value string into a Dictionary&lt;string, string&gt;.
        /// The separator defaults to comma but a different separator can be specified.
        /// </summary>
        /// <param name="csv">Comma-separated-value string to be parsed.</param>
        /// <param name="separator">(Optional) non-default separator character.</param>
        /// <param name="quote">(Optional) quote character to be trimmed from the value members of name/value pairs.</param>
        /// <returns>A Dictionary<string, string> that corresponds to the subject string.</returns>
        internal static Dictionary<string, string> ToValueByKey(this string csv, char separator = ',', char quote = '\x00')
        {
            Dictionary<string, string> resultDictionary = new Dictionary<string, string>();
            string[] pairs = csv.Split(separator);
            foreach (string pair in pairs)
            {
                string[] nameValue = pair.Split('=');
                if (nameValue.Length == 2)
                {
                    resultDictionary.Add(nameValue[0], nameValue[1].TrimmedOf(quote.ToString()));
                }
            }
            return resultDictionary;
        }

        /// <summary>
        /// Trims the specified character(s) from both ends of the subject string.
        /// </summary>
        /// <param name="subject">The subject string.</param>
        /// <param name="charsToTrim">Character(s) to trim from both ends of the subject string.</param>
        /// <returns>The subject string, trimmed of the specified character(s).</returns>
        internal static string TrimmedOf(this string subject, string charsToTrim)
        {
            return subject.TrimmedOf(charsToTrim, charsToTrim);
        }

        /// <summary>
        /// Trims the specified character(s) from the left end, and a possibly different set of characters from the right end,
        /// of the subject string.
        /// </summary>
        /// <param name="subject">The subject string.</param>
        /// <param name="charsToTrimLeft">Character(s) to trim from the left end of the subject string.</param>
        /// <param name="charsToTrimRight">Character(s) to trim from the right end of the subject string.</param>
        /// <returns>The subject string, trimmed of the specified character(s).</returns>
        internal static string TrimmedOf(this string subject, string charsToTrimLeft, string charsToTrimRight)
        {
            string result = subject;
            int index = 0;
            while (index < subject.Length && charsToTrimLeft.Contains(subject[index].ToString()))
            {
                ++index;
            }
            int lastIndex = subject.Length - 1;
            while (lastIndex >= index && charsToTrimRight.Contains(subject[lastIndex].ToString()))
            {
                --lastIndex;
            }
            if (lastIndex < index)
            {
                result = string.Empty;
            }
            else
            {
                result = result.Substring(index, lastIndex - index + 1);
            }
            return result;
        }
    }
}