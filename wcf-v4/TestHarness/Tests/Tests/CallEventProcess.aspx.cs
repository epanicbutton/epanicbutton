﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Tests
{
    public partial class CallEventProcess : System.Web.UI.Page
    {
        private static string clientGuid = (ConfigurationManager.AppSettings["clientGuid"]);
        private static string userID = (ConfigurationManager.AppSettings["userID"]);
        private static string buttonID = (ConfigurationManager.AppSettings["buttonID"]);
        private static string mailgunKey = (ConfigurationManager.AppSettings["mailgun.key"]);
        private static string mailgunDomain = (ConfigurationManager.AppSettings["mailgun.domain"]);

        protected void Page_Load(object sender, EventArgs e)
        {
            ePanicProcessService.IepbwsClient client = new ePanicProcessService.IepbwsClient();
            Dictionary<string, string> eventPairs = new Dictionary<string, string>();

            eventPairs.Add("clientGuid", clientGuid);
            eventPairs.Add("userId", userID);
            eventPairs.Add("buttonId", buttonID);

            foreach (string key in eventPairs.Keys)
            {
                Console.WriteLine("{0}={1}", key, eventPairs[key]);
            }
            Console.WriteLine(Environment.NewLine);

            string keyvalues = eventPairs.ToCsv('|', '"');

            bool result = client.EventProcess(1, keyvalues);
        }    

    }
}