USE [ePanicButton]
GO
/****** Object:  StoredProcedure [dbo].[uspGetByGUID]    Script Date: 01/03/2014 19:42:16 ******/
SET NOCOUNT ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[uspSetByGUID]
@clientguid varchar(40),
@machineguid varchar(40),
@ip varchar(20),
@mac varchar(20), 
@firstname varchar(50), 
@lastname varchar(50), 
@location nvarchar(200), 
@department nvarchar(200), 
@phone nvarchar(50),
@email nvarchar(200),
@version varchar(20),
@machineName nvarchar(200)
/*

exec uspSetByGUID 
'EA5843C0-F8C2-4BE1-A8A5-2B805DD1B167',
'9EF64ECE-A373-4F47-9742-05ABA9D3C133',
'199.199.192.192',
'9C-B7-0D-A8-9C-CB',
'Andrew',
'Wing',
'Upstairs Office',
'IT Department',
'336-225-9999',
'andrew.wing@wingweb.com',
'1.1.2',
'DELL9020'

*/
AS

declare @clientid int
declare @nodeid int
declare @result varchar(15)
declare @thisguid varchar(40)
set @clientid = 0

select @clientid = clientid from Client where ClientGuid = @clientguid and IsClosed != 1

print @clientid
--client is not active
if (@clientid = 0)
BEGIN
	set @result = 'inactive'
	set @thisguid = 'invalid'
	SELECT @result as r, @thisguid as [guid]
	return 0	
END

select @nodeid = nodeid from Node where machineguid = @machineguid

print @nodeid

if (@nodeid is not null)
BEGIN
PRINT 'UPDATE AND RETURN GUID - FIRST PASS'
		
		update Node
		set ipaddress = @ip,
			macaddress = @mac,
			userfirstname = @firstname,
			UserLastName = @lastname,
			UserLocation = @location,
			UserDepartment = @department,
			UserTelephone = @phone,
			UserEmail = @email,
			ApplicationVersion = @version,
			NodeMachineName = @machineName
		where NodeID = @nodeid
				
		set @result = 'success (1)'		
		set @thisguid = @machineguid
SELECT @result as r, @thisguid as [guid]
END


if (@nodeid is null)
BEGIN		
		select @nodeid = nodeid, @thisguid = MachineGuid from Node n 
		where UserFirstName = @firstname 
		and UserLastName = @lastname
		and UserEmail = @email
		print @nodeid
		print @thisguid
		
		
		if (@nodeid is NOT null)
		BEGIN
		PRINT 'UPDATE AND RETURN GUID - SECOND PASS'
		
		update Node
		set ipaddress = @ip,
			macaddress = @mac,
			userfirstname = @firstname,
			UserLastName = @lastname,
			UserLocation = @location,
			UserDepartment = @department,
			UserTelephone = @phone,
			UserEmail = @email,
			ApplicationVersion = @version,
			NodeMachineName = @machineName
		where NodeID = @nodeid
				
		set @result = 'success (2)'	
		SELECT @result as r, @thisguid as [guid]	
		END	
		
		if (@nodeid is null)
		BEGIN
		PRINT 'INSERT AND RETURN GUID'
		
		insert into Node
		(ipaddress, macaddress, NodeName, userfirstname, UserLastName, UserLocation, UserDepartment, UserTelephone, UserEmail, ApplicationVersion, NodeMachineName, IsDefault, MachineGuid, IsPanicButtonInstalled, IsAlertReceiverInstalled, UserSelfTestInterval, UserEnableSelfTest, RemoteCommandsProcessingInterval,
		ClientProfileRefreshEnabled, ClientUnloadingEnabled,AllowsUserToChangeProfileEnabled, AllowMoveEnabled, IsApplicationDeactivated)
		values
		(@ip, @mac, @machineName, @firstname, @lastname, @location, @department, @phone, @email, @version, @machineName, 0, @machineguid, 1, 1, 60, 1, 60,
		1,1,1,1,0)
				
		set @result = 'insert (1)'		
		SELECT @result as r, @machineguid as [guid]
		END			
END






















